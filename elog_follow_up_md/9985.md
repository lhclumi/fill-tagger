# FILL 9985
**start**:		 2024-08-08 14:26:07.039363525+02:00 (CERN time)

**end**:		 2024-08-08 17:14:46.321738525+02:00 (CERN time)

**duration**:	 0 days 02:48:39.282375


***

### [2024-08-08 14:26:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122184)
>LHC RUN CTRL: New FILL NUMBER set to 9985

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:26:08)


***

### [2024-08-08 14:27:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122190)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:27:28)


***

### [2024-08-08 14:28:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122191)
>Quench heater analysis is OK for RQ5.R5

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:28:37)

[QHD MPE ANALYSIS VIEWER 24-08-08_14:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721907/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:28:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721907/content)


***

### [2024-08-08 14:29:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122192)
>PM data

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:29:21)

[Mozilla Firefox 24-08-08_14:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721909/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:29:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721909/content)


***

### [2024-08-08 14:29:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122193)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:29:33)


***

### [2024-08-08 14:29:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122195)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:29:47)


***

### [2024-08-08 14:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122196)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:32:18)


***

### [2024-08-08 14:32:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122197)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:32:18)


***

### [2024-08-08 14:36:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122200)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:36:15)


***

### [2024-08-08 14:36:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122202)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:36:22)


***

### [2024-08-08 14:38:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122205)
>RQ5.R5 is reseted but the heater are not charging.
I call teh QPS piquet who will contact the expert

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:39:47)

[5 - RQ5.R5 DQAMG N type RQA for circuit RQ5.R5    24-08-08_14:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721919/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 14:39:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721919/content)


***

### [2024-08-08 14:42:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122206)
>VITO VIZZIELLO(VIVIZZIE) assigned RBAC Role: QPS-Piquet and will expire on: 08-AUG-24 04.42.26.377000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/08 14:42:28)


***

### [2024-08-08 14:45:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122208)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:45:36)


***

### [2024-08-08 14:46:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122210)
>EDWARD PIOTR NOWAK(ENOWAK) assigned RBAC Role: QPS-Piquet and will expire on: 08-AUG-24 04.46.02.803000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/08 14:46:05)


***

### [2024-08-08 14:46:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122210)
>EDWARD PIOTR NOWAK(ENOWAK) assigned RBAC Role: QPS-Piquet and will expire on: 08-AUG-24 04.46.02.803000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/08 14:46:05)


***

### [2024-08-08 14:47:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122211)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:47:39)


***

### [2024-08-08 14:50:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122213)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:50:04)


***

### [2024-08-08 14:50:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122215)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:50:05)


***

### [2024-08-08 14:50:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122217)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:50:08)


***

### [2024-08-08 14:50:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122221)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:50:22)


***

### [2024-08-08 14:50:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122223)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:50:44)


***

### [2024-08-08 14:52:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122225)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:52:22)


***

### [2024-08-08 14:53:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122228)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:53:11)


***

### [2024-08-08 14:53:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122229)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:53:45)


***

### [2024-08-08 14:54:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122230)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 14:54:06)


***

### [2024-08-08 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122232)
>
```
**SHIFT SUMMARY:**  
  
I arrived during the access in PZ45 for a CPU replacement with the CEM best effort and the RF LL piquets.  
When the came out with the issue solved, I closed the machine and prepare for the fill.  
  
Injection was smooth, around 40% dump threshold losses at the start of the ramp.  
As it happens often since a few days, there was some beam in the abort gap that could not be cleaned.  
I did a trim in Qh of + 0.001 both beams after the first 2 steps of B* levelling to improve the lifetime a bit.  
  
After a few hours in stable beam, RQ5.R5 QPS system triggered and dumped the beams.  
After all the reset I still was unable to charge the heaters so called the piquet who is now still working on it.  
  
  
* Miscellaneous:

  
Olivier Barriere needs 1 hour without beam to restart an FGC gateway, Enrico is informed.  
  
Delphine  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 15:03:26)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721943/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 14:57:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721943/content)


***

### [2024-08-08 15:12:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122242)
>
```
Lost S34 and S45  
  
AUG pushed in SG4 building in point 4. Mains tripped with DISCHARGE REQUEST from PC (cooling water).  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 15:32:48)

[ CCM_1 LHCOP 24-08-08_15:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721966/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/08 15:12:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721966/content)

[History Buffer 24-08-08_15:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721968/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 15:17:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721968/content)

[History Buffer 24-08-08_15:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721970/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 15:18:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721970/content)

[Screenshot from 2024-08-08 15-35-13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721983/content)
creator:	 mihostet  @194.12.144.27 (2024/08/08 15:35:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3721983/content)


***

### [2024-08-08 15:13:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122243)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 15:13:20)


***

### [2024-08-08 15:14:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122244)
>UJ63 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 15:14:05)


***

### [2024-08-08 15:14:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122245)
>PM56 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 15:14:07)


***

### [2024-08-08 15:14:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122246)
>UP63 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 15:14:09)


***

### [2024-08-08 15:23:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122251)
>ALICE VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 15:23:27)


***

### [2024-08-08 16:04:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122263)
>PRZEMYSLAW TOMASZ PLUTECKI(PPLUTECK) assigned RBAC Role: PO-LHC-Piquet and will expire on: 08-AUG-24 06.04.08.461000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/08 16:04:09)


***

### [2024-08-08 16:15:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122268)
>QPS access in P5 finished

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 16:15:59)

[5 - RQ5.R5 DQAMG N type RQA for circuit RQ5.R5    24-08-08_16:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722014/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/08 16:15:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722014/content)


***

### [2024-08-08 16:20:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122274)
>
```
The AUG in P4 was triggered by a decabling campaign in the adjacent building; the cable has been cut. For the moment it is shunted.  
  
Attempting to reset the sectors.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 17:48:03)

[5 - CIP.UA47.AR4 Powering interlock controller for the long arc cryostat A45, even side     24-08-08_16:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722050/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 16:44:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722050/content)


***

### [2024-08-08 16:47:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122284)
>RQD.A81 tripped with VS\_RUN\_TO going to standby

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 16:48:04)

[1 - RPHE.UA87.RQD.A81 Power Converter for the Circuit RQD.A81    24-08-08_16:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722094/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/08 16:48:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722094/content)


***

### [2024-08-08 16:50:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122286)
>all circuits (except EIS) back

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/08 16:51:39)

[ CCM_1 LHCOP 24-08-08_16:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722100/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/08 16:51:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3722100/content)


***

### [2024-08-08 17:14:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122304)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 17:14:45)


***

### [2024-08-08 17:14:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4122305)
>LHC RUN CTRL: New FILL NUMBER set to 9986

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/08 17:14:47)


