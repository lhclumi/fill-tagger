# FILL 10352
**start**:		 2024-11-11 16:09:08.754238525+01:00 (CERN time)

**end**:		 2024-11-11 16:29:07.184738525+01:00 (CERN time)

**duration**:	 0 days 00:19:58.430500


***

### [2024-11-11 16:09:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182103)
>LHC RUN CTRL: New FILL NUMBER set to 10352

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:09:10)


***

### [2024-11-11 16:12:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182111)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:12:27)


***

### [2024-11-11 16:13:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182113)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:13:48)


***

### [2024-11-11 16:14:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182114)
>RF cooldown started - conditions expected in ~1h

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 16:14:27)


***

### [2024-11-11 16:15:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182115)
>
```
M Jaussi confirmed that the BQM-B1 board was replaced and tested.  
We're back to the old BQM and should be all fine.  
We'll have to call him only if anything unusual is observed during injection.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 16:17:55)


***

### [2024-11-11 16:15:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182116)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:15:11)


***

### [2024-11-11 16:16:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182118)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:16:11)


***

### [2024-11-11 16:17:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182120)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:17:30)


***

### [2024-11-11 16:17:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182127)
>Helga reported that a broken cable was identified that was causing the issue of coupler not moving.  
The cable and motor have been replaced.  
Tests were successful and RF team is leaving the machine.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 16:20:17)


***

### [2024-11-11 16:18:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182121)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:18:17)


***

### [2024-11-11 16:19:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182124)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:19:08)


***

### [2024-11-11 16:19:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182126)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:19:16)


***

### [2024-11-11 16:20:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182130)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:20:43)


***

### [2024-11-11 16:20:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182132)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:20:45)


***

### [2024-11-11 16:20:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182134)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:20:48)


***

### [2024-11-11 16:21:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182138)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:21:03)


***

### [2024-11-11 16:23:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182141)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:23:03)


***

### [2024-11-11 16:24:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182144)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:24:47)


***

### [2024-11-11 16:29:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182150)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:29:04)


***

### [2024-11-11 16:29:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182151)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:29:06)


***

### [2024-11-11 16:29:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182152)
>LHC RUN CTRL: New FILL NUMBER set to 10353

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 16:29:08)


