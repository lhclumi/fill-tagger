# FILL 9709
**start**:		 2024-06-05 10:48:54.105363525+02:00 (CERN time)

**end**:		 2024-06-05 11:25:01.951363525+02:00 (CERN time)

**duration**:	 0 days 00:36:07.846000


***

### [2024-06-05 10:48:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081570)
>LHC RUN CTRL: New FILL NUMBER set to 9709

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:48:54)


***

### [2024-06-05 10:49:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081571)
>LHC RUN CTRL: ACCELERATOR MODE changed to MACHINE DEVELOPMENT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:49:04)


***

### [2024-06-05 10:49:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081572)
>accelerator mode changed to MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:49:17)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-06-05_10:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625714/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:49:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625714/content)


***

### [2024-06-05 10:50:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081579)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:50:11)


***

### [2024-06-05 10:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081581)
>LHC RUN CTRL: PARTICLE TYPE for LHC BEAM1 changed to PROTON

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:50:38)


***

### [2024-06-05 10:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081580)
>LHC RUN CTRL: ACCELERATOR MODE changed to PROTON PHYSICS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:50:38)


***

### [2024-06-05 10:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081582)
>LHC RUN CTRL: PARTICLE TYPE for LHC BEAM2 changed to PROTON

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:50:38)


***

### [2024-06-05 10:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081582)
>LHC RUN CTRL: PARTICLE TYPE for LHC BEAM2 changed to PROTON

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:50:38)


***

### [2024-06-05 10:50:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081583)
>CMS asked to go back to proton physics for ~20min for a test. They'll call back once done and we'll move back to MD.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:51:18)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-06-05_10:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625724/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:51:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625724/content)


***

### [2024-06-05 10:52:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081584)
>Event created from ScreenShot Client.  
LHC Sequencer Execution GUI (PRO) : 12.32.5 24-06-05\_10:52.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:52:08)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-06-05_10:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625726/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 10:52:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625726/content)


***

### [2024-06-05 10:52:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081585)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:52:24)


***

### [2024-06-05 10:52:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081587)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:52:47)


***

### [2024-06-05 10:53:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081589)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:53:46)


***

### [2024-06-05 10:54:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081591)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:54:13)


***

### [2024-06-05 10:55:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081592)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:55:09)


***

### [2024-06-05 10:55:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081594)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:55:18)


***

### [2024-06-05 10:55:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081595)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:55:49)


***

### [2024-06-05 10:56:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081598)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:56:35)


***

### [2024-06-05 10:57:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081599)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:57:10)


***

### [2024-06-05 10:58:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081600)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:58:14)


***

### [2024-06-05 10:58:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081602)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:58:14)


***

### [2024-06-05 10:58:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081604)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:58:15)


***

### [2024-06-05 10:58:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081608)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:58:30)


***

### [2024-06-05 10:59:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081610)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:59:06)


***

### [2024-06-05 10:59:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081612)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 10:59:12)


***

### [2024-06-05 11:00:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081613)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 11:00:29)


***

### [2024-06-05 11:02:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081615)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 11:02:13)


***

### [2024-06-05 11:17:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081621)
>changed active filling scheme

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 11:17:25)

[ INJECTION SEQUENCER  v 5.1.6 24-06-05_11:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625759/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 11:17:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625759/content)


***

### [2024-06-05 11:17:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081622)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 11:17:37)


***

### [2024-06-05 11:21:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081627)
>QPS reset of RBs failed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 11:21:13)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-06-05_11:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625768/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 11:21:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625768/content)

[24-06-05_11:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625770/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 11:21:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3625770/content)


***

### [2024-06-05 11:24:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081629)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 11:24:32)


***

### [2024-06-05 11:24:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081630)
>LHC RUN CTRL: ACCELERATOR MODE changed to MACHINE DEVELOPMENT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 11:24:49)


