# FILL 10062
**start**:		 2024-08-27 03:38:25.104363525+02:00 (CERN time)

**end**:		 2024-08-28 16:58:14.646238525+02:00 (CERN time)

**duration**:	 1 days 13:19:49.541875


***

### [2024-08-27 03:38:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132338)
>LHC RUN CTRL: New FILL NUMBER set to 10062

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 03:38:26)


***

### [2024-08-27 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132353)
>
```
**SHIFT SUMMARY:**  
  
- quiet shift in SB until ~2:30  
- warned by cryo colleage of QURC about to be lost in point 8 and prepared to dump  
- long recovery expected: swithced off all PCs, RF, LHCb dipole, while ALICE is still on for cosmic run  
  

```
  
Daniele  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 06:56:58)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743319/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 06:56:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743319/content)


***

### [2024-08-27 07:58:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132363)
>ALICE VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 07:58:44)


***

### [2024-08-27 09:10:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132388)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * PM15-US15 |
| Peoples : | * Matteo Solfaroli Camillocci (BE/OP/LHC)160611 / 79135 - <matteo.solfaroli@cern.ch> |
| Duration | 1h - **Access Needed** - |
| kk | |



creator:	 lhcop  @paas-standard-avz-a-smrst.cern.ch (2024/08/27 09:10:47)


***

### [2024-08-27 09:11:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132389)
>ATLAS VACUUM VALVES CLOSED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 09:11:35)


***

### [2024-08-27 09:31:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132402)
>LHC SEQ: ramping down ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 09:31:50)


***

### [2024-08-27 09:46:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132413)
>CMS VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 09:46:52)


***

### [2024-08-27 10:34:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132443)
>HELGA TIMKO(HTIMKO) assigned RBAC Role: RF-LHC-Piquet and will expire on: 27-AUG-24 12.34.48.440000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 10:34:54)


***

### [2024-08-27 10:52:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132452)
>OLIVIER EMMANUEL FOURNIER(OFOURNIE) assigned RBAC Role: PO-LHC-Piquet and will expire on: 27-AUG-24 12.52.26.374000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 10:52:28)


***

### [2024-08-27 11:03:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132454)
>UL16 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 11:03:04)


***

### [2024-08-27 11:38:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132474)
>UL14 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 11:38:38)


***

### [2024-08-27 12:02:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132491)
>LHC SEQ: ramp down ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 12:02:24)


***

### [2024-08-27 13:08:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132529)
>Helga has fixed the Line3B2 and trimmed back the voltage partition to the usual values.  
I also remove from the sequence the task to switch OFF the one turn feedback for this line.  


creator:	 delph  @cwe-513-vml005.cern.ch (2024/08/27 13:09:27)


***

### [2024-08-27 14:10:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132561)
>MIGUEL CERQUEIRA BASTOS(MCB) assigned RBAC Role: PO-LHC-Piquet and will expire on: 27-AUG-24 04.10.21.730000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 14:10:23)


***

### [2024-08-27 14:19:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132567)
>UJ67 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 14:19:14)


***

### [2024-08-27 14:19:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132567)
>UJ67 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 14:19:14)


***

### [2024-08-27 14:19:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132568)
>UL55 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 14:19:16)


***

### [2024-08-27 14:36:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132581)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 27-AUG-24 04.36.23.914000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 14:36:25)


***

### [2024-08-27 14:44:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132585)
>
```
After the intervention to replace the WorldFIP repeater of the network RR1E installed in US15, I see an communication issue with one of the power converter.
Asking Benjamin Ninet if this is linked to his intervention, he checked and it doesn't belong to the same network. I call the piquet EPC.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 14:50:34)

[ Equip State 24-08-27_14:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744013/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 14:46:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744013/content)


***

### [2024-08-27 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132588)
>
```
**SHIFT SUMMARY:**  
  
Access all morning all around the ring and in the experiments.   
  
Helga fixed the line 3B2 and trimmed back teh voltage partition. I also removed the switch OFF of the one turn feedback from the sequence.  
  
The cryo intervention on the pressure gauge is over, they will reconnect. Beam expected tomorrow  
  
Delphine  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 14:49:56)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744019/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 14:49:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744019/content)


***

### [2024-08-27 15:20:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132604)
>KEVIN TIMO KESSLER(KEKESSLE) assigned RBAC Role: PO-LHC-Piquet and will expire on: 27-AUG-24 09.20.26.281000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 15:20:27)


***

### [2024-08-27 18:24:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132663)
>
```
RCBV22.R8B2 and RCBH31.R8B2 back in operation:  
- cycle to +- IPNO  
- removed from SIS injection mask  
- removed from OFB masks  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/27 18:44:08)

[ LHC SIS GUI 24-08-27_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744162/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/27 18:25:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744162/content)

[Monitoring application. Currently monitoring : LHC - [2 subscriptions ] 24-08-27_18:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744172/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/27 18:29:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744172/content)

[Beam Feedbacks - Mission Control 24-08-27_18:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744182/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/27 18:38:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744182/content)

[Beam Feedbacks - Mission Control 24-08-27_18:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744184/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/27 18:39:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744184/content)


***

### [2024-08-27 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132693)
>Accesses in the shadow of the cryo recovery.  
Latest news from cryo is no beam before tomorrow afternoon.  
  
Leaving the machine in access mode (all opened points during the day in "Closed") and TI informed.  
  
AC  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/27 23:07:08)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744222/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/27 23:07:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744222/content)


***

### [2024-08-28 08:17:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132723)
>Patrol lost in UPR13, no access ongoing

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/08/28 08:17:36)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744240/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/08/28 08:17:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744240/content)


***

### [2024-08-28 08:44:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132737)
>Vitor contacted for the lost of patrol. Once this is understood I'll organize with the gerant de site the patrol...  


creator:	 delph  @cwe-513-vml005.cern.ch (2024/08/28 08:45:17)


***

### [2024-08-28 09:09:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132763)
>Access expert confirmed that the lost of patrol is due to a glitch on the handle of the end of zone door...   
He doesn't have any intervention to do about it, we can lauch the patrol.  
I call the gerant de site.  
  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 09:12:09)


***

### [2024-08-28 09:16:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132769)
>The gerant de site will organize the patrol in UPR13 with the fire brigade.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 09:16:46)


***

### [2024-08-28 10:15:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132801)
>Julien Brina and the fire brigade are in PM15 for the patrol.  


creator:	 delph  @cwe-513-vml005.cern.ch (2024/08/28 10:16:13)


***

### [2024-08-28 10:32:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132814)
>Patrol OK mow in UPR13.  


creator:	 delph  @cwe-513-vml005.cern.ch (2024/08/28 10:32:18)


***

### [2024-08-28 14:22:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132937)
>
```
Problem with an electrical transformer for one EN-EL cell of point 8 (ozone leak, ozone building up due an isolation defect). TI and EN-EL need to turn off the cells one by one to check which one is affected. They asked us and SPS to turn off all converters in point 8.  
  
First, they will turn cell EMD403 off and ground it.  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 15:23:44)

[TIP 24-08-28_14:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744678/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 14:25:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744678/content)

[TIP 24-08-28_14:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744690/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 14:32:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744690/content)

[EMD403_slash_8E ALIM. EMT403.8R ERD1.8R REDR. AUX. SR8   : Faceplate 24-08-28_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744696/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 14:36:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744696/content)


***

### [2024-08-28 14:39:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132947)
>
```
From the investigation it appears that BOTH EMD403 (off) and EMD402 (still on) have isolation issue. When EN-EL turned EMD403 off, the ozone in the air dopped by ~50%, indicating another leak of a similar size from a nearby transformer. However, EMD402 is supplying EN-CV controls racks in US85/UW85 which would cut all cooling to S78/S81/LHCb and cryo P8.
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 15:24:02)

[TIP 24-08-28_14:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744702/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/28 14:40:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744702/content)


***

### [2024-08-28 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132961)
>
```
**SHIFT SUMMARY:**  
  
Only one access this morning in point 4 for check on the wire scanner.  
Patrol lost in UPR13 around 8:15am, glitch on the door handler, gerant de site and fire brigade went for the patrol.  
  
Recovery estimate for cryo is now midnight.  
  
Issue with an electrical transformer in point 8, not clear yet what will be the intervention needed and the consequences.  
  
Delphine  
  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/28 15:13:54)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744748/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/28 15:13:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744748/content)


***

### [2024-08-28 15:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132983)
>CRYO START forced to reset all circuits

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 15:53:52)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-08-28_15:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744805/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 15:53:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744805/content)


***

### [2024-08-28 16:03:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132985)
>
```
mains in S12 did not reset - quench loop open, ST_CUR_SRC not OK.  
  
Reset by sending several OFFs to the PC.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 16:08:53)

[Set SECTOR12 24-08-28_16:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744811/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/28 16:03:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744811/content)

[Circuit_RB_A12:   24-08-28_16:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744813/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 16:05:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744813/content)

[2 - RB.A12.EVEN DQQLC N type EVEN side RB, RQF, RQD for circuit RB.A12.EVEN    24-08-28_16:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744815/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 16:05:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744815/content)

[2 - RB.A12.ODD DQQLC N type ODD side RB for circuit RB.A12.ODD    24-08-28_16:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744817/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/28 16:05:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744817/content)


***

### [2024-08-28 16:15:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132987)
>RB/RQ.A12 recovered

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/28 16:15:23)

[Set SECTOR12 24-08-28_16:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744819/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/28 16:15:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744819/content)


***

### [2024-08-28 16:17:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132989)
>RF ON

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/28 16:17:53)

[ LHC RF CONTROL 24-08-28_16:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744821/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/28 16:17:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744821/content)


***

### [2024-08-28 16:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132992)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/28 16:32:18)


***

### [2024-08-28 16:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132997)
>All RF OK!

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/28 16:42:18)

[ LHC RF CONTROL 24-08-28_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744837/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/28 16:42:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3744837/content)


***

### [2024-08-28 16:47:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4133000)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/28 16:47:22)


***

### [2024-08-28 16:58:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4133007)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/28 16:58:14)


