# FILL 10382
**start**:		 2024-11-17 17:16:31.852738525+01:00 (CERN time)

**end**:		 2024-11-18 03:10:10.241863525+01:00 (CERN time)

**duration**:	 0 days 09:53:38.389125


***

### [2024-11-17 17:16:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185671)
>LHC RUN CTRL: New FILL NUMBER set to 10382

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:16:32)


***

### [2024-11-17 17:17:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185677)
>View of last fill

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 17:17:35)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-17_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867533/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 17:17:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867533/content)

[ LHC Fast BCT v1.3.2 24-11-17_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867535/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 17:17:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867535/content)

[ LHC Beam Losses Lifetime Display 24-11-17_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867537/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/17 17:17:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867537/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-17_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867539/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/17 17:17:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867539/content)

[Monitor Real Time Trim Goniometers 24-11-17_17:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867541/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/17 17:18:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867541/content)

[Abort gap cleaning control v2.1.1 24-11-17_17:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867543/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 17:18:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867543/content)

[ CCM_2 LHCOP 24-11-17_17:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867547/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/17 17:19:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867547/content)


***

### [2024-11-17 17:17:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185680)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:17:41)


***

### [2024-11-17 17:19:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185681)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:19:42)


***

### [2024-11-17 17:20:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185685)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:20:59)


***

### [2024-11-17 17:21:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185686)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:21:03)


***

### [2024-11-17 17:22:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185688)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:22:32)


***

### [2024-11-17 17:23:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185690)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:23:10)


***

### [2024-11-17 17:25:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185692)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:25:16)


***

### [2024-11-17 17:25:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185693)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:25:35)


***

### [2024-11-17 17:25:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185695)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:25:36)


***

### [2024-11-17 17:25:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185697)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:25:38)


***

### [2024-11-17 17:25:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185701)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:25:53)


***

### [2024-11-17 17:26:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185703)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:26:28)


***

### [2024-11-17 17:26:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185705)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:26:35)


***

### [2024-11-17 17:27:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185706)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:27:51)


***

### [2024-11-17 17:29:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185710)
>Starting the RF preparation as all is back to normal

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/17 17:29:44)

[Mozilla Firefox 24-11-17_17:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867549/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/17 17:29:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867549/content)


***

### [2024-11-17 17:29:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185708)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:29:35)


***

### [2024-11-17 17:36:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185712)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:36:05)


***

### [2024-11-17 17:37:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185714)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:37:48)


***

### [2024-11-17 17:42:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185717)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:42:36)


***

### [2024-11-17 17:43:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185718)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:43:13)


***

### [2024-11-17 17:50:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185719)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:50:59)


***

### [2024-11-17 17:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185721)
>Forced Board A

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 17:52:56)

[K - MB.B8L1 DQAMC N type MB for dipole MB.B8L1    24-11-17_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867551/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 17:52:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867551/content)


***

### [2024-11-17 17:53:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185722)
>Corrrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 17:53:43)

[ Accelerator Cockpit v0.0.42 24-11-17_17:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867553/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 17:53:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867553/content)

[ Accelerator Cockpit v0.0.42 24-11-17_17:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867555/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 17:54:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867555/content)

[ Accelerator Cockpit v0.0.42 24-11-17_17:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867557/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 17:55:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867557/content)


***

### [2024-11-17 17:55:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185723)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 17:55:25)


***

### [2024-11-17 18:00:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185725)
>Starting channeling optimization

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:06)

[LHC Crystals Cockpit 24-11-17_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867561/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867561/content)


***

### [2024-11-17 18:01:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185726)
>WS

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:16)

[ LHC WIRESCANNER APP 24-11-17_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867563/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867563/content)

[ LHC WIRESCANNER APP 24-11-17_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867565/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867565/content)

[ LHC WIRESCANNER APP 24-11-17_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867567/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867567/content)

[ LHC WIRESCANNER APP 24-11-17_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867569/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 18:01:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867569/content)


***

### [2024-11-17 18:17:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185731)
>The "BQM attenuator storm" has passed -> 7SC lost

Average bunch intensity went from 2e10 to 1.93 e10 charges (- 3.5%)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:19:10)

[SPS Beam Quality Monitor - SPS.USER.LHCION1 - SPS PRO INCA server - PRO CCDA 24-11-17_18:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867577/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/17 18:18:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867577/content)

[ LHC Fast BCT v1.3.2 24-11-17_18:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867579/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:18:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867579/content)

[udp:..multicast-bevlhc1:1234 - VLC media player 24-11-17_18:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867581/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:19:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867581/content)


***

### [2024-11-17 18:47:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185732)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-11-17\_18:47.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:47:13)

[ LHC Fast BCT v1.3.2 24-11-17_18:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867587/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:47:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867587/content)


***

### [2024-11-17 18:47:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185733)
>Start of SPS BQM storm #2

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:47:28)

[ LHC Fast BCT v1.3.2 24-11-17_18:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867589/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 18:47:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867589/content)


***

### [2024-11-17 19:10:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185735)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 
 24-11-17\_19:10.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:10:42)

[ LHC Fast BCT v1.3.2 24-11-17_19:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867593/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:10:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867593/content)

[ LHC Fast BCT v1.3.2 24-11-17_19:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867595/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:10:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867595/content)

[SPS Beam Quality Monitor - SPS.USER.LHCION1 - SPS PRO INCA server - PRO CCDA 24-11-17_19:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867597/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/17 19:10:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867597/content)

[ INJECTION SEQUENCER  v 5.1.12 24-11-17_19:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867599/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:10:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867599/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-17_19:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867601/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/17 19:11:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867601/content)

[Monitor Real Time Trim Goniometers 24-11-17_19:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867603/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/17 19:11:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867603/content)

[Beam Gas Curtain 24-11-17_19:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867605/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/17 19:11:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867605/content)


***

### [2024-11-17 19:11:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185736)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:11:12)


***

### [2024-11-17 19:12:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185737)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:12:08)


***

### [2024-11-17 19:12:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185738)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:12:14)


***

### [2024-11-17 19:12:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185739)
>LHC Injection Complete

Number of injections actual / planned: 62 / 46
SPS SuperCycle length: 70.8 [s]
Actual / minimum time: 1:16:50 / 0:59:16 (129.6 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/17 19:12:15)


***

### [2024-11-17 19:14:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185741)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:14:46)


***

### [2024-11-17 19:14:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185743)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:14:48)


***

### [2024-11-17 19:36:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185747)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:36:05)


***

### [2024-11-17 19:36:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185749)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-11-17\_19:36.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:36:16)

[ LHC Fast BCT v1.3.2 24-11-17_19:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867607/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:36:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867607/content)


***

### [2024-11-17 19:36:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185750)
>RAMP

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:36:22)

[ LHC Fast BCT v1.3.2 24-11-17_19:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867609/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:36:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867609/content)

[ LHC Beam Losses Lifetime Display 24-11-17_19:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867611/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/17 19:36:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867611/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-17_19:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867613/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/17 19:36:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867613/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-17_19:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867615/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 19:36:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867615/content)

[Abort gap cleaning control v2.1.1 24-11-17_19:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867617/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 19:37:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867617/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-11-17_19:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867619/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 19:37:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867619/content)

[Monitor Real Time Trim Goniometers 24-11-17_19:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867621/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/17 19:37:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867621/content)


***

### [2024-11-17 19:36:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185751)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:36:52)


***

### [2024-11-17 19:43:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185753)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:43:07)


***

### [2024-11-17 19:43:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185754)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:43:44)


***

### [2024-11-17 19:43:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185754)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:43:44)


***

### [2024-11-17 19:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185756)
>Started Agap cleaning in parallel to adjust

This caused the crystal optimization to go bananas

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 19:50:01)

[Abort gap cleaning control v2.1.1 24-11-17_19:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867623/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 19:44:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867623/content)

[LHC Crystals Cockpit 24-11-17_19:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867627/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 19:49:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867627/content)


***

### [2024-11-17 19:46:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185757)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:46:13)


***

### [2024-11-17 19:48:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185758)
>Event created from ScreenShot Client.  
Abort gap cleaning control v2.1.1 
 24-11-17\_19:48.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 19:48:59)

[Abort gap cleaning control v2.1.1 24-11-17_19:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867625/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/17 19:48:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867625/content)


***

### [2024-11-17 19:51:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185759)
>Losses in collisions

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 19:51:28)

[ LHC BLM Fixed Display 24-11-17_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867629/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 19:51:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867629/content)

[ LHC Beam Losses Lifetime Display 24-11-17_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867631/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/17 19:51:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867631/content)


***

### [2024-11-17 19:51:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185760)
>Optimized all iPS

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:51:54)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-17_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867633/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 19:51:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867633/content)


***

### [2024-11-17 19:52:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185761)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/17 19:52:33)


***

### [2024-11-17 19:53:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185762)
>Losses IP7 in collisions

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 19:53:10)

[ LHC BLM Fixed Display 24-11-17_19:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867635/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 19:53:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867635/content)


***

### [2024-11-17 19:58:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185763)
>Event created from ScreenShot Client.  
LHC Crystals Cockpit 
 24-11-17\_19:58.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 19:58:36)

[LHC Crystals Cockpit 24-11-17_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867637/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 19:58:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867637/content)


***

### [2024-11-17 20:06:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185765)
>Reference for B1H scan

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 20:06:30)

[LHC Crystals Cockpit 24-11-17_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867639/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 20:06:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867639/content)


***

### [2024-11-17 20:29:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185767)
>Mostl likely LHCb started gas injection

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 20:29:18)

[ LHC BLM Fixed Display 24-11-17_20:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867641/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/17 20:29:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867641/content)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-17_20:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867643/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 20:29:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867643/content)


***

### [2024-11-17 20:56:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185771)
>Levelling out of steam in IP1/5/8 1h of SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 20:57:21)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-17_20:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867647/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 20:57:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867647/content)


***

### [2024-11-17 21:10:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185772)
>Separation levelling in IP2 out of steam 1h 20 min

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 21:11:17)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-17_21:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867649/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/17 21:11:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867649/content)


***

### [2024-11-17 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185804)
>
```
**SHIFT SUMMARY:**  
  
  
Got the machine in SB  
After ~3h of collisions, got dumped by a trip of RF line 7B2 (HOM coupler temperature fault)  
Waited for RF to recover and went for another fill  
  
Smooth cycle:  
Losses at injection <60%  
Long injection hindered by twice the issue with the SPS BQM  (lost over 10 SC)  
Few bunches got unstable in the Ramp as usual  
Crystal channeling overshot a bit for B1 during teh Ramp (tricked by losses?)  
IP2 levelling lasted 1h 20 min  
  
Leaving the machine in SB  
  
~~ GEorge~~  
  
  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 23:44:41)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867693/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/17 23:44:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867693/content)


***

### [2024-11-18 02:59:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185821)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 02:59:29)


***

### [2024-11-18 02:59:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185822)
>Checks completed in injectors. Dumping the beam

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 02:59:47)


***

### [2024-11-18 03:04:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185823)
>Event created from ScreenShot Client.  
Monitor Real Time Trim Goniometers 24-11-18\_03:04.png

creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/18 03:04:04)

[Monitor Real Time Trim Goniometers 24-11-18_03:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867712/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/18 03:04:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867712/content)


***

### [2024-11-18 03:04:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185824)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:04:30)


***

### [2024-11-18 03:04:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185829)
>Global Post Mortem Event

Event Timestamp: 18/11/24 03:04:40.747
Fill Number: 10382
Accelerator / beam mode: ION PHYSICS / STABLE BEAMS
Energy: 6799320 [MeV]
Intensity B1/B2: 927 / 969 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/18 03:07:31)


***

### [2024-11-18 03:04:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185868)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: acalia / Programmed dump after physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/18 03:22:34)


***

### [2024-11-18 03:05:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185825)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:05:24)


***

### [2024-11-18 03:05:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185826)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:05:31)


***

### [2024-11-18 03:05:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185827)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:05:33)


***

### [2024-11-18 03:05:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185828)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:05:35)


***

### [2024-11-18 03:10:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185830)
>LHC RUN CTRL: New FILL NUMBER set to 10383

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 03:10:11)


