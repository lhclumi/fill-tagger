# FILL 10227
**start**:		 2024-10-15 18:51:04.979738525+02:00 (CERN time)

**end**:		 2024-10-15 20:43:54.905488525+02:00 (CERN time)

**duration**:	 0 days 01:52:49.925750


***

### [2024-10-15 18:51:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163810)
>LHC RUN CTRL: New FILL NUMBER set to 10227

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:51:05)


***

### [2024-10-15 18:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163816)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:51:52)


***

### [2024-10-15 18:53:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163817)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:53:54)


***

### [2024-10-15 18:53:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163819)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:53:59)


***

### [2024-10-15 18:55:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163822)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:55:27)


***

### [2024-10-15 18:55:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163822)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:55:27)


***

### [2024-10-15 18:56:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163823)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:56:28)


***

### [2024-10-15 18:56:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163824)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:56:43)


***

### [2024-10-15 18:57:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163827)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:57:53)


***

### [2024-10-15 18:58:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163828)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:58:28)


***

### [2024-10-15 18:58:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163829)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 18:58:42)


***

### [2024-10-15 19:00:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163831)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:00:39)


***

### [2024-10-15 19:00:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163833)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:00:46)


***

### [2024-10-15 19:00:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163834)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:00:48)


***

### [2024-10-15 19:03:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163837)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:03:13)


***

### [2024-10-15 19:03:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163839)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:03:15)


***

### [2024-10-15 19:03:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163841)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:03:17)


***

### [2024-10-15 19:03:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163845)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:03:33)


***

### [2024-10-15 19:05:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163849)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:05:34)


***

### [2024-10-15 19:07:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163853)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:07:20)


***

### [2024-10-15 19:09:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163856)
>
```
Test of the AcDipoles for all both beams both planes. 
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:11:25)

[ IPOC Explorer 24-10-15_19:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824506/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/15 19:09:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824506/content)

[ IPOC Explorer 24-10-15_19:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824508/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/15 19:09:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824508/content)

[ IPOC Explorer 24-10-15_19:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824510/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/15 19:09:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824510/content)

[ IPOC Explorer 24-10-15_19:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824512/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/15 19:09:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824512/content)


***

### [2024-10-15 19:16:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163858)
>
```
I call the QPS piquet concerning the nqps issue on RB.A81.   
I do a reset on the 3 boards, he's having a look on his side.  
After the reset it still looks like a Christmas tree.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:17:57)


***

### [2024-10-15 19:21:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163859)
>
```
I also have an issue with a crate of RB.A56. I reset and change board, this solves the issue.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:25:30)

[Module QPS_56:A56.RB.A56.DR5I: (NoName) 24-10-15_19:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824522/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:22:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824522/content)


***

### [2024-10-15 19:27:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163861)
>SAMER YAMMINE(SYAMMINE) assigned RBAC Role: QPS-Piquet and will expire on: 15-OCT-24 09.27.41.076000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/15 19:27:43)


***

### [2024-10-15 19:47:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163865)
>QPS piquet ask to rebootcfc-sr1-dl1jk

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:48:33)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-10-15_19:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824535/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 19:48:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3824535/content)


***

### [2024-10-15 19:56:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163867)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 19:56:27)


***

### [2024-10-15 20:06:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163870)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 15-OCT-24 10.06.36.240000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/15 20:06:39)


***

### [2024-10-15 20:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163871)
>
```
On Tomasz request I switch OFF RQD.A81/RQF.A81/RB.A81, he will power cycle the crates and this will trip the circuits  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 20:42:20)


***

### [2024-10-15 20:42:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163891)
>RB.A81 and RQD/F.A81 tripped and open the switch during the QPS expert intervention as foreseen.  
I reset the circuits and prepare for precycle  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/15 20:43:15)


***

### [2024-10-15 20:43:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4163892)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/15 20:43:53)


