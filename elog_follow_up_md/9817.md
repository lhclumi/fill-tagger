# FILL 9817
**start**:		 2024-06-23 02:27:31.520613525+02:00 (CERN time)

**end**:		 2024-06-23 11:26:00.566863525+02:00 (CERN time)

**duration**:	 0 days 08:58:29.046250


***

### [2024-06-23 02:27:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093310)
>LHC RUN CTRL: New FILL NUMBER set to 9817

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:27:31)


***

### [2024-06-23 02:28:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093317)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:28:21)


***

### [2024-06-23 02:28:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093319)
>ELOGBOOK: STARTING B2 MKISS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:28:42)


***

### [2024-06-23 02:28:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093320)
>ELOGBOOK: STARTING B1 MKISS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:28:54)


***

### [2024-06-23 02:29:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093323)
>Launched extended softstarts as requested by MKI expert

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/23 02:29:54)

[MKI Remaining Duration - 2.0.4 24-06-23_02:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647636/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/23 02:29:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647636/content)


***

### [2024-06-23 02:30:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093324)
>Will have to wait ~ 1h30min before repowering RB78 and doing Precycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 02:30:23)

[8 - UA83.RB.A78 DQAMS N type RB 13KA for circuit UA83.RB.A78    24-06-23_02:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647638/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 02:30:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647638/content)


***

### [2024-06-23 02:30:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093325)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:30:32)


***

### [2024-06-23 02:30:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093327)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:30:54)


***

### [2024-06-23 02:31:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093329)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:31:59)


***

### [2024-06-23 02:33:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093331)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:33:15)


***

### [2024-06-23 02:33:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093333)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:33:24)


***

### [2024-06-23 02:34:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093334)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:34:01)


***

### [2024-06-23 02:34:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093336)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:34:18)


***

### [2024-06-23 02:36:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093337)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:36:26)


***

### [2024-06-23 02:36:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093339)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:36:27)


***

### [2024-06-23 02:36:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093341)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:36:28)


***

### [2024-06-23 02:36:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093343)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:36:38)


***

### [2024-06-23 02:36:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093347)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:36:43)


***

### [2024-06-23 02:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093349)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:37:11)


***

### [2024-06-23 02:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093351)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:37:12)


***

### [2024-06-23 02:37:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093352)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:37:17)


***

### [2024-06-23 02:38:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093353)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:38:39)


***

### [2024-06-23 02:40:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093357)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 02:40:23)


***

### [2024-06-23 02:41:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093359)
>Event created from ScreenShot Client.  
QHD MPE ANALYSIS VIEWER 24-06-23\_02:41.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/23 02:41:16)

[QHD MPE ANALYSIS VIEWER 24-06-23_02:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647646/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/23 02:41:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647646/content)


***

### [2024-06-23 03:20:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093363)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 03:20:04)


***

### [2024-06-23 03:20:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093364)
>CRYO conditions back already.Waiting for the mains switches to cooldown

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:21:07)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-06-23_03:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647658/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:21:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647658/content)

[8 - RR77.RB.A78 DQAMS N type RB 13KA for circuit RR77.RB.A78    24-06-23_03:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647660/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:21:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647660/content)


***

### [2024-06-23 03:22:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093365)
>Tripped during RampDown

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 03:22:17)

[Set SECTOR67 24-06-23_03:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647662/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 03:22:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647662/content)

[ Equip State 24-06-23_03:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647664/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 03:24:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647664/content)

[ Equip State 24-06-23_03:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647666/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 03:24:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647666/content)


***

### [2024-06-23 03:29:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093366)
>Resetting to Recharge the heaters on RQ9.L8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:29:29)

[8 - RQ9.L8 DQAMG N type RQB for circuit RQ9.L8    24-06-23_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647668/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:29:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647668/content)


***

### [2024-06-23 03:32:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093367)
>nQPS reset on S78

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:32:20)

[qps-lhc-swisstool 24-06-23_03:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647670/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:32:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647670/content)


***

### [2024-06-23 03:57:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093370)
>One of the two dump resistors is fine, waiting for the next

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:57:21)

[Desktop 24-06-23_03:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647674/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 03:57:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647674/content)


***

### [2024-06-23 04:25:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093373)
>Dump resistors all OK on RB 78 and RQD/F 78  
  
However, trying to power back the mains:  

```
  
Tried all possible combinations of resets, reset hw, reset energy     extraction, current lead, busbars, 3 nQPS resets still not able to recover     the main circuits of S78.  
  
Contacting QPS piquet
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 04:46:00)

[Circuit_RB_A78:   24-06-23_04:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647678/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 04:27:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647678/content)

[Module QPS_78:A78.RQD.A78.DR7C: (NoName) 24-06-23_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647680/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 04:27:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647680/content)

[Module QPS_78:A78.RQD.A78.DR7I: (NoName) 24-06-23_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647682/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 04:27:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647682/content)


***

### [2024-06-23 04:30:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093374)
>JENS STECKERT(JSTECKER) assigned RBAC Role: QPS-Piquet and will expire on: 23-JUN-24 06.30.10.832000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/23 04:30:12)


***

### [2024-06-23 05:08:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093381)
>
```
QPS loop is now closed  
  
Jens had to power cycle nQPS crate as it lost internal communication
```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:51:18)

[Module QPS_78:A78.RQD.A78.DR7I: (NoName) 24-06-23_05:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647702/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:09:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647702/content)

[Circuit_RB_A78:   24-06-23_05:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647704/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:09:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647704/content)


***

### [2024-06-23 05:08:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093381)
>
```
QPS loop is now closed  
  
Jens had to power cycle nQPS crate as it lost internal communication
```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:51:18)

[Module QPS_78:A78.RQD.A78.DR7I: (NoName) 24-06-23_05:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647702/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:09:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647702/content)

[Circuit_RB_A78:   24-06-23_05:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647704/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:09:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647704/content)


***

### [2024-06-23 05:10:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093382)
>
```
Waiting for PM completion on the fired heaters to power the circuits upon Jens request
```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:51:38)


***

### [2024-06-23 05:14:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093384)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 05:14:45)


***

### [2024-06-23 05:21:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093390)
>Waiting for timeout on the Quad switches (Dump resistor Temp)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:22:21)

[Desktop 24-06-23_05:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647708/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:22:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647708/content)


***

### [2024-06-23 05:29:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093391)
>Good to go

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:29:15)

[8 - UA83.RQF.A78 DQAMS N type RQ 13KA for circuit UA83.RQF.A78    24-06-23_05:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647710/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:29:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647710/content)


***

### [2024-06-23 05:32:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093392)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 05:32:56)


***

### [2024-06-23 05:33:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093393)
>ELOGBOOK: STARTING B1 MKISS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 05:33:28)


***

### [2024-06-23 05:33:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093394)
>ELOGBOOK: STARTING B2 MKISS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 05:33:38)


***

### [2024-06-23 05:34:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093395)
>
```
Energy tracking for BETS LBDS is OK now that S78 is back.  
  
I had reach out to the piquet to acknowledge the BETS interlock  
  
He has no rights to reset it, will try again to contact N. Magnin or N. Voumard to seek their help   

```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:33:58)

[ WinCC - Operation 24-06-23_05:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647712/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 05:35:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647712/content)


***

### [2024-06-23 05:35:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093396)
>Tripped during PRECYCLE

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 05:36:03)

[Set SECTOR67 24-06-23_05:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647716/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 05:36:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647716/content)


***

### [2024-06-23 05:54:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093401)
>
```
After recovering the 4 circuits (same QPS controller of RSS.A67B1) i send them back the Precycle function, RSS A67B1 trips again.  
  
Contacting the poor Jens again..  
  
3rd trip of the day
```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:30:37)

[Set SECTOR67 24-06-23_05:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647718/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 05:55:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647718/content)

[Mozilla Firefox 24-06-23_06:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647723/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:03:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647723/content)


***

### [2024-06-23 06:25:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093426)
>IVAN BLAZEVIC(BLAZEVIC) assigned RBAC Role: PO-LHC-Piquet and will expire on: 23-JUN-24 08.25.04.121000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/23 06:25:05)


***

### [2024-06-23 06:25:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093429)
>
```
After more analysis we find out that most probably it is the PC to blame in these events as 6ms before the change in Ures signals of QPS, two spikes of ~20mA can be observed on IA from the power converter.  
  
EPC piquet is investigating  

```


creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:32:27)

[Set SECTOR67 24-06-23_06:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647833/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/23 06:25:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647833/content)

[Multiple events 24-06-23_06:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647867/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:29:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647867/content)

[Multiple events 24-06-23_06:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647869/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:29:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647869/content)

[Multiple events 24-06-23_06:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647871/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 06:30:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647871/content)


***

### [2024-06-23 06:26:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093433)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 06:26:02)


***

### [2024-06-23 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093435)
>
```

```
**SHIFT SUMMARY:**  
  
Started the shift after the recovery from the QPS access while injecting the first beams.  
  
Again at machine half filled, a bad B2 injection (MKI8 magnet D flashover) led to losses dumping the beams.  
  
I restart and managed to bring the beams to Stable Beams.  
-Losses of 44% a start Ramp  
-Losses at 33% going in collisions  
-Losses in 3R1 popping up at the edge of 30% in several moment throughout beta* levelling (as it is always at the edge of warning).  
  
Got dumped by a QPS trip in S78 that led to a short loss of CRYO conditions. As a consequence with RB78 lost, the BETS interlock triggered in LBDS-B2, still needs to be acknowledged by the experts (piquet and EIC had no rights).  
  
  
Once recovered Cryo and the dump resistors cooled down allowing re-powering, I got stuck by another QPS issue: nQPS crate lost internal communication and needed power cycle.  
  
Finally Precycled the machine but during Precycle, got two trips of 600A in S67 (RSS.A67.B1) being investigated by EPC (after QPS piquet checked and pointed that the QPS is correctly reacting to a bad behaviour of the FGC).  
  
  
  
* To be noted:

  
I performed two Extended softstarts on MKI (both B1 and B2)
```
  
~George~  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 07:25:59)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647873/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/23 07:05:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647873/content)


***

### [2024-06-23 07:22:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093438)
>Access needed to exchange a power module in sector 67.  
RP piquet contacted  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 07:23:14)


***

### [2024-06-23 07:24:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093440)
>UJ67 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 07:24:27)


***

### [2024-06-23 08:04:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093443)
>
```
IP8 separation FF to flatten lumi excursion during beta* levelling  
  
- Michi
```


creator:	 mihostet  @cs-513-michi9.cern.ch (2024/06/23 08:05:27)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-06-23_08:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647874/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/06/23 08:05:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647874/content)

[Timber %G¿%@ Mozilla Firefox 24-06-23_08:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647876/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/06/23 08:05:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3647876/content)


***

### [2024-06-23 08:45:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093446)
>Access started   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 09:13:51)


***

### [2024-06-23 09:20:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093447)
>Nicolas Voumard called for the BETS reset  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 09:21:44)


***

### [2024-06-23 09:21:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093448)
>Altas called to inform us that they will be running at u=1 for the next fill  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 09:22:12)


***

### [2024-06-23 09:30:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093452)
>Nicolas Voumard did a warnig aknowledge on the BETS, we should be able to rearm when the sector is up again.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/23 09:31:02)


***

### [2024-06-23 09:51:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093456)
>EMILIEN COULOT(ECOULOT) assigned RBAC Role: PO-LHC-Piquet and will expire on: 23-JUN-24 11.51.38.329000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/23 09:51:41)


***

### [2024-06-23 11:25:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093465)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 11:25:59)


***

### [2024-06-23 11:26:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4093466)
>LHC RUN CTRL: New FILL NUMBER set to 9818

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/23 11:26:01)


