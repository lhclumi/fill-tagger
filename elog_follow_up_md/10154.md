# FILL 10154
**start**:		 2024-09-27 14:49:44.777613525+02:00 (CERN time)

**end**:		 2024-09-27 18:43:43.102363525+02:00 (CERN time)

**duration**:	 0 days 03:53:58.324750


***

### [2024-09-27 14:49:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151491)
>LHC RUN CTRL: New FILL NUMBER set to 10154

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 14:49:48)


***

### [2024-09-27 14:50:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151492)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 14:50:27)


***

### [2024-09-27 14:52:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151494)
>End of shift mismatches of MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 14:52:14)

[ LSA Applications Suite (v 16.6.29) 24-09-27_14:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789622/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 14:52:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789622/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_14:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789624/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 14:53:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789624/content)


***

### [2024-09-27 14:58:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151497)
>Event created from ScreenShot Client.  
 INJECTION SEQUENCER v 5.1.11 24-09-27\_14:58.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 14:58:30)

[ INJECTION SEQUENCER  v 5.1.11 24-09-27_14:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789628/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 14:58:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789628/content)


***

### [2024-09-27 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151504)
>Started the shift during the second ramp of MD12663 (wires).  
After arriving at 30cm, reducing crossing angle and switching ON and OFF the BBLRs I dumped.  
Switched to generic hypercycle, refreshed PcInterlock and removed all masks (SIS, BIC).  
  
Prepared for MD12743 (high intensity bunches).  
Some issues capturing 2x48 trains. At 14 PS needed 1h access.  
  
Leaving the machine at injection during the PS access.  
  
  
SIS masks:  
- ADT\_BUNCH\_INTENSITY B1/B2  
- SPS\_SCRAPING\_STATUS  
  
Bunch request:  
- revert 2.4e11 back to 1.62e11  
- revert LHC.SIS.INJ-INTENSITY/IntensityInterlock with nominal tolerances  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 15:11:43)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789644/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 15:11:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789644/content)


***

### [2024-09-27 15:27:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151513)
>Event created from ScreenShot Client.  
DIAMON console [PROD] 2.6.1 - 
 UNKNOWN as LHCOP - LHC 24-09-27\_15:27.png

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/27 15:27:27)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-09-27_15:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789664/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/27 15:27:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789664/content)


***

### [2024-09-27 15:29:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151515)
>
```
We are unable to request the injection, it looks all normal in the injection sequencer, the beam is produced in SPS but the LHC telegram is not updated and the injection timing never comes...  
  
I have to softstart the kickers as the conditionning expired, Greg is coming in CCC.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 15:33:54)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-09-27_15:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789672/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/27 15:33:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789672/content)


***

### [2024-09-27 16:08:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151530)
>
```
RF settings for the next injections with 2.3e11 p/b  
  
- total voltage: 6.5 MV  
- QL: 16k for all except 1B1 - 20k, 2B1 - 18k, 3B2 - 20k  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 16:12:17)

[ LSA Applications Suite (v 16.6.29) 24-09-27_16:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789726/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 16:09:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789726/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_16:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789744/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 16:12:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789744/content)


***

### [2024-09-27 16:12:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151533)
>Greg found the LTIM settings of the  ctmlhc corrupted.  
He restarted the LTIM process and it loks like it came back in a good shape.  
  
We would like to verify by injecting the pilots, but now the PS is down due to RF issue.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 16:14:12)


***

### [2024-09-27 16:25:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151541)
>Still the same issue when trying again to inject the pilots...  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 16:25:43)


***

### [2024-09-27 16:27:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151543)
>
```


| **New access request** | |
| --- | --- |
| Areas to access : | * P4 - UX45 |
| Peoples : | * Ylenia Brischetto (SY/RF/CS)167815 / 63008 - <ylenia.brischetto@cern.ch> |
| Duration | 2h  - **No Access Needed** - |
| ObsBox issues - t.b.d. in the shadow of FASER access | |


```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 16:28:22)


***

### [2024-09-27 16:32:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151546)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P2 - service areas * P6 - service areas * P8 - service areas |
| Peoples : | * Nicolas Laurent Levorin (EN/EL/CS)168191 / 61945 - <nicolas.laurent.levorin@cern.ch> |
| Duration | 2d - **Access Needed** - |
| Visite Technique pour échafaudage à poser pendant le YETS -- possible le 4 octobre ? | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:32:12)


***

### [2024-09-27 16:35:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151548)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P2 - service areas |
| Peoples : | * Giorgio Cotto (EP/UAI)- / 64412 - <giorgio.cotto@cern.ch> |
| Duration | 4h - **Access Needed** - |
| ZDC fix HV power supply issue - 4th oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:35:49)


***

### [2024-09-27 16:35:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151548)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P2 - service areas |
| Peoples : | * Giorgio Cotto (EP/UAI)- / 64412 - <giorgio.cotto@cern.ch> |
| Duration | 4h - **Access Needed** - |
| ZDC fix HV power supply issue - 4th oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:35:49)


***

### [2024-09-27 16:41:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151551)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P1 - S12(tunnel) |
| Peoples : | * Stephane Delarue (EN/EL/MO)161828 / 70987 - <stephane.delarue@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection des éclairages dans les tunnel sur le parcours VIP avant leur visite | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:41:01)


***

### [2024-09-27 16:42:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151553)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P2 - service areas |
| Peoples : | * Stephane Delarue (EN/EL/MO)161828 / 70987 - <stephane.delarue@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection des éclairages dans les tunnel sur le parcours VIP avant leur visite - 1st Oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:42:55)


***

### [2024-09-27 16:44:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151554)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P5 - PM56 |
| Peoples : | * Stephane Delarue (EN/EL/MO)161828 / 70987 - <stephane.delarue@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection des éclairages dans les tunnel sur le parcours VIP avant leur visite - 1st Oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:44:46)


***

### [2024-09-27 16:47:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151556)
>
```
Greg is now going to change a card that is not receiving the timing events. All the timing receiver are broken as if there was a little power glitch, Greg is going to replace it, central timing needs to be stopped for that.  
  
At the end a reboot of the LHC cebtral timing was enough to solve the issue  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:10:40)


***

### [2024-09-27 16:49:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151557)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * PM15-US15 |
| Peoples : | * Laurent Catin (EN/HE/HEM)160343 / 77105 - <laurent.catin@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection et surveillance ascenseur de 6h00-18h00 (visite VIP du premier octobre) - 1st Oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:49:05)


***

### [2024-09-27 16:50:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151558)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P2 - service areas |
| Peoples : | * Laurent Catin (EN/HE/HEM)160343 / 77105 - <laurent.catin@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection et surveillance ascenseur de 6h00-18h00 (visite VIP du premier octobre) - 1st Oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:50:24)


***

### [2024-09-27 16:51:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151559)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P5 - PM56 |
| Peoples : | * Laurent Catin (EN/HE/HEM)160343 / 77105 - <laurent.catin@cern.ch> |
| Duration | 1d - **Access Needed** - |
| Inspection et surveillance ascenseur de 6h00-18h00 (visite VIP du premier octobre) - 1st Oct. | |



creator:	 htimko  @paas-standard-avz-a-smrst.cern.ch (2024/09/27 16:51:17)


***

### [2024-09-27 17:15:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151565)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 17:15:46)


***

### [2024-09-27 17:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151567)
>trajectory for B2, sending small correction

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 17:21:38)

[OpenYASP DV LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_LHCMD 24-09-27_17:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789801/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 17:21:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789801/content)


***

### [2024-09-27 17:21:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151568)
>
```
Inj phase & energy 48b  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:22:26)

[Accelerator Cockpit v0.0.38 24-09-27_17:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789805/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:21:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789805/content)

[Accelerator Cockpit v0.0.38 24-09-27_17:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789809/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:21:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789809/content)

[Accelerator Cockpit v0.0.38 24-09-27_17:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789813/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:22:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789813/content)


***

### [2024-09-27 17:21:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151570)
>B1 trajectory OK with trains

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 17:22:06)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_LHCMD 24-09-27_17:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789811/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 17:22:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789811/content)


***

### [2024-09-27 17:23:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151571)
>96b inj errors

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:23:57)

[Accelerator Cockpit v0.0.38 24-09-27_17:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789815/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:24:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789815/content)

[Accelerator Cockpit v0.0.38 24-09-27_17:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789819/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:24:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789819/content)


***

### [2024-09-27 17:23:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151572)
>
```
Train of 96b at 2.3e11 injected.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:24:32)

[ LHC Fast BCT v1.3.2 24-09-27_17:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789817/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:24:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789817/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-27_17:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789821/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/27 17:24:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789821/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-27_17:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789823/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:24:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789823/content)

[ Beam Intensity - v1.5.0 24-09-27_17:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789827/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:25:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789827/content)

[ LHC BLM Fixed Display 24-09-27_17:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789829/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/27 17:26:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789829/content)


***

### [2024-09-27 17:24:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151573)
>Event created from ScreenShot Client.  
LHC RF CONTROL 24-09-27\_17:24.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:24:59)

[ LHC RF CONTROL 24-09-27_17:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789825/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:30:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789825/content)


***

### [2024-09-27 17:30:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151575)
>Event created from ScreenShot Client.  
Figure 1 24-09-27\_17:30.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:30:28)

[Figure 1 24-09-27_17:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789833/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:30:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789833/content)

[Figure 1 24-09-27_17:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789837/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:31:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789837/content)

[Figure 1 24-09-27_17:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789839/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 17:32:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789839/content)

[Figure 1 24-09-27_17:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789847/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 17:38:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789847/content)

[Figure 1 24-09-27_17:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789849/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 17:37:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789849/content)

[Figure 1 24-09-27_17:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789853/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 17:43:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789853/content)

[Figure 1 24-09-27_17:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789869/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:54:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789869/content)

[Figure 1 24-09-27_17:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789873/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:55:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789873/content)

[Figure 1 24-09-27_17:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789877/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 17:55:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789877/content)

[Figure 1 24-09-27_17:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789879/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 17:57:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789879/content)

[Figure 1 24-09-27_18:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789889/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:00:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789889/content)

[Figure 1 24-09-27_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789891/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:03:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789891/content)

[Figure 1 24-09-27_18:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789893/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:06:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789893/content)

[Figure 1 24-09-27_18:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789907/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:11:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789907/content)

[Figure 1 24-09-27_18:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789913/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 18:12:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789913/content)

[Figure 1 24-09-27_18:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789915/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:12:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789915/content)

[Figure 1 24-09-27_18:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789919/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:15:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789919/content)

[Figure 1 24-09-27_18:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789923/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:18:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789923/content)

[Figure 1 24-09-27_18:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789927/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:19:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789927/content)

[Figure 1 24-09-27_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789933/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:20:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789933/content)

[Figure 1 24-09-27_18:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789935/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:21:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789935/content)

[Figure 1 24-09-27_18:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789937/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:22:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789937/content)

[Figure 1 24-09-27_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789941/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:26:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789941/content)


***

### [2024-09-27 17:58:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151585)
>another injection of 96b in ring2

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:59:11)

[ LHC Fast BCT v1.3.2 24-09-27_17:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789881/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:59:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789881/content)

[ Beam Intensity - v1.5.0 24-09-27_17:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789883/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 17:59:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789883/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-27_17:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789885/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:00:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789885/content)


***

### [2024-09-27 18:09:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151587)
>96b injection also in B1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:10:14)

[ Beam Intensity - v1.5.0 24-09-27_18:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789899/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:10:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789899/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-27_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789901/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:10:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789901/content)

[ LHC Fast BCT v1.3.2 24-09-27_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789903/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:11:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789903/content)


***

### [2024-09-27 18:20:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151591)
>Event created from ScreenShot Client.  
LHC RF CONTROL 24-09-27\_18:20.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:20:05)

[ LHC RF CONTROL 24-09-27_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789931/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:21:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789931/content)


***

### [2024-09-27 18:23:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151592)
>Optimized RF for 2.3e11 p/b!!

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:24:04)

[Mozilla Firefox 24-09-27_18:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789939/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 18:24:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789939/content)


***

### [2024-09-27 18:27:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151593)
>
```
To revert after the high intensity MD:
  
  
- Switch back On the one turn feedbacks (all)  
  
On the injection BP :   
- Revert the total voltage both beams to the Tag Before MD 12743  
- Revert the cavity Q settings to the Tag Before MD 12743  
  
On the non_multiplexed :  
- Revert the RotatorPhase#phaseA settings to the Tag Before MD 12743  
  
- Unmask on SIS  ADT_BUNCH_INTENSITY and revert the SIS settings to the Tag Before MD 12743 (see <https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4151386>)  
  
- Unmask on SIS SPS_SCRAPING interlock  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:39:01)

[Inspector 3.5.53 - untitled 24-09-27_18:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789943/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:28:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789943/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_18:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789945/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:29:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789945/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_18:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789947/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:30:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789947/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_18:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789951/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 18:32:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789951/content)


***

### [2024-09-27 18:36:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151596)
>Please note that the FBCT is saturated for certain bunches which are now above 2.6e11!  
  
<https://its.cern.ch/jira/browse/BIIQ-900>  
  
--T.Levens  


creator:	 tlevens  @pcsy401.cern.ch (2024/09/27 18:37:06)


***

### [2024-09-27 18:39:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151597)
>Global Post Mortem Event

Event Timestamp: 27/09/24 18:39:53.096
Fill Number: 10154
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 5604 / 5534 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 18:42:43)


***

### [2024-09-27 18:39:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151605)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: delph / MD high intensity beam


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 18:59:49)


***

### [2024-09-27 18:43:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151598)
>LHC RUN CTRL: New FILL NUMBER set to 10155

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 18:43:44)


