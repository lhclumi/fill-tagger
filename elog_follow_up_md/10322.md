# FILL 10322
**start**:		 2024-11-06 01:20:26.534613525+01:00 (CERN time)

**end**:		 2024-11-06 03:08:13.501238525+01:00 (CERN time)

**duration**:	 0 days 01:47:46.966625


***

### [2024-11-06 01:20:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178371)
>LHC RUN CTRL: New FILL NUMBER set to 10322

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:20:27)


***

### [2024-11-06 01:21:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178377)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:21:09)


***

### [2024-11-06 01:23:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178378)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:23:18)


***

### [2024-11-06 01:24:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178380)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:24:31)


***

### [2024-11-06 01:24:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178381)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:24:43)


***

### [2024-11-06 01:26:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178383)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:26:12)


***

### [2024-11-06 01:26:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178385)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:26:46)


***

### [2024-11-06 01:27:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178387)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:27:02)


***

### [2024-11-06 01:28:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178388)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:28:14)


***

### [2024-11-06 01:29:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178389)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:29:12)


***

### [2024-11-06 01:29:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178391)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:29:13)


***

### [2024-11-06 01:29:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178393)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:29:16)


***

### [2024-11-06 01:29:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178397)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:29:32)


***

### [2024-11-06 01:29:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178400)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:29:36)


***

### [2024-11-06 01:30:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178401)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:30:09)


***

### [2024-11-06 01:30:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178403)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:30:16)


***

### [2024-11-06 01:30:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178404)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:30:18)


***

### [2024-11-06 01:31:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178406)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:31:31)


***

### [2024-11-06 01:31:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178406)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:31:31)


***

### [2024-11-06 01:33:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178408)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:33:15)


***

### [2024-11-06 01:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178410)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:42:26)


***

### [2024-11-06 01:51:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178412)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:51:28)


***

### [2024-11-06 01:54:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178413)
>Injection correction

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/06 01:54:42)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-06_01:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854197/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/06 01:54:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854197/content)

[ Accelerator Cockpit v0.0.42 24-11-06_01:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854199/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/06 01:56:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854199/content)


***

### [2024-11-06 01:59:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178414)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 01:59:26)


***

### [2024-11-06 02:14:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178416)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:14:14)


***

### [2024-11-06 02:14:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178417)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:14:24)


***

### [2024-11-06 02:14:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178418)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:14:30)


***

### [2024-11-06 02:14:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178419)
>LHC Injection Complete

Number of injections actual / planned: 19 / 20
SPS SuperCycle length: 43.2 [s]
Actual / minimum time: 0:15:04 / 0:19:24 (77.7 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/06 02:14:31)


***

### [2024-11-06 02:16:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178421)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:16:21)


***

### [2024-11-06 02:16:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178423)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:16:22)


***

### [2024-11-06 02:37:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178427)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:37:40)


***

### [2024-11-06 02:37:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178429)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:37:48)


***

### [2024-11-06 02:43:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178431)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:43:37)


***

### [2024-11-06 02:44:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178432)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:44:15)


***

### [2024-11-06 02:44:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178434)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 02:44:54)


***

### [2024-11-06 02:54:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178435)
>Optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/06 02:54:20)

[ LHC Luminosity Scan Client 0.63.2 [pro] 24-11-06_02:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854218/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/06 02:54:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854218/content)


***

### [2024-11-06 02:56:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178437)
>Just checking how reproducible the crystals are

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/06 02:56:40)

[LHC Crystals Cockpit 24-11-06_02:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854220/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/06 02:56:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854220/content)


***

### [2024-11-06 03:04:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178443)
>Global Post Mortem Event

Event Timestamp: 06/11/24 03:04:45.307
Fill Number: 10322
Accelerator / beam mode: ION PHYSICS / ADJUST
Energy: 6799320 [MeV]
Intensity B1/B2: 21 / 20 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: A T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/06 03:07:35)


***

### [2024-11-06 03:04:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178492)
>Global Post Mortem Event Confirmation

Dump Classification: Asynchronous dump
Operator / Comment: tpersson / Acynch dump. 


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/06 03:47:49)


***

### [2024-11-06 03:05:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178439)
>Looks good to me. Same error as before with the intensity but as I expect due to ions.   
  
Screenshot attached: Asynchronous Beam Dump overview  
- Start timestamp: 03:03:55  
- Machine configuration: Phys\_IP2pos\_ions  
  
*Sent From pyLossMaps*

creator:	 tpersson  @cwo-ccc-d3lc.cern.ch (2024/11/06 03:05:11)

[tmpScreenshot_1730858711.7296863.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854222/content)
creator:	 tpersson  @cwo-ccc-d3lc.cern.ch (2024/11/06 03:05:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3854222/content)


***

### [2024-11-06 03:07:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178441)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 03:07:03)


***

### [2024-11-06 03:07:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178442)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 03:07:05)


***

### [2024-11-06 03:08:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178445)
>LHC RUN CTRL: New FILL NUMBER set to 10323

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/06 03:08:14)


