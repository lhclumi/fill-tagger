# FILL 10301
**start**:		 2024-10-29 18:07:44.600988525+01:00 (CERN time)

**end**:		 2024-10-29 23:30:43.983863525+01:00 (CERN time)

**duration**:	 0 days 05:22:59.382875


***

### [2024-10-29 18:07:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173646)
>LHC RUN CTRL: New FILL NUMBER set to 10301

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:07:46)


***

### [2024-10-29 18:08:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173652)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:08:24)


***

### [2024-10-29 18:10:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173653)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:10:23)


***

### [2024-10-29 18:11:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173655)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:11:04)


***

### [2024-10-29 18:11:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173657)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:11:59)


***

### [2024-10-29 18:12:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173659)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:12:55)


***

### [2024-10-29 18:13:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173660)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:13:13)


***

### [2024-10-29 18:14:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173664)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:14:28)


***

### [2024-10-29 18:15:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173665)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:15:06)


***

### [2024-10-29 18:16:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173667)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:16:22)


***

### [2024-10-29 18:17:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173668)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:17:13)


***

### [2024-10-29 18:17:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173670)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:17:20)


***

### [2024-10-29 18:17:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173671)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:17:48)


***

### [2024-10-29 18:19:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173674)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:19:52)


***

### [2024-10-29 18:22:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173678)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:22:18)


***

### [2024-10-29 18:22:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173680)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:22:21)


***

### [2024-10-29 18:22:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173682)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:22:24)


***

### [2024-10-29 18:22:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173686)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:22:39)


***

### [2024-10-29 18:24:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173691)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:24:38)


***

### [2024-10-29 18:26:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173693)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:26:22)


***

### [2024-10-29 18:28:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173698)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:28:03)


***

### [2024-10-29 18:39:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173707)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:39:56)


***

### [2024-10-29 18:57:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173721)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 18:57:03)

[ Accelerator Cockpit v0.0.41 24-10-29_18:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843893/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 18:57:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843893/content)

[ Accelerator Cockpit v0.0.41 24-10-29_18:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843895/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 18:57:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843895/content)

[ Accelerator Cockpit v0.0.41 24-10-29_18:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843897/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 18:57:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843897/content)


***

### [2024-10-29 18:58:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173726)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 18:58:34)


***

### [2024-10-29 19:31:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173742)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:31:25)


***

### [2024-10-29 19:31:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173744)
>full again

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:31:37)

[ LHC Fast BCT v1.3.2 24-10-29_19:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843924/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:32:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843924/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-29_19:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843926/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:31:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843926/content)

[ LHC WIRESCANNER APP 24-10-29_19:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843928/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:31:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843928/content)

[ LHC WIRESCANNER APP 24-10-29_19:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843930/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:31:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843930/content)

[ LHC WIRESCANNER APP 24-10-29_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843932/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:32:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843932/content)

[ LHC WIRESCANNER APP 24-10-29_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843934/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:32:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843934/content)

[ LHC BSRL B2 24-10-29_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843938/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:32:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843938/content)

[ LHC BSRL B1 24-10-29_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843940/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:32:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843940/content)


***

### [2024-10-29 19:31:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173743)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:31:36)


***

### [2024-10-29 19:31:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173745)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:31:42)


***

### [2024-10-29 19:31:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173746)
>LHC Injection Complete

Number of injections actual / planned: 64 / 64
SPS SuperCycle length: 28.8 [s]
Actual / minimum time: 0:33:09 / 0:35:43 (92.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/29 19:31:43)


***

### [2024-10-29 19:34:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173747)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:34:12)


***

### [2024-10-29 19:34:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173749)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:34:16)


***

### [2024-10-29 19:34:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173751)
>blow-up looks OK this time...

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 19:34:35)

[Navigation Tool 2019-LN4-BASELINE(v3.4.0) 24-10-29_19:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843946/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 19:34:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843946/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_19:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843948/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 19:34:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843948/content)


***

### [2024-10-29 19:43:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173758)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:43:26)


***

### [2024-10-29 19:44:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173761)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:44:22)


***

### [2024-10-29 19:45:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173778)
>
```
blow-up behaved well throughout the ramp
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 20:07:04)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844000/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 20:06:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844000/content)


***

### [2024-10-29 19:45:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173762)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:45:01)


***

### [2024-10-29 19:45:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173764)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:45:52)


***

### [2024-10-29 19:51:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173767)
>optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:51:28)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843980/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:02:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843980/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843982/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:51:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843982/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843984/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:51:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843984/content)


***

### [2024-10-29 19:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173769)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 19:52:52)


***

### [2024-10-29 19:55:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173770)
>ALICE on target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:55:20)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_19:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843986/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 19:55:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843986/content)


***

### [2024-10-29 19:58:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173771)
>LDM

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:58:12)

[ LHC BSRL B1 24-10-29_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843988/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:58:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843988/content)

[ LHC BSRL B2 24-10-29_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843990/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/29 19:58:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843990/content)


***

### [2024-10-29 19:58:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173772)
>
```
LHCb has some trouble to inject SMOG and close the VELO, they called the expert  
  
After discussing with the ATLAS team (Witold et al) in the CCC, we decide to go ahead with the ATLAS scan anyway.  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 19:59:43)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-10-29_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843992/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 19:59:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843992/content)


***

### [2024-10-29 20:03:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173774)
>once again, no lifetime issues going into collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/29 20:03:56)

[ LHC Beam Losses Lifetime Display 24-10-29_20:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843996/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/29 20:03:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843996/content)


***

### [2024-10-29 20:05:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173777)
>LHCb closed the VELO, they will need ~5-10 min more to inject SMOG

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 20:05:38)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-10-29_20:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843998/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 20:05:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3843998/content)


***

### [2024-10-29 20:11:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173779)
>intensities and emittances

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:19)

[ LHC Fast BCT v1.3.2 24-10-29_20:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844002/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844002/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-29_20:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844004/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844004/content)


***

### [2024-10-29 20:11:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173779)
>intensities and emittances

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:19)

[ LHC Fast BCT v1.3.2 24-10-29_20:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844002/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844002/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-29_20:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844004/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:11:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844004/content)


***

### [2024-10-29 20:19:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173781)
>LHCb called, they started the SMOG injection

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:19:38)


***

### [2024-10-29 20:21:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173782)
>
```
X scan complete  
  
>> HORIZONTAL fit result: GAUSSIAN_PLUS_CONSTANT -> PEAK=19.36, WIDTH=0.07842, MEAN=0.007771, OFFSET=0.01843  
  
7.7um is quite large as an offset - it looks like >50% of it came with the separation if IP2 to reach their levelling target.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:35:11)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844006/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:23:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844006/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844010/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:22:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844010/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844012/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:30:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844012/content)

[Mozilla Firefox 24-10-29_20:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844014/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:34:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844014/content)


***

### [2024-10-29 20:22:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173783)
>7.77um applied

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:22:27)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844008/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:22:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844008/content)


***

### [2024-10-29 20:41:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173786)
>Y scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:41:25)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844018/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:41:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844018/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844020/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:41:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844020/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_20:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844022/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 20:42:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844022/content)


***

### [2024-10-29 21:04:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173789)
>X scan done

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:04:40)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844036/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:04:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844036/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844038/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:04:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844038/content)


***

### [2024-10-29 21:22:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173792)
>Y scan finished

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:23:04)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844058/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:23:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844058/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844060/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:23:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844060/content)


***

### [2024-10-29 21:28:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173795)
>ATLAS program done.

All IPs (except 2) re-optimized.

ALICE is preparing to go head-on for the repetition of the LSC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:29:00)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844064/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:29:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844064/content)


***

### [2024-10-29 21:33:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173796)
>ALICE optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:33:36)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_21:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844072/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 21:34:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844072/content)


***

### [2024-10-29 22:09:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173815)
>
```
Configured the 2.68 TeV cycle for high intensity trains:  
- Q' = 20/20 flat through the cycle  
- MO = -4.2/-3.5 -> -2.0/-2.0 (with two extra points in the ramp for acc limits)  
- ALICE initial separation: 2*200um (collisions BP)  
- LHCb initial separation: 2*30um (collisions BP) - ~1 sigma, just to have a defined levelling direction  
- ref orbit for collisions BP re-generated  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:19:43)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-10-29_22:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844088/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:14:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844088/content)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-10-29_22:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844090/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:14:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844090/content)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-10-29_22:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844098/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:16:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844098/content)


***

### [2024-10-29 22:28:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173818)
>
```
repeated ALICE LSC scan

still an imperfection in X...
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:31:10)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_22:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844106/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:29:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844106/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_22:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844108/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:29:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844108/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_22:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844110/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:29:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844110/content)


***

### [2024-10-29 22:42:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173822)
>
```
OFB gain to 1 to test a last LSC X scan for ALICE with OFB ON (and trim orchestration)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 22:44:43)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-29_22:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844120/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/29 22:43:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844120/content)


***

### [2024-10-29 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173874)
>**\*\*\* SHIFT SUMMARY \*\*\***  
ATLAS (and ALICE) VdM fills, second and third attempt, after the first attempt got dumped in the morning ~15 min before completing the program.  
  
During the second attempt, the longitudinal blow-up for B1 did not work in the ramp (it stopped 2s after the start), leading to bunch lengths of ~0.66ns at FT. While there was no evident danger for heating (~100 isolated bunches of ~0.8e11), the beam was not usable for VdM scans due to the higher IBS emittance growth rate and following degradation.  
Together with Mik and Helga from RF, we found that the B1 beam control FEC was in trouble, with VME bus errors and spurious IRQs firing. We tried to reboot the FEC, which dumped the beams due to the RF frequency interlock. However afterwards, the problem was gone.  
  
The third attempt finally made it into Stable Beams, completing the VdM program of ATLAS, and allowing for a repetition of the ALICE LSC scan. During the ALICE LSC, the same structure already observed yesterday during the X scan was reproducible again; finally tried one last scan with OFB on (and orchestration), which seems to "cure" the effect if run with sufficiently high EVs (small knob imperfection?)  
  
Before dumping, did a quick test of the RF voltage trim in Stable Beams, which worked without any problems.  
  
Notes:  
- pp-ref cycle re-configured for nominal trains - nominal MO/Q', initial separation for IP2+8  
- during the next ramps, the bunch length should be watched closely ... make sure the longitudinal blow-up triggers!   
  
-- Michi --  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:41:55)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844136/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:41:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844136/content)


***

### [2024-10-29 23:09:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173826)
>
```
ALICE LSC X with OFB on (and trim orchestration) - first part with 40/40 EVs, second part with 200/200 EVs
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:29:15)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_23:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844122/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/29 23:10:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844122/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-29_23:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844124/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:28:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844124/content)

[LHC Collimator Control Application (Device: TCLD.A11R2.B1.TCLD.IP2.B1.1.H) -  INCA DISABLED - PRO CCDA 24-10-29_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844161/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:52:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844161/content)


***

### [2024-10-29 23:12:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173827)
>Trim Total Voltage +0.5MV each beam

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:13:05)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_23:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844126/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:13:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844126/content)


***

### [2024-10-29 23:13:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173828)
>apply another +0.5 MV

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:14:00)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_23:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844128/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:14:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844128/content)


***

### [2024-10-29 23:16:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173829)
>applying +3.5 MV on the total voltage for each beam.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:17:25)

[ LSA Applications Suite (v 16.6.34) 24-10-29_23:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844130/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/29 23:17:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844130/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_23:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844132/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:17:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844132/content)


***

### [2024-10-29 23:18:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173830)
>I do a last step of +0,5 to see if it goes further down

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:19:05)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-29_23:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844134/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/29 23:19:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3844134/content)


***

### [2024-10-29 23:20:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173831)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 23:20:54)


***

### [2024-10-29 23:25:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173832)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 23:25:55)


***

### [2024-10-29 23:27:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173836)
>Global Post Mortem Event

Event Timestamp: 29/10/24 23:27:18.961
Fill Number: 10301
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 2679600 [MeV]
Intensity B1/B2: 940 / 948 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/29 23:30:08)


***

### [2024-10-29 23:27:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173881)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: mihostet / programmed dump


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/29 23:53:14)


***

### [2024-10-29 23:28:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173833)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 23:28:08)


***

### [2024-10-29 23:28:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173834)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 23:28:15)


***

### [2024-10-29 23:28:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4173835)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/29 23:28:34)


