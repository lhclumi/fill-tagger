# FILL 9659
**start**:		 2024-05-21 23:34:04.962863525+02:00 (CERN time)

**end**:		 2024-05-22 05:44:13.819238525+02:00 (CERN time)

**duration**:	 0 days 06:10:08.856375


***

### [2024-05-21 23:34:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072287)
>LHC RUN CTRL: New FILL NUMBER set to 9659

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:34:05)


***

### [2024-05-21 23:35:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072292)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:35:20)


***

### [2024-05-21 23:37:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072293)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:37:44)


***

### [2024-05-21 23:39:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072294)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:39:16)


***

### [2024-05-21 23:40:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072295)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:40:10)


***

### [2024-05-21 23:40:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072297)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:40:14)


***

### [2024-05-21 23:41:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072298)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:41:36)


***

### [2024-05-21 23:42:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072300)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:42:15)


***

### [2024-05-21 23:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072301)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:44:04)


***

### [2024-05-21 23:44:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072303)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:44:11)


***

### [2024-05-21 23:47:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072307)
>
```
ADT activity monitor during the last 2 fills
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/21 23:53:42)

[ ADT Activity Monitor 24-05-21_23:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601641/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/21 23:47:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601641/content)

[ ADT Activity Monitor 24-05-21_23:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601643/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/21 23:47:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601643/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-22_01:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601717/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:08:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601717/content)


***

### [2024-05-21 23:55:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072314)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:55:35)


***

### [2024-05-21 23:59:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072316)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/21 23:59:19)


***

### [2024-05-22 00:01:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072318)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:01:21)


***

### [2024-05-22 00:02:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072320)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:02:55)


***

### [2024-05-22 00:03:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072321)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:03:45)


***

### [2024-05-22 00:03:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072323)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:03:46)


***

### [2024-05-22 00:03:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072325)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:03:47)


***

### [2024-05-22 00:04:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072329)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:04:01)


***

### [2024-05-22 00:05:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072332)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:05:59)


***

### [2024-05-22 00:07:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072334)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:07:43)


***

### [2024-05-22 00:10:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072336)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:10:22)


***

### [2024-05-22 00:35:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072340)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 00:35:18)


***

### [2024-05-22 00:36:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072341)
>trajectories with 12b, send a small correction in B1V

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/22 00:37:09)

[OpenYASP V4.8.10   LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-05-22_00:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601709/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/22 00:37:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601709/content)


***

### [2024-05-22 00:38:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072342)
>36b trajectory after correction

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/22 00:39:01)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-05-22_00:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601711/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/22 00:39:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601711/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-05-22_00:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601713/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/22 00:39:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601713/content)


***

### [2024-05-22 01:07:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072343)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-05-22\_01:07.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:07:58)

[ LHC Fast BCT v1.3.2 24-05-22_01:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601715/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:08:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601715/content)


***

### [2024-05-22 01:08:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072344)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:08:46)


***

### [2024-05-22 01:08:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072345)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:08:55)


***

### [2024-05-22 01:09:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072346)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:09:04)


***

### [2024-05-22 01:09:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072347)
>LHC Injection CompleteNumber of injections actual / planned: 54 / 46SPS SuperCycle length: 33.6 [s]Actual / minimum time: 0:33:46 / 0:30:45 (109.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/22 01:09:05)


***

### [2024-05-22 01:13:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072348)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:13:12)


***

### [2024-05-22 01:13:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072350)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:13:13)


***

### [2024-05-22 01:34:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072352)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:34:32)


***

### [2024-05-22 01:34:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072354)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:34:40)


***

### [2024-05-22 01:43:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072356)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:43:11)


***

### [2024-05-22 01:43:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072356)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:43:11)


***

### [2024-05-22 01:43:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072357)
>Trim QV B2 -0.001

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/22 01:44:28)

[Accelerator Cockpit v0.0.35 24-05-22_01:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601719/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/22 01:44:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601719/content)


***

### [2024-05-22 01:44:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072358)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:44:44)


***

### [2024-05-22 01:45:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072360)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:45:29)


***

### [2024-05-22 01:48:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072361)
>NO losses when going in collisions

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/22 01:48:20)

[ LHC BLM Fixed Display 24-05-22_01:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601721/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/22 01:48:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601721/content)

[ LHC Beam Losses Lifetime Display 24-05-22_01:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601723/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/22 01:48:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601723/content)


***

### [2024-05-22 01:49:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072362)
>60% when starting optimization

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/22 01:49:45)

[ LHC BLM Fixed Display 24-05-22_01:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601725/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/22 01:49:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601725/content)


***

### [2024-05-22 01:50:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072363)
>
```
Difficult to show on a screenshot. but the tune signal on B2V is very agitated with the baseline jumping   

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:55:30)

[ TuneViewer Light V5.7.2 24-05-22_01:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601727/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/22 01:50:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601727/content)


***

### [2024-05-22 01:51:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072364)
>
```
All IPs optimized
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:56:28)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-22_01:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601729/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 01:54:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601729/content)


***

### [2024-05-22 01:52:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072366)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:52:15)


***

### [2024-05-22 01:57:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072369)
>V2 tune signal agitated

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/22 01:57:44)

[ TuneViewer Light V5.7.2 24-05-22_01:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601737/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/22 01:57:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601737/content)


***

### [2024-05-22 01:58:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072370)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 01:58:42)


***

### [2024-05-22 01:59:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072371)
>IP5 emmitance scan 9points, veto for IP1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 02:00:03)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-22_01:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601739/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 02:00:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601739/content)


***

### [2024-05-22 02:00:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072372)
>start B\* levelling  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 02:00:31)


***

### [2024-05-22 02:00:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072373)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 02:00:40)


***

### [2024-05-22 02:24:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072374)
>all experiments on target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 02:24:31)

[LHC Luminosity Scan Client 0.58.3 [pro] 24-05-22_02:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601741/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 02:24:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601741/content)


***

### [2024-05-22 05:32:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072380)
>I didn't click the stop levelling check box to stop the levelling at 36 cm and did a step more to 33cm...

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 05:33:21)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-22_05:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601743/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/22 05:33:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3601743/content)


***

### [2024-05-22 05:38:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072381)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:38:01)


***

### [2024-05-22 05:41:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072383)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:41:44)


***

### [2024-05-22 05:41:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072384)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:41:45)


***

### [2024-05-22 05:43:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072385)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:43:01)


***

### [2024-05-22 05:43:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072398)
>Global Post Mortem EventEvent Timestamp: 22/05/24 05:43:12.676Fill Number: 9659Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799320 [MeV]Intensity B1/B2: 24920 / 24435 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/22 05:46:03)


***

### [2024-05-22 05:43:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072386)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:43:33)


***

### [2024-05-22 05:43:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072387)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:43:40)


***

### [2024-05-22 05:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072388)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:44:01)


***

### [2024-05-22 05:44:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4072389)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/22 05:44:03)


