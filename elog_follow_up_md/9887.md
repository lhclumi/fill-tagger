# FILL 9887
**start**:		 2024-07-11 12:14:51.328863525+02:00 (CERN time)

**end**:		 2024-07-12 13:03:21.674613525+02:00 (CERN time)

**duration**:	 1 days 00:48:30.345750


***

### [2024-07-11 12:14:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104517)
>LHC RUN CTRL: New FILL NUMBER set to 9887

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:14:52)


***

### [2024-07-11 12:23:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104524)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:23:20)


***

### [2024-07-11 12:24:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104530)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:24:36)


***

### [2024-07-11 12:26:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104532)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:26:41)


***

### [2024-07-11 12:28:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104534)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:28:03)


***

### [2024-07-11 12:28:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104536)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:28:55)


***

### [2024-07-11 12:29:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104539)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:29:52)


***

### [2024-07-11 12:30:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104540)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:30:07)


***

### [2024-07-11 12:32:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104542)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:22)


***

### [2024-07-11 12:32:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104543)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:32)


***

### [2024-07-11 12:32:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104545)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:33)


***

### [2024-07-11 12:32:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104547)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:35)


***

### [2024-07-11 12:32:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104551)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:49)


***

### [2024-07-11 12:32:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104553)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:51)


***

### [2024-07-11 12:32:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104555)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:32:58)


***

### [2024-07-11 12:33:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104556)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:33:57)


***

### [2024-07-11 12:34:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104558)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:34:49)


***

### [2024-07-11 12:35:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104562)
>ALICE VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:35:40)


***

### [2024-07-11 12:36:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104563)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:36:37)


***

### [2024-07-11 12:53:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104574)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 12:53:21)


***

### [2024-07-11 13:03:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104582)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 13:03:04)


***

### [2024-07-11 13:06:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104596)
>
```
We called Andy after the dump and he had a look. The issue is not clear to him and he will continue looking. He told us to reprepare the RF and continue, but the RF tuner of cavity 4.B2 was in a bad state. Andy is looking.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 13:21:21)


***

### [2024-07-11 13:08:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104584)
>ANDREW BUTTERWORTH(BUTTERWO) assigned RBAC Role: RF-LHC-Piquet


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/11 13:08:16)


***

### [2024-07-11 13:22:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104599)
>Daniel Valuch called after we discussed with Wolfgang this morning and he powercycled the crate in order to get ADT module v2B1 back  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 13:23:01)


***

### [2024-07-11 13:22:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104602)
>Signal processing unit of ADT V2B1 was in a completely uninitialized state, it lost all settings. Reason for this is unknown, as the second unit in the same crate was operating normally. I have power-cycled the crate remotely and it came back normal.   
Daniel  
  


creator:	 dvaluch  @cwe-513-vml084.cern.ch (2024/07/11 13:25:42)


***

### [2024-07-11 13:24:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104601)
>Event created from ScreenShot Client.  
Inspector 3.5.53 - ACS Control 
 24-07-11\_13:24.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/11 13:24:58)

[Inspector 3.5.53 - ACS Control 24-07-11_13:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674436/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/11 13:24:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674436/content)


***

### [2024-07-11 13:29:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104609)
>Andy called back, he needs an access in UX45 to exchange a card  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 13:30:12)


***

### [2024-07-11 13:47:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104629)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 13:47:02)


***

### [2024-07-11 13:47:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104630)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 13:47:13)


***

### [2024-07-11 13:48:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104706)
>Event created from ScreenShot Client.  
 LHC Sequencer Execution GUI (PRO) : 12.32.13 24-07-11\_13:48.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 16:11:26)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-11_13:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674685/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 17:08:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674685/content)


***

### [2024-07-11 13:49:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104635)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 13:49:21)


***

### [2024-07-11 14:19:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104648)
>The RF piquet called the CCC, and he will go now to point 4  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 14:19:34)


***

### [2024-07-11 14:40:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104660)
>ALICE compensation knobs back in INJECTION and RAMP BPs

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 14:41:08)

[ LSA Applications Suite (v 16.6.7) 24-07-11_14:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674579/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 14:41:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674579/content)

[ LSA Applications Suite (v 16.6.7) 24-07-11_14:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674581/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 14:41:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674581/content)


***

### [2024-07-11 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104702)
>
```
**SHIFT SUMMARY:**  
  
Arrived during the squeeze to 1.2m.   
  
Tune trims of 2e-3 in B1H at 1.2m were very beneficial for lifetime of beam 1.  
   
Ramped up ALICE solenoid during beta* levelling (no impact observed) and put beams back in collisions in IP2. Regular spikes could be observed on the BRAN and ALICE luminosity signals with a period of ~36 seconds.    
  
Beams were dumped by an RF BIC interlock for beam 2 (sum LLRF), most likely linked to a failure of a tuner card in the low level RF crate as diagnosed by Andy. Andy contacted the RF piquet and he accessed UX45 to replace the tuner card. They are testing it while the SPS is OFF with a vacuum leak in an RF cavity.  
  
Leaving the machine ready to reinject when beam is back in SPS.  
  
* To be noted:

  
- George noticed that IP1 and IP5 were quite off from the optimizations.  
- LHCb lumi levelling failed and George saw that the luminosity signal for LHCb was shaky and that this problem was not affecting the BRANs. We called LHCb and they fixed the problem very quickly.  
-Matteo put back the ALICE compensation knobs in INJECTION and RAMP beam processes.  
- Michi introduced a bigger separation in IP1 during beta* levelling as pile-up was reaching 60 during the initial squeeze to 60 cm.  
- There was a BLM warning in 11L5 while at 49 cm. Nothing in the UFO buster.  
- Daniel Valuch powercycled the ADT crates, missing module 2 in B1V should now be operational.  
- ALICE accessed and saw that the leak was small enough to continue operation  
   
  
Benoit  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 16:10:30)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674680/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 16:08:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674680/content)


***

### [2024-07-11 15:25:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104679)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 15:25:47)


***

### [2024-07-11 15:28:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104681)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 15:28:06)


***

### [2024-07-11 15:28:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104682)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 15:28:40)


***

### [2024-07-11 16:00:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104697)
>LHCb has a problem closing their VELO so they have asked for an access.   


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/11 16:01:03)


***

### [2024-07-11 16:08:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104704)
>DIEGO BARRIENTOS(DBARRIEN) assigned RBAC Role: RF-LHC-Piquet and will expire on: 11-JUL-24 10.08.57.711000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/11 16:08:59)


***

### [2024-07-11 16:10:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104705)
>LHC SEQ: ramping down LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 16:10:59)


***

### [2024-07-11 16:10:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104705)
>LHC SEQ: ramping down LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 16:10:59)


***

### [2024-07-11 16:11:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104707)
>Ramping down the LHCb magnet on their request for the checks of the VELO

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 16:11:46)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-11_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674687/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 16:11:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674687/content)


***

### [2024-07-11 17:08:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104730)
>RF is off

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 17:08:23)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-11_17:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674787/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 17:08:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674787/content)


***

### [2024-07-11 17:27:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104742)
>JORDAN DAVID WALKER(JDWALKER) assigned RBAC Role: PO-LHC-Piquet and will expire on: 11-JUL-24 11.27.02.072000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/11 17:27:03)


***

### [2024-07-11 18:26:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104761)
>DAVID NISBET(DNISBET) assigned RBAC Role: PO-LHC-Piquet and will expire on: 12-JUL-24 12.26.17.840000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/11 18:26:20)


***

### [2024-07-11 18:46:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104766)
>The limits are now updated manually to +-280. I will start the test.

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 18:46:31)

[ Equip State 24-07-11_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674870/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 18:46:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674870/content)


***

### [2024-07-11 18:51:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104768)
>Reached 270A before it quenched.

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 18:52:16)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-07-11_18:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674874/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 18:52:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674874/content)


***

### [2024-07-11 19:16:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104774)
>Cryo conditions lost in point8. It seems a DBF was lost.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/11 19:16:55)

[Set SECTOR81 24-07-11_19:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674876/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/11 19:16:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674876/content)


***

### [2024-07-11 20:14:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104780)
>Tripped at 269A so slight detraining. Will give it another go as 
 discussed. If still not above 270 we will stop for the night.

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 20:14:54)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-07-11_20:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674878/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 20:14:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674878/content)


***

### [2024-07-11 20:57:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104783)
>Olivier called and informed that the reaon for the trip wasn't due to a 
 trip but due to a local limit on the power converter of 268A. This needs 
 an access to be changed. I said it wasn't urgent since we can't power 
 today but Oliver will follow up with his collegues for an access to fix 
 this.

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 20:58:14)

[ pmea-lv22-6.1.0 24-07-11_20:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674880/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 20:58:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674880/content)


***

### [2024-07-11 22:59:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104798)
>
```
Summary draft:  
-Arrived when they were fixing the coupler in LHC. It was fixed around 16.00 and they also did some calibration so all should be fine for this.  
-LHCb requested access because some issues with the VELO.  
-SPS informed us that there will be no beam for at least 24h, proboby longer following a burnt coupler. The thing that takes time is to pump down the vacuum and recover the conditions.   
-Together with David we changed the LIMITS.OP.I.POS and NEG to 280A for the RQTL8.L7.B1 but according to Olivier there is also a hardware limit at 268A that needs to be changed locally. I said there would be the possibliity to access tomorrow to change this.  
-ALL sectors, RF and LHCb solenoid are off. ALICE prefers to keep their solnoid on. 
```


creator:	 delph  @cwe-513-vml005.cern.ch (2024/07/12 08:23:02)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674906/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/11 22:59:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3674906/content)


***

### [2024-07-12 10:44:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104923)
>Receive the QHD analysis for an event that occurred yesterday. The heater fired for a few IPQs in sector 45 due to thunderstorm.  
As 2 circuits failed, I call Arjan who is MP3 best effort. He will check and let us know if we can power the circuits

creator:	 delph  @194.12.178.199 (2024/07/12 10:45:57)

[screenShot_July_12th_2024_10_46_18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675343/content)
creator:	 delph  @194.12.178.199 (2024/07/12 10:46:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675343/content)


***

### [2024-07-12 11:01:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104926)
>Arjan called back, there is an issue with the heater of RQ10.R5.  
Tomasz will do a test 

creator:	 delph  @194.12.178.199 (2024/07/12 11:04:34)


***

### [2024-07-12 11:03:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104925)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 12-JUL-24 01.03.53.263000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/12 11:03:56)


***

### [2024-07-12 11:07:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104928)
>Target Q' to 9 units toward 30 cm (was 10)

creator:	 lhcop  @cwe-513-vpl956.cern.ch (2024/07/12 11:07:43)

[ LSA Applications Suite (v 16.6.7) 24-07-12_11:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675353/content)
creator:	 lhcop  @cwe-513-vpl956.cern.ch (2024/07/12 11:07:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675353/content)


***

### [2024-07-12 11:23:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104934)
>
```
Access shielding in a bad state in PM18, probably not closed properly. I call the gerant de site but in the meanwhile the guy from EN_CV wants to access again, he will call me when he get out.
```


creator:	 delph  @194.12.178.199 (2024/07/12 11:32:12)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675371/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/07/12 11:23:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675371/content)


***

### [2024-07-12 11:32:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104936)
> EPC has modified the max current for RQTL8.L7.B1.

creator:	 delph  @194.12.178.199 (2024/07/12 11:32:49)


***

### [2024-07-12 11:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104939)
>PM56 lift is now fully operational.

creator:	 delph  @194.12.178.199 (2024/07/12 11:44:14)


***

### [2024-07-12 11:55:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104942)
>CMS and Alice access are over.  
LHCb still need one hour.  
Access for AFP in UL14 for the next hour.

creator:	 delph  @194.12.178.199 (2024/07/12 11:56:06)


***

### [2024-07-12 12:17:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104951)
>Running PNO.d3 on RQTL8.L7B1. Tripped around 278A

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 12:18:31)

[ I Viewer HWC 24-07-12_12:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675408/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 12:18:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675408/content)


***

### [2024-07-12 12:35:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104954)
>FGC for M1B2 coupler are in bad state, I can't load the injection settings

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/07/12 12:37:18)

[RF FGC STATUS - 4.1.4 24-07-12_12:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675410/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/07/12 12:37:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675410/content)


***

### [2024-07-12 12:39:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104957)
>I call Andy, he will have a look at the RF FGC issue, for him it should have been solved by a change of card yesterday.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 12:39:48)


***

### [2024-07-12 12:59:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104969)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 12:59:35)


***

### [2024-07-12 13:03:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104977)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:03:20)


***

### [2024-07-12 13:03:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104978)
>LHC RUN CTRL: New FILL NUMBER set to 9888

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:03:22)


