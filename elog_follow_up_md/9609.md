# FILL 9609
**start**:		 2024-05-08 20:19:55.740863525+02:00 (CERN time)

**end**:		 2024-05-09 06:55:28.353863525+02:00 (CERN time)

**duration**:	 0 days 10:35:32.613000


***

### [2024-05-08 20:20:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064372)
>LHC SEQ: ramping up ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:20:27)


***

### [2024-05-08 20:21:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064373)
>LHC SEQ: ramping up ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:21:00)


***

### [2024-05-08 20:21:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064374)
>Ra,ping UP ALICE magnets after their green light

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 20:21:20)


***

### [2024-05-08 20:23:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064380)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:23:37)


***

### [2024-05-08 20:23:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064381)
>Event created from ScreenShot Client.  
Power Converter(s) failed 24-05-08\_20:23.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:23:47)

[Power Converter(s) failed  24-05-08_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584250/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:23:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584250/content)

[Circuit_RSD1_A56B2:   24-05-08_20:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584254/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:24:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584254/content)

[ Equip State 24-05-08_20:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584258/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:25:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584258/content)


***

### [2024-05-08 20:24:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064382)
>Event created from ScreenShot Client.  
Power Converter(s) failed 24-05-08\_20:24.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:24:16)

[Power Converter(s) failed  24-05-08_20:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584252/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:24:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584252/content)

[ Equip State 24-05-08_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584260/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:26:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584260/content)


***

### [2024-05-08 20:24:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064383)
>Event created from ScreenShot Client.  
Power Converter(s) failed 24-05-08\_20:24.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:24:54)

[Power Converter(s) failed  24-05-08_20:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584256/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:24:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584256/content)

[ Equip State 24-05-08_20:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584262/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:27:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584262/content)


***

### [2024-05-08 20:25:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064384)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:25:45)


***

### [2024-05-08 20:27:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064386)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:27:20)


***

### [2024-05-08 20:28:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064387)
>Event created from ScreenShot Client.  
LHC Sequencer Execution GUI (PRO) : 12.32.1 24-05-08\_20:28.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:28:10)

[ LHC Sequencer Execution GUI (PRO) : 12.32.1  24-05-08_20:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584264/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:28:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584264/content)

[ BETS Explorer 24-05-08_20:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584266/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 20:28:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584266/content)


***

### [2024-05-08 20:28:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064388)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:28:32)


***

### [2024-05-08 20:29:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064390)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:29:00)


***

### [2024-05-08 20:29:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064392)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:29:40)


***

### [2024-05-08 20:30:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064393)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:30:21)


***

### [2024-05-08 20:30:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064395)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:30:36)


***

### [2024-05-08 20:31:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064397)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:31:53)


***

### [2024-05-08 20:32:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064398)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:32:26)


***

### [2024-05-08 20:32:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064400)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:32:32)


***

### [2024-05-08 20:32:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064401)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:32:39)


***

### [2024-05-08 20:34:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064404)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:34:26)


***

### [2024-05-08 20:35:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064406)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:35:03)


***

### [2024-05-08 20:35:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064408)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:35:04)


***

### [2024-05-08 20:35:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064410)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:35:05)


***

### [2024-05-08 20:35:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064415)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:35:19)


***

### [2024-05-08 20:37:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064417)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:37:19)


***

### [2024-05-08 20:39:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064419)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 20:39:03)


***

### [2024-05-08 21:03:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064427)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:03:27)


***

### [2024-05-08 21:08:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064433)
>Issue with ATLAS Handshake

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:08:54)

[LHC-EXPTS Handshakes 24-05-08_21:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584300/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:08:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584300/content)


***

### [2024-05-08 21:09:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064434)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:09:02)


***

### [2024-05-08 21:16:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064437)
>Corrections with pilots

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:16:43)

[RF Trim Warning 24-05-08_21:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584304/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:16:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584304/content)

[Accelerator Cockpit v0.0.35 24-05-08_21:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584306/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:16:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584306/content)

[Accelerator Cockpit v0.0.35 24-05-08_21:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584308/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:17:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584308/content)

[Accelerator Cockpit v0.0.35 24-05-08_21:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584310/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:17:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584310/content)

[Accelerator Cockpit v0.0.35 24-05-08_21:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584312/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/08 21:18:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584312/content)


***

### [2024-05-08 21:19:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064439)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:19:30)


***

### [2024-05-08 21:36:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064444)
>Event created from ScreenShot Client.  
Inner Triplets Alignment Display V5.3.5 24-05-08\_21:36.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:36:13)

[Inner Triplets Alignment Display V5.3.5 24-05-08_21:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584320/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:36:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584320/content)


***

### [2024-05-08 21:42:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064445)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 24-05-08\_21:42.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 21:42:56)

[ LHC Fast BCT v1.3.2 24-05-08_21:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584322/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 21:42:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584322/content)

[ LHC Fast BCT v1.3.2 24-05-08_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584324/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 21:43:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584324/content)


***

### [2024-05-08 21:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064451)
>Machine Filled

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 21:53:43)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-08_21:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584343/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 21:53:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584343/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-05-08_21:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584345/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/08 21:53:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584345/content)

[ LHC Beam Losses Lifetime Display 24-05-08_21:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584347/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:54:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584347/content)


***

### [2024-05-08 21:53:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064452)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:53:52)


***

### [2024-05-08 21:54:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064453)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:54:21)


***

### [2024-05-08 21:54:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064454)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:54:30)


***

### [2024-05-08 21:54:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064455)
>LHC Injection CompleteNumber of injections actual / planned: 56 / 48SPS SuperCycle length: 33.6 [s]Actual / minimum time: 0:35:00 / 0:31:52 (109.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:54:31)


***

### [2024-05-08 21:56:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064456)
>QFB ON

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:56:19)

[ LHC Beam Losses Lifetime Display 24-05-08_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584349/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 21:56:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584349/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-08_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584351/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 21:56:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584351/content)


***

### [2024-05-08 21:56:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064457)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:56:49)


***

### [2024-05-08 21:56:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064459)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 21:56:50)


***

### [2024-05-08 22:18:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064464)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:18:07)


***

### [2024-05-08 22:18:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064466)
>Fl;atTOP

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:18:24)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-08_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584363/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:18:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584363/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-05-08_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584365/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/08 22:18:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584365/content)

[ LHC Beam Losses Lifetime Display 24-05-08_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584367/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:18:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584367/content)

[ LHC Fast BCT v1.3.2 24-05-08_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584369/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:18:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584369/content)

[ LHC Fast BCT v1.3.2 24-05-08_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584371/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:18:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584371/content)


***

### [2024-05-08 22:19:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064467)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:19:07)


***

### [2024-05-08 22:27:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064470)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:27:34)


***

### [2024-05-08 22:28:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064471)
>QCHANGE without QFB

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:28:15)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-08_22:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584373/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:28:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584373/content)

[ LHC Beam Losses Lifetime Display 24-05-08_22:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584375/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:28:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584375/content)


***

### [2024-05-08 22:28:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064472)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:28:36)


***

### [2024-05-08 22:29:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064474)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:29:21)


***

### [2024-05-08 22:31:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064475)
>Collidiung

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:31:46)

[ LHC Beam Losses Lifetime Display 24-05-08_22:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584377/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:31:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584377/content)


***

### [2024-05-08 22:33:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064476)
>33% of Dump threshold

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:33:16)

[ LHC BLM Fixed Display 24-05-08_22:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584379/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:33:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584379/content)


***

### [2024-05-08 22:33:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064476)
>33% of Dump threshold

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:33:16)

[ LHC BLM Fixed Display 24-05-08_22:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584379/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/08 22:33:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584379/content)


***

### [2024-05-08 22:33:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064477)
>Optimized IP2/8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:33:25)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584381/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:33:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584381/content)


***

### [2024-05-08 22:34:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064478)
>Optimized IP1/5 (5 was far most likely after the cryo issue and its effect on triplet)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:34:40)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584383/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:34:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584383/content)


***

### [2024-05-08 22:34:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064479)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:34:52)


***

### [2024-05-08 22:38:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064480)
>IP2/IP8 on target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:38:10)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584385/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:38:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584385/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584387/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:38:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584387/content)


***

### [2024-05-08 22:42:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064481)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:42:08)


***

### [2024-05-08 22:44:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064482)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/08 22:44:07)


***

### [2024-05-08 22:45:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064483)
>XRPs IN

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:45:31)

[COLLIMATORS_VISTAR_MOV 24-05-08_22:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584389/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/08 22:45:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584389/content)


***

### [2024-05-08 22:52:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064484)
>Emittance scan in IP1/5 (15pts)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:52:21)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584391/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:52:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584391/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584393/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:52:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584393/content)


***

### [2024-05-08 22:52:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064485)
>beta\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:52:46)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-08_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584395/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/08 22:52:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584395/content)


***

### [2024-05-08 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064494)
>
```
**SHIFT SUMMARY:**  
  
  
- Recovered from access and precycled  
  
- Smooth cycle to Stable beams after CRYO recovery  
  
- collisions in IP5 were found Off once reaching SB  
  
- Beam loading effect in the PS is very much visible on first bunch of each 36b train reaching as low as 1.3e11 p/b  
  
- Leaving the machine in SB  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 23:21:04)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584399/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/08 23:21:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584399/content)


***

### [2024-05-09 01:07:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064504)
>finished B\* lev

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 01:07:15)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-09_01:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584413/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 01:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584413/content)


***

### [2024-05-09 02:43:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064508)
>Autopilot in IP5

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 02:44:03)

[LHC Luminosity Scan Client 0.58.3 [pro] 24-05-09_02:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584425/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 02:44:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584425/content)


***

### [2024-05-09 03:04:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064509)
>Autopilot in IP1

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 03:04:19)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-09_03:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584431/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/09 03:04:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584431/content)


***

### [2024-05-09 06:36:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064531)
>Global Post Mortem EventEvent Timestamp: 09/05/24 06:36:15.700Fill Number: 9609Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799320 [MeV]Intensity B1/B2: 26484 / 26718 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/09 06:39:06)


***

### [2024-05-09 06:36:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064584)
>Global Post Mortem Event ConfirmationDump Classification: Electrical DistributionOperator / Comment: acalia / Electrical glitch at 06:36. BCCM dumped and RF M2B2 tripped.

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/09 07:18:41)


***

### [2024-05-09 06:36:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064527)
>RF tripped

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/09 06:36:50)

[ LHC RF CONTROL 24-05-09_06:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584452/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/09 06:36:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584452/content)

[  24-05-09_06:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584454/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/09 06:38:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584454/content)

[RF INTERLOCKS 24-05-09_06:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584456/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/09 06:38:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584456/content)

[Abort gap cleaning control v2.0.8 24-05-09_06:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584458/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/09 06:38:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584458/content)


***

### [2024-05-09 06:40:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064532)
>TI reported that the issue was an electrical glitch. No apparent problem on the RF module from the diagnostic we have in the GUI. I'll try to reset and continue

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 06:40:23)


***

### [2024-05-09 06:41:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064533)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 06:41:42)


***

### [2024-05-09 06:41:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064534)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 06:41:44)


***

### [2024-05-09 06:41:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064535)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 06:41:50)


***

### [2024-05-09 06:41:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064536)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 06:41:53)


***

### [2024-05-09 06:43:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064538)
>
```
Actually the BCCM triggered the dump following the electrical perturbation     at 6:36  
  
There are no significant beam losses in point 7, the BLMs in point 6 seem to correspond to the dump itself.  
  
The BCT PM does not show anything suspicious.  
  
The raw BCCM data dump that are found in the PM dumps mainly shows a trigger on the total intensity loss, ie due to the dump firing itself. Just window 4 on one of the BCCM systems (system A) shows a different value of ~3E11 above the dump threshold. A flag seems also to be set for that window. The intensity trace of that BCCM shows a "wavy" structure at the end (real or noise...) that may have triggered.  
  
The BCCM may have been triggered by the EL perturbation, leading then to the loss of the RF cavity, but the B2 started to move radially on the BPMs, which fits better RF first and BCCM detects it, maybe because the beam debunched, which could explain why there were no losses at this early stage.  

```


creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:30:05)

[bic_eventseq >> Version: 3.6.2  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-05-09_06:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584464/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 06:43:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584464/content)

[pm-beam-loss-evaluation >> Version: 1.0.4  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-05-09_06:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584470/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 06:50:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584470/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-05-09_06:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584472/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 06:55:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584472/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-05-09_09:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584588/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:11:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584588/content)

[bct >> Version: 7.0.7  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-05-09_09:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584590/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:11:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584590/content)

[Parameters 24-05-09_09:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584600/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:22:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584600/content)

[Parameters 24-05-09_09:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584602/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:20:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584602/content)

[Parameters 24-05-09_09:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584604/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:25:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584604/content)

[int1T 24-05-09_09:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584606/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/09 09:26:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584606/content)

[ PM Data Browser 24-05-09_11:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584652/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 11:12:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584652/content)

[ PM ONLINE PRO GUI : 8.5.8 24-05-09_11:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584654/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 11:20:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3584654/content)


***

### [2024-05-09 06:44:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064652)
>
```
TI saw the glitch at 06:36:15.682 in BE2 and at 06:36:15.702 in LHC Zone 4.  
  
The FGC for klystron modulator RPTK.SR4.RA47U3.R4 sent a postmortem at 06:36:15.700 (but maybe the timing is as precise as the other postmortems)   
  
The BCCM sent the dump trigger at 06:36:15,721  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 11:14:12)


***

### [2024-05-09 06:55:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064539)
>LHC RUN CTRL: New FILL NUMBER set to 9610

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 06:55:28)


