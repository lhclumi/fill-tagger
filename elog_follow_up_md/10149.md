# FILL 10149
**start**:		 2024-09-27 09:08:19.761363525+02:00 (CERN time)

**end**:		 2024-09-27 12:17:25.650488525+02:00 (CERN time)

**duration**:	 0 days 03:09:05.889125


***

### [2024-09-27 09:08:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151243)
>LHC RUN CTRL: New FILL NUMBER set to 10149

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:08:21)


***

### [2024-09-27 09:08:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151249)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:08:59)


***

### [2024-09-27 09:11:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151250)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:11:09)


***

### [2024-09-27 09:11:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151251)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:11:44)


***

### [2024-09-27 09:12:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151253)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:12:36)


***

### [2024-09-27 09:13:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151254)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:13:12)


***

### [2024-09-27 09:13:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151257)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:13:38)


***

### [2024-09-27 09:13:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151258)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:13:51)


***

### [2024-09-27 09:15:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151261)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:15:02)


***

### [2024-09-27 09:15:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151263)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:15:16)


***

### [2024-09-27 09:15:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151265)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:15:41)


***

### [2024-09-27 09:17:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151266)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:17:43)


***

### [2024-09-27 09:17:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151268)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:17:44)


***

### [2024-09-27 09:17:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151270)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:17:47)


***

### [2024-09-27 09:17:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151272)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:17:48)


***

### [2024-09-27 09:17:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151274)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:17:55)


***

### [2024-09-27 09:18:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151277)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:18:03)


***

### [2024-09-27 09:20:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151279)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:20:06)


***

### [2024-09-27 09:21:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151282)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:21:56)


***

### [2024-09-27 09:26:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151285)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:26:16)


***

### [2024-09-27 09:35:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151295)
>reverting TLs optimized for indivs of the night MDs

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/27 09:35:59)

[Confirmation required 24-09-27_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789188/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/27 09:35:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789188/content)


***

### [2024-09-27 09:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151302)
>Loaded PcInterlock with new HC  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 09:43:29)

[ Power Converter Interlock GUI 24-09-27_09:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789218/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:42:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789218/content)


***

### [2024-09-27 09:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151302)
>Loaded PcInterlock with new HC  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 09:43:29)

[ Power Converter Interlock GUI 24-09-27_09:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789218/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:42:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789218/content)


***

### [2024-09-27 09:44:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151307)
>Reverted all the masks of BIS

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:44:26)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-09-27_09:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789228/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:44:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789228/content)


***

### [2024-09-27 09:45:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151312)
>IQC SIS masks reverted

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:45:35)

[ LHC-INJ-SIS Status 24-09-27_09:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789234/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 09:45:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789234/content)


***

### [2024-09-27 09:46:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151315)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 09:46:36)


***

### [2024-09-27 10:26:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151347)
>Pilot energy matching is good

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:26:22)

[Accelerator Cockpit v0.0.38 24-09-27_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789269/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:26:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789269/content)


***

### [2024-09-27 10:47:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151351)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 10:47:29)


***

### [2024-09-27 10:51:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151353)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.38 24-09-27\_10:51.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:51:22)

[Accelerator Cockpit v0.0.38 24-09-27_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789313/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:51:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789313/content)

[OpenYASP V4.9.3   LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1_MD4_Generic@0_[START] 24-09-27_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789315/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:51:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789315/content)

[Accelerator Cockpit v0.0.38 24-09-27_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789317/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 10:51:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789317/content)


***

### [2024-09-27 10:59:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151356)
>
```
**RF settings before injecting 2.3e11 p/b:**  
  
- RF total voltage to 6.5 MV  
- QL to 16k for all, except 1B1 and 3B2 that are at 20k  
- Pre-detuning phase 25 deg, while ideal would be 35 deg  
  - with 30 deg, several lines saturate: 2B1, 3B1, 6B1, 8B1, 6B2, 7B2  

```


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 11:02:54)

[ LSA Applications Suite (v 16.6.29) 24-09-27_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789319/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 11:00:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789319/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789321/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 11:00:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789321/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_11:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789323/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 11:03:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789323/content)

[ LHC RF CONTROL 24-09-27_11:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789325/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:03:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789325/content)


***

### [2024-09-27 11:08:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151358)
>Injecting 48b  


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 11:09:06)

[ LHC RF CONTROL 24-09-27_11:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789329/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:09:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789329/content)

[ LHC RF CONTROL 24-09-27_11:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789341/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:12:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789341/content)


***

### [2024-09-27 11:09:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151359)
>first 48b of average 2.21e11 !

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 11:09:50)

[ LHC Fast BCT v1.3.2 24-09-27_11:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789331/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 11:09:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789331/content)

[ LHC RF CONTROL 24-09-27_11:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789339/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:11:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789339/content)


***

### [2024-09-27 11:10:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151360)
>42% losses injecting B2 48b

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/27 11:10:20)

[ LHC BLM Fixed Display 24-09-27_11:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789335/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/27 11:10:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789335/content)


***

### [2024-09-27 11:13:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151363)
>Global Post Mortem Event

Event Timestamp: 27/09/24 11:13:32.418
Fill Number: 10149
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 1306 / 1301 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 8-RF-b1: B T -> F on CIB.UA47.R4.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 11:16:20)


***

### [2024-09-27 11:13:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151389)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: acalia / RF B1 trip


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 12:19:06)


***

### [2024-09-27 11:21:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151366)
>SIS mask avg bunch int and SPS scraping

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 11:22:06)

[ LHC-INJ-SIS Status 24-09-27_11:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789350/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 11:22:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789350/content)


***

### [2024-09-27 11:22:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151367)
>Half-detuning adjustment with 48b difficult  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:22:24)

[Figure 1 24-09-27_11:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789352/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:22:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789352/content)


***

### [2024-09-27 11:35:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151375)
>
```
ADT intensity interlock (limit at 2.4e11 p/b) blocking injection  
  
After PSB removing one turn, the intensity decreased slightly and we could inject; average at injection was 2.16e11 p/b  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:38:44)


***

### [2024-09-27 11:39:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151376)
>Beam phase loop resolves these intensities well  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:40:05)

[Inspector 3.5.53 - Beam Phase Acqn 24-09-27_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789388/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:40:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789388/content)


***

### [2024-09-27 11:40:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151377)
>Half-detuning adjustment, since beam degrades quickly  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:40:59)

[Figure 1 24-09-27_11:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789392/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:44:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789392/content)

[Figure 1 24-09-27_11:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789398/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:45:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789398/content)

[Figure 1 24-09-27_11:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789400/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:47:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789400/content)

[Figure 1 24-09-27_11:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789402/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:49:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789402/content)

[Figure 1 24-09-27_11:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789406/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:52:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789406/content)

[Figure 1 24-09-27_11:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789408/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:52:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789408/content)

[Figure 1 24-09-27_11:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789418/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:53:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789418/content)

[Figure 1 24-09-27_11:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789424/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:54:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789424/content)

[Figure 1 24-09-27_11:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789428/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:55:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789428/content)

[Figure 1 24-09-27_11:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789430/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:56:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789430/content)

[Figure 1 24-09-27_11:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789432/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:58:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789432/content)

[Figure 1 24-09-27_11:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789434/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:59:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789434/content)

[Figure 1 24-09-27_12:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789436/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:01:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789436/content)

[Figure 1 24-09-27_12:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789438/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:02:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789438/content)

[Figure 1 24-09-27_12:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789442/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:07:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789442/content)

[Figure 1 24-09-27_12:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789444/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:08:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789444/content)

[Figure 1 24-09-27_12:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789446/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 12:10:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789446/content)

[Figure 1 24-09-27_12:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789448/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:11:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789448/content)

[Figure 1 24-09-27_12:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789452/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/27 12:14:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789452/content)


***

### [2024-09-27 11:42:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151378)
>RF Interlock

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:43:20)

[Desktop 24-09-27_11:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789394/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 11:43:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789394/content)


***

### [2024-09-27 11:45:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151379)
>Event created from ScreenShot Client.  
Figure 1 24-09-27\_11:45.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 11:45:15)


***

### [2024-09-27 11:52:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151380)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-09-27\_11:52.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 11:52:51)

[ LHC Fast BCT v1.3.2 24-09-27_11:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789412/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 11:52:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789412/content)


***

### [2024-09-27 12:13:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151386)
>
```
Masked ADT Bunch intensity interlock since it's triggering with these 
    intensities.  
  
Unmasked the avg bunch intensity with a target of 2.4e11 and tight tolerances to still be protected.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 12:15:57)

[ LHC-INJ-SIS Status 24-09-27_12:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789454/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 12:14:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789454/content)

[ INJECTION SEQUENCER  v 5.1.11 24-09-27_12:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789456/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 12:15:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789456/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_12:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789458/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 12:15:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3789458/content)


***

### [2024-09-27 12:14:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151397)
>Daniel confirms that the interlock should be on single bumch intensity. Above 2.5e11 p/b, the transverse emittance could blow-up as the ADT acquisitions used for damping could be saturated  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 12:32:57)


***

### [2024-09-27 12:17:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151390)
>Global Post Mortem Event

Event Timestamp: 27/09/24 12:17:14.487
Fill Number: 10149
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 0 / 3316 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 12:20:07)


***

### [2024-09-27 12:17:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151402)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: acalia / MD


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 12:45:36)


