# FILL 10116
**start**:		 2024-09-14 20:33:17.479363525+02:00 (CERN time)

**end**:		 2024-09-15 14:14:10.174238525+02:00 (CERN time)

**duration**:	 0 days 17:40:52.694875


***

### [2024-09-14 20:33:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143701)
>LHC RUN CTRL: New FILL NUMBER set to 10116

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:33:18)


***

### [2024-09-14 20:36:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143707)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:36:05)


***

### [2024-09-14 20:38:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143709)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:38:10)


***

### [2024-09-14 20:38:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143710)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:38:10)


***

### [2024-09-14 20:39:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143712)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:39:32)


***

### [2024-09-14 20:39:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143714)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:39:40)


***

### [2024-09-14 20:40:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143715)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:40:44)


***

### [2024-09-14 20:40:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143716)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:40:53)


***

### [2024-09-14 20:41:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143718)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:41:35)


***

### [2024-09-14 20:42:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143720)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:42:06)


***

### [2024-09-14 20:42:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143722)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:42:41)


***

### [2024-09-14 20:43:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143725)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:43:59)


***

### [2024-09-14 20:44:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143727)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:44:00)


***

### [2024-09-14 20:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143729)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:44:01)


***

### [2024-09-14 20:44:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143733)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:44:16)


***

### [2024-09-14 20:44:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143735)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:44:48)


***

### [2024-09-14 20:44:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143737)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:44:55)


***

### [2024-09-14 20:46:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143738)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:46:14)


***

### [2024-09-14 20:47:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143740)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:47:58)


***

### [2024-09-14 21:09:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143744)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 21:09:44)


***

### [2024-09-14 21:21:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143748)
>
```
After discussion with the EPC piquet (Nikolai), EPC controls (Dariusz) and BE-CEM (Julien), the problem with cfc-sr8-rr8i looks to be on the FEC/CEM side (probably a broken CPU).  
  
Julien will go on site (surface Pt8 - SR8) to check and replace it.  
  
In case this does not fix the problem, an access in Pt 8 (UA and/or tunnel, depending on where the problem is) will be needed.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 21:22:45)


***

### [2024-09-14 21:41:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143749)
>
```
MKB Earthing Switch fault reset, but IPOC errors on all magnets.  
  
Nicolas is checking.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 21:42:48)

[ WinCC - Operation 24-09-14_21:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772699/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 21:42:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772699/content)

[ WinCC - Operation 24-09-14_21:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772701/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 21:42:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772701/content)


***

### [2024-09-14 22:01:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143751)
>
```
noisy SPI bus on MB.A9L1 board A - resetting - OK after
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:04:24)

[Module QPS_81:A81.RB.A81.DL1K: (NoName) 24-09-14_22:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772709/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:03:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772709/content)

[cs-513-michi9 24-09-14_22:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772713/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:03:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772713/content)


***

### [2024-09-14 22:06:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143752)
>Julien (CEM) replaced the power supply of the gateway - back online

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/14 22:07:22)

[Set SECTOR81 24-09-14_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772715/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/14 22:07:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772715/content)

[ Equip State 24-09-14_22:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772717/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:09:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772717/content)

[ Equip State 24-09-14_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772719/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:09:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772719/content)


***

### [2024-09-14 22:13:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143753)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 22:13:22)


***

### [2024-09-14 22:16:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143757)
>
```
Nicolas analyzed the event on the MKBH B1. There was an erratic after the recharge following the dump (~30s after extraction). For the moment we continue and hope it holds.  
  
In case the problem re-occurs, or during the next planennd access, a local inspection and cleaning of the generator is needed.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 22:27:05)


***

### [2024-09-14 22:41:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143767)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 22:41:49)


***

### [2024-09-14 22:56:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143773)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 22:56:48)


***

### [2024-09-14 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143780)
>
```
***** SHIFT SUMMARY *****  
Stable Beams until ~20:30, when the beams got dumped due to a loss of communication with CODs S81 (broken power supply of FGC gateway cfc-sr8-rr8i, replaced by CEM / Julien).  
  
After the dump, LBDS B1 MKB earthing switches faulty, like in the dump during the ramp yesterday (but the dump was not triggered by LBDS this time). ABT (Christophe and Nicolas) analyzed the event and found there was an erratic on one MKB generator while re-charging after the dump. During the next planned access, an inspection/cleaning should be foreseen.  
  
Note:  
- The Sequencer problem of this morning is understood. There is a race condition in Sequencer that can (erratically) freeze the server in case 10+ subsequences are spawned in too quick succession and the validation takes a bit of time. Until further notice (~next week), the preparation should be done by dragging out the sequences manually to avoid triggering this bug.  
  
-- Michi --  

```


creator:	 mihostet  @michi.cern.ch (2024/09/15 11:52:58)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772799/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/14 23:06:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772799/content)


***

### [2024-09-14 23:03:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143776)
>trajectory with train, small correction sent

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 23:03:58)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-09-14_23:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772789/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 23:03:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772789/content)


***

### [2024-09-14 23:05:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143777)
>wire scans

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 23:05:37)

[ LHC WIRESCANNER APP 24-09-14_23:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772791/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 23:05:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772791/content)

[ LHC WIRESCANNER APP 24-09-14_23:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772793/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 23:05:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772793/content)

[ LHC WIRESCANNER APP 24-09-14_23:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772795/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 23:05:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772795/content)

[ LHC WIRESCANNER APP 24-09-14_23:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772797/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 23:05:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772797/content)


***

### [2024-09-14 23:32:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143787)
>injected beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 23:32:19)

[ LHC Fast BCT v1.3.2 24-09-14_23:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772820/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 23:32:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772820/content)


***

### [2024-09-14 23:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143786)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:32:17)

[ Beam Intensity - v1.4.0 24-09-14_23:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772822/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 23:32:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772822/content)


***

### [2024-09-14 23:32:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143788)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:32:27)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-14_23:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772824/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 23:32:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772824/content)


***

### [2024-09-14 23:32:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143791)
>UFO during teh prepare ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 23:32:49)

[ LHC BLM Fixed Display 24-09-14_23:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772826/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 23:32:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772826/content)


***

### [2024-09-14 23:32:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143789)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:32:36)


***

### [2024-09-14 23:32:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143790)
>LHC Injection Complete

Number of injections actual / planned: 50 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:35:48 / 0:33:48 (105.9 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 23:32:37)


***

### [2024-09-14 23:33:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143792)
>Event created from ScreenShot Client.  
Desktop 24-09-14\_23:33.png

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/09/14 23:33:39)

[Desktop 24-09-14_23:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772828/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/09/14 23:33:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772828/content)


***

### [2024-09-14 23:34:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143794)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:34:24)


***

### [2024-09-14 23:34:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143796)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:34:25)


***

### [2024-09-14 23:34:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143796)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:34:25)


***

### [2024-09-14 23:34:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143798)
>losses at start of ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 23:35:02)

[ LHC BLM Fixed Display 24-09-14_23:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772832/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 23:35:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772832/content)


***

### [2024-09-14 23:55:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143800)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:55:43)


***

### [2024-09-14 23:55:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143802)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 23:55:51)


***

### [2024-09-15 00:04:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143806)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:04:14)


***

### [2024-09-15 00:04:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143807)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:04:50)


***

### [2024-09-15 00:05:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143809)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:06:00)


***

### [2024-09-15 00:10:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143810)
>All IPs optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 00:10:56)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_00:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772842/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 00:10:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772842/content)


***

### [2024-09-15 00:11:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143811)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:11:14)


***

### [2024-09-15 00:17:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143812)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:17:29)


***

### [2024-09-15 00:19:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143813)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 00:19:28)


***

### [2024-09-15 00:23:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143815)
>Emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 00:23:28)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_00:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772844/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 00:23:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772844/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_00:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772846/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 00:23:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772846/content)


***

### [2024-09-15 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143833)
>
```
**SHIFT SUMMARY:**  
  
I arrived when the machine was ready for injection.  
Very smooth cycle, apart from a small UFO druing prepare ramp.  
Leaving the machine in stable beams at the end of the B* levelling.  
  
Delphine  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 06:55:59)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772864/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 06:55:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772864/content)


***

### [2024-09-15 09:03:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143870)
>BBLR ON

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/15 09:03:31)

[ LHC BBLR Wire App v0.4.1 24-09-15_09:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772879/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/15 09:03:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772879/content)


***

### [2024-09-15 14:01:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144031)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:01:26)


***

### [2024-09-15 14:05:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144032)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:05:30)


***

### [2024-09-15 14:05:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144033)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:05:32)


***

### [2024-09-15 14:06:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144034)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:06:26)


***

### [2024-09-15 14:06:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144039)
>Global Post Mortem Event

Event Timestamp: 15/09/24 14:06:41.858
Fill Number: 10116
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 22051 / 22727 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/15 14:09:32)


***

### [2024-09-15 14:06:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144060)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: acalia / programmed dump after physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/15 14:22:22)


***

### [2024-09-15 14:07:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144035)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:07:14)


***

### [2024-09-15 14:07:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144036)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:07:20)


***

### [2024-09-15 14:07:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144037)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:07:22)


***

### [2024-09-15 14:07:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144038)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:07:24)


***

### [2024-09-15 14:14:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144041)
>LHC RUN CTRL: New FILL NUMBER set to 10117

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:14:11)


