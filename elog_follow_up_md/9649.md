# FILL 9649
**start**:		 2024-05-19 14:12:11.661363525+02:00 (CERN time)

**end**:		 2024-05-19 15:57:50.403613525+02:00 (CERN time)

**duration**:	 0 days 01:45:38.742250


***

### [2024-05-19 14:12:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070742)
>LHC RUN CTRL: New FILL NUMBER set to 9649

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:12:12)


***

### [2024-05-19 14:12:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070743)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:12:40)


***

### [2024-05-19 14:19:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070745)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:19:16)


***

### [2024-05-19 14:21:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070747)
>big injection oscillations for indiv

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 14:21:46)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-05-19_14:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597700/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 14:21:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597700/content)


***

### [2024-05-19 14:25:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070749)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:25:47)


***

### [2024-05-19 14:25:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070750)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:25:56)


***

### [2024-05-19 14:26:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070751)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:26:04)


***

### [2024-05-19 14:26:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070752)
>LHC Injection CompleteNumber of injections actual / planned: 4 / 4SPS SuperCycle length: 28.8 [s]Actual / minimum time: 0:06:48 / 0:06:55 (98.3 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/19 14:26:06)


***

### [2024-05-19 14:26:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070752)
>LHC Injection CompleteNumber of injections actual / planned: 4 / 4SPS SuperCycle length: 28.8 [s]Actual / minimum time: 0:06:48 / 0:06:55 (98.3 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/19 14:26:06)


***

### [2024-05-19 14:29:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070753)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:29:51)


***

### [2024-05-19 14:29:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070755)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:29:52)


***

### [2024-05-19 14:51:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070764)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:51:11)


***

### [2024-05-19 14:51:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070766)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:51:30)


***

### [2024-05-19 14:59:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070769)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 14:59:55)


***

### [2024-05-19 15:00:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070770)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:00:40)


***

### [2024-05-19 15:01:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070772)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:01:26)


***

### [2024-05-19 15:02:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070773)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:02:08)


***

### [2024-05-19 15:05:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070779)
>LHCb entering in collisions

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:05:19)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597744/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:05:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597744/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597746/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:05:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597746/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597750/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:07:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597750/content)


***

### [2024-05-19 15:07:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070780)
>ATLAS - CMS - not far off

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:07:08)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597748/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:07:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597748/content)


***

### [2024-05-19 15:08:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070781)
>ATLAS re-separated by 6 sigma

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:08:39)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597752/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:08:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597752/content)


***

### [2024-05-19 15:10:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070782)
>trimming out initial separation in LHCbclearly the "separation" was bringing the beams head-on...

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:11:24)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597754/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:10:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597754/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597756/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:11:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597756/content)


***

### [2024-05-19 15:13:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070783)
>IP8 optimized head-on -> ~91 um separation found (counteracting the initial separation)

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:14:26)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597758/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:14:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597758/content)


***

### [2024-05-19 15:16:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070784)
>moving BRAN copper blocks back IN for IP2.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:16:32)

[ Luminosity BRANB v2 24-05-19_15:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597760/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:16:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597760/content)


***

### [2024-05-19 15:18:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070785)
>IP8 head-on

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:18:55)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597762/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:18:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597762/content)


***

### [2024-05-19 15:19:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070787)
>re-establishing the 2\*36um separation target for LHCb

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:20:08)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597766/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:20:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597766/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597768/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:20:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597768/content)


***

### [2024-05-19 15:29:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070794)
>incorporating back LHCb corrections & initial separation

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:29:26)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597774/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:29:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597774/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597776/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:30:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597776/content)


***

### [2024-05-19 15:32:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070795)
>ATLAS separation reduced to 4 sigma to go to 60cm

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:33:11)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597784/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:33:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597784/content)


***

### [2024-05-19 15:37:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070799)
>
```
beta* levelling - first point mismatch on TCL6.R1 due to the setting change last night.  
  
Since the TCL6 anyway does not move during beta* levelling, for the moment we remove it from the HW group.  
  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:45:53)

[Task failed : diagnostics 24-05-19_15:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597792/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:37:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597792/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597796/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:39:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597796/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_15:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597798/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:44:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597798/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597800/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:45:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597800/content)


***

### [2024-05-19 15:53:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070802)
>at 60cm - LHCb back head-on and reoptimizing

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:53:45)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597803/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:53:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597803/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597805/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:57:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597805/content)


***

### [2024-05-19 15:56:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070803)
>separation re-established

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:57:04)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597807/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:57:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597807/content)

[LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597809/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:57:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597809/content)

[LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597811/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 15:57:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597811/content)


***

### [2024-05-19 15:57:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070813)
>Global Post Mortem EventEvent Timestamp: 19/05/24 15:57:12.447Fill Number: 9649Accelerator / beam mode: PROTON PHYSICS / ADJUSTEnergy: 6799320 [MeV]Intensity B1/B2: 30 / 31 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/19 16:00:02)


***

### [2024-05-19 15:57:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070804)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:57:23)


***

### [2024-05-19 15:57:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070805)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:57:25)


