# FILL 9611
**start**:		 2024-05-09 22:37:58.915738525+02:00 (CERN time)

**end**:		 2024-05-10 15:06:17.838738525+02:00 (CERN time)

**duration**:	 0 days 16:28:18.923000


***

### [2024-05-09 22:37:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064815)
>LHC RUN CTRL: New FILL NUMBER set to 9611

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:37:59)


***

### [2024-05-09 22:39:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064822)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:39:25)


***

### [2024-05-09 22:41:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064823)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:41:31)


***

### [2024-05-09 22:41:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064825)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:41:47)


***

### [2024-05-09 22:43:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064826)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:43:11)


***

### [2024-05-09 22:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064827)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:44:04)


***

### [2024-05-09 22:44:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064829)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:44:17)


***

### [2024-05-09 22:44:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064830)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:44:30)


***

### [2024-05-09 22:45:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064833)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:45:30)


***

### [2024-05-09 22:46:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064834)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:46:07)


***

### [2024-05-09 22:46:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064836)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:46:09)


***

### [2024-05-09 22:48:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064837)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:26)


***

### [2024-05-09 22:48:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064839)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:31)


***

### [2024-05-09 22:48:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064841)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:32)

[ CCM_1 LHCOP 24-05-10_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585903/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/10 16:38:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585903/content)


***

### [2024-05-09 22:48:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064843)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:32)


***

### [2024-05-09 22:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064844)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:33)


***

### [2024-05-09 22:48:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064848)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:48:47)


***

### [2024-05-09 22:50:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064850)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:50:47)


***

### [2024-05-09 22:52:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064852)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 22:52:31)


***

### [2024-05-09 23:08:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064857)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/09 23:08:02)


***

### [2024-05-09 23:09:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064859)
>Problem during extended soft start of MKI 8.  
According to procedure I contact piquet  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 23:11:56)

[MKI Remaining Duration - 2.0.4 24-05-09_23:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585020/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/09 23:09:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585020/content)

[ LHC Injection Kicker - LHC PRO INCA server - PRO CCDA - Version: 3.2.1  - LHC.USER.ALL 24-05-09_23:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585026/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 23:09:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585026/content)

[ WinCC - Operation 24-05-09_23:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585036/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/09 23:10:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585036/content)


***

### [2024-05-10 00:13:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064873)
>ABT piquet gave green light to continue

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 00:13:30)


***

### [2024-05-10 00:17:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064876)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 00:17:25)


***

### [2024-05-10 00:29:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064878)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 00:29:24)


***

### [2024-05-10 01:04:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064879)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:04:44)

[ LHC Fast BCT v1.3.2 24-05-10_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585123/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:04:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585123/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-10_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585125/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:04:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585125/content)


***

### [2024-05-10 01:04:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064880)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:04:54)


***

### [2024-05-10 01:05:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064881)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:05:04)


***

### [2024-05-10 01:05:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064882)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:05:13)


***

### [2024-05-10 01:05:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064883)
>LHC Injection CompleteNumber of injections actual / planned: 58 / 48SPS SuperCycle length: 28.8 [s]Actual / minimum time: 0:35:49 / 0:28:02 (127.7 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:05:14)


***

### [2024-05-10 01:06:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064884)
>Again 31L2B1 sticking out in PcInterlock at injection

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:07:15)

[ Power Converter Interlock GUI 24-05-10_01:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585127/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585127/content)


***

### [2024-05-10 01:07:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064885)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:07:51)


***

### [2024-05-10 01:07:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064887)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:07:52)


***

### [2024-05-10 01:29:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064895)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:29:10)


***

### [2024-05-10 01:29:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064897)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:29:24)


***

### [2024-05-10 01:29:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064899)
>
```
Flat top. Emittance a bit worst in B2
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:32:06)

[ LHC Fast BCT v1.3.2 24-05-10_01:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585145/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:31:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585145/content)

[ LHC Beam Losses Lifetime Display 24-05-10_01:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585149/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:29:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585149/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-10_01:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585151/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 01:29:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585151/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-10_01:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585153/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 01:30:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585153/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-05-10_01:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585155/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/10 01:30:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585155/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-10_01:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585157/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:31:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585157/content)


***

### [2024-05-10 01:37:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064900)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:37:55)


***

### [2024-05-10 01:38:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064903)
>squeeze + qchange

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:38:44)

[ LHC Fast BCT v1.3.2 24-05-10_01:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585159/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:39:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585159/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-10_01:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585161/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:38:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585161/content)

[ LHC Beam Losses Lifetime Display 24-05-10_01:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585163/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:38:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585163/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-10_01:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585165/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 01:39:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585165/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-10_01:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585167/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 01:39:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585167/content)


***

### [2024-05-10 01:38:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064901)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:38:41)


***

### [2024-05-10 01:39:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064904)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:39:37)


***

### [2024-05-10 01:42:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064905)
>Lifetime in collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:43:04)

[ LHC Beam Losses Lifetime Display 24-05-10_01:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585169/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:43:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585169/content)

[Power Loss Monitoring 24-05-10_01:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585173/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/10 01:43:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585173/content)

[ LHC BLM Fixed Display 24-05-10_01:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585175/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 01:43:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585175/content)


***

### [2024-05-10 01:44:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064907)
>Optimizations

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:45:04)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_01:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585177/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:45:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585177/content)


***

### [2024-05-10 01:46:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064910)
>B2H +1e-3 tune optimization

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:46:33)

[ LHC Beam Losses Lifetime Display 24-05-10_01:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585179/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 01:46:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585179/content)


***

### [2024-05-10 01:46:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064911)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:46:52)


***

### [2024-05-10 01:48:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064914)
>clicked start on emittance scan while IP1 was still in veto.Lumiserver correctly reverted the first trim

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:48:54)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_01:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585181/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 01:48:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585181/content)


***

### [2024-05-10 01:53:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064918)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:53:10)


***

### [2024-05-10 01:55:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064921)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 01:55:08)


***

### [2024-05-10 02:00:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064922)
>Started B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 02:01:13)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_02:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585196/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 02:01:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585196/content)


***

### [2024-05-10 02:01:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064923)
>Emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 02:01:21)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585198/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 02:01:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585198/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585200/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 02:01:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585200/content)


***

### [2024-05-10 04:23:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064951)
>B\* levelling finished

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 04:23:16)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_04:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585264/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 04:23:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585264/content)


***

### [2024-05-10 04:23:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064951)
>B\* levelling finished

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 04:23:16)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_04:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585264/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 04:23:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585264/content)


***

### [2024-05-10 05:33:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064967)
>IP1/5 in autopilots

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 05:33:55)

[LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_05:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585286/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 05:33:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585286/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-10_05:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585288/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 05:33:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585288/content)


***

### [2024-05-10 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4064986)
>Started the shift with the machine prepared for beam.  
Extended Soft start for MKI8 failed and I contacted the ABT piquet.  
After running an intermediate soft start, I got green light to continue.  
  
Smooth cycle, intensity spread in trains looks better then yesterday.  
Once in collision did +1e-3 B2H tune optimization.  
  
Leaving the machine in stable beams.

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 07:14:13)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585310/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/10 07:14:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585310/content)


***

### [2024-05-10 09:23:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065023)
>Almost 1.3 inverse femtobarn last 24hours !

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 09:24:07)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-05-10_09:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585360/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 09:24:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585360/content)


***

### [2024-05-10 12:10:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065116)
>Losses in point 3.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 12:10:35)

[ LHC BLM Fixed Display 24-05-10_12:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585685/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 12:10:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585685/content)


***

### [2024-05-10 13:54:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065142)
>Global Post Mortem EventEvent Timestamp: 10/05/24 13:54:32.651Fill Number: 9611Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799200 [MeV]Intensity B1/B2: 22847 / 23156 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK Left: B T -> F on CIB.UJ33.U3.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/10 13:57:28)


***

### [2024-05-10 13:54:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065155)
>Global Post Mortem Event ConfirmationDump Classification: Magnet quenchOperator / Comment: tpersson / Quench in sector23. 

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/10 14:16:09)


***

### [2024-05-10 13:55:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065139)
>Looks like a qunch in sector23

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/10 13:55:44)

[Set SECTOR23 24-05-10_13:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585746/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/10 13:55:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585746/content)


***

### [2024-05-10 13:57:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065143)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 13:57:46)


***

### [2024-05-10 13:57:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065144)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 13:57:48)


***

### [2024-05-10 14:00:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065145)
>Started the intermediate softstart as requested by Giorgia

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 14:00:30)

[MKI Remaining Duration - 2.0.4 24-05-10_14:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585759/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/10 14:00:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585759/content)


***

### [2024-05-10 14:04:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065151)
>This is the event history.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:07:34)

[bic_eventseq >> Version: 3.6.2  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-05-10_14:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585773/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:07:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585773/content)


***

### [2024-05-10 14:06:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065149)
>Quench in S23

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/10 14:06:20)

[Module QPS_23:A23.RB.A23.DL3E: (NoName) 24-05-10_14:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585767/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/10 14:06:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585767/content)

[Module QPS_23:A23.RB.A23.DL3K: (NoName) 24-05-10_14:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585769/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/10 14:06:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585769/content)

[History Buffer 24-05-10_14:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585775/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/10 14:07:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585775/content)

[Circuit_RB_A23:   24-05-10_14:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585777/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/10 14:08:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585777/content)


***

### [2024-05-10 14:06:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065150)
>Only warnings from dump close to in time.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 14:06:55)

[ LHC BLM Fixed Display 24-05-10_14:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585771/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/10 14:06:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585771/content)


***

### [2024-05-10 14:10:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065153)
>Dump looks clean.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:10:30)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-05-10_14:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585791/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:10:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585791/content)


***

### [2024-05-10 14:32:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065166)
>Switching off the RF

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/10 14:32:11)

[ LHC RF CONTROL 24-05-10_14:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585813/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/10 14:32:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585813/content)


***

### [2024-05-10 14:40:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065170)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 14:40:40)


***

### [2024-05-10 14:45:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065174)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 14:45:43)


***

### [2024-05-10 14:45:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065175)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 14:45:49)


***

### [2024-05-10 14:46:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065176)
>LHC SEQ: ramping down ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 14:46:55)


***

### [2024-05-10 14:47:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065177)
>LHC SEQ: ramp down ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/10 14:47:13)


***

### [2024-05-10 14:47:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065178)
>Ramping down the solenoid and dipole for ALICE. LHCb wanted to keep it on for now.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:47:37)

[ LHC Sequencer Execution GUI (PRO) : 12.32.0  24-05-10_14:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585825/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 14:47:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585825/content)


***

### [2024-05-10 14:53:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065180)
>Event created from ScreenShot Client.  
LHC vac 24-05-10\_14:53.png

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/05/10 14:53:31)

[LHC vac 24-05-10_14:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585829/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/05/10 14:53:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585829/content)


***

### [2024-05-10 14:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065181)
>the quench also closed valves.

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/05/10 14:53:58)

[LHC vac 24-05-10_14:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585831/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/05/10 14:53:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585831/content)


***

### [2024-05-10 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065195)
>\*Stable beams until 13.54 when we had a quench of 3 cells in sector23. Ran the QH-analysis and sent email to MP3.   
\*ALICE dipole and solenoid off, LHCb wanted to keep it on for now.  
\*RF and the remaining magnets turned off.  
\*Ran the intermediate softstart just after the dump as requested.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 15:20:53)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585844/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/10 15:20:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3585844/content)


