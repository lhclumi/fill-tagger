# FILL 10089
**start**:		 2024-09-05 19:57:26.016363525+02:00 (CERN time)

**end**:		 2024-09-05 21:31:28.008988525+02:00 (CERN time)

**duration**:	 0 days 01:34:01.992625


***

### [2024-09-05 19:57:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138064)
>LHC RUN CTRL: New FILL NUMBER set to 10089

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 19:57:26)


***

### [2024-09-05 19:58:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138066)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 19:58:00)


***

### [2024-09-05 20:02:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138070)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.38 24-09-05\_20:02.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/05 20:02:36)

[Accelerator Cockpit v0.0.38 24-09-05_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756565/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/05 20:02:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756565/content)

[Accelerator Cockpit v0.0.38 24-09-05_20:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756567/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/05 20:03:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756567/content)


***

### [2024-09-05 20:03:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138071)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:03:56)


***

### [2024-09-05 20:45:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138075)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:45:26)


***

### [2024-09-05 20:45:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138079)
>MAchine filled

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 20:45:40)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-05_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756569/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 20:45:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756569/content)

[ LHC Fast BCT v1.3.2 24-09-05_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756571/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 20:45:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756571/content)

[ LHC Fast BCT v1.3.2 24-09-05_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756573/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 20:47:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756573/content)

[ LHC Fast BCT v1.3.2 24-09-05_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756575/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 20:47:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756575/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-05_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756583/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/05 20:47:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756583/content)


***

### [2024-09-05 20:45:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138076)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:45:35)


***

### [2024-09-05 20:45:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138077)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:45:39)


***

### [2024-09-05 20:45:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138078)
>LHC Injection Complete

Number of injections actual / planned: 58 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:41:43 / 0:33:48 (123.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/05 20:45:40)


***

### [2024-09-05 20:47:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138081)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:47:29)


***

### [2024-09-05 20:47:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138083)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 20:47:29)


***

### [2024-09-05 21:08:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138087)
>RAMP

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/05 21:08:45)

[ LHC Beam Losses Lifetime Display 24-09-05_21:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756601/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/05 21:08:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756601/content)

[ LHC Fast BCT v1.3.2 24-09-05_21:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756603/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:08:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756603/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-05_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756605/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/05 21:09:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756605/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-05_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756607/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:09:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756607/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-05_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756609/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:09:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756609/content)


***

### [2024-09-05 21:08:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138088)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 21:08:46)


***

### [2024-09-05 21:09:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138090)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 21:09:00)


***

### [2024-09-05 21:09:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138092)
>Losses start of Ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:09:35)

[ LHC BLM Fixed Display 24-09-05_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756611/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:09:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756611/content)


***

### [2024-09-05 21:10:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138094)
>Global Post Mortem Event

Event Timestamp: 05/09/24 21:10:57.610
Fill Number: 10089
Accelerator / beam mode: PROTON PHYSICS / SQUEEZE
Energy: 6799320 [MeV]
Intensity B1/B2: 38277 / 38488 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 4-BLM\_UNM: B T -> F on CIB.UA83.L8.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/05 21:13:50)


***

### [2024-09-05 21:10:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138110)
>Global Post Mortem Event Confirmation

Dump Classification: UFO (Fast Beam Losses)
Operator / Comment: gtrad / Fast losses in 3R8 with UFO time profile, not caught by UFO buster


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/05 21:32:44)


***

### [2024-09-05 21:13:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138095)
>Event created from ScreenShot Client.  
LHC BLM Fixed Display 
 24-09-05\_21:13.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:13:51)

[ LHC BLM Fixed Display 24-09-05_21:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756613/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/05 21:13:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756613/content)


***

### [2024-09-05 21:14:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138096)
>Beams lost due to losses around IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:14:58)

[bic_eventseq >> Version: 3.6.2  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-09-05_21:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756615/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:14:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756615/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-09-05_21:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756617/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:15:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756617/content)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-09-05_21:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756619/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:15:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756619/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-09-05_21:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756621/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:16:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756621/content)

[ LHC Beam Losses Lifetime Display 24-09-05_21:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756623/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/05 21:18:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756623/content)

[ LHC Fast BCT v1.3.2 24-09-05_21:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756625/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:18:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756625/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-09-05_21:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756627/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/05 21:19:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756627/content)


***

### [2024-09-05 21:17:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138097)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 21:17:53)


***

### [2024-09-05 21:17:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138098)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 21:17:55)


***

### [2024-09-05 21:21:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138099)
>No UFO signlled by the UFO buster

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/09/05 21:22:00)

[ UFO Buster GUI - v7.5.0 -  INCA DISABLED - PRO CCDA - Version: 7.5.0 24-09-05_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756629/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/09/05 21:22:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3756629/content)


***

### [2024-09-05 21:31:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4138103)
>LHC RUN CTRL: New FILL NUMBER set to 10090

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/05 21:31:28)


