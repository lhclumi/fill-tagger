# FILL 9851
**start**:		 2024-07-02 16:43:42.688613525+02:00 (CERN time)

**end**:		 2024-07-03 00:53:30.623863525+02:00 (CERN time)

**duration**:	 0 days 08:09:47.935250


***

### [2024-07-02 16:43:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098853)
>LHC RUN CTRL: New FILL NUMBER set to 9851

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 16:43:43)


***

### [2024-07-02 16:46:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098854)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 16:46:29)


***

### [2024-07-02 17:17:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098879)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 17:17:36)


***

### [2024-07-02 17:25:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098891)
>I heard the announcer say this. I am not sure what it is about.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 17:25:30)

[Mozilla Firefox 24-07-02_17:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660383/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 17:25:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660383/content)


***

### [2024-07-02 17:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098892)
>Everything looks fine.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/02 17:27:01)

[ LHC RF CONTROL 24-07-02_17:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660385/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/02 17:27:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660385/content)


***

### [2024-07-02 17:28:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098894)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 17:28:11)


***

### [2024-07-02 17:28:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098895)
>
```
 B2 RF clock generator STATUS is not OK  
LHC BIG SISTER GUI     24-07-02_17:28.png
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 17:31:51)

[ LHC BIG SISTER GUI 24-07-02_17:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660393/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 17:28:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660393/content)


***

### [2024-07-02 17:36:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098899)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 17:36:14)


***

### [2024-07-02 17:36:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098900)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 17:36:30)


***

### [2024-07-02 17:37:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098902)
>Replaying the sequence to resynch the RF.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:37:47)

[ LHC Sequencer Execution GUI (PRO) : 12.32.12  24-07-02_17:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660417/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:37:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660417/content)


***

### [2024-07-02 17:40:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098905)
>I have got that the RF-loops are not stable.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:41:21)

[24-07-02_17:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660429/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:41:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660429/content)


***

### [2024-07-02 17:46:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098909)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 02-JUL-24 11.46.18.825000 PM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/02 17:46:24)


***

### [2024-07-02 17:46:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098910)
>Also issue with this Tomasz, spotted it and will reset it.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:46:41)

[ LHC CIRCUIT SUPERVISION v9.0 24-07-02_17:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660439/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 17:46:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660439/content)


***

### [2024-07-02 18:09:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098928)
>temperature of the VCXO of the synchro loop

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:10:20)

[Mozilla Firefox 24-07-02_18:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660493/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:10:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660493/content)


***

### [2024-07-02 18:22:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098937)
>I don't know why the temperature would be running away like this. I talked to TI and they don't see anything strange in terms of ventilation or temperature in SR4. I informed the piquet that we have had something similar in the past and he will contact Helga/Andy to discuss.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:24:20)

[Mozilla Firefox 24-07-02_18:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660517/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:24:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660517/content)

[Mozilla Firefox 24-07-02_18:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660519/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:24:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660519/content)


***

### [2024-07-02 18:31:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098941)
>Now TI received a pre-alarm that the temperature was getting to high but it is not easy to track the evolution in their application. It seems that the RF-zone is not tracked in the same way as the reset of the building.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 18:32:22)


***

### [2024-07-02 19:16:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098948)
>The temperature is now going down. TI said that there are two air condition unit in SR4 RF-room were both off and this is now fixed and both are on. Temperature is going down but will take a while before it is donw and stable.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 19:19:15)

[Mozilla Firefox 24-07-02_19:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660561/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 19:19:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660561/content)


***

### [2024-07-02 19:43:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098972)
>Looks like the temperature is rising again. The piquet is still there and says that everything is working fine but needed to do some restart so will leave it like this.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 19:45:53)

[Mozilla Firefox 24-07-02_19:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660645/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 19:43:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660645/content)


***

### [2024-07-02 20:23:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098985)
>They are still working with the ventilation in SR4. I tried to do the RF resynch but without success.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 20:24:18)

[Mozilla Firefox 24-07-02_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660685/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 20:24:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660685/content)


***

### [2024-07-02 21:12:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098989)
>The AC doesn't manage to cool down the room so they opened a door. Now it seems to be quicker.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:13:09)

[Mozilla Firefox 24-07-02_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660705/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:13:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660705/content)


***

### [2024-07-02 21:28:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098991)
>They got the cooling going again so now it is going down fast but I tried again and it still didn't work.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:29:09)

[Mozilla Firefox 24-07-02_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660707/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:29:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660707/content)


***

### [2024-07-02 21:40:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098994)
>Now the RF passed

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:40:15)

[Mozilla Firefox 24-07-02_21:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660709/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/02 21:40:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660709/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.12  24-07-02_21:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660711/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 21:44:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660711/content)


***

### [2024-07-02 21:41:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098995)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 21:41:34)


***

### [2024-07-02 21:41:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098996)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 21:41:41)


***

### [2024-07-02 21:44:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4098997)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 21:44:02)


***

### [2024-07-02 22:03:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099004)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 22:03:07)


***

### [2024-07-02 22:04:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099005)
>Big orbit oscillation, not surprising considering time at injection.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/02 22:04:40)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-02_22:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660733/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/02 22:04:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660733/content)

[Accelerator Cockpit v0.0.37 24-07-02_22:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660735/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/02 22:05:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660735/content)


***

### [2024-07-02 22:07:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099006)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 22:07:43)


***

### [2024-07-02 22:12:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099008)
>energy seem consistently be a bit large so will trim it down slightly.

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/02 22:12:52)

[ LHC Injection Quality Check 3.17.8 24-07-02_22:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660745/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/02 22:12:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660745/content)


***

### [2024-07-02 22:13:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099009)
>Trimmed the energy with half

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/02 22:13:51)

[Accelerator Cockpit v0.0.37 24-07-02_22:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660747/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/02 22:13:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660747/content)


***

### [2024-07-02 22:17:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099011)
>Did 2 sets of wire scans.

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/07/02 22:17:26)

[ LHC WIRESCANNER APP 24-07-02_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660753/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/07/02 22:17:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660753/content)


***

### [2024-07-02 22:18:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099012)
>scrapping situation

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/02 22:18:39)

[ LHC Injection Quality Check 3.17.8 24-07-02_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660755/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/02 22:18:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660755/content)


***

### [2024-07-02 22:39:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099016)
>Lowered the intensity in the booster.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 22:40:38)

[ LHC Fast BCT v1.3.2 24-07-02_22:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660785/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 22:40:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660785/content)


***

### [2024-07-02 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099031)
>
```
**Summary:**  
-Just dumped before I started.   
**-**During the ramp down the RCBXH2.R8 was power-cycled.   
-During the QPS reset the RQD/RQF fired. I could make a nQPS reset and after a short wait I could close the switches.  
-Got a error message that the RF phase was not OK. I tried to do a resynch but then the phase loops couldn't lock. Last time this was due to a oscillation in temperature so together with Theo we looked into it but this time it was really a temperature that had been running away. TI was not seeing anything so I called the RF-piquet that after some discussion with Andy confirmed that this really was a temperature. In the meanwhile TI got an alarm bu they don't have a good way to monitor the temperatore/ventilation in this part of SR4. The CV piquet was called but they were struggling to get everything working so it wasn't before closer to 22.00 it was possible to resynch the RF.  
-Injected the beam and took wire scans after the first 84 bunches. Realized that the intensity was more like 1.65e11 so asked to lower it from the booster. The original idea was not to touch it but since the intensity seems to have drifted we had to reduce it. SPS have wire scans for both settings (with higher intensity and the one with lower). In the LHC we only have for the higher ones.   
-Adjusted the injection energy including the transfer lines by 0.05permil. There is still an offset so might need to correct more in the next.  
-The QPS for the RCO in sector 45 was reset by Thomaz while we had issues with RF.   
 **To Note:**  
-The LHCb separation has been changed in the cycle (when going to collision) so good to document all the losses and be ready that losses might be ready going to collisions.  
-The scope on the left top corner is not working  
  
  
/Tobias  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:23:37)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660815/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:22:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660815/content)


***

### [2024-07-02 23:04:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099021)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:04:41)


***

### [2024-07-02 23:05:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099023)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:05:58)


***

### [2024-07-02 23:06:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099024)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:06:02)


***

### [2024-07-02 23:06:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099025)
>LHC Injection CompleteNumber of injections actual / planned: 54 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:58:20 / 0:33:48 (172.6 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/02 23:06:03)


***

### [2024-07-02 23:10:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099026)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:10:45)


***

### [2024-07-02 23:10:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099029)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:10:46)


***

### [2024-07-02 23:32:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099033)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:32:06)


***

### [2024-07-02 23:33:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099035)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:33:20)


***

### [2024-07-02 23:33:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099035)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:33:20)


***

### [2024-07-02 23:40:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099037)
>status 1.2m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:40:53)

[ LHC Fast BCT v1.3.2 24-07-02_23:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660816/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:40:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660816/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-02_23:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660818/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:40:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660818/content)


***

### [2024-07-02 23:41:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099038)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:41:39)


***

### [2024-07-02 23:42:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099039)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:42:32)


***

### [2024-07-02 23:43:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099041)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:43:22)


***

### [2024-07-02 23:50:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099042)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:50:58)


***

### [2024-07-02 23:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099043)
>
```
tune trims +2e-3 in B1H and +1e-3 in B2H just after the end of collisions BP
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/03 00:09:12)

[ LHC Beam Losses Lifetime Display 24-07-02_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660820/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/02 23:51:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660820/content)

[Accelerator Cockpit v0.0.37 24-07-02_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660822/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/02 23:52:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660822/content)


***

### [2024-07-02 23:56:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099044)
>
```
tried to put levelling in IP8 as soon as possible (as yesterday). Compared to yesterday's shift, we were at ~600 instead of ~400 at the end of the COLLISIONS beam process. Nothing particular on the losses.
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/03 00:11:42)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-02_23:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660824/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:57:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660824/content)


***

### [2024-07-02 23:57:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099046)
>emittance scan in IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:57:35)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-02_23:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660826/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/02 23:57:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660826/content)


***

### [2024-07-02 23:57:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099045)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:57:25)


***

### [2024-07-02 23:59:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099047)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/02 23:59:24)


***

### [2024-07-03 00:03:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099050)
>emittance scan IP1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:03:16)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-03_00:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660828/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:03:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660828/content)


***

### [2024-07-03 00:04:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099051)
>
```
tune trim B2H +1e-3 after emittance scans--> kept
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/03 00:08:54)

[Accelerator Cockpit v0.0.37 24-07-03_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660830/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/03 00:08:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660830/content)

[ LHC Beam Losses Lifetime Display 24-07-03_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660832/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/03 00:04:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660832/content)


***

### [2024-07-03 00:14:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099052)
>status at 60 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:14:25)

[ LHC Fast BCT v1.3.2 24-07-03_00:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660834/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:18:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660834/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-03_00:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660836/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:14:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660836/content)

[ LHC Beam Losses Lifetime Display 24-07-03_00:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660838/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/03 00:14:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660838/content)

[ LHC Bunch Length 24-07-03_00:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660840/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/03 00:14:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660840/content)

[ LHC Bunch Length 24-07-03_00:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660842/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/03 00:14:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660842/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-03_00:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660844/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/03 00:15:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660844/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-03_00:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660846/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:15:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660846/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-03_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660848/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:16:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660848/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-03_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660850/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:16:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660850/content)


***

### [2024-07-03 00:19:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099053)
>IP1 overshot

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:19:20)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-03_00:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660852/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:24:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660852/content)


***

### [2024-07-03 00:19:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099055)
>Global Post Mortem EventEvent Timestamp: 03/07/24 00:19:32.259Fill Number: 9851Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799560 [MeV]Intensity B1/B2: 35851 / 35940 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.US15.L1.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/03 00:22:27)


***

### [2024-07-03 00:20:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099054)
>Event created from ScreenShot Client.  
Set SECTOR81 24-07-03\_00:20.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/03 00:20:26)

[Set SECTOR81 24-07-03_00:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660854/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/03 00:20:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660854/content)


***

### [2024-07-03 00:24:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099056)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 00:24:37)


***

### [2024-07-03 00:25:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099057)
>Event created from ScreenShot Client.  
Circuit\_RQT12\_L1B1: 24-07-03\_00:26.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:27:06)

[Circuit_RQT12_L1B1:   24-07-03_00:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660856/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:27:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660856/content)

[1 - RPMBD.RR13.RQT12.L1B1 Power Converter for the Circuit RQT12.L1B1    24-07-03_00:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660858/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:28:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660858/content)


***

### [2024-07-03 00:28:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099058)
>Event created from ScreenShot Client.  
 Equip State 24-07-03\_00:28.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:28:20)

[ Equip State 24-07-03_00:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660860/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:28:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660860/content)

[ Equip State 24-07-03_00:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660862/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:28:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660862/content)


***

### [2024-07-03 00:32:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099059)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 00:32:37)


***

### [2024-07-03 00:39:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099062)
>
```
called QPS piquet and informed him it also happened on the evening of July 1st on the same power converter. On his advice, I try to reset  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:40:37)

[ LHC CIRCUIT SUPERVISION v9.0 24-07-03_00:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660864/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:40:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660864/content)


***

### [2024-07-03 00:45:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099066)
>standard reset of QPS and PC was sufficient to get back

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:45:43)

[ LHC CIRCUIT SUPERVISION v9.0 24-07-03_00:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660866/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:45:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660866/content)


***

### [2024-07-03 00:52:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099067)
>
```
sending to 200A and back to STANDBY
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:54:12)

[ LHC CIRCUIT SUPERVISION v9.0 24-07-03_00:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660868/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:52:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660868/content)

[ Equip State 24-07-03_00:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660870/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/03 00:52:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3660870/content)


***

### [2024-07-03 00:53:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099068)
>LHC RUN CTRL: New FILL NUMBER set to 9852

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 00:53:31)


