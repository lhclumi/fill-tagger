# FILL 10208
**start**:		 2024-10-07 20:44:15.186238525+02:00 (CERN time)

**end**:		 2024-10-08 06:40:42.366863525+02:00 (CERN time)

**duration**:	 0 days 09:56:27.180625


***

### [2024-10-07 20:44:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157778)
>LHC RUN CTRL: New FILL NUMBER set to 10208

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:44:16)


***

### [2024-10-07 20:44:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157784)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:44:53)


***

### [2024-10-07 20:44:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157785)
>
```
TCDQ BETS error ... there appears to be a glitch on the signal at the time of the dump.  
  
Called Yann to have a look - he is checking together with Christophe.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:37:52)

[ BETS Explorer 24-10-07_20:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807209/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 20:48:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807209/content)

[ BETS Explorer 24-10-07_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807211/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 20:51:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807211/content)

[Energy Comparison Error Details 24-10-07_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807213/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 20:51:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807213/content)

[ BETS Explorer 24-10-07_21:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807221/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:07:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807221/content)

[ BETS Explorer 24-10-07_21:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807227/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:07:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807227/content)


***

### [2024-10-07 20:47:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157786)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:47:02)


***

### [2024-10-07 20:47:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157788)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:47:12)


***

### [2024-10-07 20:48:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157789)
>|\*\*\* XPOC error has been reset by user 'mihostet' at 07.10.2024 20:48:19
| Comment: BLMES.04L6.B2I10\_MSDC.E4L6.B2 readout glitch (PM buffer corrupt)
|
| Dump info: BEAM 2 at 07.10.2024 20:38:33
| Beam info: Energy= 6799.44 GeV ; Intensity= 3.49E14 p+ ; #Bunches= 2352
| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR


creator:	 mihostet  @cs-ccr-pm3.cern.ch (2024/10/07 20:48:19)


***

### [2024-10-07 20:48:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157790)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:48:23)


***

### [2024-10-07 20:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157792)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:48:34)


***

### [2024-10-07 20:49:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157793)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:49:41)


***

### [2024-10-07 20:49:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157794)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:49:43)


***

### [2024-10-07 20:50:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157796)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:50:27)


***

### [2024-10-07 20:50:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157798)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:50:59)


***

### [2024-10-07 20:51:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157800)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:51:33)


***

### [2024-10-07 20:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157801)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:52:53)


***

### [2024-10-07 20:52:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157803)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:52:54)


***

### [2024-10-07 20:52:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157805)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:52:56)


***

### [2024-10-07 20:53:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157809)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:53:12)


***

### [2024-10-07 20:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157811)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:53:38)


***

### [2024-10-07 20:53:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157813)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:53:45)


***

### [2024-10-07 20:55:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157814)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:55:11)


***

### [2024-10-07 20:56:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157816)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 20:56:55)


***

### [2024-10-07 21:03:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157819)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 21:03:24)


***

### [2024-10-07 21:05:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157823)
>
```
Rafaello called, he received an alarm about a FEC of SMP in P6 being down ~20 min ago ... now everything looks up. Glitch?
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:26:56)

[Safe Machine Parameters in CCC : Detailed Overview 24-10-07_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807237/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 21:21:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807237/content)


***

### [2024-10-07 21:20:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157824)
>
```
Again trouble with AFP XRPs. A6R1 is on the warning limits. Opened the limits to parking at the request of Maciej so he can move it.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:44:11)

[ Equip State 24-10-07_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807239/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:22:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807239/content)

[Roman Pot Control Application (Device: XRPH.B6R1.B1.12-217-F-H) -  INCA DISABLED - PRO CCDA 24-10-07_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807241/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:21:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807241/content)

[Roman Pot Control Application (Device: XRPH.A6R1.B1.12-205-N-H) -  INCA DISABLED - PRO CCDA 24-10-07_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807243/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:21:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807243/content)

[Roman Pot Control Application (Device: XRPH.B6L1.B2.12-217-F-H) -  INCA DISABLED - PRO CCDA 24-10-07_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807245/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:21:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807245/content)


***

### [2024-10-07 21:26:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157825)
>
```
Maciej managed to recover the AFP XRPs; normal limits restored
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:26:45)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-10-07_21:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807247/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:26:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807247/content)


***

### [2024-10-07 21:27:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157827)
>
```

```
Call from CCC at 21:20 : dump from the TCDQ B2 BETS  
timber link [here](https://timber.cern.ch/query?tab=Visualization&name=&description=&extractionTimeZone=LOCAL&selectedOutput=0&timeRange%5Btype%5D=0&timeRange%5Bstart%5D=2024-08-08&timeRange%5BstartTime%5D=16:15:00&timeRange%5Bend%5D=2024-08-09&timeRange%5BendTime%5D=07:00:00&autoLoad=false&variables=90275534%252C90359946%252C85153688%252C85153678%252C85153749%252C85153731%252C85153733%252C85153788%252C85153787%252C85153768%252C85153802%252C85153821%252C90801085&timeRange%5Bbefore%5D=24&timeRange%5BbeforeUnit%5D=hours&timeRange%5Breference%5D%5Btype%5D=now&timeRange%5Breference%5D%5Boffset%5D=Now)  
  
Called C. Boucly for support and the conclusion is   
* The position of the TCDQ was always OK, as showed by the tracking error in timber  
* The BETS post-operation shows a glitch on all signals of the BETS B2
```

```
  
Called N. Voumard who is reviewing the situation : mostly likely an electronic problem. OP also informed us that a FEC was rebooted at the same time -> most likely a glitch occurred in P6.  
We agreed OP resume operation and call the piquet if it occurs again.  

```
  
Yann, Christophe and Nicolas V.
```


creator:	 ydutheil  @lxtunnel05.cern.ch (2024/10/07 22:03:34)

[screenShot_October_7th_2024_21_28_28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807253/content)
creator:	 ydutheil  @lxtunnel05.cern.ch (2024/10/07 21:28:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807253/content)

[screenShot_October_7th_2024_21_33_54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807263/content)
creator:	 ydutheil  @lxtunnel05.cern.ch (2024/10/07 21:33:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807263/content)

[ BETS Explorer 24-10-07_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807265/content)
creator:	 ydutheil  @cwe-513-abt1.cern.ch (2024/10/07 21:43:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807265/content)

[ BETS Explorer 24-10-07_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807267/content)
creator:	 ydutheil  @cwe-513-abt1.cern.ch (2024/10/07 21:43:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807267/content)

[ BETS Explorer 24-10-07_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807269/content)
creator:	 ydutheil  @cwe-513-abt1.cern.ch (2024/10/07 21:43:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807269/content)

[ BETS Explorer 24-10-07_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807271/content)
creator:	 ydutheil  @cwe-513-abt1.cern.ch (2024/10/07 21:44:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807271/content)


***

### [2024-10-07 21:52:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157830)
>
```
Yann called back, he checked together with Christophe, they agree it was a glitch (possibly a power glitch in P6 which also affected the SMP FEC). They give the green light to continue.  
  
In case the problem occurs again, we should call the ABT/Kicker piquet, who will discuss with Nicolas Voumard if the power supply needs to be exchanged.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 21:52:45)


***

### [2024-10-07 21:52:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157831)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 21:52:54)


***

### [2024-10-07 21:58:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157836)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 21:59:00)

[Accelerator Cockpit v0.0.38 24-10-07_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807295/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 21:59:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807295/content)

[Accelerator Cockpit v0.0.38 24-10-07_21:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807297/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 21:59:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807297/content)

[Accelerator Cockpit v0.0.38 24-10-07_22:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807299/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:00:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807299/content)

[Accelerator Cockpit v0.0.38 24-10-07_22:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807301/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:01:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807301/content)


***

### [2024-10-07 22:00:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157837)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:00:31)


***

### [2024-10-07 22:07:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157838)
>BWS

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:07:08)

[ LHC WIRESCANNER APP 24-10-07_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807317/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:07:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807317/content)

[ LHC WIRESCANNER APP 24-10-07_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807319/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:07:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807319/content)

[ LHC WIRESCANNER APP 24-10-07_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807321/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 22:07:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807321/content)


***

### [2024-10-07 22:37:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157845)
>
```
full ... once again.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 22:39:02)

[ LHC Fast BCT v1.3.2 24-10-07_22:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807327/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 22:37:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807327/content)

[ INJECTION SEQUENCER  v 5.1.12 24-10-07_22:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807329/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 22:37:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807329/content)


***

### [2024-10-07 22:38:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157846)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:38:15)


***

### [2024-10-07 22:38:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157847)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:38:25)


***

### [2024-10-07 22:38:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157848)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:38:34)


***

### [2024-10-07 22:38:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157849)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:38:03 / 0:33:48 (112.6 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 22:38:35)


***

### [2024-10-07 22:40:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157852)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:40:18)


***

### [2024-10-07 22:40:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157852)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:40:18)


***

### [2024-10-07 22:40:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157854)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 22:40:19)


***

### [2024-10-07 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157870)
>**\*\*\* SHIFT SUMMARY \*\*\***  
Two unplanned dumps in Stable Beams, two refills.  
  
The first fill got dumped after ~3h in SB due to a trip of RF line 3B2; HOM coupler temperature too high. Checked with RF piquet (Bartek), this is not the first time the issue is observed on line 3B2, which is still to be understood. He recommended to leave the line off during the ramp-down to allow for the coupler to cool down, then resume operation.  
  
The second fill got dumped just before 3h in SB on a BETS-TCDQ B2 interlock. After checking with Yann, Christophe and Nicolas V., this appears to be a spurious interlock most likely triggered by an electronic glitch in the BETS-TCDQ. About at the same time, Raffaeleo called as he got alarms on a failed SMP frontend in P6, which recovered on its own after. Possibly there was a small power cut or glitch in P6, although nothing was seen by TI. In case of another dump due to BETS-TCDQ, the ABT piquet should be called, and possibly the power supply of the BETS-TCDQ will need to be replaced (access in UA63).  
  
Refilling was smooth, although intensities were a little lower than in the fills before (~1.62e11 at injection).  
  
Notes:  
- Issue with the AFP XRPs which not return home properly; after the first dump, the PXIs in USA15 (ATLAS non-interlocked zone) needed to be rebooted. After the second dump, Maciej from AFP tried to move them manually with the override key in, but the limits set at injection appear to be a bit tight preventing the movement for one station (A6R1), possibly they could be relaxed a bit.  
- RCBCH8.L8B2 tripped during both rampdowns (and already during the ramp down in the previous shift). This is a 80-120A PC interlocked in PIC maskable ... to be observed.  
  
-- Michi --  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/07 23:20:18)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807349/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/07 23:20:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807349/content)


***

### [2024-10-07 23:01:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157858)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:01:37)


***

### [2024-10-07 23:01:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157860)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:01:45)


***

### [2024-10-07 23:10:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157862)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:10:09)


***

### [2024-10-07 23:10:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157863)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:10:46)


***

### [2024-10-07 23:11:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157865)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:11:43)


***

### [2024-10-07 23:12:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157866)
>Loss at beginning of adjust (before any real collisions). Not detected as 
 a ufo

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/07 23:13:36)

[ LHC BLM Fixed Display 24-10-07_23:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807341/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/07 23:13:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807341/content)


***

### [2024-10-07 23:14:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157867)
>losses going into collisins.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 23:14:51)

[ LHC Beam Losses Lifetime Display 24-10-07_23:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807343/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 23:14:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807343/content)


***

### [2024-10-07 23:15:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157868)
>optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:15:50)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_23:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807345/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:15:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807345/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_23:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807347/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:16:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807347/content)


***

### [2024-10-07 23:17:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157869)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:17:42)


***

### [2024-10-07 23:24:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157874)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:24:01)


***

### [2024-10-07 23:26:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157879)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:26:06)


***

### [2024-10-07 23:28:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157881)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 23:28:48)


***

### [2024-10-07 23:29:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157882)
>emittance scan inip1/5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:29:56)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_23:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807352/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:29:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807352/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_23:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807354/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:30:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807354/content)


***

### [2024-10-07 23:40:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157884)
>All IPs on target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:40:40)

[LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_23:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807356/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 23:40:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807356/content)


***

### [2024-10-08 06:36:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157920)
>Global Post Mortem Event

Event Timestamp: 08/10/24 06:36:31.240
Fill Number: 10208
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 28882 / 28816 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/08 06:39:21)


***

### [2024-10-08 06:36:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157970)
>Global Post Mortem Event Confirmation

Dump Classification: RF fault
Operator / Comment: tpersson / Trip of the the M2B2. 


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/08 07:05:49)


***

### [2024-10-08 06:37:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157916)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/08 06:37:41)


***

### [2024-10-08 06:37:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157917)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/08 06:37:43)


***

### [2024-10-08 06:37:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157918)
>The RF tripped, beam 2 module 2.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:38:15)

[ LHC RF CONTROL 24-10-08_06:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807461/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:38:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807461/content)


***

### [2024-10-08 06:39:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157921)
>Error I see.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:39:21)

[BEAM DUMP INTERLOCK B2 24-10-08_06:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807463/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:39:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807463/content)


***

### [2024-10-08 06:39:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157922)
>I will see if we can restart it.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:39:56)

[RF INTERLOCKS 24-10-08_06:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807465/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/08 06:39:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3807465/content)


***

### [2024-10-08 06:40:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157923)
>LHC RUN CTRL: New FILL NUMBER set to 10209

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/08 06:40:43)


