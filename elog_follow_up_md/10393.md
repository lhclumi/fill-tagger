# FILL 10393
**start**:		 2024-11-20 08:40:46.748238525+01:00 (CERN time)

**end**:		 2024-11-20 16:08:00.365988525+01:00 (CERN time)

**duration**:	 0 days 07:27:13.617750


***

### [2024-11-20 08:40:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187671)
>LHC RUN CTRL: New FILL NUMBER set to 10393

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/20 08:40:48)


***

### [2024-11-20 09:19:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187693)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 20-NOV-24 04.19.46.952000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/20 09:19:51)


***

### [2024-11-20 09:53:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187711)
>Power Converter Faults  
  
Device: RPTK.SR4.RA43U2.L4 (RPTK.SR4.RA43U2.L4)  
Time: 2024-11-20 09:49:59,273  
Faults: NO\_PC\_PERMIT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736839:6543055?archive=1736839:6543058?archive=1736839:6543060?archive=1736839:6543065?archive=1736839:6543068?archive=1736839:6543072)  
  
  
Device: RPTK.SR4.RA43U3.L4 (RPTK.SR4.RA43U3.L4)  
Time: 2024-11-20 09:49:59,273  
Faults: NO\_PC\_PERMIT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736837:6543054?archive=1736837:6543059?archive=1736837:6543063?archive=1736837:6543064?archive=1736837:6543069?archive=1736837:6543073)  
  
  
Device: RPTK.SR4.RA47U3.R4 (RPTK.SR4.RA47U3.R4)  
Time: 2024-11-20 09:49:59,273  
Faults: NO\_PC\_PERMIT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736838:6543053?archive=1736838:6543057?archive=1736838:6543061?archive=1736838:6543067?archive=1736838:6543071?archive=1736838:6543074)  
  
  
Device: RPTK.SR4.RA47U2.R4 (RPTK.SR4.RA47U2.R4)  
Time: 2024-11-20 09:49:59,273  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736836:6543052?archive=1736836:6543056?archive=1736836:6543062?archive=1736836:6543066?archive=1736836:6543070?archive=1736836:6543075)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/20 09:53:29)


***

### [2024-11-20 13:59:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187816)
>cloned MD6 hyper cycle for negative polarity

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/20 14:00:03)

[ Generation Application connected to server LHC (PRO database) 24-11-20_13:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871835/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/20 14:00:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871835/content)


***

### [2024-11-20 14:17:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187825)
>RQF/D and RB 78 at 6kA

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:17:20)

[Circuit_RQF_A78:   24-11-20_14:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871863/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:17:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871863/content)

[Circuit_RB_A78:   24-11-20_14:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871865/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:17:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871865/content)

[Circuit_RQD_A78:   24-11-20_14:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871867/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:17:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871867/content)


***

### [2024-11-20 14:21:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187826)
>Tested RQF/D, RB 78 with QPS snapshots and triggered a FPA on RQD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:21:42)

[ Timber ¿ Mozilla Firefox 24-11-20_14:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871869/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 14:21:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3871869/content)


***

### [2024-11-20 14:22:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187827)
>Power Converter Faults  
  
Device: RPHE.UA83.RQD.A78 (RPHE.UA83.RQD.A78)  
Time: 2024-11-20 14:18:33,303  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736864:6543457?archive=1736864:6543460?archive=1736864:6543463?archive=1736864:6543468?archive=1736864:6543470?archive=1736864:6543473)  
  
  
Device: RPTE.UA83.RB.A78 (RPTE.UA83.RB.A78)  
Time: 2024-11-20 14:18:33,318  
Faults: NO\_PC\_PERMIT VS\_STATE VS\_FAULT FAST\_ABORT  
VS faults: EARTH FAULT (FROM EXTERNAL BOX), EXTERNAL I\_EARTH OVER CURRENT, MCB FAULT, MCB, FREE WHEEL THYRISTOR ON  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736866:6543459?archive=1736866:6543462?archive=1736866:6543465?archive=1736866:6543466?archive=1736866:6543471?archive=1736866:6543474)  
  
  
Device: RPHE.UA83.RQF.A78 (RPHE.UA83.RQF.A78)  
Time: 2024-11-20 14:18:33,303  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736865:6543458?archive=1736865:6543461?archive=1736865:6543464?archive=1736865:6543467?archive=1736865:6543469?archive=1736865:6543472)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/20 14:22:02)


***

### [2024-11-20 14:50:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187839)
>JENS STECKERT(JSTECKER) assigned RBAC Role: QPS-Piquet and will expire on: 20-NOV-24 09.50.55.736000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/20 14:50:57)


***

### [2024-11-20 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187873)
>Access shift during the cryo recovery.  
At the moment, only ATLAS is in the machine.  
  
Did snapshot tests for S78 main circuits.  
I was called by QPS expert saying that it'll need to be re-done in the afternoon.  
  
AC  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/11/20 15:32:20)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872068/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/11/20 15:32:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872068/content)


***

### [2024-11-20 15:47:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187888)
>
```
mains in 78 at 6 kA informing QPS team
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 16:05:56)

[Monitoring application. Currently monitoring : LHC - [3 subscriptions ] 24-11-20_15:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872180/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 15:47:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872180/content)


***

### [2024-11-20 15:50:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187891)
>
```
FPA sent by QPS team
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 16:05:46)

[Monitoring application. Currently monitoring : LHC - [3 subscriptions ] 24-11-20_15:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872202/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/20 15:50:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3872202/content)


***

### [2024-11-20 15:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187893)
>Power Converter Faults  
  
Device: RPHE.UA83.RQD.A78 (RPHE.UA83.RQD.A78)  
Time: 2024-11-20 15:49:22,158  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736872:6543511?archive=1736872:6543514?archive=1736872:6543519?archive=1736872:6543520?archive=1736872:6543525?archive=1736872:6543527)  
  
  
Device: RPHE.UA83.RQF.A78 (RPHE.UA83.RQF.A78)  
Time: 2024-11-20 15:49:22,158  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736873:6543512?archive=1736873:6543515?archive=1736873:6543517?archive=1736873:6543521?archive=1736873:6543523?archive=1736873:6543526)  
  
  
Device: RPTE.UA83.RB.A78 (RPTE.UA83.RB.A78)  
Time: 2024-11-20 15:49:22,168  
Faults: NO\_PC\_PERMIT VS\_FAULT FAST\_ABORT  
VS faults: EARTH FAULT (FROM EXTERNAL BOX), EXTERNAL I\_EARTH OVER CURRENT, MCB FAULT, MCB, FREE WHEEL THYRISTOR ON  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736874:6543513?archive=1736874:6543516?archive=1736874:6543518?archive=1736874:6543522?archive=1736874:6543524?archive=1736874:6543528)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/20 15:52:51)


***

### [2024-11-20 16:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187903)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/20 16:07:59)


***

### [2024-11-20 16:08:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4187904)
>LHC RUN CTRL: New FILL NUMBER set to 10394

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/20 16:08:02)


