# FILL 10172
**start**:		 2024-09-28 23:01:52.535863525+02:00 (CERN time)

**end**:		 2024-09-29 07:58:01.338988525+02:00 (CERN time)

**duration**:	 0 days 08:56:08.803125


***

### [2024-09-28 23:02:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152216)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:02:45)


***

### [2024-09-28 23:04:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152217)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:04:50)


***

### [2024-09-28 23:06:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152248)
>
```
BTF / AC-KFC MD summary  
  
We spent some time at injection to setup the BTF and the ADT-AC dipole synchronised with the head-tail monitor. One field from the ObsBox buffer FESA class could not be read by the UCAP node performing the post processing for the BTF due to an RBAC issue. To solve it the role OP-deamon was given the right to get/set the BTF device/property group and the tables needed to be reloaded by Daniel on the server. The last time the BTF was used (~June), this RBAC role was not necessary, it is not clear why this has changed.  
The excitation in B2V by mDSPU1 did not work but the cause could not be found in time, the MD was done with the other beams/planes only.  
  
We then injected the full beam and went through the operational cycle until collision taking several measurements with both the BTF and the ADT-AC-dipole+HT monitor on individual bunches varying chromaticity, damper gain and octupoles at flat top, end of squeeze and end of adjust.   
  
The issue with the jamming of the ObsBox observed with many bunches in past MDs was clearly resolved, as many BTF could be taken with no issue on the ObsBox side. There were no losses and no significant emittance growth for the bunches kicked for the BTF showing that the method is safe for operation with full beam. The data quality is good, the fit methods needs to be reviewed.  
  
A large amount of data was gather with the ADT-AC-Dipole+HT, to be further analysed.  
  
There were instabilities in B1H during a measurement at fixed chromaticity for a long time too close to the instability threshold. These are not linked to the kicks (not the same bunches).  
  
  

```


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/28 23:25:58)


***

### [2024-09-28 23:06:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152219)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:06:20)


***

### [2024-09-28 23:06:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152220)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:06:21)


***

### [2024-09-28 23:07:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152223)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:07:35)


***

### [2024-09-28 23:08:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152226)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:08:47)


***

### [2024-09-28 23:09:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152227)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:09:20)


***

### [2024-09-28 23:11:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152229)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:11:31)


***

### [2024-09-28 23:11:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152231)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:11:38)


***

### [2024-09-28 23:12:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152232)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:12:26)


***

### [2024-09-28 23:14:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152234)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:14:04)


***

### [2024-09-28 23:15:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152235)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:15:33)


***

### [2024-09-28 23:16:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152237)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:16:08)


***

### [2024-09-28 23:17:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152238)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:17:58)


***

### [2024-09-28 23:17:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152240)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:17:58)


***

### [2024-09-28 23:18:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152242)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:18:00)


***

### [2024-09-28 23:18:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152246)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:18:15)


***

### [2024-09-28 23:27:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152251)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:27:30)


***

### [2024-09-28 23:29:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152252)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:29:15)


***

### [2024-09-28 23:29:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152253)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:29:33)


***

### [2024-09-28 23:36:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152255)
>All sector manually to ideal since the computer crashed and needed to be 
 rebot.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 23:37:10)

[ Equip State 24-09-28_23:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790968/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 23:36:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790968/content)


***

### [2024-09-28 23:37:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152256)
>Masking AC-dipoel, BLM etc.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 23:37:50)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-09-28_23:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790970/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 23:37:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790970/content)


***

### [2024-09-28 23:40:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152258)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 23:40:55)


***

### [2024-09-29 00:11:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152281)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:11:15)


***

### [2024-09-29 00:11:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152282)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:11:46)


***

### [2024-09-29 00:11:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152283)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:11:54)


***

### [2024-09-29 00:13:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152290)
>It complains about the contniunity of the chromaticity but it looks fine 
 to me.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:14:04)

[24-09-29_00:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791005/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:14:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791005/content)


***

### [2024-09-29 00:15:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152291)
>Also issue with the energy to incorporate.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:15:28)

[24-09-29_00:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791009/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:15:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791009/content)


***

### [2024-09-29 00:16:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152293)
>Special equastion safe beam flag.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/29 00:16:57)

[Safe Machine Parameters in CCC : Overview GUI 24-09-29_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791011/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/29 00:16:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791011/content)


***

### [2024-09-29 00:17:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152294)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:17:25)


***

### [2024-09-29 00:17:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152296)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:17:26)


***

### [2024-09-29 00:38:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152300)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 00:38:43)


***

### [2024-09-29 00:56:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152303)
>Trimming out crossing angles.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:56:59)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_00:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791027/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 00:57:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791027/content)


***

### [2024-09-29 01:01:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152304)
>Trimming out the crossing angles.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:01:26)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_01:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791029/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:01:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791029/content)


***

### [2024-09-29 01:08:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152307)
>Crossing angles are out.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:08:10)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_01:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791031/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:08:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791031/content)


***

### [2024-09-29 01:10:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152308)
>Problem trimming it in.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:10:27)

[Error 24-09-29_01:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791033/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:10:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791033/content)


***

### [2024-09-29 01:21:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152310)
>Trimming in the arc81 and arc45 knob

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:22:08)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_01:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791035/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:22:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791035/content)


***

### [2024-09-29 01:27:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152314)
>All the orbits knobs should be in.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:27:49)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_01:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791039/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 01:27:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791039/content)


***

### [2024-09-29 02:21:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152334)
>Trimming the global to 0.6

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 02:22:01)

[ LSA Applications Suite (v 16.6.29) 24-09-29_02:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791048/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 02:22:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791048/content)


***

### [2024-09-29 03:08:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152357)
>This is the one to reboot to get back nominal DOROS settings.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:09:18)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-09-29_03:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791068/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:09:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791068/content)


***

### [2024-09-29 03:11:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152359)
>Could not create knob due to problem of Imax in RQTL9.L7B1. Trying to find 
 a new corrections.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:12:25)

[Error 24-09-29_03:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791072/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:12:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791072/content)


***

### [2024-09-29 03:21:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152360)
>new b1 is in.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:21:28)

[ LSA Applications Suite (v 16.6.29) 24-09-29_03:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791078/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:21:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791078/content)


***

### [2024-09-29 03:32:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152363)
>Event created from ScreenShot Client.  
 LHC Multiturn 5.5.2 24-09-29\_03:32.png

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/29 03:32:07)

[ LHC Multiturn 5.5.2 24-09-29_03:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791085/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/29 03:32:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791085/content)


***

### [2024-09-29 03:41:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152365)
>Trimming out the global corrections.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:41:18)

[ LSA Applications Suite (v 16.6.29) 24-09-29_03:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791091/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:41:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791091/content)


***

### [2024-09-29 03:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152366)
>Event created from ScreenShot Client.  
29 - Thunar 24-09-29\_03:42.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:42:27)

[29 - Thunar 24-09-29_03:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791093/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:42:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791093/content)


***

### [2024-09-29 03:42:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152368)
>Event created from ScreenShot Client.  
29 - Thunar 24-09-29\_03:42.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:42:54)

[29 - Thunar 24-09-29_03:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791095/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:42:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791095/content)


***

### [2024-09-29 03:43:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152369)
>Event created from ScreenShot Client.  
29 - Thunar 24-09-29\_03:43.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:43:19)

[29 - Thunar 24-09-29_03:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791097/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/09/29 03:43:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791097/content)


***

### [2024-09-29 03:53:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152371)
>Trimming out the strange knobs.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:53:13)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_03:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791107/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:53:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791107/content)


***

### [2024-09-29 03:55:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152372)
>Trimming the function to 0.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:55:27)

[ LSA Applications Suite (v 16.6.29) 24-09-29_03:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791109/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:55:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791109/content)


***

### [2024-09-29 03:55:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152372)
>Trimming the function to 0.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:55:27)

[ LSA Applications Suite (v 16.6.29) 24-09-29_03:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791109/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 03:55:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791109/content)


***

### [2024-09-29 04:10:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152373)
>It is only beam 2 that has the missmatch for the trim ones.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:10:56)

[24-09-29_04:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791111/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:10:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791111/content)


***

### [2024-09-29 04:25:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152374)
>Issues with FB not getting the correct functions.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:26:06)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-29_04:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791113/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:26:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791113/content)


***

### [2024-09-29 04:27:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152375)
>I get an error trying to arm the feedback..

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:27:20)

[24-09-29_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791115/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:27:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791115/content)


***

### [2024-09-29 04:33:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152378)
>Issue with the optics ID not being the same for the orbit feedback.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/29 04:33:51)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-29_04:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791121/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/29 04:33:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791121/content)


***

### [2024-09-29 04:50:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152382)
>RTQX2.R8 out of limit

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:51:19)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-29_04:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791130/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 04:51:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791130/content)


***

### [2024-09-29 04:54:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152383)
>Correcting Chroma.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:54:19)

[Accelerator Cockpit v0.0.38 24-09-29_04:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791132/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:54:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791132/content)


***

### [2024-09-29 04:54:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152384)
>Down to 3

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:54:38)

[Accelerator Cockpit v0.0.38 24-09-29_04:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791134/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:54:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791134/content)


***

### [2024-09-29 04:55:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152386)
>Huge coupling.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:56:03)

[Accelerator Cockpit v0.0.38 24-09-29_04:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791137/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:56:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791137/content)


***

### [2024-09-29 04:57:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152387)
>Very large couplin.g

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/29 04:57:14)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-29_04:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791139/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/29 04:57:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791139/content)

[Accelerator Cockpit v0.0.38 24-09-29_04:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791141/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/29 04:57:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791141/content)


***

### [2024-09-29 05:05:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152388)
>Orbit knobs are in again.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:05:28)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-09-29_05:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791143/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:05:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791143/content)


***

### [2024-09-29 05:07:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152391)
>RTQX2.R8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:08:27)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-29_05:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791145/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:28:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791145/content)


***

### [2024-09-29 05:28:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152394)
>
```
 **This is the limit which we want to update**. It was already updated on 21 of 
    August to 4800 by Manuel at 12. See link:  
<https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4129536>  
  




| RPHGC.UA83.RTQX2.L8 | (float:1) -> 4700.0 |
| --- | --- |
| RPHGC.UA87.RTQX2.R8 | (float:1) -> 4700.0 |



  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:32:38)

[ Equip State 24-09-29_05:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791147/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:29:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791147/content)


***

### [2024-09-29 05:36:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152395)
>I called the EPC piquet to see if he can have a look and increase this 
 value.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:36:47)

[ Equip State 24-09-29_05:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791149/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 05:36:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791149/content)


***

### [2024-09-29 05:48:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152398)
>IVAN BLAZEVIC(BLAZEVIC) assigned RBAC Role: PO-LHC-Piquet


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/09/29 05:48:41)


***

### [2024-09-29 05:49:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152399)
>IVAN BLAZEVIC(BLAZEVIC) assigned RBAC Role: PO-LHC-Piquet and will expire on: 29-SEP-24 11.49.27.483000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/09/29 05:49:30)


***

### [2024-09-29 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152228)
>
```
**SHIFT SUMMARY:**  
  
I arrived at the start of the MD13463: chromaticity measurement in physics conditions...  
After solving an issue with the RBAC for obsbox2, and despite the excitation of Adt module 1 VB2 was not working, the MD went smoothly.  
  
Leaving the machine in the preparation for the next MD.  
  
Delphine  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 23:10:04)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790964/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 23:10:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790964/content)


***

### [2024-09-29 07:27:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152427)
>Incorporation of corrections.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:28:01)

[ LSA Applications Suite (v 16.6.29) 24-09-29_07:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791228/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:28:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791228/content)

[ LSA Applications Suite (v 16.6.29) 24-09-29_07:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791232/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:30:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791232/content)

[ LSA Applications Suite (v 16.6.29) 24-09-29_07:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791234/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:34:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791234/content)


***

### [2024-09-29 07:29:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152428)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.6.29) 24-09-29\_07:29.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:29:50)

[ LSA Applications Suite (v 16.6.29) 24-09-29_07:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791230/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:29:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791230/content)


***

### [2024-09-29 07:32:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152429)
>
```
**Summary - MD12723***Present:* Felix C., Kyriakos, Riccardo, Joschua, Yannis, Rogelio, Javier (and Manuel from Doros)  
*EIC:* Tobias  
  
First squeeze step to 84cm with ATS factor of 3 done. Corrected optics using new arc bumps in arc45 and arc81 in Beam 1, and global corrections in Beam 1 and Beam 2. Good corrections with beta-beating below 20%.   


LHCBEAM/HLMD_2024-09-29_B1_global_ATSF3_New
LHCBEAM/HLMD_2024-09-29_B2_global_ATSF3_New
LHCBEAM1/ATS_2024-09-29_B1_Arc45   (setting -0.6)  
LHCBEAM1/ATS_2024-09-29_B1_Arc81   (setting -1.0)  


  
Squeezed further down to 43cm with ATS factor of 6. Further squeeze to final point was limited by rtqx2.l/r8 max current because limits were reverted after MD3. Could not resolve this, as it requires a restart. Corrected optics at 43cm ATS6 with global corrections. Corrections were mostly successful in Beam 1 reaching slightly above 20% in a few points in the arcs. However Beam 2 still has close to 40% in arc45, while well corrected in all the other arcs. Global corrections cannot address this easily, and a new arc orbit bump is required for Beam 2. 

LHCBEAM/HLMD_2024-09-29_B1_global_ATSF6
LHCBEAM/HLMD_2024-09-29_B2_global_43cm_ATSF6_2nd_try


  
Attempted online calculation of a orbit bump in arc45 of Beam 2 based on Beam 1 bump, but results did not improve optics. Offline analysis needed.  
  
All knobs have been incorporated in the cycle.  
  

```


creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/09/29 07:43:21)


***

### [2024-09-29 07:38:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152430)
>These ones should be at 0 at end of ramp but at these values at 283.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:40:17)

[ LSA Applications Suite (v 16.6.29) 24-09-29_07:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791236/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/29 07:40:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3791236/content)


***

### [2024-09-29 07:52:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152433)
>Global Post Mortem Event

Event Timestamp: 29/09/24 07:52:12.026
Fill Number: 10172
Accelerator / beam mode: MACHINE DEVELOPMENT / FLAT TOP
Energy: 6799440 [MeV]
Intensity B1/B2: 0 / 1 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/29 07:55:01)


***

### [2024-09-29 07:52:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152431)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 07:52:27)


***

### [2024-09-29 07:52:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152432)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 07:52:29)


***

### [2024-09-29 07:58:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4152434)
>LHC RUN CTRL: New FILL NUMBER set to 10173

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/29 07:58:02)


