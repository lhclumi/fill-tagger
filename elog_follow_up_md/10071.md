# FILL 10071
**start**:		 2024-08-30 07:22:24.200238525+02:00 (CERN time)

**end**:		 2024-08-30 07:26:52.902863525+02:00 (CERN time)

**duration**:	 0 days 00:04:28.702625


***

### [2024-08-30 07:22:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134000)
>LHC RUN CTRL: New FILL NUMBER set to 10071

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:22:24)


***

### [2024-08-30 07:23:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134006)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:23:06)


***

### [2024-08-30 07:25:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134007)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:25:05)


***

### [2024-08-30 07:25:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134009)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:25:36)


***

### [2024-08-30 07:26:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134010)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:26:30)


***

### [2024-08-30 07:26:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134012)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:26:40)


***

### [2024-08-30 07:26:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4134013)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/30 07:26:51)


