# FILL 10079
**start**:		 2024-09-02 21:28:15.410488525+02:00 (CERN time)

**end**:		 2024-09-02 22:40:09.580988525+02:00 (CERN time)

**duration**:	 0 days 01:11:54.170500


***

### [2024-09-02 21:28:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136277)
>LHC RUN CTRL: New FILL NUMBER set to 10079

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/02 21:28:16)


***

### [2024-09-02 21:31:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136278)
>
```
several power converters failed when going to standby
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:32:40)

[Set SECTOR34 24-09-02_21:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752222/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/02 21:31:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752222/content)

[Set SECTOR56 24-09-02_21:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752224/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/02 21:31:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752224/content)

[Set SECTOR67 24-09-02_21:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752226/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/02 21:31:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752226/content)


***

### [2024-09-02 21:37:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136280)
>
```
sector 34: one corrector and one wire PC in fault off
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:40:54)

[ Equip State 24-09-02_21:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752228/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:38:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752228/content)

[ Equip State 24-09-02_21:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752230/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:38:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752230/content)

[ Equip State 24-09-02_21:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752232/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:40:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752232/content)

[ Equip State 24-09-02_21:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752234/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:38:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752234/content)


***

### [2024-09-02 21:40:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136281)
>
```
corrector and wire ok after reset
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:53:24)

[ Equip State 24-09-02_21:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752236/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:41:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752236/content)

[ Equip State 24-09-02_21:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752250/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 22:03:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752250/content)


***

### [2024-09-02 21:42:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136282)
>
```
RCD.A67B2
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 22:04:05)

[ Equip State 24-09-02_21:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752238/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:43:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752238/content)

[ Equip State 24-09-02_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752240/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:44:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752240/content)


***

### [2024-09-02 21:44:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136283)
>
```
OK after reset
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:45:04)

[Circuit_RCD_A67B2:   24-09-02_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752242/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:44:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752242/content)


***

### [2024-09-02 21:46:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136284)
>
```
RQ5
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 22:04:15)

[ Equip State 24-09-02_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752244/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:46:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752244/content)

[ Equip State 24-09-02_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752246/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:46:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752246/content)


***

### [2024-09-02 21:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136285)
>
```
RQ5 fault could be reset, but tripped again when going to standby with the same faults. EPC piquet called
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 22:03:47)

[Circuit_RQ5_L6:   24-09-02_21:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752248/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/02 21:49:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3752248/content)


***

### [2024-09-02 22:07:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4136291)
>ARAMIS SCHWANKA TREVISAN(ASCHWANK) assigned RBAC Role: PO-LHC-Piquet and will expire on: 03-SEP-24 12.07.51.336000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/09/02 22:07:52)


