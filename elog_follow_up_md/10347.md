# FILL 10347
**start**:		 2024-11-10 15:49:56.095363600+01:00 (CERN time)

**end**:		 2024-11-10 18:42:28.719613525+01:00 (CERN time)

**duration**:	 0 days 02:52:32.624249925


***

### [2024-11-10 15:49:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181469)
>LHC RUN CTRL: New FILL NUMBER set to 10347

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:49:57)


***

### [2024-11-10 15:50:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181475)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:50:34)


***

### [2024-11-10 15:51:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181476)
>All QPS look OK this time!

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 15:51:30)

[ LHC CIRCUIT SUPERVISION v9.0 24-11-10_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859592/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 15:51:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859592/content)

[ LHC SIS GUI 24-11-10_16:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859612/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/10 16:12:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859612/content)


***

### [2024-11-10 15:52:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181478)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:52:36)


***

### [2024-11-10 15:53:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181480)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:53:53)


***

### [2024-11-10 15:53:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181481)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:53:58)


***

### [2024-11-10 15:54:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181483)
>
```
IP2 co-linearity knob to -5.5 in the collision BP
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 15:55:08)

[ LSA Applications Suite (v 16.6.36) 24-11-10_15:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859594/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 15:54:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859594/content)


***

### [2024-11-10 15:55:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181484)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:55:23)


***

### [2024-11-10 15:56:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181486)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:56:01)


***

### [2024-11-10 15:56:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181488)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:56:13)


***

### [2024-11-10 15:57:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181490)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:57:32)


***

### [2024-11-10 15:58:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181493)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:58:27)


***

### [2024-11-10 15:58:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181491)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:58:26)


***

### [2024-11-10 15:58:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181495)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:58:29)


***

### [2024-11-10 15:58:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181500)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:58:43)


***

### [2024-11-10 15:58:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181501)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:58:44)


***

### [2024-11-10 15:59:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181503)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:59:19)


***

### [2024-11-10 15:59:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181505)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 15:59:26)


***

### [2024-11-10 16:00:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181506)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:00:22)


***

### [2024-11-10 16:00:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181509)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:00:43)


***

### [2024-11-10 16:01:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181511)
>Crystal evolution

creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/10 16:01:16)

[Monitor Real Time Trim Goniometers 24-11-10_16:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859600/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/10 16:01:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859600/content)


***

### [2024-11-10 16:02:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181512)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:02:27)


***

### [2024-11-10 16:04:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181514)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:04:24)


***

### [2024-11-10 16:04:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181514)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:04:24)


***

### [2024-11-10 16:17:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181515)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:17:38)


***

### [2024-11-10 16:25:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181518)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 16:26:00)

[ Accelerator Cockpit v0.0.42 24-11-10_16:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859622/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 16:26:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859622/content)

[ Accelerator Cockpit v0.0.42 24-11-10_16:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859624/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 16:26:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859624/content)

[ Accelerator Cockpit v0.0.42 24-11-10_16:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859626/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 16:27:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859626/content)


***

### [2024-11-10 16:27:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181520)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 16:27:06)


***

### [2024-11-10 16:28:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181521)
>
```
BLM pattern with pilots - do we have some residual radiation in cell 11 L/R 1 ?
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 16:29:29)

[ LHC BLM Fixed Display 24-11-10_16:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859628/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/10 16:28:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859628/content)

[ LHC BLM Fixed Display 24-11-10_16:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859630/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/10 16:28:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859630/content)


***

### [2024-11-10 16:36:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181523)
>
```
Inconsistent Fast-IC result for empty injection of the second train due to BQM seeing the probe. There is an issue in the LEIR giving empty shots...

Masked for the moment.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 16:38:41)

[Fast IC Analysis BEAM1 - 16:35:18 24-11-10_16:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859632/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 16:37:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859632/content)

[Fast IC Analysis BEAM1 - 16:37:38 24-11-10_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859634/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 16:38:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859634/content)


***

### [2024-11-10 16:58:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181528)
>
```
starting automatic channeling optimization - without blow-up as suggested by Daniele
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 17:10:06)

[LHC Crystals Cockpit 24-11-10_16:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859648/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 16:58:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859648/content)


***

### [2024-11-10 18:02:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181537)
>Machine full!  

```
  
Quite a few shots missed on rephasing errors in SPS - to be investigated.  
  
Losses during injection always around ~30%/dump on short RS.  
We should probably correct the energy error of ~0.1 permil...  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:18:06)

[ LHC Fast BCT v1.3.2 24-11-10_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859660/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:03:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859660/content)

[ INJECTION SEQUENCER  v 5.1.12 24-11-10_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859662/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:02:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859662/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-10_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859664/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/10 18:03:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859664/content)

[SPS Beam Quality Monitor - SPS.USER.LHCION1 - SPS PRO INCA server - PRO CCDA 24-11-10_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859666/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/10 18:03:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859666/content)

[ LHC Beam Losses Lifetime Display 24-11-10_18:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859672/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/10 18:06:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859672/content)

[ Accelerator Cockpit v0.0.42 24-11-10_18:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859679/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/10 18:08:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859679/content)


***

### [2024-11-10 18:03:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181538)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:03:09)


***

### [2024-11-10 18:03:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181539)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:03:20)


***

### [2024-11-10 18:03:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181540)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:03:26)


***

### [2024-11-10 18:03:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181541)
>LHC Injection Complete

Number of injections actual / planned: 75 / 46
SPS SuperCycle length: 69.6 [s]
Actual / minimum time: 1:36:19 / 0:58:21 (165.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/10 18:03:26)


***

### [2024-11-10 18:05:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181542)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:05:22)


***

### [2024-11-10 18:05:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181544)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:05:24)


***

### [2024-11-10 18:05:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181547)
>
```
max losses at start of ramp: ~37%/dump in 8R3
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:07:28)

[ LHC BLM Fixed Display 24-11-10_18:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859668/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/10 18:05:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859668/content)

[ LHC Beam Losses Lifetime Display 24-11-10_18:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859670/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/10 18:06:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859670/content)


***

### [2024-11-10 18:26:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181553)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:26:41)


***

### [2024-11-10 18:26:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181555)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:26:51)


***

### [2024-11-10 18:34:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181557)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:34:39)


***

### [2024-11-10 18:35:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181558)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:35:16)


***

### [2024-11-10 18:36:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181562)
>Global Post Mortem Event

Event Timestamp: 10/11/24 18:36:01.771
Fill Number: 10347
Accelerator / beam mode: ION PHYSICS / ADJUST
Energy: 6799200 [MeV]
Intensity B1/B2: 1864 / 1830 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 4-BLM\_UNM: B T -> F on CIB.SR7.S7.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/10 18:38:51)


***

### [2024-11-10 18:36:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181604)
>Global Post Mortem Event Confirmation

Dump Classification: Transv. beam instability
Operator / Comment: mihostet / instability B2H


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/10 19:03:05)


***

### [2024-11-10 18:36:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181560)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:36:04)


***

### [2024-11-10 18:36:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181561)
>
```
B2H instability, rise time some ~10 seconds. No evident 10 Hz structure.  
  
ADT was OFF (as not turned on manually at FT) ... will add a task to turn it on at FT - there is still enough signal for the QFB even with ADT at FT.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 19:26:23)

[ ADT Activity Monitor 24-11-10_18:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859693/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/11/10 18:37:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859693/content)

[be-bqbbq-isa >> Version: 3.0.7  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-11-10_18:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859695/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:39:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859695/content)

[be-bqbbq-isa >> Version: 3.0.7  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-11-10_18:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859697/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:38:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859697/content)

[pm-beam-loss-evaluation >> Version: 1.0.11  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-11-10_18:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859699/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:41:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859699/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-11-10_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859701/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:46:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859701/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-11-10_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859705/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/10 18:46:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3859705/content)


***

### [2024-11-10 18:40:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181564)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:40:11)


***

### [2024-11-10 18:40:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181565)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/10 18:40:13)


