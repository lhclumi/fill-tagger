# FILL 9864
**start**:		 2024-07-06 01:18:34.866738525+02:00 (CERN time)

**end**:		 2024-07-06 07:49:35.120613525+02:00 (CERN time)

**duration**:	 0 days 06:31:00.253875


***

### [2024-07-06 01:18:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101198)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:18:34)


***

### [2024-07-06 01:18:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101199)
>LHC RUN CTRL: New FILL NUMBER set to 9864

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:18:35)


***

### [2024-07-06 01:33:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101201)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:33:37)


***

### [2024-07-06 01:59:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101209)
>Missing QPS ok on all these circuits in S45 (including RB and RQF).Reported error is FIP communication lost.Once the precycle is finished I'll try the QPS reset..

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:00:45)

[Circuit_RQS_R4B2:   24-07-06_01:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665914/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:00:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665914/content)

[Circuit_RCO_A45B2:   24-07-06_02:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665916/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:01:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665916/content)

[Circuit_RQT13_R4B1:   24-07-06_02:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665918/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:00:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665918/content)

[Circuit_RB_A45:   24-07-06_02:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665922/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:00:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665922/content)

[Circuit_RQF_A45:   24-07-06_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665924/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:01:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665924/content)


***

### [2024-07-06 02:06:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101213)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 02:06:40)


***

### [2024-07-06 02:16:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101215)
>A few QPS resets on RQT13.R4B2 made the situation better.  
Now only the RCO.A45B2 is having FIP communication issues.  
I'll try to powercycle the QPS crate with the RCO and RQT13s.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:17:34)


***

### [2024-07-06 02:22:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101216)
>QPS powercycle of crate resurrected RCO.A45B2

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:22:24)

[Circuit_RCO_A45B2:   24-07-06_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665942/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:22:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665942/content)

[Circuit_RQT13_R4B1:   24-07-06_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665944/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:22:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665944/content)

[Circuit_RQT13_R4B2:   24-07-06_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665946/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:22:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665946/content)


***

### [2024-07-06 02:26:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101217)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 02:26:08)


***

### [2024-07-06 02:31:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101218)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 02:31:36)


***

### [2024-07-06 02:36:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101220)
>Wirescans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:36:47)

[ LHC WIRESCANNER APP 24-07-06_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665948/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:36:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665948/content)

[ LHC WIRESCANNER APP 24-07-06_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665950/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:36:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665950/content)

[ LHC WIRESCANNER APP 24-07-06_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665952/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:36:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665952/content)

[ LHC WIRESCANNER APP 24-07-06_02:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665954/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 02:37:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665954/content)


***

### [2024-07-06 03:03:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101223)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:03:36)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-06_03:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665972/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:03:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665972/content)

[ LHC Fast BCT v1.3.2 24-07-06_03:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665974/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:03:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665974/content)


***

### [2024-07-06 03:03:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101224)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:03:41)


***

### [2024-07-06 03:03:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101225)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:03:50)


***

### [2024-07-06 03:03:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101226)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:03:57)


***

### [2024-07-06 03:03:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101227)
>LHC Injection CompleteNumber of injections actual / planned: 47 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:32:22 / 0:33:48 (95.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:03:58)


***

### [2024-07-06 03:06:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101228)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:06:14)


***

### [2024-07-06 03:06:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101228)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:06:14)


***

### [2024-07-06 03:06:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101230)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:06:15)


***

### [2024-07-06 03:27:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101233)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:27:34)


***

### [2024-07-06 03:27:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101237)
>Flat top

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:27:52)

[ LHC Fast BCT v1.3.2 24-07-06_03:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665978/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:27:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665978/content)

[ LHC Beam Losses Lifetime Display 24-07-06_03:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665982/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:28:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665982/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-06_03:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665984/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/06 03:28:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665984/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-06_03:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665986/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/06 03:28:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665986/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-06_03:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665988/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/06 03:28:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665988/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-06_03:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665990/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:30:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665990/content)


***

### [2024-07-06 03:27:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101235)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:27:48)


***

### [2024-07-06 03:36:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101238)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:36:10)


***

### [2024-07-06 03:36:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101239)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:36:50)


***

### [2024-07-06 03:36:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101241)
>squeeze and tune change

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/06 03:37:01)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-06_03:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665992/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/06 03:37:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665992/content)

[ LHC Beam Losses Lifetime Display 24-07-06_03:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665994/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:37:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665994/content)


***

### [2024-07-06 03:37:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101242)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:37:39)


***

### [2024-07-06 03:40:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101243)
>Lifetime in collision

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:40:14)

[ LHC Beam Losses Lifetime Display 24-07-06_03:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665996/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:40:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665996/content)

[ LHC Beam Losses Lifetime Display 24-07-06_03:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665998/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:40:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665998/content)


***

### [2024-07-06 03:43:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101244)
>Optimizations

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:43:21)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-06_03:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666000/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 03:46:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666000/content)


***

### [2024-07-06 03:43:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101245)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:43:55)


***

### [2024-07-06 03:46:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101246)
>B1H +1e-3 tune trim

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:46:32)

[ LHC Beam Losses Lifetime Display 24-07-06_03:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666002/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/06 03:46:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666002/content)


***

### [2024-07-06 03:50:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101247)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:50:26)


***

### [2024-07-06 03:52:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101248)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 03:52:24)


***

### [2024-07-06 04:02:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101251)
>Emittance scans at start of collisions, 15 points

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:02:58)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-06_04:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666008/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:03:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666008/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-06_04:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666010/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:03:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666010/content)


***

### [2024-07-06 04:04:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101252)
>After a hint from the result of the emittance scan, new optimization for IP5 before B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:04:49)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-06_04:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666012/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:04:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666012/content)


***

### [2024-07-06 04:05:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101253)
>Before B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:06:03)

[ LHC Fast BCT v1.3.2 24-07-06_04:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666014/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 06:42:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666014/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-06_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666016/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 04:06:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666016/content)


***

### [2024-07-06 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101264)
>Started shift in stable beams.  
Announcer server had to be rebooted.  
CCC phone 77600 is redirected to the SIM number of the smartphone as CERN Phone app shows "Connection failed".  
  
RB.A56 dumped the beams with a correctors trip, QPS did not open the switches.  
EPC piquet reset the converter and reported that they'll need to access if it happens again.  
I mentioned that the access on monday could be used for preventive maintenance.  
  
Problem with QPS FIP connection lost in S45 (RB and RQF affected on top of the usual RCO).  
Resets on RQT13.R4B2 helped the main circuits but for the RCO a powercycle of the QPS crate was needed.  
  
Smooth filling and leaving the machine in stable beams during B\* levelling.  
  
Performed emittance scans 15p at start of collisions. At 30cm HO and before dump other scans (15p) are requested.  
  
AC

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/06 07:26:33)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666096/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/06 07:26:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666096/content)


***

### [2024-07-06 07:43:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101269)
>Global Post Mortem EventEvent Timestamp: 06/07/24 07:43:56.500Fill Number: 9864Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799680 [MeV]Intensity B1/B2: 30966 / 31050 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.USC55.R5.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/06 07:46:49)


***

### [2024-07-06 07:44:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101265)
>Trip

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/06 07:44:13)

[Set SECTOR56 24-07-06_07:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666103/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/06 07:44:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666103/content)


***

### [2024-07-06 07:44:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101266)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 07:44:36)


***

### [2024-07-06 07:44:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101267)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 07:44:38)


***

### [2024-07-06 07:46:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101268)
>Trip of RCBCH9.R5B1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 07:46:22)

[ Equip State 24-07-06_07:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666105/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 07:46:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666105/content)

[Circuit_RCBCH9_R5B1:   24-07-06_07:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666107/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/06 07:52:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666107/content)

[bic_eventseq >> Version: 3.6.2  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-07-06_07:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666109/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 07:52:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666109/content)

[ PM ONLINE PRO GUI : 8.6.1 24-07-06_07:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666111/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 07:52:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3666111/content)


***

### [2024-07-06 07:49:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101270)
>LHC RUN CTRL: New FILL NUMBER set to 9865

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 07:49:36)


