# FILL 9582
**start**:		 2024-05-02 08:43:33.437613525+02:00 (CERN time)

**end**:		 2024-05-02 12:42:06.297863525+02:00 (CERN time)

**duration**:	 0 days 03:58:32.860250


***

### [2024-05-02 08:43:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060003)
>LHC RUN CTRL: New FILL NUMBER set to 9582

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:43:33)


***

### [2024-05-02 08:44:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060009)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:44:23)


***

### [2024-05-02 08:44:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060011)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:44:39)


***

### [2024-05-02 08:46:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060012)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:46:37)


***

### [2024-05-02 08:46:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060014)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:46:52)


***

### [2024-05-02 08:48:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060016)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:48:26)


***

### [2024-05-02 08:48:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060017)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:48:47)


***

### [2024-05-02 08:49:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060020)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:49:17)


***

### [2024-05-02 08:49:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060022)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:49:23)


***

### [2024-05-02 08:50:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060029)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:50:44)


***

### [2024-05-02 08:50:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060030)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:50:49)


***

### [2024-05-02 08:51:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060032)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:51:31)


***

### [2024-05-02 08:53:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060034)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:12)


***

### [2024-05-02 08:53:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060038)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:14)


***

### [2024-05-02 08:53:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060036)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:14)


***

### [2024-05-02 08:53:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060040)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:15)


***

### [2024-05-02 08:53:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060042)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:18)


***

### [2024-05-02 08:53:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060047)
>ramping up RQ4.LR3 on David's request to test

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 08:53:36)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-05-02_08:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572643/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 08:53:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572643/content)


***

### [2024-05-02 08:53:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060045)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:53:29)


***

### [2024-05-02 08:55:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060050)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:55:29)


***

### [2024-05-02 08:55:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060053)
>at flat top current

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 08:56:06)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-05-02_08:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572647/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 08:56:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572647/content)

[ LSA Applications Suite (v 16.5.36) 24-05-02_08:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572649/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 08:56:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572649/content)


***

### [2024-05-02 08:57:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060055)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 08:57:13)


***

### [2024-05-02 09:05:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060064)
>RQ4.LR3 holding at top energy, ramping down

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:05:21)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-05-02_09:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572675/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:10:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572675/content)


***

### [2024-05-02 09:12:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060071)
>QPS reset of RBs failing as usual

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:12:41)

[ LHC Sequencer Execution GUI (PRO) : 12.32.0  24-05-02_09:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572686/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:12:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572686/content)


***

### [2024-05-02 09:13:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060072)
>forced board B

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:13:49)

[8 - RCBXH2.R8 DQAMG N type A for circuit RCBXH2.R8    24-05-02_09:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572688/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:13:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572688/content)

[Circuit_RCBXH2_R8:   24-05-02_09:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572690/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:13:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572690/content)


***

### [2024-05-02 09:15:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060074)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 09:15:37)


***

### [2024-05-02 09:18:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060076)
>corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/02 09:18:35)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-02_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572694/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/02 09:18:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572694/content)

[RF Trim Warning 24-05-02_09:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572696/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 09:19:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572696/content)

[Accelerator Cockpit v0.0.35 24-05-02_09:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572698/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 09:19:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572698/content)

[Accelerator Cockpit v0.0.35 24-05-02_09:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572702/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 09:20:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572702/content)

[Accelerator Cockpit v0.0.35 24-05-02_09:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572706/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 09:20:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572706/content)


***

### [2024-05-02 09:23:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060082)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 09:23:49)


***

### [2024-05-02 09:29:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060085)
>for Stephane

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:29:40)

[ INJECTION SEQUENCER  v 5.1.5 24-05-02_09:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572732/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:29:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572732/content)


***

### [2024-05-02 09:30:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060088)
>setup falg

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:30:34)

[Safe Machine Parameters in CCC : Overview GUI 24-05-02_09:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572734/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:30:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572734/content)


***

### [2024-05-02 09:30:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060089)
>masks

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:30:46)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572736/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:30:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572736/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572738/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:30:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572738/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572740/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:31:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572740/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572742/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:31:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572742/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572744/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:31:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572744/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_09:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572746/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 09:31:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572746/content)


***

### [2024-05-02 09:34:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060090)
>INDIV in B2

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:34:48)

[ LHC WIRESCANNER APP 24-05-02_09:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572750/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:34:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572750/content)

[ LHC WIRESCANNER APP 24-05-02_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572756/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:35:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572756/content)


***

### [2024-05-02 09:35:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060093)
>INDIV B2 - V

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:35:12)

[ LHC WIRESCANNER APP 24-05-02_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572754/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:35:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572754/content)

[ LHC WIRESCANNER APP 24-05-02_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572758/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:35:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572758/content)


***

### [2024-05-02 09:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060095)
>pilots - B2H

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:37:18)

[ LHC WIRESCANNER APP 24-05-02_09:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572762/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:37:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572762/content)

[ LHC WIRESCANNER APP 24-05-02_09:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572770/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:37:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572770/content)


***

### [2024-05-02 09:37:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060096)
>pilot - B2V

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:37:32)

[ LHC WIRESCANNER APP 24-05-02_09:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572766/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:37:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572766/content)

[ LHC WIRESCANNER APP 24-05-02_09:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572772/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:38:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572772/content)


***

### [2024-05-02 09:52:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060103)
>modified MO to get ot -2 at FT

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:52:33)

[ LSA Applications Suite (v 16.5.36) 24-05-02_09:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:52:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572806/content)


***

### [2024-05-02 09:52:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060103)
>modified MO to get ot -2 at FT

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:52:33)

[ LSA Applications Suite (v 16.5.36) 24-05-02_09:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:52:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572806/content)


***

### [2024-05-02 09:58:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060108)
>INDIVs - B1H

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:58:55)

[ LHC WIRESCANNER APP 24-05-02_09:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572820/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:58:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572820/content)

[ LHC WIRESCANNER APP 24-05-02_09:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572824/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:59:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572824/content)


***

### [2024-05-02 09:59:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060109)
>INDIVs - B1V

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:59:06)

[ LHC WIRESCANNER APP 24-05-02_09:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572822/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:59:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572822/content)

[ LHC WIRESCANNER APP 24-05-02_09:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572826/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 09:59:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572826/content)


***

### [2024-05-02 10:00:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060111)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:00:29)


***

### [2024-05-02 10:00:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060112)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:00:38)


***

### [2024-05-02 10:00:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060113)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:00:47)


***

### [2024-05-02 10:00:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060114)
>LHC Injection CompleteNumber of injections actual / planned: 17 / 20SPS SuperCycle length: 28.8 [s]Actual / minimum time: 0:36:57 / 0:14:36 (253.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 10:00:48)


***

### [2024-05-02 10:06:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060121)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:06:00)


***

### [2024-05-02 10:06:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060123)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:06:01)


***

### [2024-05-02 10:27:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060143)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 10:27:18)


***

### [2024-05-02 10:50:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060177)
>loaded tming tables to trigger dBLM with WS - to be reverted

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 10:50:37)

[ Timing editor 24-05-02_10:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573077/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 10:50:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573077/content)


***

### [2024-05-02 10:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060181)
>parking limits of TCP.H&V in B2

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 10:53:50)

[ Equip State 24-05-02_10:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573080/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/02 10:53:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573080/content)


***

### [2024-05-02 11:01:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060190)
>Finished scraping b2v

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:01:59)

[LHC Collimator Control Application (Device: TCP.D6R7.B2.TCP.IP7.B2.1.V) -  INCA DISABLED - PRO CCDA 24-05-02_11:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573156/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:01:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573156/content)


***

### [2024-05-02 11:02:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060193)
>Scraping more of b2v to 1.14

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:02:41)

[LHC Collimator Control Application (Device: TCP.D6R7.B2.TCP.IP7.B2.1.V) -  INCA DISABLED - PRO CCDA 24-05-02_11:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573164/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:02:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573164/content)


***

### [2024-05-02 11:07:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060196)
>cut @3.5sigmas of measured emittance of nominal bunch

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/02 11:08:35)

[ LHC Fast BCT v1.3.2 24-05-02_11:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573198/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/02 11:08:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573198/content)


***

### [2024-05-02 11:32:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060206)
>Performing scraping on b2v after blowup to 3.0 sigma

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:33:15)

[LHC Collimator Control Application (Device: TCP.D6R7.B2.TCP.IP7.B2.1.V) -  INCA DISABLED - PRO CCDA 24-05-02_11:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573245/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 11:33:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573245/content)


***

### [2024-05-02 12:00:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060232)
>Interesting loss pattern with vertical primaries in b2 open one sigma more. Only .5 sigma retraction and rest of hierarchy respected.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/02 12:01:46)

[ LHC BLM Fixed Display 24-05-02_12:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573333/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/02 12:01:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573333/content)

[ LHC BLM Fixed Display 24-05-02_12:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573335/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/02 12:01:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573335/content)


***

### [2024-05-02 12:32:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060251)
>Global Post Mortem EventEvent Timestamp: 02/05/24 12:32:43.175Fill Number: 9582Accelerator / beam mode: PROTON PHYSICS / FLAT TOPEnergy: 6799320 [MeV]Intensity B1/B2: 27 / 26 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 8-COLL#MOT-b2: B T -> F on CIB.TZ76.U7.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/02 12:35:35)


***

### [2024-05-02 12:34:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060253)
>Global Post Mortem EventEvent Timestamp: 02/05/24 12:34:42.929Fill Number: 9582Accelerator / beam mode: PROTON PHYSICS / FLAT TOPEnergy: 6799440 [MeV]Intensity B1/B2: 26 / 0 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/02 12:36:51)


***

### [2024-05-02 12:34:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060282)
>Global Post Mortem Event ConfirmationDump Classification: Operational mistakeOperator / Comment: dmirarch / TCP opened triggering energy limits, forgotten to mask 

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/02 12:52:45)


***

### [2024-05-02 12:34:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060248)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 12:34:51)


***

### [2024-05-02 12:34:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060249)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 12:34:53)


***

### [2024-05-02 12:38:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060254)
>masked coll mov...

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 12:38:30)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-05-02_12:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573401/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/02 12:38:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573401/content)


***

### [2024-05-02 12:41:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060256)
>Event created from ScreenShot Client.new BWS panel B1V2 NavPy 1.4.0 24-05-02\_12:41.png

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/02 12:41:58)

[ NavPy 1.4.0 24-05-02_12:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573405/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/02 12:41:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3573405/content)


***

### [2024-05-02 12:42:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4060257)
>LHC RUN CTRL: New FILL NUMBER set to 9583

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/02 12:42:06)


