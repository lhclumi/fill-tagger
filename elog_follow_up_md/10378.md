# FILL 10378
**start**:		 2024-11-16 09:06:45.297738525+01:00 (CERN time)

**end**:		 2024-11-16 18:27:22.244738525+01:00 (CERN time)

**duration**:	 0 days 09:20:36.947000


***

### [2024-11-16 09:06:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185129)
>LHC RUN CTRL: New FILL NUMBER set to 10378

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:06:46)


***

### [2024-11-16 09:07:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185135)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:07:37)


***

### [2024-11-16 09:08:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185136)
>Event created from ScreenShot Client.  
24-11-16\_09:08.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 09:08:47)

[24-11-16_09:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866898/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 09:14:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866898/content)


***

### [2024-11-16 09:09:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185137)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:09:42)


***

### [2024-11-16 09:10:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185139)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:10:57)


***

### [2024-11-16 09:11:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185140)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:11:05)


***

### [2024-11-16 09:12:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185142)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:12:30)


***

### [2024-11-16 09:13:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185146)
>
```
diff betweeen scope settngs HW and LSA. I drive the LSA settings
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 09:14:36)

[ LSA Applications Suite (v 16.6.36) 24-11-16_09:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866900/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 09:13:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866900/content)


***

### [2024-11-16 09:13:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185144)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:13:09)


***

### [2024-11-16 09:13:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185147)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:13:50)


***

### [2024-11-16 09:15:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185149)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:15:34)


***

### [2024-11-16 09:15:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185151)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:15:36)


***

### [2024-11-16 09:15:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185153)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:15:39)


***

### [2024-11-16 09:15:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185157)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:15:54)


***

### [2024-11-16 09:16:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185160)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:16:17)


***

### [2024-11-16 09:16:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185161)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:16:26)


***

### [2024-11-16 09:16:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185163)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:16:33)


***

### [2024-11-16 09:16:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185164)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:16:44)


***

### [2024-11-16 09:17:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185165)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:17:36)


***

### [2024-11-16 09:17:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185166)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:17:54)


***

### [2024-11-16 09:19:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185168)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:19:38)


***

### [2024-11-16 09:26:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185171)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:26:02)


***

### [2024-11-16 09:34:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185175)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:34:36)


***

### [2024-11-16 09:40:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185177)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 09:40:40)


***

### [2024-11-16 09:44:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185178)
>64% of dump thrshold when injected B2

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/16 09:44:26)

[ LHC BLM Fixed Display 24-11-16_09:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866916/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/16 09:44:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866916/content)


***

### [2024-11-16 09:44:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185180)
>B2 trajectory

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/11/16 09:45:04)

[OpenYASP DV LHCB2Transfer . LHC_ION_14Inj_slip_stacking_Pb82_Q26_NorthExtraction_2023_V1 24-11-16_09:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866918/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/11/16 09:45:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866918/content)


***

### [2024-11-16 09:48:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185181)
>B1 trajectory

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/11/16 09:48:13)

[OpenYASP DV LHCB1Transfer . LHC_ION_14Inj_slip_stacking_Pb82_Q26_NorthExtraction_2023_V1 24-11-16_09:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866922/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/11/16 09:48:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866922/content)


***

### [2024-11-16 09:58:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185185)
>
```
No issue with the BQM itself so far, but several time we have one injection missing in SPS  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 10:01:44)


***

### [2024-11-16 10:26:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185188)
>SPS BQM attenuator issue...  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 10:26:22)

[SPS Beam Quality Monitor - SPS.USER.LHCION1 - SPS PRO INCA server - PRO CCDA 24-11-16_10:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866986/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/16 10:29:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3866986/content)


***

### [2024-11-16 10:58:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185192)
>injection finished  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 10:58:31)

[ Beam Intensity - v1.5.0 24-11-16_10:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867000/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 10:58:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867000/content)

[ LHC Fast BCT v1.3.2 24-11-16_10:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867002/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 10:58:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867002/content)

[SPS Beam Quality Monitor - SPS.USER.LHCION4 - SPS PRO INCA server - PRO CCDA 24-11-16_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867004/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/16 11:00:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867004/content)


***

### [2024-11-16 10:59:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185193)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 10:59:27)


***

### [2024-11-16 10:59:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185194)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 10:59:37)


***

### [2024-11-16 10:59:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185195)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 10:59:43)


***

### [2024-11-16 10:59:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185196)
>LHC Injection Complete

Number of injections actual / planned: 64 / 46
SPS SuperCycle length: 70.8 [s]
Actual / minimum time: 1:19:05 / 0:59:16 (133.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/16 10:59:44)


***

### [2024-11-16 10:59:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185196)
>LHC Injection Complete

Number of injections actual / planned: 64 / 46
SPS SuperCycle length: 70.8 [s]
Actual / minimum time: 1:19:05 / 0:59:16 (133.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/16 10:59:44)


***

### [2024-11-16 11:02:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185197)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:02:00)


***

### [2024-11-16 11:02:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185199)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:02:02)


***

### [2024-11-16 11:23:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185202)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:23:19)


***

### [2024-11-16 11:23:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185204)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:23:27)


***

### [2024-11-16 11:31:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185206)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:31:20)


***

### [2024-11-16 11:32:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185207)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:32:15)


***

### [2024-11-16 11:33:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185209)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:33:30)


***

### [2024-11-16 11:39:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185210)
>IP s optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 11:39:31)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_11:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867008/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 11:58:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867008/content)


***

### [2024-11-16 11:39:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185211)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 11:39:48)


***

### [2024-11-16 11:40:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185212)
>beam at start of collisions

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 11:40:30)

[ LHC Fast BCT v1.3.2 24-11-16_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867010/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 11:40:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867010/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-11-16_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867012/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 11:40:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867012/content)

[ LHC Beam Losses Lifetime Display 24-11-16_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867014/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/16 11:40:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867014/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-16_11:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867016/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/16 11:41:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867016/content)


***

### [2024-11-16 11:56:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185214)
>Event created from ScreenShot Client.  
Abort gap cleaning control v2.1.1 24-11-16\_11:56.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/16 11:56:52)

[Abort gap cleaning control v2.1.1 24-11-16_11:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867020/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/16 11:56:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867020/content)


***

### [2024-11-16 12:59:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185230)
>IP 1 out of levelling after 1h20mins in stable beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 12:59:33)

[LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_12:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867050/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:06:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867050/content)


***

### [2024-11-16 13:06:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185232)
>IP5 out of levelling after 1h25 in SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:06:54)

[LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_13:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867052/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:11:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867052/content)


***

### [2024-11-16 13:11:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185233)
>IP8 out of levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:11:16)

[LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_13:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867054/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:23:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867054/content)


***

### [2024-11-16 13:23:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185236)
>IP2 also out of lebvelling after 1h40 in SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:24:06)

[LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_13:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867056/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 13:24:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867056/content)


***

### [2024-11-16 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185254)
>
```
**SHIFT SUMMARY:**  
  
I arrived in stable beams.  
The plan was to dump at 9am, but just when I was about to start the handshake beams were dumped by a trip of the RF line 7B2 (arc detector in the circulator)  
I could reset the cavity and prepare for the next filling.  
  
For some reason and for tehsecond time these days, I needed to reload the Rf scope settings, something is changing the vertical scale directly in teh FESA class. I just resent the values and all was fine.  
  
The filling took usually a long time, a few trains with bunches missing at the start of the filling, then once the BQM attenuator. The SPS needed to scrap the beam that has retrieved a good intensity in the injectors. B2 injection gave each time losses at maximum 65% of the dump threshold. Injection took 1h20mins.  
  
The rest of the cycle was smooth, levelling for Atlas lasted 1h40mins  
  
Delphine  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 15:01:05)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867071/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 15:01:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867071/content)


***

### [2024-11-16 18:21:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185282)
>Global Post Mortem Event

Event Timestamp: 16/11/24 18:21:45.396
Fill Number: 10378
Accelerator / beam mode: ION PHYSICS / STABLE BEAMS
Energy: 6799320 [MeV]
Intensity B1/B2: 1035 / 1042 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 7-WIC: B T -> F on CIB.TZ76.U7.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/16 18:24:36)


***

### [2024-11-16 18:21:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185333)
>Global Post Mortem Event Confirmation

Dump Classification: Power converter fault(s)
Operator / Comment: gtrad / Trip of warm magnet RQ5.LR7


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/16 19:03:46)


***

### [2024-11-16 18:21:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185279)
>Event created from ScreenShot Client.  
Set SECTOR78 24-11-16\_18:21.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/11/16 18:21:58)

[Set SECTOR78 24-11-16_18:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867094/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/11/16 18:21:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867094/content)

[ Equip State 24-11-16_18:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867096/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/11/16 18:24:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867096/content)


***

### [2024-11-16 18:22:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185280)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 18:22:37)


***

### [2024-11-16 18:22:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185281)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 18:22:40)


***

### [2024-11-16 18:25:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185283)
>Power Converter Fault  
  
Device: RPTF.SR7.RQ5.LR7 (RPTF.SR7.RQ5.LR7)  
Time: 2024-11-16 18:21:45,398  
Faults: VS\_FAULT  
VS faults: U\_CAPA OVER RIPPLE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1736302:6539025?archive=1736302:6539026?archive=1736302:6539027?archive=1736302:6539028?archive=1736302:6539029?archive=1736302:6539030)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/16 18:25:13)


***

### [2024-11-16 18:25:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185284)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.63.5 
 [pro] 24-11-16\_18:25.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 18:25:20)

[LHC Luminosity Scan Client 0.63.5 [pro] 24-11-16_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867098/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 18:25:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867098/content)

[ LHC Beam Losses Lifetime Display 24-11-16_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867100/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/16 18:25:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867100/content)

[Monitor Real Time Trim Goniometers 24-11-16_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867102/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/16 18:25:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867102/content)

[ CCM_2 LHCOP 24-11-16_18:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867104/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/11/16 18:25:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867104/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-16_18:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867106/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/16 18:26:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867106/content)

[Beam Gas Curtain 24-11-16_18:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867108/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/16 18:26:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867108/content)

[ LHC Fast BCT v1.3.2 24-11-16_18:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867110/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 18:26:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867110/content)

[ Beam Intensity - v1.5.0 24-11-16_18:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867112/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 18:29:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867112/content)

[ Beam Intensity - v1.5.0 24-11-16_18:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867114/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/16 18:30:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3867114/content)


***

### [2024-11-16 18:27:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4185285)
>LHC RUN CTRL: New FILL NUMBER set to 10379

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/16 18:27:23)


