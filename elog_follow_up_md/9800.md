# FILL 9800
**start**:		 2024-06-19 12:44:52.745988525+02:00 (CERN time)

**end**:		 2024-06-19 16:15:24.030863525+02:00 (CERN time)

**duration**:	 0 days 03:30:31.284875


***

### [2024-06-19 12:44:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090392)
>LHC RUN CTRL: New FILL NUMBER set to 9800

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:44:53)


***

### [2024-06-19 12:45:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090399)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:45:45)


***

### [2024-06-19 12:48:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090400)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:48:03)


***

### [2024-06-19 12:48:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090402)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:48:10)


***

### [2024-06-19 12:50:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090405)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:50:39)


***

### [2024-06-19 12:50:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090406)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:50:40)


***

### [2024-06-19 12:51:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090408)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:51:45)


***

### [2024-06-19 12:51:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090409)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:51:46)


***

### [2024-06-19 12:53:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090413)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:53:49)


***

### [2024-06-19 12:54:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090417)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:54:06)


***

### [2024-06-19 12:54:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090418)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:54:36)


***

### [2024-06-19 12:54:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090420)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:54:40)


***

### [2024-06-19 12:54:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090421)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:54:42)


***

### [2024-06-19 12:56:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090422)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:56:16)


***

### [2024-06-19 12:56:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090424)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:56:19)


***

### [2024-06-19 12:56:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090426)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:56:21)


***

### [2024-06-19 12:56:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090430)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:56:36)


***

### [2024-06-19 12:57:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090432)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:57:35)


***

### [2024-06-19 12:58:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090433)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 12:58:36)


***

### [2024-06-19 13:00:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090436)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 13:00:20)


***

### [2024-06-19 13:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090442)
>QPS reset failed in S45

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:10:02)

[ LHC Sequencer Execution GUI (PRO) : 12.32.12  24-06-19_13:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641812/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:10:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641812/content)


***

### [2024-06-19 13:19:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090453)
>Trying SEnd logging data after the unsuccessful reset

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:20:09)

[2 - RCO.A45B2 DQAMG N type A for circuit RCO.A45B2    24-06-19_13:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641856/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:20:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641856/content)


***

### [2024-06-19 13:21:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090455)
>Resetting and waiting before a Power cycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:21:42)

[ LHC CIRCUIT SUPERVISION v9.0 24-06-19_13:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641866/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:21:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641866/content)


***

### [2024-06-19 13:25:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090459)
>Recovered QPS OK

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:25:23)

[Circuit_RCO_A45B2:   24-06-19_13:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641882/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 13:25:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641882/content)


***

### [2024-06-19 13:29:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090462)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 13:29:06)


***

### [2024-06-19 13:32:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090468)
>
```

```
Recap on the ASD attempts at 30cm over last 24h:  
  
Over night an ASD have been performed at 00:20:30 at beta* 30cm and BBLR ON (no XRPs inserted)  
  
This morning an attempt for ASD at beta* 30cm and BBLR and XRPs IN at 09:19 failed with losses dumping the beam with RS8 (655ms) in MQY 5R6. The dump took place before the LM app switches OFF the RF system, just as soon as the orbit bump in IR6 (around TCDQ) was about to reach the desired value of 1.2mm.  
  
Another attempt  for ASD at beta* 30cm and BBLR powered and XRPs inserted at 12:32 failed with the same BLM interlock at the same location and very much coincident with the time when the TCDQ bump was about to get completely inserted   
  
(we estimate from the measured orbit at dump that 90% of the trim was in place)  

```
  
  
The BLM buffers during the first dump, featuring the 10 Hz signature hinted that it could be the origin of the dump; quick investigations with CV920 Cryo valves and ADT reading point that no activity took place around that moment.  
  
The second dump have no evident BPM signatures of 10Hz oscillations either.  

```
  
As seen in attachment, transient losses at 5R6 where also observed when inserting the TCDQ bump, however only RS 1.3s buffer depth is enough to see it in PM (can t compare the levels of RS8 theone dumping)  
  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 14:07:38)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-06-19_14:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641972/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 14:06:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641972/content)

[Screenshot from 2024-06-19 14-15-28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641992/content)
creator:	 mihostet  @194.12.133.144 (2024/06/19 14:20:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641992/content)

[Screenshot from 2024-06-19 14-19-08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641998/content)
creator:	 mihostet  @194.12.133.144 (2024/06/19 14:22:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641998/content)


***

### [2024-06-19 13:40:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090479)
>SBF at setup

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 13:40:46)

[Safe Machine Parameters in CCC : Overview GUI 24-06-19_13:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641950/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 13:40:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3641950/content)


***

### [2024-06-19 13:50:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090484)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 13:50:30)


***

### [2024-06-19 14:06:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090492)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:06:30)


***

### [2024-06-19 14:06:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090493)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:06:40)


***

### [2024-06-19 14:08:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090494)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:08:32)


***

### [2024-06-19 14:08:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090495)
>LHC Injection CompleteNumber of injections actual / planned: 29 / 34SPS SuperCycle length: 28.8 [s]Actual / minimum time: 0:18:03 / 0:21:19 (84.7 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 14:08:35)


***

### [2024-06-19 14:11:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090497)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:11:44)


***

### [2024-06-19 14:11:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090500)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:11:46)


***

### [2024-06-19 14:33:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090526)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:33:06)


***

### [2024-06-19 14:33:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090528)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:33:19)


***

### [2024-06-19 14:43:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090539)
>Event created from ScreenShot Client.  
Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-06-19\_14:43.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 14:43:40)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-06-19_14:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642092/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 14:43:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642092/content)


***

### [2024-06-19 14:43:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090538)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:43:30)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-06-19_14:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642094/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 14:43:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642094/content)


***

### [2024-06-19 14:44:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090543)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:44:26)


***

### [2024-06-19 14:45:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090545)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:45:15)


***

### [2024-06-19 14:48:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090547)
>Inserting XRPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:48:26)


***

### [2024-06-19 14:49:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090549)
>IP optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:50:22)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-19_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642112/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:50:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642112/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-19_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642114/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:50:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642114/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-19_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642116/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:50:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642116/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-19_14:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642123/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:53:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642123/content)


***

### [2024-06-19 14:54:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090552)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:54:15)


***

### [2024-06-19 14:55:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090553)
>XRPs inserted

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 14:56:07)

[COLLIMATORS_VISTAR_MOV 24-06-19_14:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642127/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 14:56:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642127/content)


***

### [2024-06-19 14:56:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090554)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 14:56:13)


***

### [2024-06-19 14:57:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090557)
>LM B1H - 120cm - XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 14:56:16  
- Background acquisitions: 10  
- Max losses timestamp: 14:56:49  
- Machine configuration: Loss Map performed on a configuration not requested  
  
*Sent From pyLossMaps*

creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:57:19)

[tmpScreenshot_1718801839.1021402.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642143/content)
creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:57:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642143/content)


***

### [2024-06-19 14:58:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090560)
>LM B1V - 120cm - XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 14:57:22  
- Background acquisitions: 10  
- Max losses timestamp: 14:57:37  
- Machine configuration: Loss Map performed on a configuration not requested  
  
*Sent From pyLossMaps*

creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:58:34)

[tmpScreenshot_1718801914.7132182.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642149/content)
creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:58:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642149/content)


***

### [2024-06-19 14:59:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090561)
>LM B2H - 120cm - XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 14:58:36  
- Background acquisitions: 10  
- Max losses timestamp: 14:59:01  
- Machine configuration: Loss Map performed on a configuration not requested  
  
*Sent From pyLossMaps*

creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:59:37)

[tmpScreenshot_1718801977.1037457.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642153/content)
creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 14:59:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642153/content)


***

### [2024-06-19 15:00:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090564)
>B1V\_Q9 ADTObsBox pickup. Last turns before dump of fill 9798.  


creator:	 spsop  @cwo-ccc-a1lc.cern.ch (2024/06/19 15:01:42)

[screenShot_June_19th_2024_15_02_11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642161/content)
creator:	 spsop  @cwo-ccc-a1lc.cern.ch (2024/06/19 15:02:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642161/content)

[screenShot_June_19th_2024_15_12_12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642201/content)
creator:	 spsop  @cwo-ccc-a1lc.cern.ch (2024/06/19 15:12:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642201/content)


***

### [2024-06-19 15:00:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090563)
>LM B2V - 120cm - XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 14:59:54  
- Background acquisitions: 10  
- Max losses timestamp: 15:00:24  
- Machine configuration: Loss Map performed on a configuration not requested  
  
*Sent From pyLossMaps*

creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:00:49)

[tmpScreenshot_1718802048.9284992.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642159/content)
creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:00:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642159/content)


***

### [2024-06-19 15:08:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090569)
>orbit difference: ASD at 12:00 minus ASD at 00:20 last night

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 15:09:05)

[OpenYASP DV LHCRING . PHYSICS-6.8TeV-1.2m-2024_V1@135_[END] 24-06-19_15:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642187/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 15:09:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642187/content)


***

### [2024-06-19 15:25:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090601)
>We are at beta\* 30cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:25:29)

[ LHC Fast BCT v1.3.2 24-06-19_15:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642283/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:25:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642283/content)

[Beam : 1-Bunch evolution number : 17851 24-06-19_15:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642287/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:25:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642287/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-19_15:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642289/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:25:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642289/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-19_15:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642291/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/19 15:25:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642291/content)


***

### [2024-06-19 15:27:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090603)
>Turning ON BBLR

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:27:57)


***

### [2024-06-19 15:29:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090606)
>BBLR powered

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:29:15)

[ LHC BBLR Wire App v0.4.1 24-06-19_15:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642303/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:29:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642303/content)


***

### [2024-06-19 15:35:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090616)
>1mm bump

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:35:25)

[ LHC BLM Fixed Display 24-06-19_15:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642329/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:35:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642329/content)

[OpenYASP DV LHCRING . PHYSICS-6.8TeV-1.2m-2024_V1@135_[END] 24-06-19_15:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642331/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 15:35:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642331/content)


***

### [2024-06-19 15:36:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090617)
>1.03mm bump

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:37:06)

[ LHC BLM Fixed Display 24-06-19_15:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642335/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:37:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642335/content)


***

### [2024-06-19 15:38:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090621)
>1.07mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:38:51)

[ LHC BLM Fixed Display 24-06-19_15:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642347/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:38:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642347/content)


***

### [2024-06-19 15:38:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090621)
>1.07mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:38:51)

[ LHC BLM Fixed Display 24-06-19_15:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642347/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:38:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642347/content)


***

### [2024-06-19 15:39:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090624)
>1.1mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:39:27)

[ LHC BLM Fixed Display 24-06-19_15:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642351/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:39:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642351/content)


***

### [2024-06-19 15:40:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090627)
>1.11mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:41:05)

[ LHC BLM Fixed Display 24-06-19_15:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642360/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:41:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642360/content)


***

### [2024-06-19 15:41:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090628)
>back to 0

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:41:59)

[ LHC BLM Fixed Display 24-06-19_15:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642362/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:41:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642362/content)


***

### [2024-06-19 15:44:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090630)
>Change of Losses slope as soon as the bump was removed. Clearly the losses seem to come from bunch 1785

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:45:00)

[ LHC Fast BCT v1.3.2 24-06-19_15:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642372/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:45:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642372/content)


***

### [2024-06-19 15:51:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090641)
>Shaked out completely Bunch 1785 in B1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:51:56)

[ SINGLE BUNCH BLOW-UP APP 24-06-19_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642424/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:51:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642424/content)

[ LHC Fast BCT v1.3.2 24-06-19_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642426/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:51:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642426/content)

[ LHC Beam Losses Lifetime Display 24-06-19_15:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642428/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/19 15:52:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642428/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-19_15:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642432/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 15:52:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642432/content)


***

### [2024-06-19 15:54:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090644)
>0.9mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:54:25)

[ LHC BLM Fixed Display 24-06-19_15:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642434/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:54:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642434/content)


***

### [2024-06-19 15:55:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090645)
>1mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:55:15)

[ LHC BLM Fixed Display 24-06-19_15:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642438/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:55:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642438/content)

[ LHC BLM Fixed Display 24-06-19_15:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642440/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:55:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642440/content)


***

### [2024-06-19 15:56:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090646)
>1.04mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:56:13)

[ LHC BLM Fixed Display 24-06-19_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642444/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:56:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642444/content)


***

### [2024-06-19 15:56:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090648)
>1.06mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:56:44)

[ LHC BLM Fixed Display 24-06-19_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642448/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:56:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642448/content)


***

### [2024-06-19 15:57:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090649)
>1.08mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:57:17)

[ LHC BLM Fixed Display 24-06-19_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642454/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:57:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642454/content)

[ LHC BLM Fixed Display 24-06-19_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642458/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:57:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642458/content)


***

### [2024-06-19 15:57:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090650)
>1.09mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:57:43)

[ LHC BLM Fixed Display 24-06-19_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642460/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:57:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642460/content)


***

### [2024-06-19 15:58:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090652)
>1.1mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:58:38)

[ LHC BLM Fixed Display 24-06-19_15:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642464/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:58:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642464/content)


***

### [2024-06-19 15:59:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090654)
>1.13mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:59:38)

[ LHC BLM Fixed Display 24-06-19_15:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642468/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 15:59:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642468/content)


***

### [2024-06-19 16:00:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090658)
>1.15mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:00:58)

[ LHC BLM Fixed Display 24-06-19_16:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642490/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:00:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642490/content)


***

### [2024-06-19 16:02:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090665)
>-4e-3 B1H tune

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 16:03:03)

[Accelerator Cockpit v0.0.37 24-06-19_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642506/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 16:05:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642506/content)


***

### [2024-06-19 16:03:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090667)
>1mm with separated tunes

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:03:36)

[ LHC BLM Fixed Display 24-06-19_16:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642512/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:03:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642512/content)


***

### [2024-06-19 16:05:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090671)
>
```
1.15mm with reduced Q1H
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/19 16:06:10)

[ LHC BLM Fixed Display 24-06-19_16:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642520/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:05:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642520/content)


***

### [2024-06-19 16:07:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090674)
>1.24mm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:07:50)

[ LHC BLM Fixed Display 24-06-19_16:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642530/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/19 16:07:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642530/content)


***

### [2024-06-19 16:12:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090687)
>Global Post Mortem EventEvent Timestamp: 19/06/24 16:12:58.528Fill Number: 9800Accelerator / beam mode: PROTON PHYSICS / ADJUSTEnergy: 6799800 [MeV]Intensity B1/B2: 14 / 19 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/19 16:15:50)


***

### [2024-06-19 16:13:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090680)
>ASD with BBLR powered and XRPs inserted at beta\*30cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 16:13:42)

[ LHC LOSS MAPS 24-06-19_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642550/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/19 16:13:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642550/content)


***

### [2024-06-19 16:13:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090681)
>ASD with BBLR powered and XRPs inserted at beta\*30cm  
  
Screenshot attached: Asynchronous Beam Dump overview  
- Start timestamp: 16:09:37  
- Machine configuration: Loss Map performed on a configuration not requested  
  
*Sent From pyLossMaps*

creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 16:13:46)

[tmpScreenshot_1718806426.2975047.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642552/content)
creator:	 gtrad  @cwo-ccc-d3lc.cern.ch (2024/06/19 16:13:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3642552/content)


***

### [2024-06-19 16:14:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090682)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 16:14:37)


***

### [2024-06-19 16:14:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090683)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 16:14:41)


***

### [2024-06-19 16:15:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4090686)
>LHC RUN CTRL: New FILL NUMBER set to 9801

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/19 16:15:24)


