# FILL 10199
**start**:		 2024-10-04 12:47:02.411363525+02:00 (CERN time)

**end**:		 2024-10-05 04:07:09.683738525+02:00 (CERN time)

**duration**:	 0 days 15:20:07.272375


***

### [2024-10-04 12:47:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155944)
>LHC RUN CTRL: New FILL NUMBER set to 10199

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:47:03)


***

### [2024-10-04 12:49:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155945)
>
```
ROF.A34B1 tripped going to standby - OK after reset
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 12:51:05)

[ Equip State 24-10-04_12:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801489/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 12:50:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801489/content)


***

### [2024-10-04 12:50:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155946)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:50:50)


***

### [2024-10-04 12:53:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155952)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:53:02)


***

### [2024-10-04 12:55:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155955)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:55:08)


***

### [2024-10-04 12:55:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155957)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:55:12)


***

### [2024-10-04 12:56:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155960)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:56:38)


***

### [2024-10-04 12:57:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155961)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:57:46)


***

### [2024-10-04 12:57:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155962)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 12:57:52)


***

### [2024-10-04 13:00:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155964)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:00:25)


***

### [2024-10-04 13:01:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155967)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:01:47)


***

### [2024-10-04 13:01:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155969)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:01:54)


***

### [2024-10-04 13:02:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155970)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:02:29)


***

### [2024-10-04 13:04:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155972)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:04:55)


***

### [2024-10-04 13:04:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155974)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:04:57)


***

### [2024-10-04 13:04:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155976)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:04:59)


***

### [2024-10-04 13:05:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155980)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:05:14)


***

### [2024-10-04 13:05:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155983)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:05:35)


***

### [2024-10-04 13:05:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155984)
>RF ON

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/04 13:05:44)

[ LHC RF CONTROL 24-10-04_13:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801507/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/04 13:05:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801507/content)


***

### [2024-10-04 13:06:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155985)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:06:10)


***

### [2024-10-04 13:07:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155987)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:07:13)


***

### [2024-10-04 13:08:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155990)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:08:57)


***

### [2024-10-04 13:21:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155997)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:21:57)


***

### [2024-10-04 13:30:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156004)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:30:05)


***

### [2024-10-04 13:35:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156010)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:36:01)

[Accelerator Cockpit v0.0.38 24-10-04_13:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801580/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:36:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801580/content)

[Accelerator Cockpit v0.0.38 24-10-04_13:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801582/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:36:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801582/content)

[Accelerator Cockpit v0.0.38 24-10-04_13:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801584/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:36:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801584/content)

[Accelerator Cockpit v0.0.38 24-10-04_13:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801586/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:37:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801586/content)


***

### [2024-10-04 13:38:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156012)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 13:38:34)


***

### [2024-10-04 13:41:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156016)
>
```
structure in ~24L1 - bad BPM?
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:48:48)

[ Power Converter Interlock GUI 24-10-04_13:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801590/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:52:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801590/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-10-04_13:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801592/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:42:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801592/content)


***

### [2024-10-04 13:43:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156018)
>BWS - problem with automatic bunch selection in B2

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:44:33)

[ LHC WIRESCANNER APP 24-10-04_13:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801608/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:44:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801608/content)

[ LHC WIRESCANNER APP 24-10-04_13:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801610/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:44:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801610/content)

[ LHC WIRESCANNER APP 24-10-04_13:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801612/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:44:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801612/content)

[ LHC WIRESCANNER APP 24-10-04_13:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801614/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:44:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801614/content)


***

### [2024-10-04 13:47:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156022)
>
```
orbit and RT kicks OK after disabling BPM.25L1.B2 in H (had ~2mm offset)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:49:06)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-10-04_13:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801618/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:49:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801618/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-10-04_13:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801620/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 13:47:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801620/content)

[ Power Converter Interlock GUI 24-10-04_13:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801622/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 13:48:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801622/content)


***

### [2024-10-04 13:53:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156028)
>
```
First bunch of each train on the low side - already in SPS, gets more pronounced during scraping. Probably bunches get touched by the kicker pulse...
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:01:33)

[ LHC Fast BCT v1.3.2 24-10-04_13:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801636/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:54:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801636/content)

[ LHC Fast BCT v1.3.2 24-10-04_13:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801638/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 13:54:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801638/content)

[Figure 1 24-10-04_14:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801646/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:00:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801646/content)


***

### [2024-10-04 14:11:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156035)
>Machine Filled

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:11:21)

[ LHC Fast BCT v1.3.2 24-10-04_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801656/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:11:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801656/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-04_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801658/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:11:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801658/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-04_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801660/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/04 14:11:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801660/content)

[ Beam Intensity - v1.5.0 24-10-04_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801662/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:12:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801662/content)

[ LHC Beam Losses Lifetime Display 24-10-04_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801664/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:11:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801664/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-04_14:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801668/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/04 14:14:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801668/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-04_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801710/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/04 14:36:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801710/content)


***

### [2024-10-04 14:11:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156036)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:11:37)


***

### [2024-10-04 14:11:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156037)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:11:47)


***

### [2024-10-04 14:11:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156038)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:11:57)


***

### [2024-10-04 14:11:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156039)
>LHC Injection Complete

Number of injections actual / planned: 53 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:33:23 / 0:33:48 (98.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:11:57)


***

### [2024-10-04 14:13:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156047)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:13:58)


***

### [2024-10-04 14:13:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156049)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:13:59)


***

### [2024-10-04 14:16:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156053)
>
```
After discussing with PS (Alex and Gilles) we see that it is better to remove this task for the moment, relying on the  "Preparation" request (in our preparation sequence) to trigger the PS cavities warm up.

GT
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:11:51)

[ LHC Sequence Editor (DEV) : 12.33.5  24-10-04_14:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801672/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:18:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801672/content)


***

### [2024-10-04 14:35:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156063)
>RAMP

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:35:04)

[ LHC Fast BCT v1.3.2 24-10-04_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801700/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:35:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801700/content)

[ LHC Fast BCT v1.3.2 24-10-04_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801702/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:35:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801702/content)

[ LHC Beam Losses Lifetime Display 24-10-04_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801704/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:35:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801704/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-04_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801706/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/04 14:35:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801706/content)

[Abort gap cleaning control v2.1.0 24-10-04_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801708/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/04 14:35:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801708/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-04_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801712/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/04 14:36:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801712/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-04_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801714/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/04 14:36:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801714/content)


***

### [2024-10-04 14:35:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156064)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:35:19)


***

### [2024-10-04 14:35:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156066)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:35:26)


***

### [2024-10-04 14:43:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156071)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:43:41)


***

### [2024-10-04 14:44:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156072)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:44:19)


***

### [2024-10-04 14:45:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156074)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:45:16)


***

### [2024-10-04 14:47:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156078)
>Lifetime in collisions above 30h for both beams

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:48:06)

[ LHC Beam Losses Lifetime Display 24-10-04_14:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801728/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:48:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801728/content)


***

### [2024-10-04 14:47:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156078)
>Lifetime in collisions above 30h for both beams

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:48:06)

[ LHC Beam Losses Lifetime Display 24-10-04_14:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801728/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 14:48:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801728/content)


***

### [2024-10-04 14:49:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156080)
>Optimizations

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:10:01)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_14:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801732/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:49:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801732/content)


***

### [2024-10-04 14:50:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156082)
>Optimizations

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:10:08)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801734/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:50:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801734/content)


***

### [2024-10-04 14:51:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156083)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:51:02)


***

### [2024-10-04 14:51:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156084)
>ALICE lumi levelling stopped - lumi dropping quickly and step a bit too small  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:11:04)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_14:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801736/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:51:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801736/content)


***

### [2024-10-04 14:52:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156086)
>
```
LHCb arrived high (above 2000 Hz/ub), will increase separation for the next Fill
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:10:25)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_14:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801738/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 14:53:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801738/content)


***

### [2024-10-04 14:57:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156089)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:57:21)


***

### [2024-10-04 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156148)
>
```
***** SHIFT SUMMARY *****  
EoF test of the 3Qy correction with Ewen ([summary](/elogbook-server/GET/showEventInLogbook/4155736)) followed by dump for FASER/SND emulsion exchange access. In the shadow, also various other maintenance accesses have taken place, in particular QDS boards in cells 8-9 around IP5 and IP2 were replaced by more radiation tolerant versions in view of the ion run (due to technical difficulties, one board in IP2 could not be replaced).  
  
Smooth recovery after the access and fill to Stable Beams.  
  
Notes:  
- Both LHCb and ATLAS arrived too high. Increased both separations (by 2*5um and 2*1um, respectively) for next fill.  
- Disabled bad BPM at 25L1 (2mm outlier) at injection which triggered a build-up of a funny structure in S81.  
- Automatic bunch selection during BWS at injection did not work - only 12b selected for B2.  
  
-- Michi (and George for 1h of covering) --  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:19:55)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801960/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:18:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801960/content)


***

### [2024-10-04 14:59:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156092)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/04 14:59:20)


***

### [2024-10-04 15:09:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156096)
>Emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 15:09:39)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_15:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801784/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 15:09:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801784/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_15:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801786/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 15:09:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801786/content)


***

### [2024-10-04 16:01:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156142)
>
```
added 2*1um separation in IP1
and 2*5um in IP8  
  
- Michi  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:12:03)

[ LSA Applications Suite (v 16.6.29) 24-10-04_16:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801917/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:01:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801917/content)

[ LSA Applications Suite (v 16.6.29) 24-10-04_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801921/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 16:02:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3801921/content)


***

### [2024-10-04 22:26:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156236)
>State faulty MKI

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/04 22:26:30)

[Mozilla Firefox 24-10-04_22:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802385/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/04 22:26:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802385/content)

[MKI STATUS  v 0.0.2 24-10-04_23:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802397/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 23:01:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802397/content)


***

### [2024-10-04 22:54:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156242)
>
```
Heat load was going up for a while.Cryo thinks that we were more than say 60% of the limit where we would have been in troubles. In fact we were very close to the point where we would have had issues. 
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 22:57:36)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-04_22:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802393/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 22:56:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802393/content)


***

### [2024-10-04 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156246)
>
```
**SHIFT SUMMARY:**  
*Stable beams most part of the shift  
*Some heating issues after ~5h in stable beams in R5 triplet. CMS has increased the pile up to 64 so seem to be linked. In our tool it is still at 60% but cryo said that for them it looks like it was much closer than that.  
*MKIB2 went to status faulty (more details in first entry next shift)  
  
Leaving in stable beams  
  
/Tobias    
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/04 23:15:51)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802405/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/04 23:15:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802405/content)


***

### [2024-10-04 23:00:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156244)
>Event created from ScreenShot Client.  
MKI STATUS v 0.0.2 24-10-04\_23:00.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 23:00:57)

[MKI STATUS  v 0.0.2 24-10-04_23:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802395/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 23:00:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802395/content)

[ WinCC - Operation 24-10-04_23:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802399/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 23:10:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802399/content)

[ WinCC - Operation 24-10-04_23:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802401/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 23:12:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802401/content)

[ WinCC - Operation 24-10-04_23:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802403/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/04 23:12:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802403/content)


***

### [2024-10-04 23:26:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156248)
>Head On in both IPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 23:27:03)

[ Page 1 Comment 24-10-04_23:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802410/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 23:27:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802410/content)


***

### [2024-10-04 23:29:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156250)
>Turning ON BBLR

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 23:29:02)

[Set BBLR_PC 24-10-04_23:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802416/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/04 23:31:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802416/content)

[ LHC BBLR Wire App v0.5.2 24-10-04_23:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802418/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 23:31:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802418/content)

[ LHC Beam Losses Lifetime Display 24-10-04_23:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802422/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 23:31:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802422/content)


***

### [2024-10-04 23:33:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156251)
>
```
Quick tune scan, no better Tune working point was found
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/04 23:42:36)

[ LHC Beam Losses Lifetime Display 24-10-04_23:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802430/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/04 23:34:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802430/content)


***

### [2024-10-05 01:34:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156264)
>Event created from ScreenShot Client.  
DIAMON console [PROD] 2.6.1 - 
 UNKNOWN as LHCOP - LHC 24-10-05\_01:34.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 01:34:40)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-10-05_01:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802577/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 01:34:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802577/content)


***

### [2024-10-05 03:59:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156279)
>Global Post Mortem Event

Event Timestamp: 05/10/24 03:59:10.380
Fill Number: 10199
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 23097 / 23514 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.US15.L1.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/05 04:02:00)


***

### [2024-10-05 03:59:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156307)
>Global Post Mortem Event Confirmation

Dump Classification: Power converter fault(s)
Operator / Comment: gtrad / Trip of orbit corrector in S81 dumping Physics beam


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/05 04:13:57)


***

### [2024-10-05 03:59:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156276)
>Trip of Orbit corrector causing dump

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/05 03:59:38)

[Set SECTOR81 24-10-05_03:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802604/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/05 03:59:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802604/content)

[ Equip State 24-10-05_04:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802606/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:03:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802606/content)

[ Equip State 24-10-05_04:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802608/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:03:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802608/content)


***

### [2024-10-05 04:00:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156277)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/05 04:00:34)


***

### [2024-10-05 04:00:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156278)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/05 04:00:37)


***

### [2024-10-05 04:04:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156280)
>Event created from ScreenShot Client.  
udp:..multicast-bevlhc3:1234 - 
 VLC media player 24-10-05\_04:04.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:04:22)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-10-05_04:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802610/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:04:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802610/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-05_04:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802612/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:04:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802612/content)

[ LHC Fast BCT v1.3.2 24-10-05_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802614/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:06:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802614/content)

[fgc-data-red >> Version: 5.0.11  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-10-05_04:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802616/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/05 04:09:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3802616/content)


