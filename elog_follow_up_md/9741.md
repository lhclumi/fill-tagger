# FILL 9741
**start**:		 2024-06-09 00:34:48.620363525+02:00 (CERN time)

**end**:		 2024-06-09 02:19:17.809238525+02:00 (CERN time)

**duration**:	 0 days 01:44:29.188875


***

### [2024-06-09 00:34:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084200)
>LHC RUN CTRL: New FILL NUMBER set to 9741

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 00:34:48)


***

### [2024-06-09 00:35:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084201)
>Event created from ScreenShot Client.  
LHC Beam Quality Monitor - LHC.USER.ALL - INCA DISABLED - PRO CCDA 24-06-09\_00:35.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 00:35:18)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-09_00:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630644/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 00:35:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630644/content)


***

### [2024-06-09 00:35:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084202)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 00:35:33)


***

### [2024-06-09 00:36:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084208)
>Global Post Mortem EventEvent Timestamp: 09/06/24 00:36:30.113Fill Number: 9741Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PROBE BEAMEnergy: 449760 [MeV]Intensity B1/B2: 0 / 0 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 3-MKI2: A T -> F on CIB.UA27.R2.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/09 00:39:22)


***

### [2024-06-09 00:36:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084218)
>Global Post Mortem Event ConfirmationDump Classification: OtherOperator / Comment: dmirarch / DUMP with postmortem without beam occurred when arming the LBDS

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/09 00:56:41)


***

### [2024-06-09 00:37:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084206)
>got a dump with PM when arming the LBDS, with TCDQ\_BPMS

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:38:15)

[ XPOC Viewer - PRO 24-06-09_00:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630649/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:38:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630649/content)


***

### [2024-06-09 00:38:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084207)
>
```
RETRIG error on beam 1, called the SY-ABT piquet who called Nicolas Magnin
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:11:30)

[ XPOC Viewer - PRO 24-06-09_00:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630653/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:39:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630653/content)

[ XPOC Viewer - PRO 24-06-09_01:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630681/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:11:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630681/content)


***

### [2024-06-09 00:40:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084209)
>Event created from ScreenShot Client.  
24-06-09\_00:40.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:40:27)

[24-06-09_00:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630655/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:40:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630655/content)


***

### [2024-06-09 00:47:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084214)
>Event created from ScreenShot Client.  
Generation Application connected to server LHC (PRO database) 24-06-09\_00:47.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:47:06)

[ Generation Application connected to server LHC (PRO database) 24-06-09_00:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630659/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:47:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630659/content)


***

### [2024-06-09 01:17:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084222)
>|\*\*\* XPOC error has been reset by user 'nmagnin' at 09.06.2024 01:17:33| Comment: Problem with RTRIG module, BIS pulse not detetced.|| Dump info: BEAM 1 at 09.06.2024 00:36:30| Beam info: Energy= 449.76 GeV ; Intensity= 0.00E0 p+ ; #Bunches= 0| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR

creator:	 nmagnin  @cs-ccr-pm3.cern.ch (2024/06/09 01:17:33)


***

### [2024-06-09 01:19:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084223)
>Event created from ScreenShot Client.  
LHC Beam Quality Monitor - LHC.USER.ALL - INCA DISABLED - PRO CCDA 24-06-09\_01:19.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 01:20:00)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-09_01:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630683/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 01:20:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630683/content)


***

### [2024-06-09 01:20:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084225)
>Global Post Mortem EventEvent Timestamp: 09/06/24 01:20:39.717Fill Number: 9741Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PROBE BEAMEnergy: 449760 [MeV]Intensity B1/B2: 0 / 0 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/09 01:23:32)


***

### [2024-06-09 01:23:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084226)
>Nicolas Magnin called back and said we can continue. It is not clear what happened.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:24:11)


***

### [2024-06-09 01:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084229)
>blocked by Post Mortem SIS interlock (the MACH\_PROT one)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 01:27:02)

[ LHC-INJ-SIS Status 24-06-09_01:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630687/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 01:27:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630687/content)

[ LHC SIS GUI 24-06-09_01:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630689/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 01:27:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630689/content)


***

### [2024-06-09 01:30:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084239)
>
```
masked POST_MORTEM_MACH_PROT_OK after OK from Jorg.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 02:01:50)

[ LHC SIS GUI 24-06-09_01:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630707/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 01:59:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630707/content)


***

### [2024-06-09 01:46:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084231)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-09\_01:46.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 01:46:21)

[Accelerator Cockpit v0.0.37 24-06-09_01:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630695/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 01:46:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630695/content)


***

### [2024-06-09 01:47:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084232)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 01:47:40)


***

### [2024-06-09 01:51:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084233)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:11)

[tmpScreenshot_1717890670.8385165.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630697/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630697/content)


***

### [2024-06-09 01:51:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084234)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:14)

[tmpScreenshot_1717890674.062494.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630699/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630699/content)


***

### [2024-06-09 01:51:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084235)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:16)

[tmpScreenshot_1717890676.6784754.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630701/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630701/content)


***

### [2024-06-09 01:51:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084236)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:19)

[tmpScreenshot_1717890679.046528.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630703/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 01:51:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630703/content)


***

### [2024-06-09 01:51:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084237)
>
```
TI operator informed that the harmonic filter SEQ4 for the powering station LHC4 is in fault. He is calling the EPC piquet  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 01:53:15)


***

### [2024-06-09 02:13:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084241)
>forced SBF to NORMAL

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 02:13:58)

[Safe Machine Parameters in CCC : Overview GUI 24-06-09_02:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630711/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 02:13:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630711/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.6  24-06-09_02:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630713/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 02:14:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630713/content)


