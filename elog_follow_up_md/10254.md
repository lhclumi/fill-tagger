# FILL 10254
**start**:		 2024-10-19 20:35:29.013238525+02:00 (CERN time)

**end**:		 2024-10-19 22:59:14.841238525+02:00 (CERN time)

**duration**:	 0 days 02:23:45.828000


***

### [2024-10-19 20:35:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167350)
>LHC RUN CTRL: New FILL NUMBER set to 10254

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:35:29)


***

### [2024-10-19 20:36:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167357)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:36:07)


***

### [2024-10-19 20:36:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167358)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:36:56)


***

### [2024-10-19 20:38:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167359)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:38:09)


***

### [2024-10-19 20:39:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167361)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:39:29)


***

### [2024-10-19 20:39:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167362)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:39:31)


***

### [2024-10-19 20:39:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167364)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:39:42)


***

### [2024-10-19 20:40:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167366)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:40:57)


***

### [2024-10-19 20:41:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167369)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:41:34)


***

### [2024-10-19 20:41:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167371)
>Reverted BIS masks

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 20:41:58)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-10-19_20:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830844/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 20:41:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830844/content)


***

### [2024-10-19 20:42:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167373)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:42:07)


***

### [2024-10-19 20:42:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167374)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:42:41)


***

### [2024-10-19 20:43:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167375)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:00)


***

### [2024-10-19 20:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167377)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:01)


***

### [2024-10-19 20:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167377)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:01)


***

### [2024-10-19 20:44:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167379)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:06)


***

### [2024-10-19 20:44:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167383)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:21)


***

### [2024-10-19 20:44:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167385)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:44:54)


***

### [2024-10-19 20:45:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167387)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:45:01)


***

### [2024-10-19 20:46:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167388)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:46:20)


***

### [2024-10-19 20:48:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167390)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:48:04)


***

### [2024-10-19 20:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167392)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:48:44)


***

### [2024-10-19 21:02:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167396)
>masked ADT bunch intensity and set desired intensity to 2.2e11

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 21:02:59)

[ LHC-INJ-SIS Status 24-10-19_21:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830866/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 21:03:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830866/content)

[ INJECTION SEQUENCER  v 5.1.12 24-10-19_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830868/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:03:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830868/content)


***

### [2024-10-19 21:05:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167397)
>
```
**MD13883 Summary**  
  
After many delays (see earlier entrees), the optics measurements started at 19:45 (instead of 8:30). It was possible to perform few on/off momentum kicks at beta* 60/18 only for beam 2. For beam 1 due to AC dipole issues only on momentum kicks are performed. From the analysis of both beam the beta beating at H/V plains is below ~10% for beam 2 while for beam 1 ~20% at H and ~5% at V. The beams are dumped after 30 minutes for the next MD.  
  
  

```


creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/10/19 21:23:51)

[BEAM 1 beta beat.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830894/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/10/19 21:20:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830894/content)

[BEAM 2 beta beat.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830896/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/10/19 21:20:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830896/content)


***

### [2024-10-19 21:07:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167398)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 21:07:42)


***

### [2024-10-19 21:24:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167404)
>loaded RF settings for high intensity according to shift summary of RF MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:24:25)

[ LSA Applications Suite (v 16.6.34) 24-10-19_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830898/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:26:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830898/content)


***

### [2024-10-19 21:30:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167406)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 21:30:38)


***

### [2024-10-19 21:38:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167410)
>Event created from ScreenShot Client.  
 LHC RF CONTROL 24-10-19\_21:38.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/19 21:38:11)

[ LHC RF CONTROL 24-10-19_21:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830902/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/19 21:38:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830902/content)


***

### [2024-10-19 21:49:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167415)
>Event created from ScreenShot Client.  
 BSRT-vs-Emit v0.1.2 - January 2024 - INCA DISABLED - PRO CCDA 24-10-19\_21:49.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:21)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830912/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830912/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830914/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830914/content)

[LHC Cryogenic Heat Load 2.2.3 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830916/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830916/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830918/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830918/content)

[ LHC Beam Losses Lifetime Display 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830920/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830920/content)

[ LHC Fast BCT v1.3.2 24-10-19_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830922/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:49:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830922/content)


***

### [2024-10-19 21:56:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167417)
>First fill at 2.3e11

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:56:16)

[ LHC Fast BCT v1.3.2 24-10-19_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830928/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:56:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830928/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830930/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 22:16:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830930/content)

[Abort gap cleaning control v2.1.0 24-10-19_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830932/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 21:56:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830932/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-19_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830934/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/19 21:56:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830934/content)


***

### [2024-10-19 21:57:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167418)
>Event created from ScreenShot Client.  
LHC BLM Fixed Display 
 24-10-19\_21:57.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/19 21:57:16)

[ LHC BLM Fixed Display 24-10-19_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830936/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/19 21:57:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830936/content)


***

### [2024-10-19 21:57:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167419)
>Event created from ScreenShot Client.  
LHC Cryogenic Heat Load 2.2.3 24-10-19\_21:57.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:57:55)

[LHC Cryogenic Heat Load 2.2.3 24-10-19_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830938/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:06:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830938/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-19_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830940/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:57:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830940/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-19_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830942/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:58:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830942/content)

[ LHC Fast BCT v1.3.2 24-10-19_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830944/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:58:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830944/content)

[ LHC Beam Losses Lifetime Display 24-10-19_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830946/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:58:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830946/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830948/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 21:58:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830948/content)


***

### [2024-10-19 22:00:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167420)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 22:00:11)


***

### [2024-10-19 22:09:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167423)
>Event created from ScreenShot Client.  
LHC Cryogenic Heat Load 2.2.3 24-10-19\_22:09.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:36)

[LHC Cryogenic Heat Load 2.2.3 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830950/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830950/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830952/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830952/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830954/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830954/content)

[ LHC Fast BCT v1.3.2 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830956/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830956/content)

[ LHC Beam Losses Lifetime Display 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830958/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830958/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830960/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830960/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830962/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:09:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830962/content)


***

### [2024-10-19 22:22:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167425)
>Event created from ScreenShot Client.  
LHC Cryogenic Heat Load 2.2.3 24-10-19\_22:22.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:22:54)

[LHC Cryogenic Heat Load 2.2.3 24-10-19_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830970/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:22:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830970/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-19_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830972/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:22:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830972/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-19_22:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830974/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:23:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830974/content)

[ LHC Fast BCT v1.3.2 24-10-19_22:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830976/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:23:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830976/content)

[ LHC Beam Losses Lifetime Display 24-10-19_22:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830978/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:23:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830978/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-10-19_22:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830980/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:23:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830980/content)

[ LHC Fast BCT v1.3.2 24-10-19_22:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830982/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:23:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830982/content)


***

### [2024-10-19 22:56:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167440)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.6.34) 24-10-19\_22:56.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 22:56:06)

[ LSA Applications Suite (v 16.6.34) 24-10-19_22:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831000/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 22:56:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831000/content)


***

### [2024-10-19 22:57:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167442)
>Started the shift with MD13883 waiting for beam.  
The problem was CMS BCM interlock that was active due to a communication issue in CMS.  
A network switch was not working and it prevented the BCM crate to release the interlock.  
CMS and IT replaced the switch and with the help of BI the situation was restored.  
MD13883 summary: https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4167397  
  
We decided to attempt to arrive at the end of the second squeeze (60\_18) but the time was very tight.  
The cycle with 1 pilot was smooth. At 60cm I flattened the machine and inverted crossing angles of IP1 and IP5 according to instructions.  
The settings manipulation at 60cm were more complicated than expected to make the new squeeze loadable.  
In the end I managed to synchronize everithing and correctly setup the OFB and started the squeeze to 60\_18.  
Once arrived the MD team managed to do some measures for ~30min.  
  
Dumped and prepare for the MD9551 with trains at high intensity (2.3e11) already prepared in the SPS.  
I loaded the RF settings that were in the RF MD summary (https://logbook.cern.ch/elogbook-server/#/logbook?logbookId=322&dateFrom=2024-10-19T01:48:44&dateTo=2024-10-19T07:48:44&eventToHighlight=4166955).  
Injection of trains of 2.3e11 was ok. Some ~40% losses.  
  
  
Notes:  
- the ADT\_BUNCH\_INTENSITY\_B1 / B2 is masked in SIS   
- required bunch intensity from injection sequencer is 2.32e11  
- RF settings for high intensity to be reverted !  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 22:57:53)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831004/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 22:57:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831004/content)


***

### [2024-10-19 22:58:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167443)
>Event created from ScreenShot Client.  
LHC Cryogenic Heat Load 2.2.3 24-10-19\_22:58.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 22:58:21)

[LHC Cryogenic Heat Load 2.2.3 24-10-19_22:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831005/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/19 23:34:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831005/content)


***

### [2024-10-19 22:58:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167449)
>Global Post Mortem Event

Event Timestamp: 19/10/24 22:58:54.751
Fill Number: 10254
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 21665 / 21588 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/19 23:01:46)


