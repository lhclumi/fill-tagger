# FILL 10307
**start**:		 2024-10-31 14:15:47.690238525+01:00 (CERN time)

**end**:		 2024-11-01 09:28:04.949488525+01:00 (CERN time)

**duration**:	 0 days 19:12:17.259250


***

### [2024-10-31 14:15:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175145)
>LHC RUN CTRL: New FILL NUMBER set to 10307

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:15:51)


***

### [2024-10-31 14:22:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175158)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:22:39)


***

### [2024-10-31 14:25:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175161)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:25:01)


***

### [2024-10-31 14:26:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175163)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:26:58)


***

### [2024-10-31 14:27:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175165)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:27:27)


***

### [2024-10-31 14:29:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175170)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:29:02)


***

### [2024-10-31 14:31:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175172)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:25)


***

### [2024-10-31 14:31:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175174)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:31)


***

### [2024-10-31 14:31:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175177)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:33)


***

### [2024-10-31 14:31:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175176)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:33)


***

### [2024-10-31 14:31:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175179)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:34)


***

### [2024-10-31 14:31:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175183)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:31:49)


***

### [2024-10-31 14:33:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175189)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:33:48)


***

### [2024-10-31 14:35:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175192)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:35:32)


***

### [2024-10-31 14:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175205)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:48:44)


***

### [2024-10-31 14:51:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175207)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 14:51:16)


***

### [2024-10-31 15:11:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175218)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:11:35)


***

### [2024-10-31 15:15:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175222)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:15:40)


***

### [2024-10-31 15:18:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175228)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:18:07)


***

### [2024-10-31 15:18:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175232)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:18:41)


***

### [2024-10-31 15:23:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175237)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:23:58)


***

### [2024-10-31 15:40:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175413)
>Unfortunately the RF intervention was not successful, the SPARE card was removed and we are back to the original one. With Mik we agree that we go for another attempt, highly likely the problem will reoccur: He will investigate further a prepare a new card for the next intervention.  
For the moment we start the filling and if the blowup fails to trigger at start of Ramp, he agrees I can launch it manually via the fesa class.  
  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:22:48)


***

### [2024-10-31 15:40:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175251)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 15:40:56)


***

### [2024-10-31 15:59:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175269)
>Double checking ATLAS sep is introduced in the Physics BP

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:00:15)

[ LSA Applications Suite (v 16.6.35) 24-10-31_16:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847966/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:00:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847966/content)


***

### [2024-10-31 16:13:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175272)
>
```
Machine filled
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:12:00)

[ LHC Fast BCT v1.3.2 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847976/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:13:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847976/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847978/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:13:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847978/content)

[ LHC WIRESCANNER APP 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847982/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:13:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847982/content)

[ LHC WIRESCANNER APP 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847984/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:13:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847984/content)

[ LHC WIRESCANNER APP 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847986/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:13:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847986/content)

[ LHC WIRESCANNER APP 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847988/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/31 16:13:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847988/content)

[ LHC Beam Losses Lifetime Display 24-10-31_16:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847990/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:13:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847990/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-31_16:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847992/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 16:14:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847992/content)


***

### [2024-10-31 16:13:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175273)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:14:00)


***

### [2024-10-31 16:14:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175274)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:14:11)


***

### [2024-10-31 16:14:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175275)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:14:15)


***

### [2024-10-31 16:14:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175276)
>LHC Injection Complete

Number of injections actual / planned: 52 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:33:21 / 0:33:48 (98.7 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:14:16)


***

### [2024-10-31 16:15:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175277)
>QFB ON

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:15:30)

[ LHC Beam Losses Lifetime Display 24-10-31_16:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847994/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:15:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847994/content)


***

### [2024-10-31 16:15:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175278)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:15:50)


***

### [2024-10-31 16:15:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175280)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:15:52)


***

### [2024-10-31 16:16:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175282)
>
```
Long. Blowup started!!!  
As agreed with Mik, I was ready to start manually the blowup of the RAMP, in case the issue happens again (it is highly likely to re-occur)  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:11:47)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-31_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847996/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 16:16:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3847996/content)


***

### [2024-10-31 16:25:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175288)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:25:07)


***

### [2024-10-31 16:25:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175290)
>RAMP

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:25:29)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-31_16:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848003/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:25:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848003/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-31_16:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848005/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 16:25:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848005/content)

[ LHC Beam Losses Lifetime Display 24-10-31_16:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848007/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:25:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848007/content)

[ LHC Fast BCT v1.3.2 24-10-31_16:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848011/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:26:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848011/content)

[ LHC Fast BCT v1.3.2 24-10-31_16:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848013/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:26:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848013/content)


***

### [2024-10-31 16:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175291)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:26:29)


***

### [2024-10-31 16:27:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175292)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:27:14)


***

### [2024-10-31 16:27:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175296)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:27:56)


***

### [2024-10-31 16:30:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175297)
>ATLAS separation to be increased for the next fill

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:30:39)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_16:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848017/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:30:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848017/content)

[ LSA Applications Suite (v 16.6.35) 24-10-31_23:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848320/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:16:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848320/content)


***

### [2024-10-31 16:30:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175297)
>ATLAS separation to be increased for the next fill

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:30:39)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_16:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848017/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:30:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848017/content)

[ LSA Applications Suite (v 16.6.35) 24-10-31_23:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848320/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:16:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848320/content)


***

### [2024-10-31 16:31:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175298)
>Optimized all IPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:31:41)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_16:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848019/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:31:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848019/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_16:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848021/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:31:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848021/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_16:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848023/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 16:31:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848023/content)


***

### [2024-10-31 16:32:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175299)
>Same losses as yeterday in IP1/5 in long RS

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:33:05)

[ LHC BLM Fixed Display 24-10-31_16:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848025/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:33:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848025/content)


***

### [2024-10-31 16:33:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175300)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:33:28)


***

### [2024-10-31 16:34:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175301)
>The tune crossing!

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:34:11)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-31_16:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848027/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/31 16:34:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848027/content)


***

### [2024-10-31 16:40:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175303)
>0.5MV added inRF total volattge

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 16:40:30)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-31_16:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848033/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 16:40:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848033/content)


***

### [2024-10-31 16:41:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175304)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/31 16:41:42)


***

### [2024-10-31 16:42:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175305)
>
```
Not much margin to play at start of fill with tunes to optimize lifetime
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:10:16)

[ LHC Beam Losses Lifetime Display 24-10-31_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848035/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:42:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848035/content)

[ LHC Beam Losses Lifetime Display 24-10-31_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848037/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 16:42:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848037/content)


***

### [2024-10-31 17:32:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175326)
>Bunch length evolution  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:08:38)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-31_17:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848097/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/31 17:32:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848097/content)


***

### [2024-10-31 18:20:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175343)
>Event created from ScreenShot Client.  
LHC Beam Losses Lifetime Display 
 24-10-31\_18:20.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:20:51)

[ LHC Beam Losses Lifetime Display 24-10-31_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848172/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:20:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848172/content)


***

### [2024-10-31 18:42:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175348)
>
```
During the Lumi optimization autopilot wavy peroiodic oscillations on lifetime for both beams; Since crosssection is too high (~6x more than burn-off level) it hints that the extra losses are also linked somehow to the lumi production. (either tune or orbit deviation could be the source of this observation on the lifetime)  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:08:21)

[ LHC Beam Losses Lifetime Display 24-10-31_18:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848182/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:42:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848182/content)

[ LHC Beam Losses Lifetime Display 24-10-31_18:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848184/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:42:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848184/content)

[ LHC Beam Losses Lifetime Display 24-10-31_18:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848186/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:43:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848186/content)

[ LHC Beam Losses Lifetime Display 24-10-31_18:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848188/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:43:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848188/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848190/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 18:44:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848190/content)

[ LHC Beam Losses Lifetime Display 24-10-31_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848194/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/31 18:46:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848194/content)


***

### [2024-10-31 22:04:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175406)
>ALICE lumi is down, levelling task failed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 22:05:10)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_22:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848310/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 22:05:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848310/content)

[Task failed : diagnostics 24-10-31_22:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848312/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 22:05:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848312/content)


***

### [2024-10-31 22:26:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175408)
>
```
Started ALICE OP levelling on BLM signals
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 22:59:26)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_22:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848318/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 22:26:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848318/content)


***

### [2024-10-31 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175414)
>
```
**SHIFT SUMMARY:**  
  
Smooth cycle to STABLE beams;  
  
Lifetime are better going in collisions (>15h), and work point for an optimization to >50h is consistent with the previous fills after the first hour of collisions.  
  
ATLAS lumi was higher than required mu=4, I increased the separation by 1 sigma for the next fill  
  
Long. blowup in the RAMP is still risking to fail in the coming cycles, a new intervention is needed by the experts. Meanwhile a manual triggering via FESA is tolerated in case of failure.  
  
~~George~~  
  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:26:56)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848330/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/31 23:26:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848330/content)


***

### [2024-10-31 23:52:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175415)
>IP1 levelling out of steam

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 23:52:16)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-31_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848341/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/31 23:52:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848341/content)


***

### [2024-11-01 02:23:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175421)
>
```
For Helga and RF: when resetting the longitudinal blow-up, the values in the "noise" buffer are zeroed, but NOT the values in "noiseScaled". Could this explain the funny bunch length jumps in the first 1-2s of the ramp...?
```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/01 02:24:50)

[ NavPy 1.6.4 24-11-01_02:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848393/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/01 02:23:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848393/content)


***

### [2024-11-01 02:56:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175423)
>small tune scan

all beams/planes -1e-3

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 02:56:46)

[ LHC Beam Losses Lifetime Display 24-11-01_02:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848411/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 02:56:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848411/content)


***

### [2024-11-01 05:53:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175435)
>
```
B1 lifetime (and effective cross-section) worse than B2. Tried tune optimization, no significantly better configuration found.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/01 05:57:23)

[ LHC Beam Losses Lifetime Display 24-11-01_05:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848453/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 05:54:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848453/content)

[ LHC Beam Losses Lifetime Display 24-11-01_05:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848457/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 05:56:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848457/content)


***

### [2024-11-01 06:00:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175437)
>
```
B1 Q' = 15/15 ... almost doubles lifetime.  
  
... we are paying for the RF voltage bunch length levelling with a higher dp/p and hence get a bigger tune footprint with high chroma!  
  
B1 also clearly sees some longitudinal shaving.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/01 06:05:10)

[ Accelerator Cockpit v0.0.42 24-11-01_06:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848459/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/01 06:00:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848459/content)

[ LHC Beam Losses Lifetime Display 24-11-01_06:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848461/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 06:00:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848461/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-01_06:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848470/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/01 06:05:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848470/content)


***

### [2024-11-01 06:02:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175440)
>octupoles -10% - no big effect.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/01 06:02:47)

[ LSA Applications Suite (v 16.6.35) 24-11-01_06:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848466/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/01 06:02:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848466/content)

[ LHC Beam Losses Lifetime Display 24-11-01_06:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848468/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 06:02:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848468/content)


***

### [2024-11-01 06:09:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175441)
>funny RT trim structure in B2H in cells ~9R4-13R4

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/01 06:09:57)

[OpenYASP DV LHCRING . PHYSICS-2.68TeV-3.1m-2024_V1@120_[END] 24-11-01_06:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848476/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/01 06:09:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848476/content)

[ Power Converter Interlock GUI 24-11-01_06:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848478/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/01 06:10:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848478/content)


***

### [2024-11-01 06:15:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175443)
>Both RF voltages have reached 13 MV

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/01 06:16:12)

[ LSA Applications Suite (v 16.6.35) 24-11-01_06:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848486/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/01 06:16:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848486/content)


***

### [2024-11-01 08:33:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175464)
>Event created from ScreenShot Client.  
LHC Beam Quality Monitor - 
 LHC.USER.ALL - INCA DISABLED - PRO CCDA 24-11-01\_08:33.png

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/01 08:33:25)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-01_08:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848552/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/01 08:33:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3848552/content)


***

### [2024-11-01 09:21:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175481)
>Global Post Mortem Event

Event Timestamp: 01/11/24 09:21:58.196
Fill Number: 10307
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 2679600 [MeV]
Intensity B1/B2: 27707 / 30586 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 14-FMCM\_RQ5.LR3: A T -> F on CIB.SR3.S3.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/01 09:24:55)


***

### [2024-11-01 09:23:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4175480)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/01 09:23:22)


