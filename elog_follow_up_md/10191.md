# FILL 10191
**start**:		 2024-10-03 03:39:07.291613525+02:00 (CERN time)

**end**:		 2024-10-03 07:39:35.703863525+02:00 (CERN time)

**duration**:	 0 days 04:00:28.412250


***

### [2024-10-03 03:39:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154947)
>LHC RUN CTRL: New FILL NUMBER set to 10191

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:39:08)


***

### [2024-10-03 03:39:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154953)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:39:45)


***

### [2024-10-03 03:41:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154954)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:41:49)


***

### [2024-10-03 03:41:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154956)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:41:54)


***

### [2024-10-03 03:43:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154957)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:43:12)


***

### [2024-10-03 03:43:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154959)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:43:21)


***

### [2024-10-03 03:44:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154960)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:44:27)


***

### [2024-10-03 03:44:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154965)
>changed filling scheme to standard production (pre-md)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 03:45:36)

[ INJECTION SEQUENCER  v 5.1.12 24-10-03_03:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798589/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 03:46:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798589/content)


***

### [2024-10-03 03:44:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154961)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:44:41)


***

### [2024-10-03 03:45:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154963)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:45:15)


***

### [2024-10-03 03:45:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154967)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:45:47)


***

### [2024-10-03 03:47:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154969)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:47:41)


***

### [2024-10-03 03:47:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154969)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:47:41)


***

### [2024-10-03 03:47:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154971)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:47:41)


***

### [2024-10-03 03:47:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154973)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:47:43)


***

### [2024-10-03 03:47:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154977)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:47:58)


***

### [2024-10-03 03:48:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154979)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:48:37)


***

### [2024-10-03 03:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154981)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:48:43)


***

### [2024-10-03 03:49:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154982)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:49:56)


***

### [2024-10-03 03:51:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154984)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 03:51:40)


***

### [2024-10-03 04:00:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154986)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 04:00:09)


***

### [2024-10-03 04:00:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154987)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 04:00:14)


***

### [2024-10-03 04:13:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154989)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 04:13:04)


***

### [2024-10-03 04:14:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154990)
>
```
QPS issue in RB.A81. Calling QPS piquet to check on the lost FIP connection with MB.B9L1
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 04:17:59)

[Module QPS_81:A81.RB.A81.DL1K: (NoName) 24-10-03_04:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798595/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 04:15:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798595/content)

[K - MB.B9L1 DQAMC N type MB for dipole MB.B9L1    24-10-03_04:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798597/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 04:15:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798597/content)


***

### [2024-10-03 04:35:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154992)
>IR2 correction to be propagated to PcInterlock?

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/03 04:36:10)

[ Power Converter Interlock GUI 24-10-03_04:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798605/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/03 04:36:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798605/content)


***

### [2024-10-03 04:44:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154994)
>MATHIEU FAVRE(MFAVRE) assigned RBAC Role: QPS-Piquet and will expire on: 03-OCT-24 10.44.40.500000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/03 04:44:42)


***

### [2024-10-03 05:20:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154997)
>at the request of the QPS piquet I restarted first the FESA class and then rebooted the gateway concerned  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 05:23:14)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-10-03_05:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798629/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 05:20:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798629/content)

[Reboot cfc-sr1-dl1jk 24-10-03_05:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798631/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 05:22:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798631/content)


***

### [2024-10-03 05:25:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154998)
>switching OFF RB.A81 at the request of QPS piquet to attempt a last procedure before access..  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 05:25:57)


***

### [2024-10-03 05:26:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154999)
>Global Post Mortem Event

Event Timestamp: 03/10/24 05:26:35.013
Fill Number: 10191
Accelerator / beam mode: PROTON PHYSICS / INJECTION PROBE BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 0 / 0 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: SW Permit: B T -> F on CIB.SR3.S3.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/03 05:29:23)


***

### [2024-10-03 05:26:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155020)
>Global Post Mortem Event Confirmation

Dump Classification: Breaking of BPL without beam
Operator / Comment: acalia / access


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/03 07:01:38)


***

### [2024-10-03 05:42:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155000)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 05:42:21)


***

### [2024-10-03 05:42:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155001)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 05:42:39)


***

### [2024-10-03 05:48:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155005)
>QPS piquet need to access P1 tunnel. I contact RP  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 05:48:55)


***

### [2024-10-03 06:56:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155018)
>Started the shift in stable beams.  
  
BLMBI.11R5.B0T20\_MBB-LEGR\_11R5 showed various spikes in losses (only this one BLM) and finally dumped the beam around 03:30.  
The dump was caught by the UFO buster but it is not fully clear to me. Maybe to be checked offline.  
  
Switched to production filling scheme and going to injection I found that MB.B9L1 (RB.A81) lost FIP communication.  
I contacted MPE piquet and we tried all the resets but finally an access is needed.  
  
Leaving the machine prepared for access in P1.  
  
Notes:  
- check BBLR settings that cannot be driven, ref mismatch on RQTs  
- ADT v1b1 again tripped during the previous ramp  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 06:56:30)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798654/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 06:56:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798654/content)


***

### [2024-10-03 07:36:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155022)
>Problem solved with the QPS.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 07:36:18)

[Module QPS_81:A81.RB.A81.DL1K: (NoName) 24-10-03_07:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798657/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 07:36:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798657/content)


***

### [2024-10-03 07:37:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155023)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 07:37:04)


***

### [2024-10-03 07:39:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155024)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 07:39:17)


