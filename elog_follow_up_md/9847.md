# FILL 9847
**start**:		 2024-07-01 11:54:44.422738525+02:00 (CERN time)

**end**:		 2024-07-01 14:18:23.676988525+02:00 (CERN time)

**duration**:	 0 days 02:23:39.254250


***

### [2024-07-01 11:54:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097809)
>LHC RUN CTRL: New FILL NUMBER set to 9847

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 11:54:45)


***

### [2024-07-01 11:56:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097817)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 11:56:04)


***

### [2024-07-01 11:58:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097824)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 11:58:08)


***

### [2024-07-01 11:58:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097827)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 11:58:17)


***

### [2024-07-01 12:00:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097832)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:00:16)


***

### [2024-07-01 12:00:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097835)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:00:47)


***

### [2024-07-01 12:00:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097837)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:00:56)


***

### [2024-07-01 12:02:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097842)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:02:21)


***

### [2024-07-01 12:04:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097847)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:04:46)


***

### [2024-07-01 12:04:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097849)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:04:48)


***

### [2024-07-01 12:04:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097851)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:04:50)


***

### [2024-07-01 12:04:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097854)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:04:53)


***

### [2024-07-01 12:05:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097856)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:05:03)


***

### [2024-07-01 12:05:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097859)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:05:07)


***

### [2024-07-01 12:07:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097863)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:07:06)


***

### [2024-07-01 12:08:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097870)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:08:50)


***

### [2024-07-01 12:27:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097887)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:27:01)


***

### [2024-07-01 12:27:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097888)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 12:27:05)


***

### [2024-07-01 12:29:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097889)
>RF power piquet needs an access to exchange a PLC that cannot be reached remotely  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 12:29:30)


***

### [2024-07-01 14:18:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097994)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 14:18:14)


***

### [2024-07-01 14:18:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097995)
>LHC RUN CTRL: New FILL NUMBER set to 9848

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 14:18:24)


