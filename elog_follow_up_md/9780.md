# FILL 9780
**start**:		 2024-06-16 00:12:54.893488600+02:00 (CERN time)

**end**:		 2024-06-16 01:59:25.408988525+02:00 (CERN time)

**duration**:	 0 days 01:46:30.515499925


***

### [2024-06-16 00:12:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087613)
>LHC RUN CTRL: New FILL NUMBER set to 9780

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:12:55)


***

### [2024-06-16 00:13:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087614)
>DANIELE MIRARCHI(DMIRARCH) assigned RBAC Role: CO-Timing-PIQUET and will expire on: 16-JUN-24 02.13.26.844000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/16 00:13:28)


***

### [2024-06-16 00:14:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087621)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:14:29)


***

### [2024-06-16 00:14:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087622)
>|\*\*\* XPOC error has been reset by user 'delph' at 16.06.2024 00:14:43| Comment: Asynch dump test|| Dump info: BEAM 1 at 16.06.2024 00:10:35| Beam info: Energy= 6799.80 GeV ; Intensity= 0.00E0 p+ ; #Bunches= 8| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR

creator:	 delph  @cs-ccr-pm3.cern.ch (2024/06/16 00:14:43)


***

### [2024-06-16 00:15:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087623)
>|\*\*\* XPOC error has been reset by user 'delph' at 16.06.2024 00:15:01| Comment: Asynch dump test|| Dump info: BEAM 2 at 16.06.2024 00:10:35| Beam info: Energy= 6799.80 GeV ; Intensity= 0.00E0 p+ ; #Bunches= 7| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR

creator:	 delph  @cs-ccr-pm3.cern.ch (2024/06/16 00:15:01)


***

### [2024-06-16 00:16:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087624)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:16:37)


***

### [2024-06-16 00:16:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087626)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:16:45)


***

### [2024-06-16 00:18:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087627)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:18:13)


***

### [2024-06-16 00:18:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087627)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:18:13)


***

### [2024-06-16 00:19:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087629)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:19:18)


***

### [2024-06-16 00:19:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087630)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:19:27)


***

### [2024-06-16 00:20:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087632)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:20:08)


***

### [2024-06-16 00:20:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087633)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:20:16)


***

### [2024-06-16 00:22:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087636)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:22)


***

### [2024-06-16 00:22:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087637)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:41)


***

### [2024-06-16 00:22:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087639)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:42)


***

### [2024-06-16 00:22:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087641)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:43)


***

### [2024-06-16 00:22:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087645)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:56)


***

### [2024-06-16 00:22:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087646)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:22:58)


***

### [2024-06-16 00:23:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087648)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:23:22)


***

### [2024-06-16 00:23:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087650)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:23:28)


***

### [2024-06-16 00:24:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087652)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:24:56)


***

### [2024-06-16 00:26:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087654)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:26:40)


***

### [2024-06-16 00:37:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087656)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:37:52)


***

### [2024-06-16 00:49:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087663)
>QPS state again,,,,

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/16 00:49:34)

[ LHC SIS GUI 24-06-16_00:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637635/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/16 00:49:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637635/content)

[ LHC Fast BCT v1.3.2 24-06-16_00:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637637/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 00:53:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637637/content)


***

### [2024-06-16 00:51:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087664)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 00:51:31)

[qps-lhc-swisstool 24-06-16_00:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637641/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 00:52:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637641/content)


***

### [2024-06-16 00:55:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087666)
>still the same after the power cycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 00:55:46)

[ LHC Fast BCT v1.3.2 24-06-16_00:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637643/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 00:55:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3637643/content)


***

### [2024-06-16 00:58:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087668)
>GIORGIO D'ANGELO(GDANGELO) assigned RBAC Role: QPS-Piquet and will expire on: 16-JUN-24 02.58.44.219000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/16 00:58:46)


***

### [2024-06-16 00:59:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087669)
>calling QPS piquet for the RCO.A45B2 issue  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 01:00:01)


***

### [2024-06-16 01:46:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087677)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 01:46:38)


***

### [2024-06-16 01:47:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087678)
>Issue with the QPS is solved, I restart the circuits and reload injection setting  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/16 01:47:47)


***

### [2024-06-16 01:53:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087679)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 01:53:21)


***

### [2024-06-16 01:59:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4087680)
>LHC RUN CTRL: New FILL NUMBER set to 9781

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/16 01:59:26)


