# FILL 10113
**start**:		 2024-09-14 07:34:21.882738525+02:00 (CERN time)

**end**:		 2024-09-14 07:53:35.928238525+02:00 (CERN time)

**duration**:	 0 days 00:19:14.045500


***

### [2024-09-14 07:34:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143250)
>LHC RUN CTRL: New FILL NUMBER set to 10113

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 07:34:22)


***

### [2024-09-14 07:35:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143253)
>
```
sequencer stuck when sending parallelized preparation. killing the application and restarting it, but it does not restart. Calling the sequencer piquet
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:44:43)

[ LHC Sequencer Execution GUI (PRO) : 12.33.0  24-09-14_07:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770945/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:36:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770945/content)


***

### [2024-09-14 07:46:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143254)
>Event created from ScreenShot Client.  
Equip State 24-09-14\_07:46.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:47:04)

[ Equip State 24-09-14_07:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770947/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:50:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770947/content)


***

### [2024-09-14 07:48:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143257)
>back after reset and power permit

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:48:33)

[Circuit_RCBXH3_L8:   24-09-14_07:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770949/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 07:48:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770949/content)


