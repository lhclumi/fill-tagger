# FILL 9689
**start**:		 2024-05-30 05:13:45.018488525+02:00 (CERN time)

**end**:		 2024-05-30 05:41:05.789238525+02:00 (CERN time)

**duration**:	 0 days 00:27:20.770750


***

### [2024-05-30 05:13:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078356)
>LHC RUN CTRL: New FILL NUMBER set to 9689

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/30 05:13:45)


***

### [2024-05-30 05:15:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078357)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/30 05:15:17)


***

### [2024-05-30 05:16:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078358)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/30 05:16:35)


***

### [2024-05-30 05:41:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078365)
>LHC RUN CTRL: New FILL NUMBER set to 9690

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/30 05:41:05)


