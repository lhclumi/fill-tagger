# FILL 9888
**start**:		 2024-07-12 13:03:21.674613525+02:00 (CERN time)

**end**:		 2024-07-12 16:45:11.988863525+02:00 (CERN time)

**duration**:	 0 days 03:41:50.314250


***

### [2024-07-12 13:03:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104978)
>LHC RUN CTRL: New FILL NUMBER set to 9888

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:03:22)


***

### [2024-07-12 13:12:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104992)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:12:48)


***

### [2024-07-12 13:14:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104994)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:14:17)


***

### [2024-07-12 13:14:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104996)
>
```
One RF crate is down (cfv.ux45.acs3b2t), I try a reboot without success...
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 13:23:12)

[ LHC RF CONTROL 24-07-12_13:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675485/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/12 13:15:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675485/content)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-07-12_13:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675489/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 13:16:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675489/content)


***

### [2024-07-12 13:15:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104997)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:15:38)


***

### [2024-07-12 13:16:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104999)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:16:48)


***

### [2024-07-12 13:16:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105000)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:17:00)


***

### [2024-07-12 13:17:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105003)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:17:42)


***

### [2024-07-12 13:19:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105005)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:19:03)


***

### [2024-07-12 13:19:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105005)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:19:03)


***

### [2024-07-12 13:21:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105010)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:29)


***

### [2024-07-12 13:21:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105012)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:33)


***

### [2024-07-12 13:21:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105016)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:35)


***

### [2024-07-12 13:21:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105018)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:40)


***

### [2024-07-12 13:21:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105022)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:47)


***

### [2024-07-12 13:21:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105023)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:21:50)


***

### [2024-07-12 13:23:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105027)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:23:51)


***

### [2024-07-12 13:25:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105031)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 13:25:35)


***

### [2024-07-12 13:26:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105033)
>The issue with the RF crate is teh same issue as the RF FGC status, Andy will need to access in PZ45, probably a circuit breaker iof the rack.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 13:27:23)


***

### [2024-07-12 14:16:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105055)
>LHC SEQ: ramping up LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 14:16:34)


***

### [2024-07-12 14:18:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105057)
>access in PZ45 for Andy.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 14:19:06)


***

### [2024-07-12 14:30:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105067)
>QPS crate for A23 flickering (DQAMGS.RB.A23.B23R2), I try reset and switch board, but doesn't solve the issue. I call the QPS piquet.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 14:31:21)

[C - DQAMGS.RB.A23.B23R2 DQAMGS N type for circuit RB.A23    24-07-12_14:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675555/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 14:31:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675555/content)


***

### [2024-07-12 14:38:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105068)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P1 - S12(tunnel) |
| Peoples : | * Daniel Prelipcean (SY/STI/BMI)- / - - <daniel.prelipcean@cern.ch> |
| Duration | 2h - **Access Needed** - |
| SND location for RADMON | |



creator:	 msolfaro  @paas-standard-avz-b-2wsjq.cern.ch (2024/07/12 14:38:18)


***

### [2024-07-12 14:39:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105070)
>VITO VIZZIELLO(VIVIZZIE) assigned RBAC Role: QPS-Piquet and will expire on: 12-JUL-24 04.39.09.545000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/12 14:39:12)


***

### [2024-07-12 14:43:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105071)
>QPS piquet called back, tehy could reset the crate now but it may trip the power converter and then we would have to precycle.  
He advise that we ignore it for now and call the QPS piquet at the bext occasion when RB.A23 has no current  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 14:44:27)


***

### [2024-07-12 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105102)
>
```
**SHIFT SUMMARY:**  
  
Access most of the morning waiting for SPS to recover from the RF coupler fault.  
- PM56 elevator is now OK  
- RQTL8.L7B1 was modified for higher current, I manage to do a test, it quenched at 278A.  
- Many other small intervention in LHC and experiments  
  
during last evenning, some Heater discharges on IPQs of sector 45, it was checked by MP3 (2 analysis was failed), and we get the green light to power the circuit.  
  
One QPS crate of RB.A23 would need a reset when the current is 0A as the crate was flickering, nothing urgent, it even stopped since 20 mins.  
  
I saw that some RF FGCs were in bad state and called Andy. He needed to access in PZ45 to restart a rack. The action tripped many other crates. The FGC problem is solved but I can't switch ON the low level. Andy is still checking.  
  
* To be noted:

  
On LHCb request, thenext fill will be done with 375b to check teh succes of the velo intervention  
  
The Q' was trimmed by Matteo in the B*levelling function to reach 9 instead of 10 at 30cm  
  
  

```
Delphine  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 15:37:25)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675600/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 15:37:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675600/content)


***

### [2024-07-12 15:17:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105086)
>
```
calling back Andy who was just driving back, as the RF can't be switched 
    ON for 5B1, 5B2,6B2 and 8B2
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/12 15:38:58)

[Desktop 24-07-12_15:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675585/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/12 15:19:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675585/content)

[Desktop 24-07-12_15:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675587/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/12 15:19:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675587/content)


***

### [2024-07-12 15:25:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105092)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 15:25:02)


***

### [2024-07-12 15:25:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105093)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 15:25:43)


***

### [2024-07-12 15:44:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105107)
>ANDREW BUTTERWORTH(BUTTERWO) assigned RBAC Role: RF-LHC-Piquet and will expire on: 12-JUL-24 05.44.44.076000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/12 15:44:47)


***

### [2024-07-12 15:53:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105112)
>
```
Increased MKI8 and MKI2 SS and Intermediate SS ramping and enlarging voltages by 100V  

```


creator:	 gifavia  @pcte25462.dyndns.cern.ch (2024/07/12 16:29:40)

[screenShot_July_12th_2024_15_55_11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675661/content)
creator:	 gifavia  @pcte25462.dyndns.cern.ch (2024/07/12 15:55:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675661/content)

[screenShot_July_12th_2024_15_58_45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675667/content)
creator:	 gifavia  @pcte25462.dyndns.cern.ch (2024/07/12 15:58:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675667/content)

[screenShot_July_12th_2024_16_05_06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675689/content)
creator:	 gifavia  @pcte25462.dyndns.cern.ch (2024/07/12 16:05:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675689/content)

[screenShot_July_12th_2024_16_14_51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675697/content)
creator:	 gifavia  @pcte25462.dyndns.cern.ch (2024/07/12 16:14:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3675697/content)


***

### [2024-07-12 16:02:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105122)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 16:02:42)


***

### [2024-07-12 16:06:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105124)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 16:06:28)


***

### [2024-07-12 16:23:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105131)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/12 16:23:26)


