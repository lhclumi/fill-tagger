# FILL 9629
**start**:		 2024-05-14 21:52:08.361488525+02:00 (CERN time)

**end**:		 2024-05-15 03:57:16.121488525+02:00 (CERN time)

**duration**:	 0 days 06:05:07.760000


***

### [2024-05-14 21:52:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067559)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 21:52:08)


***

### [2024-05-14 21:52:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067560)
>LHC RUN CTRL: New FILL NUMBER set to 9629

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 21:52:08)


***

### [2024-05-14 21:56:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067561)
>Probobly linked to the real time reset.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/14 21:57:11)

[Set SECTOR12 24-05-14_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591055/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/14 21:57:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591055/content)


***

### [2024-05-14 21:58:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067562)
>Ran another pre-pare circuits and this tripped the RQT13.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/14 21:59:01)

[Set SECTOR45 24-05-14_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591057/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/14 21:59:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591057/content)


***

### [2024-05-14 22:03:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067564)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 22:03:05)


***

### [2024-05-14 22:05:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067566)
>Restored coupling, tune, chroma and removed the MQ knob.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 22:05:44)

[ LSA Applications Suite (v 16.5.36) 24-05-14_22:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591059/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 22:05:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591059/content)


***

### [2024-05-14 22:07:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067567)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETOI could only close the injection handshake now due to the issue with LHCb

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 22:08:40)


***

### [2024-05-14 22:36:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067576)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 22:36:45)


***

### [2024-05-14 22:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067580)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 22:44:04)


***

### [2024-05-14 22:52:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067583)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 22:52:30)


***

### [2024-05-14 22:54:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067584)
>tunes "a bit" off

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/14 22:54:24)

[ TuneViewer Light V5.7.2 24-05-14_22:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591085/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/05/14 22:54:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591085/content)


***

### [2024-05-14 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067616)
>
```
**SHIFT SUMMARY:**  
*Arrived during the non-factorization for VdM. We did some scans for CMS and ATLAS followed by scraping. We dump on BPM when the intensity got low.  
* Prepared for injection. Issue with the BIS Pre-Operationa checks : Check BiC CIBU CONFIG =DB which fails. Michi checked and it is linked to a new release but the desired solution would be to update on the MP side. Roman is also informed.  
*Started trimming in the knob that changed the MQ but it there was a problem in the matching so had to make a quick one. We had to change the MQT a lot to compensate. We were able to see a change in the vertical dispersion but need to see what conclusion we can draw from it offline.  
*Did a pre-cycle but many magnets tripped during the middle of the pre-cycle. It is possible it was linked to reset trims in the preparation. I have not added a fault on this because I don't know then main cause.   
*After going back and doing all the resets I could do another pre-cycle without any issues.  
*Going back to injection the vertical tune were still very off (all corrections had been reverted).  
*LHCb had issues with communication but it was in the shadow of our issues with the QPS.   
  
  
* To be reverted:

*Call ATAS for unmasking the BCM  
*Remove the keys for the AC-dipole  
*Remove the masks for LHC.  
*Remove collimator mot in SPS BIS   
*Play the nominal collimator sequence  
*Change hypercycle back to the normal MD one.   
  
/Tobias  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 03:19:27)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591144/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 00:48:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591144/content)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15_02:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591155/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:35:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591155/content)


***

### [2024-05-14 23:04:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067586)
>large tune and chroma trims.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 23:04:42)

[Accelerator Cockpit v0.0.35 24-05-14_23:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591087/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 23:04:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591087/content)


***

### [2024-05-14 23:04:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067586)
>large tune and chroma trims.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 23:04:42)

[Accelerator Cockpit v0.0.35 24-05-14_23:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591087/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 23:04:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591087/content)


***

### [2024-05-14 23:20:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067600)
>Masking to inject with the special settings

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 23:20:27)

[Masks CIB.SR8.INJ2.2 24-05-14_23:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591109/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 23:20:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591109/content)


***

### [2024-05-14 23:54:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067603)
>This one is stuck. Restarting it.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 23:54:27)

[ LHC-INJ-SIS Status 24-05-14_23:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591117/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 23:54:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591117/content)


***

### [2024-05-15 00:28:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067614)
>This is when the QPS tripped.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 00:29:24)

[Mozilla Firefox 24-05-15_00:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591136/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 00:29:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591136/content)


***

### [2024-05-15 02:32:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067618)
>Event created from ScreenShot Client.  
LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15\_02:32.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:32:53)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591151/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:32:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591151/content)


***

### [2024-05-15 02:35:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067619)
>Event created from ScreenShot Client.  
LSA Applications Suite (v 16.5.36) 24-05-15\_02:35.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:35:13)

[ LSA Applications Suite (v 16.5.36) 24-05-15_02:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591153/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:35:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591153/content)


***

### [2024-05-15 02:36:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067620)
>Event created from ScreenShot Client.  
LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15\_02:36.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:36:34)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591157/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:36:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591157/content)


***

### [2024-05-15 02:37:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067621)
>Event created from ScreenShot Client.  
LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15\_02:37.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:37:46)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-15_02:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591159/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 02:37:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591159/content)


***

### [2024-05-15 03:15:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067623)
>
```
masking CODs for fake injections to save the MKIs
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 03:19:02)

[Action input dialog 24-05-15_03:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591161/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:15:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591161/content)

[ Power Converter Interlock GUI 24-05-15_03:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591163/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:18:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591163/content)


***

### [2024-05-15 03:20:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067624)
>CODs unmasked after successful fake injections  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 03:20:49)


***

### [2024-05-15 03:29:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067626)
>MD11743 completed. switching to MD11723  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:30:21)


***

### [2024-05-15 03:30:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067627)
>called ATLAS to inform that they can unmask the BCM  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:30:36)


***

### [2024-05-15 03:32:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067628)
>Event created from ScreenShot Client.  
Generation Application connected to server LHC (PRO database) 24-05-15\_03:32.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:32:33)

[ Generation Application connected to server LHC (PRO database) 24-05-15_03:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591167/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:43:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591167/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.0  24-05-15_03:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591181/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:48:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591181/content)


***

### [2024-05-15 03:33:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067629)
>Event created from ScreenShot Client.  
Power Converter Interlock GUI 24-05-15\_03:33.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:33:21)

[ Power Converter Interlock GUI 24-05-15_03:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591169/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:33:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591169/content)


***

### [2024-05-15 03:33:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067630)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:33:31)


***

### [2024-05-15 03:34:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067631)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:34:44)


***

### [2024-05-15 03:35:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067632)
>unmasking SPS BIS

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:35:59)

[(null) 24-05-15_03:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591171/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:35:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591171/content)

[(null) 24-05-15_03:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591173/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:36:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591173/content)

[(null) 24-05-15_03:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591175/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:36:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591175/content)


***

### [2024-05-15 03:37:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067635)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:37:15)


***

### [2024-05-15 03:38:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067636)
>Event created from ScreenShot Client.  
LHC SIS GUI 24-05-15\_03:38.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:38:43)

[ LHC SIS GUI 24-05-15_03:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591177/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:38:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591177/content)

[ LHC SIS GUI 24-05-15_03:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591179/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 03:39:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591179/content)


***

### [2024-05-15 03:43:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067637)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:43:52)


***

### [2024-05-15 03:46:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067639)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:46:10)


***

### [2024-05-15 03:46:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067640)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:46:45)


***

### [2024-05-15 03:52:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067642)
>ACDipole keys put back in the CCC drawer  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 03:52:37)


***

### [2024-05-15 03:54:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067643)
>
```
**Summary from MD11743 (optics at injection)**  
  
First half of MD was used to look at possible source of large vertical dispersion in LHCB2.  Various attempts had been made to understand this in commissioning, but showed no influence on the Dy bump. For this test MQ in arc56 were trimmed down by about 5%, with other arcs of MQ used to compensate. For the first time we see a clear shift to the vertical dispersion due to the arc56 MQ trim (screenshot):  
  
-- Likely indicates MQ with large roll error in this arc
```

```
  
Second half of the MD was used to look at the nonlinear optics at injection.   
Unfortunately recovering from the MQ trim took quite a long time (issues with QPS during the following precycle), so were unable to try any measurements of the RDT decay.  
  
We expect from simulations/commissioning that the MO also generate large contributions to the f1004 and f1210 resonances (larger than the actual decapole and skew octupole errors themselves). Managed to find trims of the spools and MCOSX that could vary these RDTs on a comparable scale to the contribution which is expected to be generated by the MO (without changing Q''', coupling etc).  
  
Didn't have time at the end to try and find any correction for these RDTs directly, but tried varying the knobs defined with strong MO and Q' to see if we could see any effect on the lifetime. For the f1004 saw very clear impact from varying the MCD knob which were some non-negligible proportion of the lifetime drop due to powering the MO. From RDT measurements vs MCD today and measurements of the shift with MO during commissioning can try to find a correction for the f1004 driven by MO. For the f1210 a4 resonance situation was less clear, will need to look offline.  
  
  
  
  
 
```


creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 04:37:23)

[HandVdispersion_before-vs-after-MQtrim.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591185/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 04:04:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591185/content)

[screenShot_May_15th_2024_04_05_09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591191/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 04:05:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591191/content)

[screenShot_May_15th_2024_04_06_41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591193/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 04:37:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591193/content)

[screenShot_May_15th_2024_04_07_15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591195/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/15 04:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591195/content)


***

### [2024-05-15 03:57:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067644)
>LHC RUN CTRL: New FILL NUMBER set to 9630

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:57:16)


