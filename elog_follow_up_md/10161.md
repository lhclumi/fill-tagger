# FILL 10161
**start**:		 2024-09-27 23:51:50.044363525+02:00 (CERN time)

**end**:		 2024-09-28 00:22:01.369488525+02:00 (CERN time)

**duration**:	 0 days 00:30:11.325125


***

### [2024-09-27 23:51:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151714)
>LHC RUN CTRL: New FILL NUMBER set to 10161

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 23:51:51)


***

### [2024-09-27 23:52:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151715)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 23:52:12)


***

### [2024-09-27 23:55:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151716)
>correcting

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 23:55:57)

[Accelerator Cockpit v0.0.38 24-09-27_23:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790237/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 23:55:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790237/content)

[Accelerator Cockpit v0.0.38 24-09-27_23:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790239/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 23:56:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790239/content)

[Accelerator Cockpit v0.0.38 24-09-27_23:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790241/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 23:57:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790241/content)


***

### [2024-09-27 23:58:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151717)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 23:58:28)


***

### [2024-09-28 00:12:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151719)
>lower damper gain

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/28 00:12:52)

[ LSA Applications Suite (v 16.6.29) 24-09-28_00:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790247/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/28 00:12:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790247/content)


***

### [2024-09-28 00:13:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151720)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 00:13:03)


***

### [2024-09-28 00:13:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151721)
>trains of ~1.65e11 in

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 00:13:21)

[ LHC Fast BCT v1.3.2 24-09-28_00:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790249/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 00:13:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790249/content)

[Monitoring application. Currently monitoring : LHC - [2 subscriptions ] 24-09-28_00:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790251/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/28 00:15:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790251/content)

[ LHC RF CONTROL 24-09-28_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790253/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/28 00:16:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790253/content)


***

### [2024-09-28 00:16:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151722)
>
```
abort gap cleaning OFF (to avoid tune perturbation) and tunes corrected  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/28 00:18:02)

[Abort gap cleaning control v2.1.0 24-09-28_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790255/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/28 00:16:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790255/content)

[Accelerator Cockpit v0.0.38 24-09-28_00:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790257/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/28 00:17:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790257/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-28_00:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790259/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/28 00:18:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790259/content)


***

### [2024-09-28 00:20:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151725)
>Global Post Mortem Event

Event Timestamp: 28/09/24 00:20:41.176
Fill Number: 10161
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 5757 / 5719 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 8-BPMs L&R syst.'A': A T -> F on CIB.UA67.R6.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/28 00:23:32)


***

### [2024-09-28 00:20:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151729)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: mihostet / MD6925: B2 instability during kick for tune measurement (trains of ~1.65e11 at injection)


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/28 00:36:17)


***

### [2024-09-28 00:22:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151723)
>LHC RUN CTRL: New FILL NUMBER set to 10162

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 00:22:02)


