# FILL 9579
**start**:		 2024-05-01 17:32:28.098613525+02:00 (CERN time)

**end**:		 2024-05-01 23:08:24.713738525+02:00 (CERN time)

**duration**:	 0 days 05:35:56.615125


***

### [2024-05-01 17:32:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059593)
>LHC RUN CTRL: New FILL NUMBER set to 9579

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 17:32:28)


***

### [2024-05-01 17:40:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059595)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 17:40:36)


***

### [2024-05-01 17:57:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059599)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 17:57:04)


***

### [2024-05-01 18:01:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059600)
>Event created from ScreenShot Client.  
OpenYASP DV LHCB2Transfer . LHC\_3inj\_Nom\_48b\_Q20\_2024\_V1 24-05-01\_18:01.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/01 18:01:30)

[OpenYASP DV LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-05-01_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571839/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/01 18:01:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571839/content)


***

### [2024-05-01 18:01:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059601)
>Event created from ScreenShot Client.  
OpenYASP DV LHCB1Transfer . LHC\_3inj\_Nom\_48b\_Q20\_2024\_V1 24-05-01\_18:01.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/01 18:01:38)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-05-01_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571841/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/01 18:01:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571841/content)


***

### [2024-05-01 18:31:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059605)
>
```
suspicious BPM.32R7.B2 in H and BPM.25L8.B2 in V are completely dominating the MAX value for B2 injection oscillations...  
For some bunches they are auto-excluded by the sigma cut, for others they are not, which can lead to spurious latches - probably they should be fully excluded from the analysis.  
  
For now, we will mask the IQC injection oscillation permit for B2.  

```


creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/01 18:59:05)

[LHC Injection Quality Check - Playback 3.17.7 24-05-01_18:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571849/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/01 18:32:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571849/content)

[LHC Injection Quality Check - Playback 3.17.7 24-05-01_18:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571851/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/01 18:32:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571851/content)

[LHC Injection Quality Check - Playback 3.17.7 24-05-01_18:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571853/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/01 18:32:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571853/content)


***

### [2024-05-01 18:41:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059607)
>had to mask IQC\_PERMIT\_INJ\_OSCIL\_B2 as there was a suspcicious BPM that triggered

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:42:04)

[ LHC SIS GUI 24-05-01_18:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571857/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:42:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571857/content)

[ LHC-INJ-SIS Status 24-05-01_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571859/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:44:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571859/content)


***

### [2024-05-01 18:48:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059608)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 18:48:13)


***

### [2024-05-01 18:48:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059609)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 18:48:22)


***

### [2024-05-01 18:48:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059610)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 18:48:30)


***

### [2024-05-01 18:48:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059611)
>LHC Injection CompleteNumber of injections actual / planned: 64 / 48SPS SuperCycle length: 33.6 [s]Actual / minimum time: 0:51:25 / 0:31:52 (161.3 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:48:31)


***

### [2024-05-01 18:52:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059615)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 18:52:14)


***

### [2024-05-01 18:52:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059613)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 18:52:14)


***

### [2024-05-01 18:58:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059618)
>losses during the ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/01 18:58:56)

[ LHC BLM Fixed Display 24-05-01_18:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571869/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/01 18:58:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571869/content)


***

### [2024-05-01 18:59:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059619)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 24-05-01\_18:59.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 18:59:18)

[ LHC Fast BCT v1.3.2 24-05-01_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571871/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 18:59:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571871/content)


***

### [2024-05-01 18:59:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059619)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 24-05-01\_18:59.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 18:59:18)

[ LHC Fast BCT v1.3.2 24-05-01_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571871/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 18:59:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571871/content)


***

### [2024-05-01 18:59:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059620)
>Event created from ScreenShot Client.  
LHC Beam Losses Lifetime Display 24-05-01\_18:59.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:59:41)

[ LHC Beam Losses Lifetime Display 24-05-01_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571873/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 18:59:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571873/content)


***

### [2024-05-01 19:13:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059622)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:13:31)


***

### [2024-05-01 19:13:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059624)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:13:43)


***

### [2024-05-01 19:22:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059631)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:22:06)


***

### [2024-05-01 19:22:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059633)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:22:49)


***

### [2024-05-01 19:23:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059635)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:23:34)


***

### [2024-05-01 19:28:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059640)
>Event created from ScreenShot Client.  
LHC BLM Fixed Display 24-05-01\_19:28.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/01 19:28:36)

[ LHC BLM Fixed Display 24-05-01_19:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571919/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/01 19:28:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571919/content)


***

### [2024-05-01 19:28:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059641)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:28:58)


***

### [2024-05-01 19:37:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059651)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 24-05-01\_19:37.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:37:37)

[ LHC Fast BCT v1.3.2 24-05-01_19:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571957/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:37:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571957/content)


***

### [2024-05-01 19:37:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059652)
>Event created from ScreenShot Client.  
BSRT-vs-Emit v0.1.2 - January 2024 - INCA DISABLED - PRO CCDA 24-05-01\_19:37.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:37:43)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-01_19:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571961/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:37:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571961/content)


***

### [2024-05-01 19:42:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059653)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01\_19:42.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:43:00)

[ LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01_19:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571967/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:43:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571967/content)


***

### [2024-05-01 19:43:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059654)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01\_19:43.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:43:07)

[ LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01_19:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571969/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 19:43:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571969/content)


***

### [2024-05-01 19:43:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059655)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:43:22)


***

### [2024-05-01 19:45:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059657)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 19:45:20)


***

### [2024-05-01 19:47:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059660)
>Event created from ScreenShot Client.  
LHC Beam Losses Lifetime Display 24-05-01\_19:47.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 19:47:33)

[ LHC Beam Losses Lifetime Display 24-05-01_19:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571983/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/01 19:47:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3571983/content)


***

### [2024-05-01 20:16:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059677)
>Started separation levelling in IP1 and IP5 only now (45 cm) instead of 48 cm  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/01 20:17:22)

[ LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01_20:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572029/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 20:20:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572029/content)

[ LHC Luminosity Scan Client 0.58.1 [pro] 24-05-01_20:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572035/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/01 20:20:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572035/content)


***

### [2024-05-01 22:16:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059736)
>Global Post Mortem EventEvent Timestamp: 01/05/24 22:16:46.081Fill Number: 9579Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799320 [MeV]Intensity B1/B2: 30065 / 32515 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 7-WIC: B T -> F on CIB.UJ33.U3.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/01 22:19:36)


***

### [2024-05-01 22:16:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059823)
>Global Post Mortem Event ConfirmationDump Classification: Power converter fault(s)Operator / Comment: delph / RQ4.LR3 tripped

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/01 23:58:25)


***

### [2024-05-01 22:17:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059735)
>RQ4.LR3 FAULT OFF

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/01 22:17:25)

[Set SECTOR34 24-05-01_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572190/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/05/01 22:17:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572190/content)


***

### [2024-05-01 22:20:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059737)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 22:20:10)


***

### [2024-05-01 22:33:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059742)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/01 22:33:34)


***

### [2024-05-01 22:47:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059747)
>
```
ATLAS called us and informed that one AFP Roman pot is not in its parking position after the spring extraction. This prevents ATLAS from giving the injection handshake.  
  
He will investigate.  
  
The AFP override key is IN.    

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/01 23:12:26)


***

### [2024-05-01 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059826)
>
```
**SHIFT SUMMARY:**  
  
Took the machine during preparation for physics fill following the Van der Mer cycle set up and loss maps.  
  
We had to call Yann Dutheil as the XPOC latched with a RETRIG error.  
  
Cryo told us to wait even though the beam intensity on the previous cycle was quite low.   
  
Since I had injected probes early to check that all was ok, the orbit had drifted quite a bit by the time we injected trains and beams got dumped during injection because of a too large orbit of B1H.  
  
I restarted injection and got stuck with beam 2 injections as I could not unlatch the injection oscillations SIS interlock. I masked to complete filling as Michi noticed that it could be linked to suspicious BPM readings. Then ramped the machine and brought to stable beams.  
  
After some time at 35 cm, beams were dumped by a trip of RQ4.LR3 (warm magnet). One AFP pot did not retract to the correct parking position, but this was noticed and solved by one AFP expert.   
  
Many thanks to Joerg, Michi and David for the remote help during the shift!      
  
* To be noted:

  
- QPS reset for RBs did not succeed  
- we unmasked the IQC_PERMIT_INJ_OSCIL_B2 for the next fill  
- It should be discussed whether the two BPMs (BPM.32R7.B2 in H and BPM.25L8.B2 in V) should be masked  
- it looked like the losses in collision did not go much higher than 30% this time (to be confirmed offline as the buffer is too short)    
  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 00:06:29)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572269/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/02 00:05:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3572269/content)


***

### [2024-05-01 23:00:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4059772)
>called EPC piquet for the RQ4.LR3  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/01 23:15:16)


