# FILL 10104
**start**:		 2024-09-11 11:19:29.136238525+02:00 (CERN time)

**end**:		 2024-09-11 18:35:08.959363525+02:00 (CERN time)

**duration**:	 0 days 07:15:39.823125


***

### [2024-09-11 11:19:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141493)
>LHC RUN CTRL: New FILL NUMBER set to 10104

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 11:19:30)


***

### [2024-09-11 11:20:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141495)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 11:20:05)


***

### [2024-09-11 12:08:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141528)
>**RF Half-detuning Adjustment:**  
Adjusted the half-detuning set point phase (phase A) in most lines.  
Beam 1:  
1B1: from -142 deg. to -145 deg.  
2B1: from 68 deg. to 60 deg.  
3B1: from 99.3 deg. to 93.3 deg.  
4B1: 111.5 deg. was already good  
5B1: from 119.5 deg. to 121.5 deg.  
6B1: from -67.5 deg. to -70.5 deg.  
7B1: -61.5 deg. was already good  
8B1: from -111 deg. to -114 deg.  
  
Beam 2:  
1B2: from 29 deg. to 23 deg.  
2B2: from 4.8 deg. to 7.8 deg.  
3B2: from -21.2 deg. to -24.2 deg.  
4B2: from -167 deg. to -163 deg.  
5B2: from 42.3 deg. to 40.3 deg.  
6B2: -159.3 deg. was already good  
  
We did not have time to adjust cavity 7B2 and 8B2.  


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 12:18:01)


***

### [2024-09-11 12:51:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141552)
>drifts at injection

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 12:51:29)

[Accelerator Cockpit v0.0.38 24-09-11_12:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767298/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 12:51:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767298/content)

[Accelerator Cockpit v0.0.38 24-09-11_12:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767302/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 12:51:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767302/content)

[Accelerator Cockpit v0.0.38 24-09-11_12:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767306/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 12:53:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767306/content)


***

### [2024-09-11 12:54:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141553)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 12:54:37)


***

### [2024-09-11 13:06:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141564)
>Large injection oscillations just of a sudden.. not sure what the reason 
 is.. Will take another shot before correcting.

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/11 13:06:34)

[ LHC Injection Quality Check 3.17.8 24-09-11_13:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767330/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/11 13:06:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767330/content)


***

### [2024-09-11 13:46:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141581)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 13:46:36)


***

### [2024-09-11 13:46:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141582)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 13:46:47)


***

### [2024-09-11 13:46:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141583)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 13:46:56)


***

### [2024-09-11 13:46:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141584)
>LHC Injection Complete

Number of injections actual / planned: 72 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:52:20 / 0:33:48 (154.8 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/11 13:46:58)


***

### [2024-09-11 13:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141590)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 13:48:44)


***

### [2024-09-11 13:48:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141592)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 13:48:45)


***

### [2024-09-11 13:48:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141594)
>36%/dump

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 13:49:25)

[ LHC BLM Fixed Display 24-09-11_13:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767381/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 13:49:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767381/content)


***

### [2024-09-11 13:49:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141595)
>Start of ramp.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 13:49:28)

[ LHC Fast BCT v1.3.2 24-09-11_13:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767383/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 13:49:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767383/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-11_13:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767385/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 13:49:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767385/content)


***

### [2024-09-11 14:10:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141605)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:10:02)


***

### [2024-09-11 14:10:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141608)
>QFB off for a little abort gap cleaning

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 14:10:45)

[Abort gap cleaning control v2.1.0 24-09-11_14:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767448/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 14:10:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767448/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-09-11_14:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767454/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 14:11:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767454/content)


***

### [2024-09-11 14:11:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141609)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:11:20)


***

### [2024-09-11 14:20:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141614)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:20:09)


***

### [2024-09-11 14:20:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141615)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:20:50)


***

### [2024-09-11 14:21:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141617)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:21:45)


***

### [2024-09-11 14:41:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141637)
>Event created from ScreenShot Client.  
 LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11\_14:41.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:46:47)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_14:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767539/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:46:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767539/content)


***

### [2024-09-11 14:47:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141639)
>Final optimzation of the crossing plane.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:47:42)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_14:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767543/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:47:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767543/content)


***

### [2024-09-11 14:49:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141642)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:49:49)


***

### [2024-09-11 14:51:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141643)
>IP2 optimized.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:51:57)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_14:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767547/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 14:51:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767547/content)


***

### [2024-09-11 14:56:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141645)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:56:11)


***

### [2024-09-11 14:58:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141647)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 14:58:08)


***

### [2024-09-11 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141681)
>
```
**SHIFT SUMMARY:**  
*Arrived after the electrical glitch, could restart without issues.  
*Decided to go for the access for ALICE. Some other access as well for EN and cryo in the shadow.   
*Changed the polarity for LHCb.  
*Large blow up of a train due to problem with kicker in SPS.  
*We had to reduce the separation a lot to be able to find collisions again. Also the crossing plane was very off.   
*Issues with LHCb not publishing lumi made the excersise more difficult.   
*We are now going through all the steps to be able to incorporate the new settings for LHCb.   
  
/Tobias  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 16:33:00)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767711/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 15:45:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767711/content)


***

### [2024-09-11 15:01:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141650)
>emittance scan in ip1 and ip5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:01:49)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767593/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:01:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767593/content)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767595/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:01:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767595/content)


***

### [2024-09-11 15:10:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141655)
>We separate LHCb -0.3 sigma in sep plane and go for the B\* fast levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:10:56)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767621/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:10:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767621/content)


***

### [2024-09-11 15:18:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141663)
>IP9 optimisation at 1.06m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:19:03)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767651/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:19:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767651/content)


***

### [2024-09-11 15:27:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141673)
>LHCb is not publishing the target, tyhey give the number and we use the OP 
 levelling (1664,7 Hz/ub)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:27:47)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767676/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:27:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767676/content)


***

### [2024-09-11 15:30:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141675)
>LHCb at target at 1.06m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:31:08)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767681/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:31:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767681/content)


***

### [2024-09-11 15:35:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141677)
>IP8 optimization at 0.99m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:35:12)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767695/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:35:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767695/content)


***

### [2024-09-11 15:37:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141678)
>Levelling to target at 0.99m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:37:39)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767697/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:37:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767697/content)


***

### [2024-09-11 15:45:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141682)
>IP8 optimization at 93cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:46:13)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767712/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:46:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767712/content)


***

### [2024-09-11 15:47:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141684)
>IP8 on target again at 0.93cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:47:46)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767718/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:47:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767718/content)


***

### [2024-09-11 15:51:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141685)
>IP8 optimization at 0.88m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:51:44)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767720/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:51:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767720/content)


***

### [2024-09-11 15:53:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141686)
>IP8 back to target at 0.88m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:53:21)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767722/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:53:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767722/content)


***

### [2024-09-11 15:57:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141687)
>IP8 optimization at 0.83m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:58:09)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767738/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:58:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767738/content)


***

### [2024-09-11 15:59:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141688)
>IP8 back to target at 0.83m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:59:42)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_15:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767740/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 15:59:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767740/content)


***

### [2024-09-11 16:03:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141692)
>IP8 optimization at .78m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:04:10)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767751/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:04:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767751/content)


***

### [2024-09-11 16:03:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141692)
>IP8 optimization at .78m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:04:10)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767751/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:04:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767751/content)


***

### [2024-09-11 16:05:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141694)
>IP8 back to target at 0.78m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:05:15)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767757/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:05:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767757/content)


***

### [2024-09-11 16:09:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141696)
>IP8 optimized at 0.73m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:09:45)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767763/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:09:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767763/content)


***

### [2024-09-11 16:11:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141697)
>IP8 back on target at 0.73m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:11:42)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767769/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:11:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767769/content)


***

### [2024-09-11 16:11:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141698)
>start levelling for IP 1 and 5 as we are so slow with the B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:12:06)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767771/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:12:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767771/content)


***

### [2024-09-11 16:18:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141699)
>IP8 re-optimizef at 0.68cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:18:25)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767773/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:18:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767773/content)


***

### [2024-09-11 16:20:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141702)
>IP8 back to target at 0.68cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:20:58)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767781/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:20:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767781/content)


***

### [2024-09-11 16:25:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141708)
>IP8 optimized at 0.64cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:25:41)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767788/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:25:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767788/content)


***

### [2024-09-11 16:27:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141709)
>IP8 back to target at 0.64m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:27:53)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767794/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:27:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767794/content)


***

### [2024-09-11 16:31:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141710)
>IP8 optimized at 0.6m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:32:37)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767812/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:32:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767812/content)


***

### [2024-09-11 16:34:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141711)
>IP8 back to targte at 60cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:34:28)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767824/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:34:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767824/content)


***

### [2024-09-11 16:40:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141714)
>IP8 optimized at 56cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:40:39)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767830/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:40:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767830/content)


***

### [2024-09-11 16:40:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141715)
>IP8 already at target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:41:10)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767834/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:41:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767834/content)


***

### [2024-09-11 16:46:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141722)
>
```
reverted the beta* levelling FF to the last state for POSITIVE  
  
We compare the last POSITIVE state (from mid-July) to what we would get if we would apply the FF to the beta* levelling FF which was set up for NEGATIVE. Up to a constant offset and the 2 first points where there was no LHCb lumi, we find back the old correction within ~1-2um i the separation plane!  
  
Crossing seems to be a bit less obvious - we will apply some FF.  
  
For the next time, we should consider reverting the beta* levelling FF to the last setting for the other polarity.  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 17:10:08)

[ LSA Applications Suite (v 16.6.26) 24-09-11_16:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767844/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 16:47:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767844/content)

[ LSA Applications Suite (v 16.6.26) 24-09-11_17:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767867/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 17:06:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767867/content)

[ LSA Applications Suite (v 16.6.26) 24-09-11_17:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767871/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 17:04:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767871/content)


***

### [2024-09-11 16:46:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141721)
>IP8 optimization at 0.52cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:46:21)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_16:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767842/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:46:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767842/content)


***

### [2024-09-11 16:54:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141727)
>Both IP1 and 5 on target, sep levelling ON

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:54:33)

[Desktop 24-09-11_16:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767860/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 16:54:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767860/content)


***

### [2024-09-11 17:23:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141743)
>
```
I had to do some manipulation on IP1 and IP5 because during the B* 
    levelling by step I separated by hand with the wrong sign and we were sitting on 
    the wrong side of the separation,  creating over-shoots at each B* 
    levelling steps.

So I've put back the sep on teh right side and all is back in order (to avoid 
    going throw head-on, I first separated on the crossing, then switched the 
    beams on the separation plane, then removed the crossing separation)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 17:33:25)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_17:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767895/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 17:26:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767895/content)


***

### [2024-09-11 17:28:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141746)
>
```
Mathieu Donze noticed a LHC collimator control system close to memory saturation. It should nit dump the beam but prevent to prepare the collimators for next fill.  
Please call him when the beam is dumped, he's piquet
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 17:31:48)


***

### [2024-09-11 18:10:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141763)
>IP8 crossing optimissation : still very good

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 18:10:38)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-11_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767930/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 18:10:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767930/content)


***

### [2024-09-11 18:28:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141783)
>Global Post Mortem Event

Event Timestamp: 11/09/24 18:28:05.129
Fill Number: 10104
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 33567 / 33683 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 8-RF-b2: A T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/11 18:30:57)


***

### [2024-09-11 18:28:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141779)
>FGC for RF function in bad state generated an interlock

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/11 18:28:42)

[RF FGC STATUS - 4.1.4 24-09-11_18:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767963/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/11 18:28:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767963/content)

[BEAM DUMP INTERLOCK B2 24-09-11_18:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767965/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 18:28:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767965/content)


***

### [2024-09-11 18:29:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141780)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 18:29:21)


***

### [2024-09-11 18:29:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141781)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 18:29:24)


***

### [2024-09-11 18:29:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141782)
>
```
updating collisions BP - first incorporating value from the last trim before starting beta* levelling, then re-distributing target-correction to have the same target as before.  
  
This should bring us close to the lumi of LHCb at the start of beta* levelling (which we don't know exactly, but the working BRAN indicates some ~300)  
  
Re-generated the reference orbit for the collisions BP.  
  
- Michi  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 18:43:38)

[ LSA Applications Suite (v 16.6.26) 24-09-11_18:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767967/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 18:29:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767967/content)


***

### [2024-09-11 18:33:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141784)
>
```
Calling Mathieu Donze for the collimator issue.  
  
Calling the RF piquet for the RF-FGC issue..   

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 19:17:47)


