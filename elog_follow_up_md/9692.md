# FILL 9692
**start**:		 2024-05-31 06:15:47.907363525+02:00 (CERN time)

**end**:		 2024-05-31 11:26:49.658488525+02:00 (CERN time)

**duration**:	 0 days 05:11:01.751125


***

### [2024-05-31 06:15:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078791)
>LHC RUN CTRL: New FILL NUMBER set to 9692

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:15:48)


***

### [2024-05-31 06:18:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078800)
>
```
RF and ADT off  
  
Module V2B2 of the ADT doens't switch off since the intervention several days ago (before which it wasn't switching on)
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 07:26:05)

[ LHC RF CONTROL 24-05-31_06:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618834/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/31 06:19:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618834/content)

[ TRANSVERSE DAMPER CONTROL 24-05-31_06:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618836/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/31 06:19:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618836/content)


***

### [2024-05-31 06:19:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078798)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:19:28)


***

### [2024-05-31 06:21:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078801)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:21:13)


***

### [2024-05-31 06:22:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078803)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:22:08)


***

### [2024-05-31 06:23:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078805)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:23:16)


***

### [2024-05-31 06:23:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078807)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:23:25)


***

### [2024-05-31 06:25:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078808)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:25:41)


***

### [2024-05-31 06:25:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078810)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:25:42)


***

### [2024-05-31 06:25:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078812)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:25:43)


***

### [2024-05-31 06:25:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078816)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:25:55)


***

### [2024-05-31 06:25:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078817)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:25:58)


***

### [2024-05-31 06:26:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078819)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:26:04)


***

### [2024-05-31 06:26:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078821)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:26:10)


***

### [2024-05-31 06:27:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078822)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:27:56)


***

### [2024-05-31 06:29:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078824)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:29:07)


***

### [2024-05-31 06:29:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078826)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:29:40)


***

### [2024-05-31 06:35:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078828)
>LHC SEQ: preparing the whole LHC for access

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:35:28)


***

### [2024-05-31 06:35:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078829)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:35:30)


***

### [2024-05-31 06:44:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078832)
>LHC SEQ: ramping down ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:44:00)


***

### [2024-05-31 06:44:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078832)
>LHC SEQ: ramping down ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:44:00)


***

### [2024-05-31 06:44:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078833)
>LHC SEQ: ramp down ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:44:14)


***

### [2024-05-31 06:44:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078834)
>ALICE magnets ramping down

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:44:46)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-05-31_06:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618840/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:44:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618840/content)


***

### [2024-05-31 06:44:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078835)
>
```
exceptions timeout when closing valves
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:51:11)

[24-05-31_06:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618842/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:45:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618842/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-05-31_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618844/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:45:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618844/content)

[LHC vac 24-05-31_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618846/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:45:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618846/content)

[Access Zone: PLCstate 24-05-31_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618848/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 06:46:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618848/content)


***

### [2024-05-31 06:46:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078836)
>ALL LHC VACUUM VALVES CLOSED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:46:11)


***

### [2024-05-31 06:46:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078837)
>LHC SEQ: UNDULATOR L4 AND R4 OFF

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 06:46:44)


***

### [2024-05-31 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078842)
>
```

```
**SHIFT SUMMARY:**  
  
- quiet shift in SB  
- dumped at 6am after ~22.5h of SB to bridge SPS dipole replacement and prepared for access  
  
* To be noted:

  
- ADT Module V2B2 of the ADT doens't switch off since the intervention several days ago (before which it wasn't switching on)  
  
  

```
Daniele  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 07:25:54)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618850/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 07:25:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618850/content)


***

### [2024-05-31 08:59:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078853)
>VITO VIZZIELLO(VIVIZZIE) assigned RBAC Role: QPS-Piquet and will expire on: 31-MAY-24 10.59.25.291000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/31 08:59:27)


***

### [2024-05-31 09:15:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078861)
>Changing LHCb polarity

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:15:18)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618892/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:15:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618892/content)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618894/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:17:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618894/content)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618898/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:21:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618898/content)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618900/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:22:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618900/content)


***

### [2024-05-31 09:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078862)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.5.44) 24-05-31\_09:21.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:21:35)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618896/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:21:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618896/content)


***

### [2024-05-31 09:23:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078864)
>Checking the tune shift when getting into collisions. With LHCb positive polarity we had 2e-3 less trim on H trims (for both beams) with slightly different timing. V trims were the same. We leave it as they are and try like this

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:24:58)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618902/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:24:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618902/content)


***

### [2024-05-31 09:26:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078865)
>HUGUES THIESEN(HTHIESEN) assigned RBAC Role: PO-LHC-Piquet and will expire on: 31-MAY-24 11.26.31.885000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/31 09:26:34)


***

### [2024-05-31 09:28:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078868)
>TCL.6L1 settings back to 1.6 mm

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:29:08)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618904/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:29:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618904/content)


***

### [2024-05-31 09:30:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078869)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 31-MAY-24 11.30.00.016000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/31 09:30:02)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618908/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:30:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618908/content)

[ Generation Application connected to server LHC (PRO database) 24-05-31_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618910/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:32:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618910/content)

[ LSA Applications Suite (v 16.5.44) 24-05-31_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618912/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/31 09:32:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618912/content)


***

### [2024-05-31 09:39:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078872)
>MIGUEL CERQUEIRA BASTOS(MCB) assigned RBAC Role: PO-LHC-Piquet and will expire on: 31-MAY-24 11.39.53.458000 AM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/31 09:39:56)


***

### [2024-05-31 10:27:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078890)
>
```
RD2.R1 on STANDBY as requested by EPC
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/31 10:28:26)

[Circuit_RD2_R1:   24-05-31_10:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618975/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/31 10:27:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618975/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618979/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/31 10:28:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3618979/content)


***

### [2024-05-31 10:40:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078901)
> 

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/05/31 10:40:04)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619029/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/05/31 10:40:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619029/content)


***

### [2024-05-31 10:40:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078902)
> 

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/05/31 10:40:19)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619031/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/05/31 10:40:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619031/content)


***

### [2024-05-31 10:47:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078912)
>Flipping polarity of LHCB dipole

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 10:47:14)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-05-31_10:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619055/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 10:48:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619055/content)


***

### [2024-05-31 10:48:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078915)
>LHC SEQ: ramping down LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 10:48:54)


***

### [2024-05-31 11:23:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078947)
>Access completed

creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/31 11:23:54)


***

### [2024-05-31 11:24:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078948)
>Event created from ScreenShot Client.  
udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-05-31\_11:24.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 11:24:22)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-05-31_11:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619123/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 11:24:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619123/content)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-05-31_11:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619125/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 11:24:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619125/content)


***

### [2024-05-31 11:24:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078949)
>LHC SEQ: LHCB DIPOLE POLARITY=POS (RBXWSH.L8=POS, RBXWH.L8=NEG, RBXWSH.R8=NEG)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 11:24:51)


***

### [2024-05-31 11:25:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078950)
>LHC SEQ: ramping up LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 11:25:15)


***

### [2024-05-31 11:26:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4078951)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 11:26:42)


