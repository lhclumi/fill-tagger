# FILL 10085
**start**:		 2024-09-04 18:39:17.469863525+02:00 (CERN time)

**end**:		 2024-09-04 20:23:11.849988525+02:00 (CERN time)

**duration**:	 0 days 01:43:54.380125


***

### [2024-09-04 18:39:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137472)
>LHC RUN CTRL: New FILL NUMBER set to 10085

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:39:18)


***

### [2024-09-04 18:39:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137478)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:39:58)


***

### [2024-09-04 18:42:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137484)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:42:01)


***

### [2024-09-04 18:42:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137486)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:42:09)


***

### [2024-09-04 18:43:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137488)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:43:23)


***

### [2024-09-04 18:43:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137491)
>skipping

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 18:44:01)

[ LHC Sequencer Execution GUI (PRO) : 12.33.0  24-09-04_18:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755089/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 18:44:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755089/content)


***

### [2024-09-04 18:44:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137493)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:44:41)


***

### [2024-09-04 18:44:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137494)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:44:51)


***

### [2024-09-04 18:45:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137496)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:45:26)


***

### [2024-09-04 18:47:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137498)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:47:50)


***

### [2024-09-04 18:47:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137500)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:47:51)


***

### [2024-09-04 18:47:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137502)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:47:52)


***

### [2024-09-04 18:48:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137506)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:48:07)


***

### [2024-09-04 18:48:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137508)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:48:13)


***

### [2024-09-04 18:48:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137509)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:48:49)


***

### [2024-09-04 18:48:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137511)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:48:56)


***

### [2024-09-04 18:48:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137511)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:48:56)


***

### [2024-09-04 18:50:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137513)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:50:08)


***

### [2024-09-04 18:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137516)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:50:38)


***

### [2024-09-04 18:51:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137517)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:51:13)


***

### [2024-09-04 18:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137518)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 18:51:52)


***

### [2024-09-04 19:01:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137521)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:01:38)


***

### [2024-09-04 19:10:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137526)
>successfull

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 19:10:46)

[ LHC Sequencer Execution GUI (PRO) : 12.33.0  24-09-04_19:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755142/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 19:10:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755142/content)


***

### [2024-09-04 19:14:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137529)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:14:03)


***

### [2024-09-04 19:15:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137530)
>ATLAS blocking injection

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 19:15:34)

[LHC-EXPTS Handshakes 24-09-04_19:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755150/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 19:15:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755150/content)


***

### [2024-09-04 19:23:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137534)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:23:55)


***

### [2024-09-04 19:29:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137553)
>
```
Wirescans
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:03:54)

[ LHC WIRESCANNER APP 24-09-04_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755198/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:02:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755198/content)

[ LHC WIRESCANNER APP 24-09-04_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755200/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:02:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755200/content)

[ LHC WIRESCANNER APP 24-09-04_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755202/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:02:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755202/content)

[ LHC WIRESCANNER APP 24-09-04_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755204/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:02:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755204/content)


***

### [2024-09-04 19:32:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137535)
>INJECTION SIS WRAP VIEWER is stuck

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 19:32:39)

[ LHC-INJ-SIS Status 24-09-04_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755156/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 19:32:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755156/content)


***

### [2024-09-04 19:58:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137542)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:58:45)


***

### [2024-09-04 19:59:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137543)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:59:01)


***

### [2024-09-04 19:59:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137544)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 19:59:22)


***

### [2024-09-04 19:59:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137545)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:35:27 / 0:33:48 (104.9 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 19:59:23)


***

### [2024-09-04 20:00:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137546)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 20:00:35)


***

### [2024-09-04 20:00:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137548)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 20:00:36)


***

### [2024-09-04 20:01:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137550)
>
```
MAchine filled  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/04 20:03:29)

[ LHC Fast BCT v1.3.2 24-09-04_20:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755190/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 20:01:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755190/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-04_20:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755192/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 20:01:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755192/content)

[SPS Beam Quality Monitor - SPS.USER.LHC4 - SPS PRO INCA server - PRO CCDA 24-09-04_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755206/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/04 20:02:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755206/content)

[ LHC Fast BCT v1.3.2 24-09-04_20:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755208/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 20:03:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755208/content)

[ LHC Fast BCT v1.3.2 24-09-04_20:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755210/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 20:03:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755210/content)


***

### [2024-09-04 20:19:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137565)
>Global Post Mortem Event

Event Timestamp: 04/09/24 20:19:27.776
Fill Number: 10085
Accelerator / beam mode: PROTON PHYSICS / RAMP
Energy: 6048600 [MeV]
Intensity B1/B2: 37494 / 37500 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 6-ATLAS\_Det: B T -> F on CIB.US15.R1.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/04 20:22:19)


***

### [2024-09-04 20:19:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137559)
>
```
Dumped by ATLAS detector BIS  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/04 21:54:35)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-09-04_20:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755216/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 20:19:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755216/content)


***

### [2024-09-04 20:20:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137560)
>Until beam dump

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/04 20:20:24)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-04_20:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755218/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/04 20:20:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755218/content)

[ LHC Beam Losses Lifetime Display 24-09-04_20:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755220/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/04 20:20:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3755220/content)


***

### [2024-09-04 20:21:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137561)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 20:21:54)


***

### [2024-09-04 20:21:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137563)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 20:21:58)


***

### [2024-09-04 20:22:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4137564)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/04 20:22:01)


