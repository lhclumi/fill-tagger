# FILL 10260
**start**:		 2024-10-20 07:24:25.148738525+02:00 (CERN time)

**end**:		 2024-10-20 08:54:18.186988525+02:00 (CERN time)

**duration**:	 0 days 01:29:53.038250


***

### [2024-10-20 07:24:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167633)
>LHC RUN CTRL: New FILL NUMBER set to 10260

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 07:24:25)


***

### [2024-10-20 07:24:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167634)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 07:24:54)


***

### [2024-10-20 07:26:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167636)
>Left: negative octupole polarity,
Right: positive octupole polarity.

lifetimes are better with positive polarity but instabilities also come 
 earlier and harder.

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 07:27:25)

[ LHC Beam Losses Lifetime Display 24-10-20_07:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831462/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 07:29:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831462/content)


***

### [2024-10-20 07:35:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167638)
>tunes tuned for negative octupole polarity

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 07:35:28)

[ Accelerator Cockpit v0.0.41 24-10-20_07:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831465/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 07:35:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831465/content)


***

### [2024-10-20 07:36:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167639)
>coupling corrected

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 07:37:04)

[ Accelerator Cockpit v0.0.41 24-10-20_07:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831467/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 07:37:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831467/content)


***

### [2024-10-20 07:41:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167642)
>MO = 3.2/2.5 (= "negative" polarity)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 07:41:20)

[ LSA Applications Suite (v 16.6.34) 24-10-20_07:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831469/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 07:41:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831469/content)


***

### [2024-10-20 07:41:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167641)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 07:41:16)


***

### [2024-10-20 07:49:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167643)
>
```
Abort gap cleaning was not working, no visible sweep in the tune spectrum - V (abort gap) cleaning window function in HW was 0 since ~6:20!  
  
Re-driven the window functions for AbortGapCleanVB{1,2} from LSA.  
It looks like the abort gap check application can still overwrite them.  
  
This explains the increased abort gap populations and following losses during dumps in IR6/3/7 during the previous fill...  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:09:40)

[ TuneViewer Light V5.7.2 24-10-20_07:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831471/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/10/20 07:49:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831471/content)

[ LSA Applications Suite (v 16.6.34) 24-10-20_07:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831473/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 07:50:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831473/content)

[Mozilla Firefox 24-10-20_08:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831481/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:00:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831481/content)

[cs-513-michi9 24-10-20_08:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831501/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:08:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831501/content)


***

### [2024-10-20 07:58:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167645)
>lhc fill halfway through

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 07:58:22)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-20_07:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831477/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 08:16:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831477/content)


***

### [2024-10-20 08:12:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167648)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 08:12:31)


***

### [2024-10-20 08:12:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167649)
>
```
Full with negative polarity.
```

```
  
No instabilities, emittances calm.  
  
Lifetimes slightly lower than a standard physics fill (e.g. compared to BPT), but still > 100h for both beams.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:15:57)

[ LHC Fast BCT v1.3.2 24-10-20_08:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831503/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:13:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831503/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-20_08:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831505/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:12:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831505/content)

[ LHC Beam Losses Lifetime Display 24-10-20_08:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831507/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:15:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831507/content)

[Mozilla Firefox 24-10-20_08:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831509/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:15:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831509/content)

[ Beam Intensity - v1.5.0 24-10-20_08:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831511/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:16:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831511/content)


***

### [2024-10-20 08:31:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167657)
>
```
**# MD 9551 E-cloud heat load with high intensity at injection (25 ns)**  
  
Close to 100% availability with no major issue.  
  
We did four fills to measure heat load, staying for around 1 hour at injection to have the heat loads stabilized.  
First we took the 2.3e11 p/b, then went with 1.9e11 p/b, 1.5e11 p/b and finally 1.2e11 p/b. All fills had the operational octupole and chromaticity settings.   
  
Injecting the 2.3e11 p/b beams was easy. The fill with low intensity was very unstable, confirming once again that instability issues become more important with lower bunch intensities.  
  
A careful analysis of the heat loads will be done offline.  

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 08:51:07)


***

### [2024-10-20 08:36:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167661)
>
```
**# MD12804 Negative octupole polarity and electron clouds at injection energy**  
  
Once again, we had close to 100% availability with no major issue.  
  
We started with finding the optimal tune for lifetime when putting the octupoles at a negative polarity (knob = 4.2). We found it around 62.293/60.313 as expected by DA simulations.   
  
We then measured the instability thresholds with the two polarities and chromaticity set to 20. For positive octupole polarity, the threshold for instability is larger by about 1 unit in the knob.  
  
Generally, the lifetime is better with positive polarity but it is still acceptable (> 100h) even with negative polarity.  
  
We finished the MD by doing an operational-style filling but using the tunes of 62.295/60.313, negative polarity octupoles: MO = 3.2/2.5 (B1/B2). The typical operational settings are 62.275/60.293 with octupoles MO = -4.2/-3.5 (B1/B2). No instability was observed and the lifetime was above 100 hours.  
  
The MD was successful and the LHC can run with negative octupole polarity at injection comfortably.  
  
The two e-cloud MDs were efficient and we finished one hour ahead of time.  
  

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/10/20 08:46:13)


***

### [2024-10-20 08:44:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167662)
>
```
debunching rate with a full machine staying at injection:   
B1 ~1e12 p / 15 min  
B2 ~1e12 p / 13 min  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:46:58)

[ Beam Intensity - v1.5.0 24-10-20_08:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831551/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:45:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831551/content)

[Abort gap cleaning control v2.1.0 24-10-20_08:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831561/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 08:58:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831561/content)


***

### [2024-10-20 08:53:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167668)
>Global Post Mortem Event

Event Timestamp: 20/10/24 08:53:53.463
Fill Number: 10260
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449760 [MeV]
Intensity B1/B2: 37868 / 37674 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/20 08:56:45)


***

### [2024-10-20 08:53:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167675)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: mihostet / end of MD12804 - full beam waiting ~30 min at injection.


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/20 09:09:02)


***

### [2024-10-20 08:54:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167666)
>LHC RUN CTRL: New FILL NUMBER set to 10261

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 08:54:19)


