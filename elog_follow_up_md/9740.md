# FILL 9740
**start**:		 2024-06-08 22:22:59.946238525+02:00 (CERN time)

**end**:		 2024-06-09 00:34:48.620363525+02:00 (CERN time)

**duration**:	 0 days 02:11:48.674125


***

### [2024-06-08 22:23:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084159)
>LHC RUN CTRL: New FILL NUMBER set to 9740

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 22:23:00)


***

### [2024-06-08 22:23:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084160)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 22:23:19)


***

### [2024-06-08 22:27:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084163)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 22:27:50)


***

### [2024-06-08 22:30:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084164)
>increasing RF total voltage

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:30:36)

[ LSA Applications Suite (v 16.6.1) 24-06-08_22:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630575/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:26:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630575/content)


***

### [2024-06-08 22:54:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084174)
>
```
*Sent By LHC WS APP*
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:21:56)

[tmpScreenshot_1717880373.4436643.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630589/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:59:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630589/content)


***

### [2024-06-08 22:55:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084173)
>
```
*Sent By LHC WS APP*
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:21:49)

[tmpScreenshot_1717880367.628301.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630587/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:59:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630587/content)


***

### [2024-06-08 22:56:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084172)
>
```
*Sent By LHC WS APP*
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:21:41)

[tmpScreenshot_1717880363.9796565.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630585/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:59:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630585/content)


***

### [2024-06-08 22:57:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084171)
>
```
*Sent By LHC WS APP*
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:21:33)

[tmpScreenshot_1717880358.2196333.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630583/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:59:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630583/content)


***

### [2024-06-08 22:58:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084170)
>
```
*Sent By LHC WS APP*
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:21:11)

[tmpScreenshot_1717880349.6374102.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630581/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:59:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630581/content)


***

### [2024-06-08 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084178)
>
```

```
**SHIFT SUMMARY:**  
  
- got the machine during collimator alignemnt at flat top for HL-LHC optics MD  
- dumped and updated collimator centre functions  
- interlock on crab cavities in SPS prevented injecting B1 and went for a new ramp with HL-LHC optics for loss maps during the ramp with nominal collimator settings  
- MD completed succesfully ([here](https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4084097) the summary) and moved to the new one on longitudinal halo scraping  
- issues with SPS beam dump prevented starting MD before ~21:15   
- leaving the machine while scraping  
  
* To be noted:

  
- TL energy matching performed and to be propagated  
- QPS reset of RBs failed  
  
* To be reverted:

  
- RF settings   
  

```
  
Daniele  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:20:59)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630597/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 23:20:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630597/content)


***

### [2024-06-08 23:39:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084183)
>Event created from ScreenShot Client.  
LHC Beam Quality Monitor - LHC.USER.ALL - INCA DISABLED - PRO CCDA 24-06-08\_23:39.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/08 23:39:19)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-08_23:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630608/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/08 23:39:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630608/content)


***

### [2024-06-09 00:11:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084190)
>Event created from ScreenShot Client.  
LHC Beam Quality Monitor - LHC.USER.ALL - INCA DISABLED - PRO CCDA 24-06-09\_00:11.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 00:11:40)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-09_00:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630622/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 00:11:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630622/content)


***

### [2024-06-09 00:26:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084195)
>Forced Safe beam flag to normal as we are below 5e11 p/b

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:27:31)

[Safe Machine Parameters in CCC : Overview GUI 24-06-09_00:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630632/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:27:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630632/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.6  24-06-09_00:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630640/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 00:33:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630640/content)


***

### [2024-06-09 00:28:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084196)
>checked that BPMs are masked

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:29:24)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-06-09_00:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630634/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:29:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630634/content)


***

### [2024-06-09 00:31:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084197)
>indeed, the interlock came

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:31:56)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-06-09_00:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630636/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:31:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630636/content)

[ LINAC4.SPS.LHC BIS Monitor V12.5.0 24-06-09_00:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630638/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/09 00:32:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630638/content)


***

### [2024-06-09 00:34:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084200)
>LHC RUN CTRL: New FILL NUMBER set to 9741

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 00:34:48)


