# FILL 10223
**start**:		 2024-10-13 19:42:55.331738525+02:00 (CERN time)

**end**:		 2024-10-14 06:47:29.699863525+02:00 (CERN time)

**duration**:	 0 days 11:04:34.368125


***

### [2024-10-13 19:42:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162343)
>LHC RUN CTRL: New FILL NUMBER set to 10223

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:42:56)


***

### [2024-10-13 19:44:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162349)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:44:39)


***

### [2024-10-13 19:44:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162350)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:44:57)


***

### [2024-10-13 19:46:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162352)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:46:45)


***

### [2024-10-13 19:46:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162354)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:46:47)


***

### [2024-10-13 19:48:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162355)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:48:07)


***

### [2024-10-13 19:49:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162358)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:49:16)


***

### [2024-10-13 19:49:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162359)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:49:33)


***

### [2024-10-13 19:50:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162361)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:50:06)


***

### [2024-10-13 19:50:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162362)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:50:12)


***

### [2024-10-13 19:52:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162364)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:52:29)


***

### [2024-10-13 19:52:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162365)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:52:37)


***

### [2024-10-13 19:52:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162367)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:52:38)


***

### [2024-10-13 19:52:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162369)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:52:41)


***

### [2024-10-13 19:52:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162373)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:52:56)


***

### [2024-10-13 19:53:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162375)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:53:28)


***

### [2024-10-13 19:53:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162377)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:53:35)


***

### [2024-10-13 19:54:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162378)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:54:54)


***

### [2024-10-13 19:54:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162380)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:54:55)


***

### [2024-10-13 19:55:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162382)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:55:30)


***

### [2024-10-13 19:56:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162383)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 19:56:38)


***

### [2024-10-13 20:08:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162390)
>
```
Turned ON a couple that were interlocking the aperture of the vlaves around the quenched triplet  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:06:55)

[Synoptic LHC 24-10-13_20:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819772/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 20:09:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819772/content)

[VVGRF.221.1L1.X 24-10-13_20:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819774/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 20:09:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819774/content)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-10-13_20:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819776/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 20:09:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819776/content)

[History 24-10-13_20:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819778/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 20:09:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819778/content)


***

### [2024-10-13 20:10:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162391)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 20:10:21)


***

### [2024-10-13 20:16:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162392)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 20:16:07)


***

### [2024-10-13 20:22:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162396)
>
```
Task failed but looking around all seems OK  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:08:09)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-10-13_20:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819785/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 20:22:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819785/content)

[ LHC SIS GUI 24-10-13_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819792/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 20:23:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819792/content)


***

### [2024-10-13 20:23:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162398)
>Forced board B

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/13 20:23:19)

[ LHC CIRCUIT SUPERVISION v9.0 24-10-13_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819790/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/13 20:23:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819790/content)


***

### [2024-10-13 20:27:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162399)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 20:27:14)


***

### [2024-10-13 20:31:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162400)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 20:31:52)


***

### [2024-10-13 21:02:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162403)
>Filling with OFB ON

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:02:57)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-13_21:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819800/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:02:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819800/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-13_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819802/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:03:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819802/content)


***

### [2024-10-13 21:04:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162404)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:04:16)


***

### [2024-10-13 21:04:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162405)
>
```
Machine filled
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:09:33)

[ LHC Beam Losses Lifetime Display 24-10-13_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819804/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:04:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819804/content)

[ LHC Fast BCT v1.3.2 24-10-13_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:04:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819806/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-13_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819808/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:04:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819808/content)


***

### [2024-10-13 21:04:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162406)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:04:28)


***

### [2024-10-13 21:04:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162407)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:04:37)


***

### [2024-10-13 21:04:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162408)
>LHC Injection Complete

Number of injections actual / planned: 49 / 48
SPS SuperCycle length: 43.2 [s]
Actual / minimum time: 0:32:47 / 0:39:33 (82.9 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:04:38)


***

### [2024-10-13 21:06:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162409)
>QFBON before start RAMP

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:06:36)

[ LHC Beam Losses Lifetime Display 24-10-13_21:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819810/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:06:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819810/content)


***

### [2024-10-13 21:06:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162410)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:06:47)


***

### [2024-10-13 21:06:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162412)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:06:48)


***

### [2024-10-13 21:28:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162421)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:28:08)


***

### [2024-10-13 21:28:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162423)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:28:42)


***

### [2024-10-13 21:28:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162423)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:28:42)


***

### [2024-10-13 21:28:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162425)
>RAMP

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:28:49)

[ LHC Beam Losses Lifetime Display 24-10-13_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819818/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:28:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819818/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-13_21:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819820/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:29:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819820/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-13_21:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819822/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:29:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819822/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-13_21:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819824/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/13 21:29:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819824/content)


***

### [2024-10-13 21:37:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162426)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:37:13)


***

### [2024-10-13 21:37:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162427)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:37:57)


***

### [2024-10-13 21:38:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162429)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:38:57)


***

### [2024-10-13 21:39:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162430)
>Tunes before collisions

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:39:59)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-13_21:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819826/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/13 21:39:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819826/content)


***

### [2024-10-13 21:41:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162431)
>
```
Lifetime on BV2 going in collisions, reaching 10h
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:10:35)

[ LHC Beam Losses Lifetime Display 24-10-13_21:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819828/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:41:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819828/content)


***

### [2024-10-13 21:43:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162432)
>
```
IP optimization  
Offset in IP5 is building up in the last fills  
Offset in IP1 most likely after the quench even though from WPS did not seem excessive  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:11:17)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-13_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819830/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:43:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819830/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-13_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819832/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:44:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819832/content)

[Inner Triplets Alignment Display V5.3.5 24-10-13_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819834/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:47:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819834/content)

[Inner Triplets Alignment Display V5.3.5 24-10-13_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819836/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:47:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819836/content)

[Inner Triplets Alignment Display V5.3.5 24-10-13_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819838/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:47:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819838/content)

[ LHC Beam Losses Lifetime Display 24-10-13_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819840/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/13 21:47:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819840/content)


***

### [2024-10-13 21:44:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162433)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:44:47)


***

### [2024-10-13 21:47:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162434)
>No instability observed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:48:04)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-13_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819842/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 21:48:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819842/content)

[ ADT Activity Monitor 24-10-13_21:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819844/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/10/13 21:48:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819844/content)

[ ADT Activity Monitor 24-10-13_21:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819846/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/10/13 21:48:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819846/content)


***

### [2024-10-13 21:51:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162435)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/13 21:51:02)


***

### [2024-10-13 22:02:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162439)
>Emittance scan finished in IP1/5

Seen the centers, i will reoptimize

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:02:40)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-13_22:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819848/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:02:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819848/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-13_22:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819850/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:02:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819850/content)


***

### [2024-10-13 22:04:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162440)
>reoptimized IP1/5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:04:56)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-13_22:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819852/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:04:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819852/content)


***

### [2024-10-13 22:16:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162443)
>
```
Incorporated back in Physics BP (on correction)  
 the found offset of Lumi scan knobs after optimization  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:12:16)

[ LSA Applications Suite (v 16.6.34) 24-10-13_22:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819854/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:16:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819854/content)

[ LSA Applications Suite (v 16.6.34) 24-10-13_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819856/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/13 22:17:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819856/content)


***

### [2024-10-13 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162453)
>
```
**SHIFT SUMMARY:**  
  
Cryo recovery almost 4h after Triplet Quench  
Smooth filling and cycle till Stable Beams  
Offset at IP1/5 after optimization incorporated back in Physics  
Leaving the machine in Stable beams  
  
~~ George ~~  
  
* To be noted:

The broken corrector in R7 (RCBH29.R7B1) is still pending intervention before the MDs as it is not garanteed it has enough margin for the foreseen "exotic cycle"  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:15:37)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819870/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/13 23:15:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819870/content)


***

### [2024-10-14 03:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162475)
>lost ATLAS toroid

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 03:42:37)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-10-14_03:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819896/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 03:42:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819896/content)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-10-14_03:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819898/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 03:42:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819898/content)


***

### [2024-10-14 03:59:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162476)
>
```
keeping IP1 on exp. levelling upon ATLAS request
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 04:00:21)


***

### [2024-10-14 05:49:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162483)
>IP5 head-on after ~8h in SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 05:49:56)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-14_05:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 05:49:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819902/content)

[Beam process - 0.4.2 24-10-14_05:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819904/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/14 05:50:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819904/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-14_05:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819908/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 05:50:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819908/content)


***

### [2024-10-14 06:05:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162487)
>IP1 head-on as well

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:05:47)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-14_06:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819921/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:06:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819921/content)

[Beam process - 0.4.2 24-10-14_06:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819923/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/14 06:05:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819923/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-14_06:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819925/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:06:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819925/content)


***

### [2024-10-14 06:07:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162488)
>BBLR switched ON

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/14 06:07:57)

[Set BBLR_PC 24-10-14_06:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819927/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/14 06:07:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819927/content)

[ LHC Beam Losses Lifetime Display 24-10-14_06:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819929/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/14 06:08:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819929/content)


***

### [2024-10-14 06:38:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162493)
>Global Post Mortem Event

Event Timestamp: 14/10/24 06:38:14.590
Fill Number: 10223
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 27283 / 27256 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.UA23.L2.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/14 06:41:08)


***

### [2024-10-14 06:38:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162490)
>dump

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/14 06:38:47)

[Set SECTOR12 24-10-14_06:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819937/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/14 06:38:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819937/content)

[Circuit_RSD2_A12B1:   24-10-14_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819939/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:41:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819939/content)

[Circuit_RSD1_A12B1:   24-10-14_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819941/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:41:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819941/content)

[Circuit_RSF1_A12B1:   24-10-14_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819943/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:41:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819943/content)

[Circuit_RSF2_A12B1:   24-10-14_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819945/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:41:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819945/content)

[Mozilla Firefox 24-10-14_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819949/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:46:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819949/content)

[History Buffer 24-10-14_06:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819959/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:58:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819959/content)


***

### [2024-10-14 06:39:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162491)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/14 06:40:00)


***

### [2024-10-14 06:40:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162492)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/14 06:40:02)


***

### [2024-10-14 06:43:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4162494)
>XPOC latched for missing data in TSU

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:44:00)

[ XPOC Viewer - PRO 24-10-14_06:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819947/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/14 06:44:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3819947/content)


