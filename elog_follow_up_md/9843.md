# FILL 9843
**start**:		 2024-06-30 19:19:48.667988525+02:00 (CERN time)

**end**:		 2024-07-01 02:16:12.864238525+02:00 (CERN time)

**duration**:	 0 days 06:56:24.196250


***

### [2024-06-30 19:20:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097380)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:20:44)


***

### [2024-06-30 19:22:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097381)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:22:58)


***

### [2024-06-30 19:23:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097383)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:23:21)


***

### [2024-06-30 19:24:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097384)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:24:36)


***

### [2024-06-30 19:25:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097386)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:25:43)


***

### [2024-06-30 19:25:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097388)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:25:52)


***

### [2024-06-30 19:26:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097389)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:26:39)


***

### [2024-06-30 19:26:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097391)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:26:42)


***

### [2024-06-30 19:29:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097392)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:04)


***

### [2024-06-30 19:29:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097394)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:05)


***

### [2024-06-30 19:29:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097396)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:07)


***

### [2024-06-30 19:29:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097400)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:21)


***

### [2024-06-30 19:29:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097402)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:40)


***

### [2024-06-30 19:29:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097404)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:29:46)


***

### [2024-06-30 19:31:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097405)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:31:21)


***

### [2024-06-30 19:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097407)
>Event created from ScreenShot Client.  
RF INTERLOCKS 24-06-30\_19:32.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:32:27)

[RF INTERLOCKS 24-06-30_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655756/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:32:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655756/content)

[ LHC RF CONTROL 24-06-30_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655758/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:32:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655758/content)


***

### [2024-06-30 19:33:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097408)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:33:05)


***

### [2024-06-30 19:35:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097410)
>OK after reset

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:35:40)

[RF INTERLOCKS 24-06-30_19:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655760/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:35:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655760/content)

[ LHC RF CONTROL 24-06-30_19:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655762/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/30 19:35:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655762/content)


***

### [2024-06-30 19:36:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097411)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:36:04)


***

### [2024-06-30 19:36:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097413)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:36:40)


***

### [2024-06-30 19:37:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097414)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:37:14)


***

### [2024-06-30 19:52:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097416)
>
```
again had to force board A for circuit_RCBXH2_R8
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 19:53:27)

[Circuit_RCBXH2_R8:   24-06-30_19:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655768/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 19:52:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655768/content)


***

### [2024-06-30 19:53:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097417)
>still QPS reset RBs failed without issue

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 19:54:29)

[ LHC Sequencer Execution GUI (PRO) : 12.32.12  24-06-30_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655770/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 19:54:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655770/content)

[ LHC CIRCUIT SUPERVISION v9.0 24-06-30_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655772/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 19:54:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655772/content)


***

### [2024-06-30 19:57:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097418)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 19:57:31)


***

### [2024-06-30 20:06:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097420)
>issue with getting pilot in the PS  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:06:53)


***

### [2024-06-30 20:08:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097422)
>corrections with pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:09:02)

[RF Trim Warning 24-06-30_20:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655781/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:09:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655781/content)

[Accelerator Cockpit v0.0.37 24-06-30_20:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655787/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:10:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655787/content)

[Accelerator Cockpit v0.0.37 24-06-30_20:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655789/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:10:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655789/content)

[Accelerator Cockpit v0.0.37 24-06-30_20:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655791/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:11:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655791/content)


***

### [2024-06-30 20:13:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097426)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:13:31)


***

### [2024-06-30 20:17:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097427)
>TL with 12 bunches

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:17:41)

[Desktop 24-06-30_20:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655793/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:17:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655793/content)


***

### [2024-06-30 20:20:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097428)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:03)

[tmpScreenshot_1719771603.4952533.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655795/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655795/content)


***

### [2024-06-30 20:20:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097429)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:11)

[tmpScreenshot_1719771611.679321.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655797/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655797/content)


***

### [2024-06-30 20:20:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097430)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:15)

[tmpScreenshot_1719771615.055279.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655799/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:20:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655799/content)


***

### [2024-06-30 20:21:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097431)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:21:06)

[tmpScreenshot_1719771666.6080685.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655801/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:21:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655801/content)


***

### [2024-06-30 20:28:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097432)
>Event created from ScreenShot Client.  
RF Trim Warning 24-06-30\_20:28.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:28:46)

[RF Trim Warning 24-06-30_20:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655803/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 20:28:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655803/content)


***

### [2024-06-30 20:35:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097433)
>Event created from ScreenShot Client.  
LHC Injection Quality Check 3.17.8 24-06-30\_20:35.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:35:51)

[ LHC Injection Quality Check 3.17.8 24-06-30_20:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655805/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:35:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655805/content)


***

### [2024-06-30 20:45:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097434)
>TL with trains

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:45:33)

[OpenYASP V4.8.10   LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 . SPS.USER.LHC3 24-06-30_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655807/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/06/30 20:45:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655807/content)


***

### [2024-06-30 20:50:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097435)
>status end of filling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:50:43)

[ LHC Fast BCT v1.3.2 24-06-30_20:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655809/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:51:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655809/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-30_20:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655811/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 20:50:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655811/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-30_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655813/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 20:51:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655813/content)

[ LHC Bunch Length 24-06-30_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655815/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 20:51:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655815/content)

[ LHC RF Beam Spectra Display v0.3.5 24-06-30_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655817/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 20:51:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655817/content)


***

### [2024-06-30 20:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097436)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:50:50)


***

### [2024-06-30 20:50:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097437)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:50:59)


***

### [2024-06-30 20:51:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097438)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:51:03)


***

### [2024-06-30 20:51:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097439)
>LHC Injection CompleteNumber of injections actual / planned: 55 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:37:33 / 0:33:48 (111.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 20:51:04)


***

### [2024-06-30 20:54:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097440)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:54:40)


***

### [2024-06-30 20:54:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097442)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 20:54:40)


***

### [2024-06-30 21:00:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097444)
>Event created from ScreenShot Client.  
LHC BLM Fixed Display 24-06-30\_21:00.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/30 21:00:35)

[ LHC BLM Fixed Display 24-06-30_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655819/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/30 21:00:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655819/content)


***

### [2024-06-30 21:03:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097445)
>Event created from ScreenShot Client.  
LHC RF Beam Spectra Display v0.3.5 24-06-30\_21:03.png

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:03:05)

[ LHC RF Beam Spectra Display v0.3.5 24-06-30_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655821/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:03:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655821/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-30_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655823/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:03:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655823/content)

[ LHC RF Beam Spectra Display v0.3.5 24-06-30_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655825/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:04:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655825/content)


***

### [2024-06-30 21:15:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097446)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:15:58)


***

### [2024-06-30 21:16:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097448)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:16:13)


***

### [2024-06-30 21:24:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097450)
>status at 1.2 m

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:24:34)

[ LHC Fast BCT v1.3.2 24-06-30_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655827/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:24:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655827/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-30_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655829/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:24:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655829/content)

[ LHC RF Beam Spectra Display v0.3.5 24-06-30_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655831/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:24:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655831/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-30_21:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655833/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/30 21:25:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655833/content)


***

### [2024-06-30 21:24:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097451)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:24:42)


***

### [2024-06-30 21:25:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097452)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:25:26)


***

### [2024-06-30 21:26:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097454)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:26:14)


***

### [2024-06-30 21:26:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097454)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:26:14)


***

### [2024-06-30 21:30:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097455)
>optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:30:40)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_21:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655835/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:31:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655835/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_21:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655837/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:31:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655837/content)


***

### [2024-06-30 21:32:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097456)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:32:08)


***

### [2024-06-30 21:38:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097457)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:38:35)


***

### [2024-06-30 21:40:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097458)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/30 21:40:32)


***

### [2024-06-30 21:42:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097459)
>lifetimes in collision

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 21:43:15)

[ LHC Beam Losses Lifetime Display 24-06-30_21:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655839/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 21:43:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655839/content)


***

### [2024-06-30 21:45:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097460)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:45:19)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_21:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655841/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:45:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655841/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_21:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655843/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 21:45:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655843/content)


***

### [2024-06-30 21:51:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097465)
>
```
beta* levelling is stuck, probably because of an attempt at a tune trim I did too late
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:12:53)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_21:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655845/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:00:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655845/content)


***

### [2024-06-30 21:59:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097464)
>MICHAEL ARTUR HOSTETTLER(MIHOSTET) assigned RBAC Role: LHC-OP-SOFTWARE-Piquet and will expire on: 30-JUN-24 11.59.03.058000 PM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/30 21:59:05)


***

### [2024-06-30 22:16:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097467)
>
```
Effect of tune trim +1e-3 on B1H. Beam 1 lifetime very bad compared to beam 2 from page 1 
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:30:44)

[ LHC Beam Losses Lifetime Display 24-06-30_22:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655871/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 22:16:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655871/content)

[Mozilla Firefox 24-06-30_22:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655897/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:29:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655897/content)


***

### [2024-06-30 22:25:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097468)
>Michi restarted the lumi scan server. Thanks Matteo and Michi for the help!  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:26:55)


***

### [2024-06-30 22:34:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097469)
>
```
tune trims +2e-3 on B2H and +1e-3 on B1H at 56 cm
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:43:37)

[ LHC Beam Losses Lifetime Display 24-06-30_22:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655921/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 22:34:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655921/content)


***

### [2024-06-30 22:35:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097470)
>
```
ATLAS lost quite some lumi compared to CMS during beta* levelling
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 23:00:46)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655927/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:36:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655927/content)


***

### [2024-06-30 22:44:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097471)
>status of levellings at 49 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:44:21)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655932/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:44:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655932/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655934/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:44:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655934/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655936/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:44:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655936/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655938/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:44:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655938/content)


***

### [2024-06-30 22:57:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097472)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30\_22:57.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:57:40)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-06-30_22:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655940/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/30 22:57:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655940/content)


***

### [2024-06-30 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097477)
>
```
**SHIFT SUMMARY:**  
  
Arrived in stable beams when cryo had an issue with cooling the triplets in IR5 and the beams were separated in CMS to help recover conditions. CMS was put back head-on after cryo recovered the situation. It seems a sensor was giving the wrong temperature and it was discarded in the following fill.  
  
Smooth ramp down, refill and ramp. I started beta* levelling but noticed that B1 lifetime was not great and the converters were not armed yet, so I thought I could squeeze in a tune trim to recover lifetime. That was a bad idea as this froze beta* levelling without sending the tune trim. Many thanks to Michi and Matteo who helped in remote to revert the situation.  
  
A tune trim of +1e-3 in B1H at 120 cm was clearly beneficial and recovered lifetime.  
Further tune trims at 56 cm were also beneficial (+1E-3 in B2H was clear, but an additional +1e-3 in both B1H and B2H were less clear).  
  
Leaving the machine at 45 cm with all IPS in separation levelling.   
  
  
* To be noted:

  
- access needed by CMS (2 hours during working hours)  
- again had to force board A for circuit_RCBXH2_R8 (QPS piquet had advised a power cycle when possible)  
  
Benoit  
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/01 00:05:31)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655954/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/30 23:18:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655954/content)


***

### [2024-06-30 22:59:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097474)
>the FEC looks ok though

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/06/30 23:00:14)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-06-30_22:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655942/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/06/30 23:00:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655942/content)


***

### [2024-06-30 23:07:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097476)
>lifetime step in B1 at 45cm

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 23:07:37)

[ LHC Beam Losses Lifetime Display 24-06-30_23:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655952/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/30 23:07:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3655952/content)


***

### [2024-07-01 02:05:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097485)
>Global Post Mortem EventEvent Timestamp: 01/07/24 02:05:43.667Fill Number: 9843Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799680 [MeV]Intensity B1/B2: 29762 / 31665 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.USC55.R5.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/01 02:08:37)


***

### [2024-07-01 02:06:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097481)
>dump

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/01 02:06:13)

[Set SECTOR56 24-07-01_02:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656007/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/01 02:06:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656007/content)


***

### [2024-07-01 02:07:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097482)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 02:07:23)


***

### [2024-07-01 02:07:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097483)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/01 02:07:26)


***

### [2024-07-01 02:08:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097484)
>QPS sending PM data

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:08:28)

[Circuit_RQT13_R5B1:   24-07-01_02:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656009/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:08:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656009/content)


***

### [2024-07-01 02:09:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097486)
>PC faults

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:09:36)

[ Equip State 24-07-01_02:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656011/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:09:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656011/content)

[ Equip State 24-07-01_02:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656013/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:09:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656013/content)


***

### [2024-07-01 02:12:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4097487)
>only ~38A, not sending email to MP3

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:12:42)

[Mozilla Firefox 24-07-01_02:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656015/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/01 02:12:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3656015/content)


