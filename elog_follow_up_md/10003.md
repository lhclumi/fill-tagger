# FILL 10003
**start**:		 2024-08-14 18:04:58.180613525+02:00 (CERN time)

**end**:		 2024-08-14 18:40:55.439488525+02:00 (CERN time)

**duration**:	 0 days 00:35:57.258875


***

### [2024-08-14 18:04:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125894)
>LHC RUN CTRL: New FILL NUMBER set to 10003

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/14 18:04:59)


***

### [2024-08-14 18:07:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125895)
>Event created from ScreenShot Client.  
Set SECTOR23 24-08-14\_18:07.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/14 18:07:13)

[Set SECTOR23 24-08-14_18:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730884/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/14 18:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730884/content)


***

### [2024-08-14 18:13:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125897)
>communication lost with 4 boards in sector 23  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/14 18:14:03)

[ LHC CIRCUIT SUPERVISION v9.0 24-08-14_18:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730886/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/14 18:15:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730886/content)


***

### [2024-08-14 18:18:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125898)
>powercycling the crate for which the 4 boards lost communication

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/14 18:18:50)

[qps-lhc-swisstool 24-08-14_18:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730888/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/14 18:18:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730888/content)

[Success 24-08-14_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730890/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/14 18:20:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3730890/content)


***

### [2024-08-14 18:28:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125899)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/14 18:28:04)


***

### [2024-08-14 18:40:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4125901)
>LHC RUN CTRL: New FILL NUMBER set to 10004

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/14 18:40:56)


