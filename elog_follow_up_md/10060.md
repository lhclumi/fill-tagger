# FILL 10060
**start**:		 2024-08-26 18:27:21.501488525+02:00 (CERN time)

**end**:		 2024-08-26 19:51:08.725488525+02:00 (CERN time)

**duration**:	 0 days 01:23:47.224000


***

### [2024-08-26 18:27:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132178)
>LHC RUN CTRL: New FILL NUMBER set to 10060

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:27:22)


***

### [2024-08-26 18:28:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132184)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:28:09)


***

### [2024-08-26 18:30:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132185)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:30:10)


***

### [2024-08-26 18:30:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132187)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:30:19)


***

### [2024-08-26 18:31:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132188)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:31:34)


***

### [2024-08-26 18:31:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132190)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:31:46)


***

### [2024-08-26 18:32:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132191)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:32:49)


***

### [2024-08-26 18:33:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132192)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:33:00)


***

### [2024-08-26 18:33:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132194)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:33:38)


***

### [2024-08-26 18:33:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132194)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:33:38)


***

### [2024-08-26 18:33:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132196)
>larger losses in the dump lines than usual

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 18:33:56)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-08-26_18:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743100/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 18:35:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743100/content)


***

### [2024-08-26 18:34:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132198)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:34:10)


***

### [2024-08-26 18:34:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132199)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:34:45)


***

### [2024-08-26 18:35:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132221)
>
```
The EPC piquet called back, reset and restarted the corrector converter and gave the ok to restart. If it comes back, EPC will have to access to exchange the power modules.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 18:52:15)


***

### [2024-08-26 18:36:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132200)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:36:02)


***

### [2024-08-26 18:36:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132202)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:36:03)


***

### [2024-08-26 18:36:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132204)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:36:05)


***

### [2024-08-26 18:36:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132208)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:36:20)


***

### [2024-08-26 18:36:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132210)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:36:54)


***

### [2024-08-26 18:37:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132212)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:37:01)


***

### [2024-08-26 18:38:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132213)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:38:19)


***

### [2024-08-26 18:40:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132215)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:40:03)


***

### [2024-08-26 18:42:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132217)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:42:56)


***

### [2024-08-26 18:55:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132223)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 18:55:06)


***

### [2024-08-26 18:58:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132224)
>corrections on pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 18:58:19)

[OpenYASP V4.9.2   LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-26_18:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743116/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 18:58:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743116/content)

[Accelerator Cockpit v0.0.38 24-08-26_18:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743118/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 18:58:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743118/content)

[Accelerator Cockpit v0.0.38 24-08-26_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743126/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 18:59:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743126/content)

[Accelerator Cockpit v0.0.38 24-08-26_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743144/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 18:59:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743144/content)

[Accelerator Cockpit v0.0.38 24-08-26_19:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743148/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:00:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743148/content)

[Accelerator Cockpit v0.0.38 24-08-26_19:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743150/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:01:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743150/content)


***

### [2024-08-26 19:02:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132228)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 19:02:23)


***

### [2024-08-26 19:06:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132229)
>Event created from ScreenShot Client.  
RF Trim Warning 24-08-26\_19:06.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:07:00)

[RF Trim Warning 24-08-26_19:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743152/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:07:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743152/content)


***

### [2024-08-26 19:10:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132230)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:11)

[tmpScreenshot_1724692211.0519931.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743154/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743154/content)


***

### [2024-08-26 19:10:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132231)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:13)

[tmpScreenshot_1724692213.7639267.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743156/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743156/content)


***

### [2024-08-26 19:10:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132232)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:16)

[tmpScreenshot_1724692215.947995.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743158/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743158/content)


***

### [2024-08-26 19:10:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132233)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:18)

[tmpScreenshot_1724692218.2040162.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743160/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 19:10:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743160/content)


***

### [2024-08-26 19:45:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132239)
>Global Post Mortem Event

Event Timestamp: 26/08/24 19:45:21.036
Fill Number: 10060
Accelerator / beam mode: PROTON PHYSICS / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 36994 / 31968 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: SW Permit: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/26 19:48:10)


***

### [2024-08-26 19:49:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132240)
>error from sequencer

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:49:35)

[ INJECTION SEQUENCER  v 5.1.8 24-08-26_19:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743174/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:49:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743174/content)


***

### [2024-08-26 19:50:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132245)
>Event created from ScreenShot Client.  
LHC SIS GUI 24-08-26\_19:50.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 19:54:19)

[ LHC SIS GUI 24-08-26_19:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743182/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 19:54:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743182/content)


