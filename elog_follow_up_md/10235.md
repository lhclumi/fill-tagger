# FILL 10235
**start**:		 2024-10-17 12:07:29.977363525+02:00 (CERN time)

**end**:		 2024-10-17 13:17:50.678613525+02:00 (CERN time)

**duration**:	 0 days 01:10:20.701250


***

### [2024-10-17 12:07:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165117)
>LHC RUN CTRL: New FILL NUMBER set to 10235

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/17 12:07:31)


***

### [2024-10-17 12:11:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165120)
>Eloise confirmed that the crystals replacement chambers are IN and ready for high intensity beam

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 12:11:46)


***

### [2024-10-17 12:15:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165122)
>LHC SEQ: preparing the whole LHC for access

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/17 12:15:26)


***

### [2024-10-17 12:15:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165123)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/17 12:15:32)


***

### [2024-10-17 12:25:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165132)
>greated dedicated HW and sequences for individual beams for NLO

creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/10/17 12:25:53)

[ LSA Applications Suite (v 16.6.34) 24-10-17_12:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827466/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/10/17 12:25:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827466/content)

[ LHC Sequence Editor (DEV) : 12.33.5  24-10-17_12:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827484/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/10/17 12:33:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827484/content)

[ LHC Sequence Editor (DEV) : 12.33.5  24-10-17_12:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827486/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/10/17 12:37:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827486/content)


***

### [2024-10-17 12:43:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165144)
>HELGA TIMKO(HTIMKO) assigned RBAC Role: RF-LHC-Piquet and will expire on: 17-OCT-24 06.43.40.041000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/17 12:43:41)


***

### [2024-10-17 12:55:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165150)
>
```
For RF MD with HL-LHC intensities, we decide not to copy settings from MD#4 BP, as the QL we used at that time required to switch off the OTFB. Instead, we try to capture with lower voltage and nominal QL:  
- **5.5 MV** total voltage for both beams  
- OTFB **closed** on all lines  
- QL: effective 20k, which in our experience requires setting for 1B1 and 3B2 24k, and 2B1 22k  
- pre-detuning phase 30 deg  
- half-detuning phase as used for operation as a starting point -- we'll optimize with beam  
  
  

```


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/10/17 13:29:37)

[ LSA Applications Suite (v 16.6.34) 24-10-17_12:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827496/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/10/17 12:55:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827496/content)


***

### [2024-10-17 13:00:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165151)
>Second cryo intervention finished. Recovering from access and precycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 13:00:20)


***

### [2024-10-17 13:11:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165153)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/17 13:11:34)


***

### [2024-10-17 13:15:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165155)
>RCBV17.L8B2 cannot be switched ON. Powering failure

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 13:17:03)

[Circuit_RCBV17_L8B2:   24-10-17_13:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827502/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 13:17:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827502/content)

[2 - RPLA.18L8.RCBV17.L8B2 Power Converter for the Circuit RCBV17.L8B2    24-10-17_13:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827504/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 13:18:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827504/content)

[ Equip State 24-10-17_13:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827506/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/17 13:19:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3827506/content)


***

### [2024-10-17 13:17:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4165156)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/17 13:17:49)


