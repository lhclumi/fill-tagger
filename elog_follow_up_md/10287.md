# FILL 10287
**start**:		 2024-10-26 21:41:35.451738525+02:00 (CERN time)

**end**:		 2024-10-27 02:52:26.151488525+01:00 (CERN time)

**duration**:	 0 days 06:10:50.699750


***

### [2024-10-26 21:41:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171448)
>LHC RUN CTRL: New FILL NUMBER set to 10287

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:41:36)


***

### [2024-10-26 21:41:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171449)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:41:57)


***

### [2024-10-26 21:43:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171450)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:43:52)


***

### [2024-10-26 21:58:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171454)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:58:32)


***

### [2024-10-26 21:58:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171455)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:58:42)


***

### [2024-10-26 21:58:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171456)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 21:58:47)


***

### [2024-10-26 21:58:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171457)
>LHC Injection Complete

Number of injections actual / planned: 23 / 34
SPS SuperCycle length: 28.8 [s]
Actual / minimum time: 0:14:55 / 0:21:19 (70.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/26 21:58:48)


***

### [2024-10-26 22:00:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171458)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:00:35)


***

### [2024-10-26 22:00:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171460)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:00:37)


***

### [2024-10-26 22:09:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171465)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:09:49)


***

### [2024-10-26 22:10:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171467)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:10:44)


***

### [2024-10-26 22:10:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171467)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:10:44)


***

### [2024-10-26 22:11:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171472)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:11:39)


***

### [2024-10-26 22:12:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171474)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/26 22:12:23)


***

### [2024-10-26 22:24:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171478)
>
```
Huge orbit offset at LHCb : we call Jorg, he says that he has added an offset at LHCb of -1.3mm and forgot to regenerate the reference orbit.  
As we are aligning at point 1 we leave ot like that for the moment  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/26 22:37:22)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-26_22:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839792/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/26 22:25:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839792/content)


***

### [2024-10-26 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171484)
>
```
**SHIFT SUMMARY:**  
  
I arrived during the fill for aperture measurement and collimation checks, the summary can be found here : <https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4171357>  
  
Then I dumped and prepared for protons and PP REF cycle.  
It took me some time before I could keep the beam in the machine,  
First B2 was lost immediately, this was solved with transfer line steering.  
Then only half of the intensity was injected for both beams. The injection phase was very wrong and we injected in the wrong bucket.  
In addition the crystal were not in parking but at flat top settings creating losses.  
Once these issues were solved, the injection was good, I made the usual corrections.  
  
I filled with 2 pilots per beams and a few pilots for the AFP alignment and brought the beams in collisions.  
  
I optimized all IPs and now the AFP alignment is started.  
  
  
* To be noted:

  
Offset in IP8 : Jorg needs to regenerate the reference orbit to include the new -1.3mm offset he had introduce for IP8.  
  
RQS.A67B1 tripped when after the QPS reset.  
  
Crystal B1 V back in the HW group after Santiago fixed the low level settings.  
  
Stefano Mazzoni did the calibration of the abort gap.  
  
Delphine  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/27 00:17:20)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839808/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/26 23:01:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839808/content)


***

### [2024-10-26 23:05:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171486)
>I copy the correction I did for the indiv on REF as I think it will be a 
 better starting point for train injection

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/26 23:08:32)

[Set SECTOR23 24-10-26_23:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839809/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/26 23:08:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839809/content)

[Confirmation required 24-10-26_23:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839811/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/26 23:08:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839811/content)


***

### [2024-10-26 23:12:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171487)
>S. Mazzoni checked BSRA calibration. Less light than usual is expected because we're in the transition between ondularo and dipole.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/26 23:12:02)


***

### [2024-10-26 23:40:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171495)
>updating TCL.4 settings in IR1 to 32s

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/26 23:40:19)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-26_23:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839817/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/26 23:40:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839817/content)


***

### [2024-10-27 00:49:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171504)
>
```
updating TCTPH.4L1.B1 settings for ion cycle (squeeze + physics) based on today's check outcome.
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 01:12:34)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-27_00:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839837/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:50:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839837/content)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-27_00:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839839/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:51:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839839/content)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-27_00:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839841/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:56:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839841/content)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-27_00:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839843/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:53:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839843/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_00:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839845/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:54:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839845/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_00:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839847/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:54:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839847/content)

[ LHC Collimator Settings App v0.10.0 connected to server LHC 24-10-27_00:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839849/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 00:55:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839849/content)


***

### [2024-10-27 01:08:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171511)
>
```
opened by -4 mm the right jaw of TCLD.A11R2.B1 based on today's measurement outcome.

Best suited highest param to have changes correctly propagated to the low level param is COLL_JAW and COLL_JAW_TOLERANCE: to be noted that's not a clean hiearchy and any change at N_SIGMA level will remove these changes!
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 01:13:04)

[ LSA Applications Suite (v 16.6.34) 24-10-27_01:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839859/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 01:12:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839859/content)


***

### [2024-10-27 01:21:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171516)
>Jorg called to inform that refence orbit for ppref hase been correctly updated and regenerated. Thus, we should be fine for validation in the next fill planned.

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 01:21:54)


***

### [2024-10-27 02:05:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171521)
>generating AFP settings for ppref run

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:05:14)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839903/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:05:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839903/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839905/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:05:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839905/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839907/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:05:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839907/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839909/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:09:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839909/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839911/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:09:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839911/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839913/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:10:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839913/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839917/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:12:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839917/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839919/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:13:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839919/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839921/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:13:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839921/content)

[ LSA Applications Suite (v 16.6.34) 24-10-27_02:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839923/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/27 02:13:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839923/content)


***

### [2024-10-27 02:30:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171529)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 02:30:11  
- Background acquisitions: 10  
- Max losses timestamp: 02:30:24  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:30:52)

[tmpScreenshot_1729992652.734793.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839937/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:30:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839937/content)


***

### [2024-10-27 02:31:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171530)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 02:31:04  
- Background acquisitions: 10  
- Max losses timestamp: 02:31:17  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:31:33)

[tmpScreenshot_1729992693.37556.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839939/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:31:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839939/content)


***

### [2024-10-27 02:32:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171531)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 02:31:41  
- Background acquisitions: 10  
- Max losses timestamp: 02:31:54  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:32:13)

[tmpScreenshot_1729992733.7446866.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839941/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:32:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839941/content)


***

### [2024-10-27 02:32:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171532)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 02:32:17  
- Background acquisitions: 10  
- Max losses timestamp: 02:32:30  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:32:51)

[tmpScreenshot_1729992771.5924664.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839943/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:32:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839943/content)


***

### [2024-10-27 02:33:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171533)
>Q' =3 for dp/p LM

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/27 02:33:49)

[ Accelerator Cockpit v0.0.41 24-10-27_02:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839945/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/27 02:33:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839945/content)


***

### [2024-10-27 02:35:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171534)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: +dp/p  
- Start timestamp: 02:34:48  
- Background acquisitions: 10  
- Max losses timestamp: 02:35:26  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:35:37)

[tmpScreenshot_1729992936.9623861.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839947/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:35:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839947/content)


***

### [2024-10-27 02:36:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171536)
>preliminary after AFP alignment  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: -dp/p  
- Start timestamp: 02:35:50  
- Background acquisitions: 10  
- Max losses timestamp: 02:36:28  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:36:42)

[tmpScreenshot_1729993002.0273197.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839953/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:36:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839953/content)


***

### [2024-10-27 02:37:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171537)
>masks for ASD

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/27 02:37:55)

[ LHC SIS GUI 24-10-27_02:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839955/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/27 02:37:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839955/content)

[ LHC SIS GUI 24-10-27_02:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839957/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/27 02:38:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839957/content)


***

### [2024-10-27 02:42:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171542)
>Global Post Mortem Event

Event Timestamp: 27/10/24 02:42:44.194
Fill Number: 10287
Accelerator / beam mode: PROTON PHYSICS / ADJUST
Energy: 2679600 [MeV]
Intensity B1/B2: 17 / 17 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/27 02:45:37)


***

### [2024-10-27 02:42:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171596)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: dmirarch / preliminary ASD after AFP alignment


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/27 03:21:48)


***

### [2024-10-27 02:42:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171540)
>preliminary after AFP alignment  
  
Screenshot attached: Asynchronous Beam Dump overview  
- Start timestamp: 02:38:16  
- Machine configuration: Phys\_ppref  
  
*Sent From pyLossMaps*

creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:42:54)

[tmpScreenshot_1729993374.456072.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839965/content)
creator:	 dmirarch  @cwo-ccc-d3lc.cern.ch (2024/10/27 02:42:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3839965/content)


***

### [2024-10-27 02:46:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171543)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/27 02:46:55)


***

### [2024-10-27 02:52:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4171544)
>LHC RUN CTRL: New FILL NUMBER set to 10288

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/27 02:52:27)


