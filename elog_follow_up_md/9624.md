# FILL 9624
**start**:		 2024-05-14 06:51:39.651863525+02:00 (CERN time)

**end**:		 2024-05-14 08:58:59.644738525+02:00 (CERN time)

**duration**:	 0 days 02:07:19.992875


***

### [2024-05-14 06:51:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066967)
>LHC RUN CTRL: New FILL NUMBER set to 9624

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 06:51:40)


***

### [2024-05-14 06:52:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066968)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 06:52:23)


***

### [2024-05-14 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066969)
>
```
**SHIFT SUMMARY:**  
  
Injected INDIVs with low longitudinal emittance and variable intensity for MD11786.   
   
  
* To be noted:

  
- Got an issue with energy interlock for TCTVs in IR2 and IR8 when coming back from the previous MD  
- the orbit and dRF for the INDIV got really bad after staying too long without going back to the pilot. Got very large COD currents and large RMS orbit towards the end of the shift.  
- Also the chosen intensities are very close to the threshold that cannot be seen when moving to injection Physics Beam. We were sometimes blind.   
  
* To be reverted:

  
- RF masks  
- RF voltage  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 06:59:52)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589723/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 06:59:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589723/content)


***

### [2024-05-14 07:00:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066970)
>LHC RUN CTRL: BEAM MODE changed to INJECTION SETUP BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 07:00:26)


***

### [2024-05-14 07:12:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066977)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_07:12.png  
reduced to 1.8e10 p/b, 2 deg kick - appears damped  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:13:42)

[Beam profile analysis 24-05-14_07:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589732/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:12:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589732/content)


***

### [2024-05-14 07:15:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066978)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_07:15.png  
bunch #2, 1.8e10 p/b, 2 deg kick
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:15:48)

[Beam profile analysis 24-05-14_07:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589734/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:15:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589734/content)

[Beam profile analysis 24-05-14_07:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589736/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:16:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589736/content)


***

### [2024-05-14 07:21:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066979)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_07:21.png  
bunch #3, 1.8e10 p/b, 2 deg kick
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:21:50)

[Beam profile analysis 24-05-14_07:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589738/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:21:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589738/content)

[Beam profile analysis 24-05-14_07:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589740/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 07:25:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589740/content)


***

### [2024-05-14 07:32:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066986)
>
```
Summary of MD11786 - Longitudinal Threshold of Landau Damping  
  
During the MD, a parameter scan was performed to determine the effective impedance and cut-off frequency through the loss of Landau damping mechanism.  
  
As a first step, we validated the measurement method in which we inject a beam in matched conditions (3.5 MV LHC voltage), we open the beam phase loop, we kick the beam longitudinally with a phase offset in the synchro loop. We did systematic scans at different bunch intensities. First analysis showed the threshold to be around 1.7e10-1.8e10 p/b for an SPS extracted bunch length of 0.8 ns.  
  
We encountered difficulties with the longitudinal profile acquisition due to frequent hanging of the FESA class. Due to this, often we could only measure the oscillations of the twice-kicked beam. It would be worthwhile to retake some data in a future MD, once this problem is fixed.  
  
  
  

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 07:57:28)


***

### [2024-05-14 07:32:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066982)
>Benoit brough the CCC phone with him, CCC calls redirected to my personal phone, to be reverted

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 07:32:59)


***

### [2024-05-14 07:43:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066985)
>Benoit came back with the phone, call fwd disbled

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 07:43:08)


***

### [2024-05-14 08:09:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066994)
>sanity check expired...

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 08:09:58)

[Status Application 24-05-14_08:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589760/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 08:09:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589760/content)


***

### [2024-05-14 08:15:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066995)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:15:45)


***

### [2024-05-14 08:19:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066997)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:19:41)


***

### [2024-05-14 08:19:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066999)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:19:47)


***

### [2024-05-14 08:40:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067006)
>MD11786 done, start preparing for MD11644

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 08:40:13)


***

### [2024-05-14 08:40:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067007)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:40:52)


***

### [2024-05-14 08:40:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067008)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:40:53)


***

### [2024-05-14 08:46:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067013)
>VdM hypercycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 08:46:32)

[ Generation Application connected to server LHC (PRO database) 24-05-14_08:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589780/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 08:46:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589780/content)


***

### [2024-05-14 08:46:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067014)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:46:49)


***

### [2024-05-14 08:48:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067017)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:48:48)


***

### [2024-05-14 08:51:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067019)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:51:00)


***

### [2024-05-14 08:51:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067020)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:51:17)


***

### [2024-05-14 08:53:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067023)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:53:18)


***

### [2024-05-14 08:53:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067024)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 08:53:54)


