# FILL 10103
**start**:		 2024-09-11 07:07:37.735738525+02:00 (CERN time)

**end**:		 2024-09-11 11:19:29.136238525+02:00 (CERN time)

**duration**:	 0 days 04:11:51.400500


***

### [2024-09-11 07:08:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141261)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:08:23)


***

### [2024-09-11 07:10:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141262)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:10:24)


***

### [2024-09-11 07:10:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141265)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:10:30)


***

### [2024-09-11 07:11:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141266)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_40MHZ (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:11:46)


***

### [2024-09-11 07:11:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141268)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:11:57)


***

### [2024-09-11 07:12:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141269)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:12:59)


***

### [2024-09-11 07:13:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141270)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:13:11)


***

### [2024-09-11 07:13:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141273)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:13:49)


***

### [2024-09-11 07:14:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141277)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:14:23)


***

### [2024-09-11 07:14:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141279)
>Got the green light from RF and now everything is restarted and works 
 fine.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 07:15:08)

[ LHC RF CONTROL 24-09-11_07:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766573/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/11 07:15:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766573/content)


***

### [2024-09-11 07:14:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141278)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:14:56)


***

### [2024-09-11 07:16:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141280)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:16:13)


***

### [2024-09-11 07:16:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141282)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:16:14)


***

### [2024-09-11 07:16:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141284)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:16:15)


***

### [2024-09-11 07:16:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141289)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:16:30)


***

### [2024-09-11 07:17:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141292)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:17:07)


***

### [2024-09-11 07:17:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141295)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:17:13)


***

### [2024-09-11 07:18:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141299)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:18:29)


***

### [2024-09-11 07:20:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141302)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 07:20:13)


***

### [2024-09-11 08:02:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141315)
>LHC SEQ: ramping down LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 08:02:45)


***

### [2024-09-11 08:04:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141316)
>ALICE VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 08:04:07)


***

### [2024-09-11 08:16:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141322)
>Checked LLRF state after the power glitch.   
Nothing out of ordinary, all the lines have tripped due to the PCs entering FLT\_STOP state.  
Ready for restart of the RF.  
  
B.Bielawski - LHC LLRF Piquet  


creator:	 bbielaws  @pcsy76.dyndns.cern.ch (2024/09/11 08:21:48)


***

### [2024-09-11 08:26:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141323)
>Changing the polarity of LHCb dipole.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 08:26:15)

[ LHC Sequencer Execution GUI (PRO) : 12.33.0  24-09-11_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766664/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 08:26:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766664/content)


***

### [2024-09-11 08:26:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141326)
>LHC SEQ: LHCB DIPOLE POLARITY=POS (RBXWSH.L8=POS, RBXWH.L8=NEG, RBXWSH.R8=NEG)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 08:26:48)


***

### [2024-09-11 08:27:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141327)
>LHC SEQ: ramping up LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 08:27:10)


***

### [2024-09-11 09:13:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141355)
>For the change of the polarity in LHCb

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:13:42)

[ LSA Applications Suite (v 16.6.26) 24-09-11_09:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766698/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:13:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766698/content)

[ LSA Applications Suite (v 16.6.26) 24-09-11_09:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766700/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:14:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766700/content)


***

### [2024-09-11 09:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141367)
>increasing the separation

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:21:36)

[ LSA Applications Suite (v 16.6.26) 24-09-11_09:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766710/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:21:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766710/content)

[ LSA Applications Suite (v 16.6.26) 24-09-11_09:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766714/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:22:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766714/content)


***

### [2024-09-11 09:59:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141391)
>Michi re-generated the orbit.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 09:59:21)


***

### [2024-09-11 10:01:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141395)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 10:01:13)


***

### [2024-09-11 10:04:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141396)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 10:04:25)


***

### [2024-09-11 10:14:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141406)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 10:14:07)


***

### [2024-09-11 10:26:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141413)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 10:26:30)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-11_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766839/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 10:26:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766839/content)

[Accelerator Cockpit v0.0.38 24-09-11_10:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766841/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:27:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766841/content)

[Accelerator Cockpit v0.0.38 24-09-11_10:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766853/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:28:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766853/content)

[Accelerator Cockpit v0.0.38 24-09-11_10:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766855/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:28:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766855/content)


***

### [2024-09-11 10:29:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141421)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 10:29:59)


***

### [2024-09-11 10:40:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141433)
>Event created from ScreenShot Client.  
Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-11\_10:40.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 10:40:32)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-11_10:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766926/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/11 10:40:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766926/content)


***

### [2024-09-11 10:41:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141435)
>Relative large phase error. I correct half of it from the 12 and see.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:41:29)

[Accelerator Cockpit v0.0.38 24-09-11_10:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766934/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:41:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766934/content)


***

### [2024-09-11 10:45:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141438)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:45.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:45:03)

[Figure 1 24-09-11_10:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766952/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:45:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766952/content)


***

### [2024-09-11 10:45:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141439)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:45.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:45:48)

[Figure 1 24-09-11_10:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766954/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:45:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766954/content)


***

### [2024-09-11 10:47:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141440)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:47.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:47:01)

[Figure 1 24-09-11_10:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766962/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:47:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766962/content)


***

### [2024-09-11 10:48:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141441)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:48.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:48:31)

[Figure 1 24-09-11_10:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766972/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:48:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766972/content)


***

### [2024-09-11 10:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141444)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:51.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:51:53)

[Figure 1 24-09-11_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766980/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:51:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766980/content)


***

### [2024-09-11 10:52:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141445)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:52.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:52:13)

[Figure 1 24-09-11_10:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766982/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:52:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766982/content)


***

### [2024-09-11 10:52:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141446)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:52.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:52:53)

[Figure 1 24-09-11_10:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766984/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:52:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766984/content)


***

### [2024-09-11 10:53:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141447)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:53.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:53:47)

[Figure 1 24-09-11_10:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766988/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:53:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766988/content)


***

### [2024-09-11 10:54:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141448)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:54.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:54:31)

[Figure 1 24-09-11_10:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766990/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:54:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766990/content)


***

### [2024-09-11 10:55:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141449)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:55.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:55:13)

[Figure 1 24-09-11_10:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766996/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:55:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766996/content)


***

### [2024-09-11 10:56:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141451)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:56.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:56:20)

[Figure 1 24-09-11_10:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766998/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:56:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3766998/content)


***

### [2024-09-11 10:57:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141453)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:57.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:57:19)

[Figure 1 24-09-11_10:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767000/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:57:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767000/content)


***

### [2024-09-11 10:57:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141454)
>Phase looks good now

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:57:33)

[Accelerator Cockpit v0.0.38 24-09-11_10:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767002/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/11 10:57:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767002/content)


***

### [2024-09-11 10:58:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141455)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:58.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:06)

[Figure 1 24-09-11_10:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767004/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767004/content)


***

### [2024-09-11 10:58:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141456)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:58.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:52)

[Figure 1 24-09-11_10:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767008/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767008/content)


***

### [2024-09-11 10:58:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141456)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_10:58.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:52)

[Figure 1 24-09-11_10:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767008/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 10:58:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767008/content)


***

### [2024-09-11 11:00:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141457)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:00.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:00:03)

[Figure 1 24-09-11_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767012/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:00:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767012/content)


***

### [2024-09-11 11:00:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141458)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:00.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:00:36)

[Figure 1 24-09-11_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767014/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:00:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767014/content)


***

### [2024-09-11 11:02:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141460)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:02.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:02:13)

[Figure 1 24-09-11_11:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767020/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:02:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767020/content)


***

### [2024-09-11 11:03:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141461)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:03.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:03:03)

[Figure 1 24-09-11_11:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767022/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:03:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767022/content)


***

### [2024-09-11 11:04:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141464)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:04.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:04:32)

[Figure 1 24-09-11_11:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767026/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:04:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767026/content)


***

### [2024-09-11 11:05:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141465)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:05.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:05:13)

[Figure 1 24-09-11_11:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767028/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:05:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767028/content)


***

### [2024-09-11 11:06:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141469)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:06.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:06:02)

[Figure 1 24-09-11_11:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767038/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:06:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767038/content)


***

### [2024-09-11 11:06:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141471)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:06.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:06:47)

[Figure 1 24-09-11_11:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767042/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:06:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767042/content)


***

### [2024-09-11 11:07:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141474)
>Problem injecting beam 1 due to MKE6. They are trying to restart it.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/11 11:07:37)

[(null) 24-09-11_11:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767048/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/11 11:07:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767048/content)


***

### [2024-09-11 11:07:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141473)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:07.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:07:23)

[Figure 1 24-09-11_11:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767046/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:07:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767046/content)


***

### [2024-09-11 11:08:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141475)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:08.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:08:09)

[Figure 1 24-09-11_11:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767056/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:08:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767056/content)


***

### [2024-09-11 11:09:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141476)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:09.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:09:42)

[Figure 1 24-09-11_11:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767058/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:09:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767058/content)


***

### [2024-09-11 11:10:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141479)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:10.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:10:24)

[Figure 1 24-09-11_11:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767064/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:10:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767064/content)


***

### [2024-09-11 11:11:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141481)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:11.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:11:07)

[Figure 1 24-09-11_11:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767066/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:11:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767066/content)


***

### [2024-09-11 11:11:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141482)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:11.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:11:37)

[Figure 1 24-09-11_11:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767072/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:11:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767072/content)


***

### [2024-09-11 11:12:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141483)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:12.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:12:26)

[Figure 1 24-09-11_11:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767074/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:12:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767074/content)


***

### [2024-09-11 11:13:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141485)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:13.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:13:17)

[Figure 1 24-09-11_11:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767078/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:13:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767078/content)


***

### [2024-09-11 11:14:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141486)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:14.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:14:49)

[Figure 1 24-09-11_11:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767082/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:14:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767082/content)


***

### [2024-09-11 11:15:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141487)
>Event created from ScreenShot Client.  
Figure 1 24-09-11\_11:16.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:16:00)

[Figure 1 24-09-11_11:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767084/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/09/11 11:16:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767084/content)


***

### [2024-09-11 11:17:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141489)
>Very bad shot. Seems to come from the kicker. We will have to restart.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 11:17:23)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-11_11:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767088/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/11 11:26:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767088/content)


***

### [2024-09-11 11:17:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141496)
>Global Post Mortem Event

Event Timestamp: 11/09/24 11:17:58.450
Fill Number: 10103
Accelerator / beam mode: PROTON PHYSICS / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 29931 / 38106 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/11 11:20:49)


***

### [2024-09-11 11:17:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141517)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: tpersson / Programmed dump due to a bad injection that increased the emittance significantly 


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/11 11:59:45)


***

### [2024-09-11 11:18:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141491)
>Huge injection oscillations in the first bunches.

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/11 11:18:43)

[LHC Injection Quality Check - Playback 3.18.0 24-09-11_11:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767090/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/11 11:18:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767090/content)

[LHC Injection Quality Check - Playback 3.18.0 24-09-11_11:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767104/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/11 11:22:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3767104/content)


***

### [2024-09-11 11:19:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4141493)
>LHC RUN CTRL: New FILL NUMBER set to 10104

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/11 11:19:30)


