# FILL 9739
**start**:		 2024-06-08 21:29:13.190613525+02:00 (CERN time)

**end**:		 2024-06-08 22:22:59.946238525+02:00 (CERN time)

**duration**:	 0 days 00:53:46.755625


***

### [2024-06-08 21:29:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084129)
>LHC RUN CTRL: New FILL NUMBER set to 9739

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:29:13)


***

### [2024-06-08 21:29:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084130)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:29:33)


***

### [2024-06-08 21:31:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084131)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:31:57)


***

### [2024-06-08 21:34:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084132)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-08\_21:34.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:34:48)

[Accelerator Cockpit v0.0.37 24-06-08_21:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630527/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:34:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630527/content)


***

### [2024-06-08 21:37:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084134)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:37:51)

[tmpScreenshot_1717875471.4131913.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630529/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:37:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630529/content)


***

### [2024-06-08 21:38:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084135)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:06)

[tmpScreenshot_1717875486.0930774.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630531/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630531/content)


***

### [2024-06-08 21:38:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084136)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:15)

[tmpScreenshot_1717875495.7174044.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630533/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630533/content)


***

### [2024-06-08 21:38:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084137)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:21)

[tmpScreenshot_1717875500.9333706.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630535/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:38:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630535/content)


***

### [2024-06-08 21:50:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084143)
>B1 BSRL

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:50:32)

[BLDMLHC GUI 24-06-08_21:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630547/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:50:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630547/content)


***

### [2024-06-08 21:51:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084144)
>Pilot

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:51:19)

[BLDMLHC GUI 24-06-08_21:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630549/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:51:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630549/content)


***

### [2024-06-08 21:57:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084148)
>Event created from ScreenShot Client.  
BLDMLHC GUI 24-06-08\_21:57.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:57:29)

[BLDMLHC GUI 24-06-08_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630553/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:57:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630553/content)


***

### [2024-06-08 21:57:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084149)
>Event created from ScreenShot Client.  
BLDMLHC GUI 24-06-08\_21:57.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:57:37)

[BLDMLHC GUI 24-06-08_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630555/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 21:57:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630555/content)

[BLDMLHC GUI 24-06-08_22:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630569/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:12:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630569/content)


***

### [2024-06-08 22:00:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084151)
>updated limits for TCP.H in RI7 to allow scraping down to 2.5 sigma

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:01:26)

[ LSA Applications Suite (v 16.6.1) 24-06-08_22:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630561/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 22:01:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630561/content)


***

### [2024-06-08 22:07:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084154)
>Event created from ScreenShot Client.  
BLDMLHC GUI 24-06-08\_22:07.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:07:50)

[BLDMLHC GUI 24-06-08_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630565/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:07:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630565/content)


***

### [2024-06-08 22:07:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084155)
>Event created from ScreenShot Client.  
BLDMLHC GUI 24-06-08\_22:07.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:07:58)

[BLDMLHC GUI 24-06-08_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630567/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:07:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630567/content)


***

### [2024-06-08 22:12:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084156)
>Event created from ScreenShot Client.  
BLDMLHC GUI 24-06-08\_22:12.png

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:12:42)

[BLDMLHC GUI 24-06-08_22:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630571/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/08 22:12:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630571/content)


