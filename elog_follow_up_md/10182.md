# FILL 10182
**start**:		 2024-10-01 16:27:26.449863525+02:00 (CERN time)

**end**:		 2024-10-01 20:27:14.538238525+02:00 (CERN time)

**duration**:	 0 days 03:59:48.088375


***

### [2024-10-01 16:27:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153922)
>LHC RUN CTRL: New FILL NUMBER set to 10182

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:27:26)


***

### [2024-10-01 16:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153928)
>BE-CEM and STI called back and they said that the collimator is now ok after adding some grease and trying 50 cycles  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/01 16:33:08)


***

### [2024-10-01 16:33:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153930)
>access completed  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/01 16:33:20)


***

### [2024-10-01 16:33:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153929)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:33:18)


***

### [2024-10-01 16:35:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153933)
>LHC SEQ: ALICE SOLENOID POLARITY SET TO POSITIVE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:35:41)


***

### [2024-10-01 16:35:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153934)
>LHC SEQ: ramping up ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:35:54)


***

### [2024-10-01 16:36:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153935)
>ALICE polarity switched to positive upon their request

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 16:36:46)

[ LSA Applications Suite (v 16.6.29) 24-10-01_16:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794861/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:03:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794861/content)

[ LSA Applications Suite (v 16.6.29) 24-10-01_16:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794863/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 16:36:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794863/content)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-10-01_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794869/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 16:38:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794869/content)


***

### [2024-10-01 16:38:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153941)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:38:16)


***

### [2024-10-01 16:38:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153943)
>Event created from ScreenShot Client.  
Set SECTOR23 24-10-01\_16:38.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/01 16:39:37)

[Set SECTOR23 24-10-01_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794879/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/01 16:39:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794879/content)


***

### [2024-10-01 16:39:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153944)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:39:45)

[ Equip State 24-10-01_16:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794883/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 16:39:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794883/content)


***

### [2024-10-01 16:40:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153945)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:40:21)


***

### [2024-10-01 16:41:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153948)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:41:52)


***

### [2024-10-01 16:42:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153949)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:42:18)


***

### [2024-10-01 16:42:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153950)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:42:38)


***

### [2024-10-01 16:43:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153952)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:43:12)


***

### [2024-10-01 16:44:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153956)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:44:21)


***

### [2024-10-01 16:44:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153957)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:44:42)


***

### [2024-10-01 16:44:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153959)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:44:56)


***

### [2024-10-01 16:47:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153960)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:05)


***

### [2024-10-01 16:47:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153961)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:07)


***

### [2024-10-01 16:47:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153963)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:09)


***

### [2024-10-01 16:47:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153965)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:10)


***

### [2024-10-01 16:47:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153967)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:12)


***

### [2024-10-01 16:47:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153969)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:17)


***

### [2024-10-01 16:47:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153972)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:47:27)


***

### [2024-10-01 16:49:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153974)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:49:25)


***

### [2024-10-01 16:49:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153976)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:49:34)


***

### [2024-10-01 16:51:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153980)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:51:09)


***

### [2024-10-01 16:58:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153984)
>LHC SEQ: ALICE DIPOLE POLARITY=POS (RBXWTV.L2 POS, RBWMDV.L2 NEG, RBXWTV.R2 NEG)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:58:32)


***

### [2024-10-01 16:58:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153984)
>LHC SEQ: ALICE DIPOLE POLARITY=POS (RBXWTV.L2 POS, RBWMDV.L2 NEG, RBXWTV.R2 NEG)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:58:32)


***

### [2024-10-01 16:58:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153985)
>LHC SEQ: ramping up ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 16:58:44)


***

### [2024-10-01 17:03:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153987)
>ALICE asked to not ramp up the dipole until they check the detector  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:04:02)


***

### [2024-10-01 17:04:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153991)
>A new firmware and FESA class have been deployed on the ring Fast BCT System B. We will be testing this over the coming fills, so the intensities reported by this system may not be correct as we tune the settings.  
  
There is no change to Fast BCT System A.  
  
--T.Levens  


creator:	 tlevens  @pcsy401.cern.ch (2024/10/01 17:08:00)


***

### [2024-10-01 17:05:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153988)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 17:05:17)


***

### [2024-10-01 17:14:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153996)
>after QPS reset

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/01 17:14:16)

[Set SECTOR45 24-10-01_17:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794948/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/01 17:14:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794948/content)


***

### [2024-10-01 17:18:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154000)
>RQT13\_L5 OK after reset

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:18:46)

[Circuit_RQT13_L5B2:   24-10-01_17:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794972/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:20:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794972/content)

[Circuit_RQT13_L5B2:   24-10-01_17:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794974/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:18:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794974/content)


***

### [2024-10-01 17:18:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154001)
>for George

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/01 17:18:59)

[PS2TOF-TrajectoryFB - UCAP hierarchy 24-10-01_17:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794976/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/01 17:18:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794976/content)

[ps2tof-layout.json](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795680/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/01 20:05:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795680/content)


***

### [2024-10-01 17:27:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154005)
>vacuum valves in S81 not opening

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:27:28)

[Terminal - lhcop@cwo-ccc-d9lf:.opt.home.lhcop 24-10-01_17:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794998/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:27:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3794998/content)

[VVGRT.235.7L1.B 24-10-01_17:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795000/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:27:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795000/content)

[VVGRT.181.7R8.B 24-10-01_17:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795002/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:27:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795002/content)

[Synoptic LHC 24-10-01_17:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795004/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:27:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795004/content)

[State_History: StateArchiveDetails 24-10-01_17:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795006/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:28:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795006/content)

[State_History: StateArchiveDetails 24-10-01_17:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795008/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 17:28:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795008/content)


***

### [2024-10-01 17:42:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154010)
>called VSC piquet several times but no answer. Called Giuseppe who is checking  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 17:43:07)


***

### [2024-10-01 17:46:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154013)
>
```
TI warned us that there was an electrical glitch in RR13 and that the DQRs lost powering. That may require an access. The TE-MPE piquet also called us to warn that there is an issue.   

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 18:41:46)


***

### [2024-10-01 18:15:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154043)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 18:15:49)


***

### [2024-10-01 18:28:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154057)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 18:28:49)


***

### [2024-10-01 18:32:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154066)
>
```
I called TE-VSC back and he told me they need an access in US15. We also therefore organize the access of EN-EL and EN-CV for the DQRs in RE21   

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 18:42:04)


***

### [2024-10-01 19:01:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154074)
>
```
VSC access starting in US15  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 19:03:54)


***

### [2024-10-01 19:11:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154079)
>VGPB back on

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 19:11:19)

[VGPB.499.13L1.B 24-10-01_19:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795636/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 19:11:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795636/content)

[VGPB.499.12L1.B 24-10-01_19:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795638/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/01 19:11:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795638/content)


***

### [2024-10-01 19:19:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154083)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 19:19:20)


***

### [2024-10-01 19:20:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154098)
>EN-EL piquet was blocked by traffic coming from Switzerland and it was too late for gate C. The TI operator asked CSA if they could open the gate as there was a need for urgent intervention in LHC, and they agreed to let the piquet access CERN from gate C exceptionnally.     


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 20:12:42)


***

### [2024-10-01 19:20:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154086)
>
```
piquet VSC called back, all ok from his side and we can indeed close the valve. An electronic device (TPG300) was broken  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 19:20:41)


***

### [2024-10-01 19:39:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154090)
>UL14 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 19:39:32)


***

### [2024-10-01 19:40:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154100)
>access for DQR started  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 20:15:06)


***

### [2024-10-01 20:08:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154096)
>
```
tripped on standby, OK after reset
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/01 20:51:48)

[Set SECTOR23 24-10-01_20:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795681/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/01 20:08:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795681/content)


***

### [2024-10-01 20:14:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154099)
>access for DQR

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/01 20:14:35)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795685/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/01 20:14:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3795685/content)


***

### [2024-10-01 20:26:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154102)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 20:26:51)


***

### [2024-10-01 20:27:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4154103)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/01 20:27:02)


