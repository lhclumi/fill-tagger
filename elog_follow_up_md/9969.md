# FILL 9969
**start**:		 2024-08-02 14:13:57.738613525+02:00 (CERN time)

**end**:		 2024-08-03 06:55:35.689238525+02:00 (CERN time)

**duration**:	 0 days 16:41:37.950625


***

### [2024-08-02 14:13:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118064)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 14:13:57)


***

### [2024-08-02 14:13:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118065)
>LHC RUN CTRL: New FILL NUMBER set to 9969

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 14:13:58)


***

### [2024-08-02 14:17:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118066)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 14:17:19)


***

### [2024-08-02 14:48:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118099)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 14:48:44)


***

### [2024-08-02 14:53:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118105)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 14:53:34)


***

### [2024-08-02 14:56:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118107)
>WRAP inj SIS status is stuck. I can navigate in the GUI but it stopped 
 receiving updates

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/02 14:57:29)

[ LHC-INJ-SIS Status 24-08-02_14:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710047/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/02 14:57:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710047/content)


***

### [2024-08-02 14:58:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118110)
>Big radial offset with pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 14:59:14)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-02_14:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710051/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 14:59:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710051/content)


***

### [2024-08-02 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118144)
>Started the shift in stable beams.  
  
RQT13.R5B1/2 dumped the beam. QPS lost FIP connection and they have to access.  
Softstart of MKI B1 failed due to a spark M2B. Giorgia had a look and confirmed it was ok to continue after an extended soft start.  
  
Issue with elevators of P5 and P8. We need to warn TI when we access there since the TI supervisor can control the elevator remotely.  
This is needed to call the elevator from downstairs.  
  
Leaving the machine during the injection of usual BCMS beam (not the new type).  
  
AC

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 15:25:30)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710115/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 15:25:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710115/content)


***

### [2024-08-02 15:04:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118117)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:04:44)


***

### [2024-08-02 15:06:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118120)
>radial offset with 12b

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 15:06:59)

[OpenYASP V4.9.0   LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-02_15:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710068/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 15:06:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710068/content)


***

### [2024-08-02 15:10:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118125)
>Wirescans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:10:57)

[ LHC WIRESCANNER APP 24-08-02_15:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710083/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:10:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710083/content)

[ LHC WIRESCANNER APP 24-08-02_15:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710085/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:11:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710085/content)

[ LHC WIRESCANNER APP 24-08-02_15:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710087/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:11:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710087/content)

[ LHC WIRESCANNER APP 24-08-02_15:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710089/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:11:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710089/content)


***

### [2024-08-02 15:35:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118160)
>One slightly weird injection for beam2. No losses observed but the 
 vertical orbit was slightly worse.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:35:57)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-02_15:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710169/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 15:35:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710169/content)

[LHC Injection Quality Check - Playback 3.17.8 24-08-02_22:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710542/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/02 22:58:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710542/content)


***

### [2024-08-02 15:44:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118171)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:44:31)


***

### [2024-08-02 15:44:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118172)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:44:41)


***

### [2024-08-02 15:44:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118173)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:44:45)


***

### [2024-08-02 15:44:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118174)
>LHC Injection Complete

Number of injections actual / planned: 58 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:40:02 / 0:33:48 (118.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/02 15:44:46)


***

### [2024-08-02 15:47:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118175)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:47:07)


***

### [2024-08-02 15:47:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118177)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 15:47:08)


***

### [2024-08-02 15:51:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118182)
>Damper tripped during the ramp. Trying to restart.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 15:52:25)

[ TRANSVERSE DAMPER CONTROL 24-08-02_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710197/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 15:52:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710197/content)


***

### [2024-08-02 15:55:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118184)
>Up to 7% in the beginning of the ramp., We should correct the phase at 
 injection next time.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/02 15:56:01)

[ LHC BLM Fixed Display 24-08-02_15:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710199/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/02 15:56:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710199/content)

[Accelerator Cockpit v0.0.37 24-08-03_00:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710567/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 00:07:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710567/content)


***

### [2024-08-02 15:56:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118185)
>Managed to turn on the damper again. Don't know why it tripped.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 15:56:41)

[ TRANSVERSE DAMPER CONTROL 24-08-02_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710201/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 15:56:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710201/content)


***

### [2024-08-02 16:08:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118188)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:08:25)


***

### [2024-08-02 16:08:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118190)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:08:34)


***

### [2024-08-02 16:11:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118192)
>Can't get it much better than this.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 16:11:19)

[Abort gap cleaning control v2.1.0 24-08-02_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710227/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 16:11:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710227/content)

[Abort gap cleaning control v2.1.0 24-08-02_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710229/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 16:11:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710229/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-08-02_16:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710231/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/02 16:11:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710231/content)


***

### [2024-08-02 16:16:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118199)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:16:57)


***

### [2024-08-02 16:17:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118200)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:17:51)


***

### [2024-08-02 16:18:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118204)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:18:44)


***

### [2024-08-02 16:22:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118207)
>IP2 and IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:22:55)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710249/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:22:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710249/content)


***

### [2024-08-02 16:24:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118208)
>Optimization IP1 and IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:24:34)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710255/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:24:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710255/content)


***

### [2024-08-02 16:25:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118209)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:25:02)


***

### [2024-08-02 16:31:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118211)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:31:57)


***

### [2024-08-02 16:34:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118212)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/02 16:34:00)


***

### [2024-08-02 16:37:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118213)
>emittance scan in ip1 and ip5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:03)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710277/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710277/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710279/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710279/content)


***

### [2024-08-02 16:37:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118213)
>emittance scan in ip1 and ip5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:03)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710277/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710277/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710279/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 16:38:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710279/content)


***

### [2024-08-02 21:28:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118273)
>Levelling LHCb on the brans after their request. The values when they were 
 publishing looks very similar to what they gave so I use it

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 21:29:49)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710534/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 21:29:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710534/content)


***

### [2024-08-02 21:37:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118274)
>back to LHCb lumi

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 21:37:49)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_21:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710536/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 21:37:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710536/content)


***

### [2024-08-02 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118289)
>
```
**SHIFT SUMMARY:**  
-Arrived during the filling. A bit of phase error in the injection but we were just on the limit of the range so would have needed to dump and re-sync so decided to avoid it.   
-Seemed that there was one bad kicker in beam 2 but only caused a small emittance increase and the orbit was still pretty good. Decided to continue and didn't have any more issues.   
-72% losses in the beginning of the ramp  
-The ADT Moduel v2b1 and Module h1b2 tripped during the ramp. I could restart it without any major issues.   
-The rest of the cycle smooth with a bit high level in the abort gap in beam 1.   
  
  
* To be noted:

  
Probably good to correct the injection phase next fill.    
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/02 23:10:19)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710546/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/02 23:10:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710546/content)


***

### [2024-08-02 23:57:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118292)
>IP1 is head on after 7.5h of Stable Beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 23:57:28)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_23:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710549/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 23:57:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710549/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-02_23:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710551/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/02 23:57:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710551/content)


***

### [2024-08-03 00:01:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118293)
>Also IP5 is Head on now

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 00:01:45)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_00:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710553/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 00:01:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710553/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_00:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710555/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 00:02:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710555/content)


***

### [2024-08-03 00:02:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118294)
>Event created from ScreenShot Client.  
LHC Beam Losses Lifetime Display 
 24-08-03\_00:02.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 00:02:29)

[ LHC Beam Losses Lifetime Display 24-08-03_00:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710557/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 00:02:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710557/content)


***

### [2024-08-03 00:03:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118295)
>Turning ON BBLR

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 00:03:07)

[ LHC BBLR Wire App v0.4.1 24-08-03_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710559/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 00:04:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710559/content)

[ LHC Beam Losses Lifetime Display 24-08-03_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710561/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 00:04:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710561/content)

[Set BBLR_PC 24-08-03_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710563/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/03 00:04:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710563/content)


***

### [2024-08-03 00:07:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118296)
>Lifetime improvement with tune trimas after turning ON BBLR

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 00:07:15)

[ LHC Beam Losses Lifetime Display 24-08-03_00:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710565/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 00:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710565/content)


***

### [2024-08-03 06:33:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118330)
>Emittance scans IP1 and IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:33:54)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_06:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710652/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:33:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710652/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_06:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710654/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:34:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710654/content)


***

### [2024-08-03 06:34:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118331)
>Status before dump

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:35:06)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710656/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:35:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710656/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710658/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:35:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710658/content)

[ LHC Fast BCT v1.3.2 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710660/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:35:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710660/content)

[ LHC Beam Losses Lifetime Display 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710662/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 06:35:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710662/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710664/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 06:35:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710664/content)

[Abort gap cleaning control v2.1.0 24-08-03_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710666/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 06:35:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710666/content)

[LHC Cryogenic Heat Load 2.2.3 24-08-03_06:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710668/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/08/03 06:36:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710668/content)

[udp:..multicast-bevlhc1:1234 - VLC media player 24-08-03_06:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710670/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:40:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710670/content)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-08-03_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710672/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:41:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710672/content)

[udp:..multicast-bevlhclumi:1234 - VLC media player 24-08-03_06:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710674/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:41:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710674/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-03_07:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710813/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/03 07:51:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710813/content)


***

### [2024-08-03 06:36:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118333)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:36:57)


***

### [2024-08-03 06:40:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118334)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:40:46)


***

### [2024-08-03 06:40:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118335)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:40:48)


***

### [2024-08-03 06:42:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118336)
>XRPs OUT

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 06:42:06)

[COLLIMATORS_VISTAR_MOV 24-08-03_06:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710676/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 06:42:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710676/content)


***

### [2024-08-03 06:42:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118337)
>LHCb did not acknolwdge the dump ahandshake, contacting them

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 06:42:35)

[LHC-EXPTS Handshakes 24-08-03_06:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710678/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 06:42:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710678/content)


***

### [2024-08-03 06:43:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118338)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:43:52)


***

### [2024-08-03 06:44:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118342)
>Global Post Mortem Event

Event Timestamp: 03/08/24 06:44:22.337
Fill Number: 9969
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 21625 / 21942 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/03 06:47:15)


***

### [2024-08-03 06:44:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118398)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: acalia / Programmed dump after physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/03 07:12:37)


***

### [2024-08-03 06:45:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118339)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:45:06)


***

### [2024-08-03 06:45:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118340)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:45:12)


***

### [2024-08-03 06:45:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118341)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 06:45:14)


***

### [2024-08-03 06:47:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118343)
>
```
cwo-ccc-d0lf died again...  
Hard rebooting it  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 07:32:17)


***

### [2024-08-03 06:49:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118344)
>
```
Before Ramp down, sent the RF bucket change trim before running the RF sequence to spare the setup at the next injection 
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 07:32:07)

[Confirmation required 24-08-03_06:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710680/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 06:49:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710680/content)


***

### [2024-08-03 06:50:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118346)
>
```
TEsting the new parallelized sequence for preparation  
All tasks beside collimation preparation work;  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 07:31:20)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-08-03_06:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710684/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 06:50:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3710684/content)


