# FILL 9948
**start**:		 2024-07-30 18:42:11.297238525+02:00 (CERN time)

**end**:		 2024-07-30 18:59:01.314238525+02:00 (CERN time)

**duration**:	 0 days 00:16:50.017000


***

### [2024-07-30 18:42:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115956)
>LHC RUN CTRL: New FILL NUMBER set to 9948

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:42:11)


***

### [2024-07-30 18:43:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115960)
>
```
Cryo not sure about cause of loss of communication with 78.  
However, they give to green light to resume operations, preparing for precycle.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 18:43:55)


***

### [2024-07-30 18:44:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115966)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:44:40)


***

### [2024-07-30 18:50:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115989)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:50:30)


***

### [2024-07-30 18:52:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115994)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:52:42)


***

### [2024-07-30 18:53:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115997)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:53:02)


***

### [2024-07-30 18:55:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4115999)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:55:28)


***

### [2024-07-30 18:55:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116001)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:55:33)


***

### [2024-07-30 18:59:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116008)
>LHC RUN CTRL: New FILL NUMBER set to 9949

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 18:59:01)


