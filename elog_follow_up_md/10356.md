# FILL 10356
**start**:		 2024-11-12 05:07:54.086863525+01:00 (CERN time)

**end**:		 2024-11-12 13:41:02.173363525+01:00 (CERN time)

**duration**:	 0 days 08:33:08.086500


***

### [2024-11-12 05:07:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182433)
>LHC RUN CTRL: New FILL NUMBER set to 10356

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:07:54)


***

### [2024-11-12 05:08:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182439)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:08:32)


***

### [2024-11-12 05:09:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182440)
>
```
MB.A9R5 iQPS SPI bus stuck, will reset at standby
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 05:11:49)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-11-12_05:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861082/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 05:09:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861082/content)


***

### [2024-11-12 05:10:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182441)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_125NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:10:36)


***

### [2024-11-12 05:11:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182443)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:11:59)


***

### [2024-11-12 05:13:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182445)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:13:29)


***

### [2024-11-12 05:14:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182447)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:14:02)


***

### [2024-11-12 05:15:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182449)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:15:28)


***

### [2024-11-12 05:16:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182450)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:16:27)


***

### [2024-11-12 05:16:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182452)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:16:28)


***

### [2024-11-12 05:16:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182454)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:16:30)


***

### [2024-11-12 05:16:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182458)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:16:45)


***

### [2024-11-12 05:17:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182461)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:17:24)


***

### [2024-11-12 05:17:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182463)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:17:31)


***

### [2024-11-12 05:18:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182464)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:18:42)


***

### [2024-11-12 05:19:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182466)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:19:11)


***

### [2024-11-12 05:20:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182468)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:20:26)


***

### [2024-11-12 05:20:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182470)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:20:34)


***

### [2024-11-12 05:23:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182472)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:23:01)


***

### [2024-11-12 05:23:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182474)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:23:36)


***

### [2024-11-12 05:24:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182475)
>
```
Abort gap did hardly re-populate during the fill and the dump was cleaner.  
It could be worth cleaning preventively in the future fills...  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 05:25:53)

[Abort gap cleaning control v2.1.1 24-11-12_05:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861094/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 05:24:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861094/content)

[pm-beam-loss-evaluation >> Version: 1.0.11  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-11-12_05:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861096/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 05:26:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861096/content)


***

### [2024-11-12 05:30:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182476)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:30:30)


***

### [2024-11-12 05:44:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182481)
>Power Converter Faults  
  
Device: RPMBD.RR57.ROF.A56B2 (RPMBD.RR57.ROF.A56B2)  
Time: 2024-11-12 05:41:26,140  
Faults: NO\_PC\_PERMIT VS\_FAULT VS\_EXTINTLOCK FAST\_ABORT  
VS faults: SW-OPEN, CTRL-PROTEC-MODULE, FASTPA  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734770:6532795?archive=1734770:6532800?archive=1734770:6532803?archive=1734770:6532808?archive=1734770:6532810?archive=1734770:6532815?archive=1734770:6532818?archive=1734770:6532821?archive=1734770:6532826?archive=1734770:6532829?archive=1734770:6532832?archive=1734770:6532837?archive=1734770:6532838)  
  
  
Device: RPMBD.RR57.ROF.A56B1 (RPMBD.RR57.ROF.A56B1)  
Time: 2024-11-12 05:41:28,700  
Faults: NO\_PC\_PERMIT VS\_FAULT VS\_EXTINTLOCK FAST\_ABORT  
VS faults: SW-OPEN, CTRL-PROTEC-MODULE, FASTPA  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734769:6532796?archive=1734769:6532798?archive=1734769:6532806?archive=1734769:6532807?archive=1734769:6532812?archive=1734769:6532816?archive=1734769:6532822?archive=1734769:6532824?archive=1734769:6532830?archive=1734769:6532834?archive=1734769:6532836?archive=1734769:6532839?archive=1734769:6532842)  
  
  
Device: RPMBD.RR57.ROD.A56B2 (RPMBD.RR57.ROD.A56B2)  
Time: 2024-11-12 05:41:28,700  
Faults: NO\_PC\_PERMIT VS\_FAULT VS\_EXTINTLOCK FAST\_ABORT  
VS faults: SW-OPEN, CTRL-PROTEC-MODULE, FASTPA  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734768:6532793?archive=1734768:6532797?archive=1734768:6532802?archive=1734768:6532805?archive=1734768:6532811?archive=1734768:6532814?archive=1734768:6532820?archive=1734768:6532825?archive=1734768:6532827?archive=1734768:6532831?archive=1734768:6532835?archive=1734768:6532840?archive=1734768:6532843)  
  
  
Device: RPMBD.RR57.ROD.A56B1 (RPMBD.RR57.ROD.A56B1)  
Time: 2024-11-12 05:41:28,700  
Faults: NO\_PC\_PERMIT VS\_FAULT VS\_EXTINTLOCK FAST\_ABORT  
VS faults: SW-OPEN, CTRL-PROTEC-MODULE, FASTPA  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734767:6532794?archive=1734767:6532799?archive=1734767:6532801?archive=1734767:6532804?archive=1734767:6532809?archive=1734767:6532813?archive=1734767:6532817?archive=1734767:6532819?archive=1734767:6532823?archive=1734767:6532828?archive=1734767:6532833?archive=1734767:6532841?archive=1734767:6532844)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/12 05:44:03)


***

### [2024-11-12 05:49:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182483)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:49:09)


***

### [2024-11-12 05:52:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182484)
>correction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 05:52:04)

[ Accelerator Cockpit v0.0.42 24-11-12_05:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861107/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 05:52:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861107/content)

[RF Trim Warning 24-11-12_05:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861109/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 05:52:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861109/content)

[ Accelerator Cockpit v0.0.42 24-11-12_05:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861111/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 05:52:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861111/content)

[ Accelerator Cockpit v0.0.42 24-11-12_05:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861113/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 05:53:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861113/content)


***

### [2024-11-12 05:53:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182485)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 05:53:10)


***

### [2024-11-12 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182522)
>**\*\*\* SHIFT SUMMARY \*\*\***  
Stable Beams all night until the programmed dump at ~5am for refill.  
  
Notes:  
- Cleaned the abort gap in Stable Beams at the request of ATLAS during the LPC (ZDC baseline). Cleaning was effective and brought down the population from ~1e9 to ~1e7, with negligible re-population during the following 5h of collisions.  
- B2H crystal angular scan with ATLAS separated by 8 sigma during the dump handshake: found an offset of ~10 urad wrt the optimum in collisions, consistent with the behavior at the start of Stable Beams.  
  
-- Michi --  


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/12 08:17:37)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861158/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/11/12 08:17:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861158/content)


***

### [2024-11-12 07:07:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182495)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:07:10)


***

### [2024-11-12 07:07:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182496)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:07:24)


***

### [2024-11-12 07:07:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182497)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:07:30)


***

### [2024-11-12 07:07:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182498)
>LHC Injection Complete

Number of injections actual / planned: 62 / 46
SPS SuperCycle length: 69.6 [s]
Actual / minimum time: 1:14:21 / 0:58:21 (127.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/12 07:07:31)


***

### [2024-11-12 07:09:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182499)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:09:54)


***

### [2024-11-12 07:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182501)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:09:56)


***

### [2024-11-12 07:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182501)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:09:56)


***

### [2024-11-12 07:31:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182505)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:31:13)


***

### [2024-11-12 07:31:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182507)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:31:27)


***

### [2024-11-12 07:37:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182509)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:37:23)


***

### [2024-11-12 07:38:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182510)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:38:18)


***

### [2024-11-12 07:39:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182512)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:39:01)


***

### [2024-11-12 07:44:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182513)
>collisions

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 07:44:44)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-12_07:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861126/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 07:44:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861126/content)

[ LHC Beam Losses Lifetime Display 24-11-12_07:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861128/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/12 07:44:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861128/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-11-12_07:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861130/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 07:45:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861130/content)

[ LHC Beam Losses Lifetime Display 24-11-12_07:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861134/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/12 07:45:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861134/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-12_07:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861136/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/12 07:45:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861136/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-12_07:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861138/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/12 07:46:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861138/content)


***

### [2024-11-12 07:45:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182514)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 07:45:07)

[ LHC Fast BCT v1.3.2 24-11-12_07:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861132/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 07:45:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861132/content)


***

### [2024-11-12 08:26:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182523)
>
```
exception

2024-11-12 07:08:41,410 - pyjapc - ERROR - Parameter TCPCH.A5R7.B2/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCH.A5R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_linear' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A5R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCH.A5R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_linear' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A5R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:41,414 - pyjapc - ERROR - Parameter TCPCV.A6L7.B1/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6L7.B1/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6L7.B1" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6L7.B1/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6L7.B1" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:41,414 - pyjapc - ERROR - Parameter TCPCH.A4L7.B1/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCH.A4L7.B1/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A4L7.B1" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCH.A4L7.B1/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A4L7.B1" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:41,414 - pyjapc - ERROR - Parameter TCPCV.A6R7.B2/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:41,518 - pyjapc - ERROR - Parameter TCPCH.A5R7.B2/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCH.A5R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A5R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCH.A5R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCH.A5R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:41,519 - pyjapc - ERROR - Parameter TCPCV.A6R7.B2/Acquisition received exception:
RdaException
cern.japc.core.NoDataAvailableException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
cern.cmw.rda3.common.exception.ServerException: NO_DATA_AVAILABLE: Access point 'TCPCV.A6R7.B2/Acquisition' has no data: FESA_13021 The field 'controllerPosition_rotational' has no data for the cycle selector 'ALL' (no set was done before calling get).. src/fesa-core/DataStore/AcquisitionFieldCommon.cpp:57
device="TCPCV.A6R7.B2" property="Acquisition" selector="" requestType="NOTIFY"
2024-11-12 07:08:44,903 - pyCrystalCockpit.logic.optimization - INFO - Acquiring initial losses and angle for plane B2V
2024-11-12 07:08:55,169 - pyCrystalCockpit.logic.optimization - INFO - Starting scan in positive direction for plane B2V
2024-11-12 07:08:55,169 - pyCrystalCockpit.motionDelegate - INFO - Applying real time trim of 9.64 urad on TCPCV.A6R7.B2
2024-11-12 07:09:05,816 - pyCrystalCockpit.logic.optimization - INFO - Moving out from optimal channeling, optimization scan direction reverted.
2024-11-12 07:09:05,816 - pyCrystalCockpit.motionDelegate - INFO - Applying real time trim of -9.64 urad on TCPCV.A6R7.B2
2024-11-12 07:09:06,826 - pyCrystalCockpit.logic.optimization - INFO - Starting scan in negative direction for plane B2V
2024-11-12 07:09:06,826 - pyCrystalCockpit.motionDelegate - INFO - Applying real time trim of -9.64 urad on TCPCV.A6R7.B2

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 08:27:23)

[LHC Crystals Cockpit 24-11-12_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861159/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/12 08:26:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861159/content)


***

### [2024-11-12 09:10:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182539)
>QUick cleaning of AG to few E8

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 09:11:08)

[Abort gap cleaning control v2.1.1 24-11-12_09:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861173/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 09:11:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861173/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-11-12_09:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861175/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 09:11:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861175/content)


***

### [2024-11-12 09:14:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182541)
>Event created from ScreenShot Client.  
Abort gap cleaning control v2.1.1 
 24-11-12\_09:14.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 09:14:47)

[Abort gap cleaning control v2.1.1 24-11-12_09:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861177/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 09:14:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861177/content)


***

### [2024-11-12 09:54:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182562)
>Access control sw update is ongoing

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 09:54:20)


***

### [2024-11-12 09:54:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182563)
>fake status due to sw update

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/11/12 09:54:56)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861214/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/11/12 09:54:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861214/content)


***

### [2024-11-12 11:06:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182585)
>
```
Doubled the initial separation for the next fill (from 12 um to 24 um). Regenerated the OFB ref orbits.
```


creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/11/12 11:09:37)

[ LSA Applications Suite (v 16.6.36) 24-11-12_11:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861346/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/12 11:06:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861346/content)


***

### [2024-11-12 11:40:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182599)
>Event created from ScreenShot Client.  
Mozilla Firefox 24-11-12\_11:40.png

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/12 11:40:12)

[Mozilla Firefox 24-11-12_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861402/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/12 11:40:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861402/content)

[Abort gap cleaning control v2.1.1 24-11-12_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861404/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 11:40:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861404/content)

[Abort gap cleaning control v2.1.1 24-11-12_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861406/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/12 11:40:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861406/content)


***

### [2024-11-12 12:06:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182607)
>Access team sw update aborted. System is restored in the previous stase

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 12:06:56)


***

### [2024-11-12 13:25:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182620)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:25:19)


***

### [2024-11-12 13:28:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182621)
>
```
RF settings for coupler 7B1 back to 60K and partition back to 12 (also regenerated inj and end of ramp) at the request of Helga  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 13:37:31)

[ LSA Applications Suite (v 16.6.36) 24-11-12_13:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861477/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 13:28:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861477/content)

[ LSA Applications Suite (v 16.6.36) 24-11-12_13:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861479/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/12 13:29:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3861479/content)


***

### [2024-11-12 13:30:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182623)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:30:22)


***

### [2024-11-12 13:30:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182629)
>Global Post Mortem Event

Event Timestamp: 12/11/24 13:30:29.027
Fill Number: 10356
Accelerator / beam mode: ION PHYSICS / STABLE BEAMS
Energy: 6799320 [MeV]
Intensity B1/B2: 870 / 900 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/12 13:33:19)


***

### [2024-11-12 13:31:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182624)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:31:14)


***

### [2024-11-12 13:31:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182625)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:31:21)


***

### [2024-11-12 13:31:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182626)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:31:27)


***

### [2024-11-12 13:31:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182627)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:31:29)


***

### [2024-11-12 13:41:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182634)
>LHC RUN CTRL: New FILL NUMBER set to 10357

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/12 13:41:03)


