# FILL 9932
**start**:		 2024-07-25 16:55:39.899238525+02:00 (CERN time)

**end**:		 2024-07-26 00:46:52.276488525+02:00 (CERN time)

**duration**:	 0 days 07:51:12.377250


***

### [2024-07-25 16:55:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112623)
>LHC RUN CTRL: New FILL NUMBER set to 9932

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:55:40)


***

### [2024-07-25 17:03:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112634)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:03:26)


***

### [2024-07-25 17:04:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112635)
>CV has restarted the second AC unit in point 4 (it was off for a test)

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 17:04:56)

[Mozilla Firefox 24-07-25_17:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696042/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 17:04:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696042/content)


***

### [2024-07-25 17:05:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112637)
>KS

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 17:05:40)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_17:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696044/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 17:05:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696044/content)


***

### [2024-07-25 17:05:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112636)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:05:39)


***

### [2024-07-25 17:05:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112638)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:05:41)


***

### [2024-07-25 17:07:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112640)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:07:10)


***

### [2024-07-25 17:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112641)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:07:58)


***

### [2024-07-25 17:08:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112643)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:08:07)


***

### [2024-07-25 17:08:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112644)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:08:22)


***

### [2024-07-25 17:09:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112646)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:09:37)


***

### [2024-07-25 17:10:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112649)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:10:01)


***

### [2024-07-25 17:10:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112651)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:10:11)


***

### [2024-07-25 17:12:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112653)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:16)


***

### [2024-07-25 17:12:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112655)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:23)


***

### [2024-07-25 17:12:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112656)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:26)


***

### [2024-07-25 17:12:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112658)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:27)


***

### [2024-07-25 17:12:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112660)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:28)


***

### [2024-07-25 17:12:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112664)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:12:43)


***

### [2024-07-25 17:14:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112667)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:14:44)


***

### [2024-07-25 17:16:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112670)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:16:28)


***

### [2024-07-25 17:28:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112677)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:28:17)


***

### [2024-07-25 17:36:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112683)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:36:23)


***

### [2024-07-25 17:40:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112687)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-07-25\_17:40.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 17:40:26)

[Accelerator Cockpit v0.0.37 24-07-25_17:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696094/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 17:40:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696094/content)

[RF Trim Warning 24-07-25_17:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696096/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 17:40:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696096/content)


***

### [2024-07-25 17:45:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112688)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 17:45:41)


***

### [2024-07-25 17:52:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112691)
>Wirescans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 17:52:11)

[ LHC WIRESCANNER APP 24-07-25_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696114/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 17:52:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696114/content)

[ LHC WIRESCANNER APP 24-07-25_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696116/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 17:52:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696116/content)

[ LHC WIRESCANNER APP 24-07-25_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696118/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 17:52:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696118/content)

[ LHC WIRESCANNER APP 24-07-25_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696120/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 17:52:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696120/content)


***

### [2024-07-25 18:20:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112697)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:20:14)

[ LHC Fast BCT v1.3.2 24-07-25_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696136/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:20:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696136/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-25_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696138/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:20:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696138/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-25_18:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696140/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/25 18:20:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696140/content)


***

### [2024-07-25 18:20:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112698)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:20:23)


***

### [2024-07-25 18:20:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112699)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:20:33)


***

### [2024-07-25 18:20:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112700)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:20:37)


***

### [2024-07-25 18:20:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112701)
>LHC Injection Complete

Number of injections actual / planned: 50 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:34:57 / 0:33:48 (103.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 18:20:37)


***

### [2024-07-25 18:21:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112703)
>KS

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:35:37)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696142/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:21:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696142/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696144/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:22:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696144/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696146/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:22:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696146/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696148/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:22:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696148/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696154/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:24:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696154/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696156/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:24:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696156/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696178/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:35:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696178/content)

[Beam Feedbacks - Mission Control 24-07-25_18:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696180/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:35:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696180/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696184/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:36:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696184/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696186/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:36:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696186/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-25_18:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696220/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:42:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696220/content)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-25_18:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696222/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:42:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696222/content)


***

### [2024-07-25 18:22:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112704)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:22:54)


***

### [2024-07-25 18:22:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112706)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:22:55)


***

### [2024-07-25 18:24:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112709)
>Small losses in IR3 at start of ramp B1

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:25:11)

[ LHC BLM Fixed Display 24-07-25_18:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696158/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:25:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696158/content)


***

### [2024-07-25 18:44:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112723)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:44:12)


***

### [2024-07-25 18:44:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112725)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:44:25)


***

### [2024-07-25 18:44:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112725)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:44:25)


***

### [2024-07-25 18:44:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112727)
>Ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:44:38)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696236/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:44:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696236/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696238/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:44:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696238/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-25_18:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696246/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/25 18:44:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696246/content)

[ LHC Beam Losses Lifetime Display 24-07-25_18:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696248/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 18:45:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696248/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-25_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696258/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:46:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696258/content)


***

### [2024-07-25 18:52:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112742)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:52:56)


***

### [2024-07-25 18:53:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112744)
>qchange + squeeze

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:53:40)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_18:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696302/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 18:53:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696302/content)

[ LHC Beam Losses Lifetime Display 24-07-25_18:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696306/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 18:53:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696306/content)


***

### [2024-07-25 18:54:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112746)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:54:04)


***

### [2024-07-25 18:54:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112748)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 18:54:57)


***

### [2024-07-25 18:58:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112750)
>
```
collisions
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:58:31)

[ LHC Fast BCT v1.3.2 24-07-25_18:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696318/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:58:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696318/content)

[ LHC Beam Losses Lifetime Display 24-07-25_18:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696320/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 18:58:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696320/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_19:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696382/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:15:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696382/content)


***

### [2024-07-25 18:59:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112752)
>optimization IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:59:11)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_18:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696328/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 18:59:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696328/content)


***

### [2024-07-25 19:00:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112756)
>optimizations IP1, 2, 5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:00:38)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_19:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696340/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:00:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696340/content)


***

### [2024-07-25 19:02:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112758)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 19:02:28)


***

### [2024-07-25 19:03:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112759)
>Second optimization IP1 and IP5 before emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:03:21)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_19:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696352/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:03:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696352/content)


***

### [2024-07-25 19:08:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112762)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 19:08:57)


***

### [2024-07-25 19:10:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112764)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 19:10:56)


***

### [2024-07-25 19:15:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112768)
>Emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:15:17)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_19:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696376/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 22:47:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696376/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_19:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696384/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 19:15:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696384/content)


***

### [2024-07-25 22:45:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112827)
>Started and ended shift with stable beams... but a different fill.  
Dumped previous fill around 17 and had a smooth fill and cycle.  
  
AC

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/25 22:45:33)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696533/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/25 22:45:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696533/content)


***

### [2024-07-25 23:49:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112843)
>Global Post Mortem Event

Event Timestamp: 25/07/24 23:49:09.085
Fill Number: 9932
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 31049 / 31054 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 5-PIC\_UNM: B T -> F on CIB.UA23.L2.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/25 23:52:05)


***

### [2024-07-25 23:50:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112842)
>Lost sector12 but doesn't seem to have been a quench.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/25 23:50:23)

[Desktop 24-07-25_23:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696563/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/25 23:50:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696563/content)


***

### [2024-07-25 23:52:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112844)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 23:52:25)


***

### [2024-07-25 23:52:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112845)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 23:52:27)

[ CCM_1 LHCOP 24-07-25_23:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696565/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 23:54:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696565/content)


***

### [2024-07-25 23:58:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112851)
>seems likeit was the RQF.A12 that triggered it but no heaters were 
 triggered but I tend to remember that this happend before for sector12as 
 well.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:06:21)

[ PIC SUPERVISION v9.0.0 24-07-25_23:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696576/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:06:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696576/content)


***

### [2024-07-26 00:10:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112854)
>Yeah really looks like this is coming from the QPS.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:10:33)

[Analyze PM buffers 24-07-26_00:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696581/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:10:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696581/content)

[event_data >> Version: 7.26.0  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-07-26_00:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696586/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:13:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696586/content)


***

### [2024-07-26 00:15:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112861)
>
```
Called the QPS piquet and he is having a look  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:43:01)


***

### [2024-07-26 00:22:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112858)
>Ti came and informed there was fault of the water for the DQR in point 1. The number is FDED-00103, it is possible it is related to the problem but it did appear a bit later than the dump so maybe it is a concequence of it rather than the cause. We don't need an urgent access for this in prinicple but when there is an opportunity it should be fixed.    


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:23:41)


***

### [2024-07-26 00:29:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112862)
>Also the PC says it was an external fault 


 
```
(String[]:2) -> VS    :EXTERNAL FAST ABORT            :FAULT  :UT:LT, VS    :AUX POWER SUPPLY               :FAULT  :UT:LT
---------------------------------------- 
```


```
24:28:44 Result for device RPHE.UA23.RQF.A12 :
(String[]:3) -> VS    :EXTERNAL FAST ABORT            :FAULT  :UT:LT, VS    :AUX POWER SUPPLY               :FAULT  :UT:LT, VS    :FAST ABORT UNSAFE              :FAULT  :UT:LT
---------------------------------------- 
```


```
24:28:44 Command 'FGC_51:DIAG.FAULTS:READ' completed 
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:29:32)

[ Equip State 24-07-26_00:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696594/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:29:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696594/content)

[ Equip State 24-07-26_00:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696596/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 00:29:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696596/content)


***

### [2024-07-26 00:46:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112866)
>LHC RUN CTRL: New FILL NUMBER set to 9933

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 00:46:53)


