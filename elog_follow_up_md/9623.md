# FILL 9623
**start**:		 2024-05-13 23:50:12.367238525+02:00 (CERN time)

**end**:		 2024-05-14 06:51:39.651863525+02:00 (CERN time)

**duration**:	 0 days 07:01:27.284625


***

### [2024-05-13 23:50:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066875)
>LHC RUN CTRL: New FILL NUMBER set to 9623

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/13 23:50:12)


***

### [2024-05-14 00:00:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066888)
>Some issues with collimator energy interlocks for TCTV in B1 and B2 (IR2 and IR8) when reverting collimator changes with the sequence were solved by Tobias, Theo and Fredrick, but MD11786 started about 30 min late.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 00:57:25)


***

### [2024-05-14 00:07:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066877)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 00:07:56)


***

### [2024-05-14 00:10:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066878)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 00:10:25)


***

### [2024-05-14 00:11:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066879)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 00:11:34)


***

### [2024-05-14 00:24:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066880)
>Event created from ScreenShot Client.  
RF Trim Warning 24-05-14\_00:24.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:24:31)

[RF Trim Warning 24-05-14_00:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589582/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:24:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589582/content)


***

### [2024-05-14 00:26:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066881)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-14\_00:26.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:26:34)

[Accelerator Cockpit v0.0.35 24-05-14_00:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589584/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:26:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589584/content)


***

### [2024-05-14 00:27:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066882)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-14\_00:27.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:27:28)

[Accelerator Cockpit v0.0.35 24-05-14_00:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589586/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:27:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589586/content)


***

### [2024-05-14 00:34:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066883)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-14\_00:35.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:35:02)

[Accelerator Cockpit v0.0.35 24-05-14_00:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589588/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 00:35:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589588/content)


***

### [2024-05-14 00:36:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066884)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:

creator:	 copera  @cs-ccr-inca1.cern.ch (2024/05/14 00:36:33)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589590/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/05/14 00:36:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589590/content)


***

### [2024-05-14 00:36:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066885)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:

creator:	 copera  @cs-ccr-inca1.cern.ch (2024/05/14 00:36:34)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589591/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/05/14 00:36:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589591/content)


***

### [2024-05-14 00:39:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066886)
>LHC RUN CTRL: BEAM MODE changed to INJECTION SETUP BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 00:39:47)


***

### [2024-05-14 00:50:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066887)
>RF voltage was changed to 2.5 MV. to be reverted after the MD

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 00:51:14)

[  24-05-14_00:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589592/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 00:51:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589592/content)

[ LSA Applications Suite (v 16.5.36) 24-05-14_00:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589596/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 00:52:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589596/content)


***

### [2024-05-14 01:14:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066889)
>correction on B1

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/14 01:15:08)

[Desktop 24-05-14_01:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589598/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/14 01:15:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589598/content)


***

### [2024-05-14 01:15:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066890)
>correction on Beam 2

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/14 01:15:37)

[OpenYASP DV LHCB2Transfer . LHC_INDIV_1inj_Q20_2024_V3 24-05-14_01:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589600/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/14 01:15:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589600/content)


***

### [2024-05-14 01:22:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066895)
>Event created from ScreenShot Client.  
Figure 1 24-05-14\_01:22.png

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/14 01:22:25)

[Figure 1 24-05-14_01:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589604/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/14 01:22:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589604/content)


***

### [2024-05-14 02:01:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066902)
>applied the method set by the MD coordination: dump and inject the probe without going back to injection probe beam  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 02:02:37)


***

### [2024-05-14 02:21:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066916)
>many trips of SPS cavity 1 since the beginning of the MD  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/14 02:21:52)


***

### [2024-05-14 03:00:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066926)
>
```
**MD11786**  
The beam phase loop is opened at about 15000 turns and a 10-degree phase kick is applied on Beam 2 after ~2000 turns. Above the (expected) LLD threshold with an intensity of about 7e10 p/b and bunch length of 0.8 ns -> undamped oscillations.   

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 05:18:26)

[mean_phase.png - Image Viewer [3.4] 24-05-14_03:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589620/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:01:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589620/content)

[bunchlength.png - Image Viewer [1.4] 24-05-14_03:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589622/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:01:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589622/content)


***

### [2024-05-14 03:10:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066927)
>
```
**MD11786**The beam phase loop is opened and a 5-degree phase kick is applied right after on Beam 1. Below the (expected) LLD threshold with an intensity of about 5e9 p/b and bunch length of 0.8 ns -> oscillations are damped.
```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 05:18:12)

[Figure 3 24-05-14_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589624/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:10:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589624/content)

[Figure 4 24-05-14_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589626/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:10:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589626/content)


***

### [2024-05-14 03:10:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066927)
>
```
**MD11786**The beam phase loop is opened and a 5-degree phase kick is applied right after on Beam 1. Below the (expected) LLD threshold with an intensity of about 5e9 p/b and bunch length of 0.8 ns -> oscillations are damped.
```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 05:18:12)

[Figure 3 24-05-14_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589624/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:10:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589624/content)

[Figure 4 24-05-14_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589626/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 03:10:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589626/content)


***

### [2024-05-14 04:47:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066930)
>Event created from ScreenShot Client.  
Figure 3 24-05-14\_04:47.png

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 04:47:39)

[Figure 3 24-05-14_04:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589640/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 04:47:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589640/content)

[Figure 4 24-05-14_04:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589642/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/05/14 04:48:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589642/content)


***

### [2024-05-14 05:07:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066934)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:07.png  
bunch intensity ~3e10 p/b  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:13:15)

[Beam profile analysis 24-05-14_05:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589650/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:07:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589650/content)

[Beam profile analysis 24-05-14_05:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589652/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:08:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589652/content)


***

### [2024-05-14 05:12:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066935)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:12.png  
bunch intensity ~3e10 p/b (bunch #2)  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:25:04)

[Beam profile analysis 24-05-14_05:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589654/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:12:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589654/content)

[Beam profile analysis 24-05-14_05:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589656/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:13:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589656/content)


***

### [2024-05-14 05:17:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066936)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:17.png  
with zero kick (bunch #2)  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:25:12)

[Beam profile analysis 24-05-14_05:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589658/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:17:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589658/content)

[Beam profile analysis 24-05-14_05:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589660/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:19:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589660/content)


***

### [2024-05-14 05:21:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066937)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:21.png  
New bunch (#3) at 3e10 p/b, zero kick  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:25:30)

[Beam profile analysis 24-05-14_05:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589662/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:21:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589662/content)

[Beam profile analysis 24-05-14_05:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589664/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:24:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589664/content)


***

### [2024-05-14 05:28:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066939)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:28.png  
Bunch #3 at 3e10 p/b, 5 deg kick  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:29:12)

[Beam profile analysis 24-05-14_05:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589668/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:28:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589668/content)

[Beam profile analysis 24-05-14_05:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589670/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:29:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589670/content)


***

### [2024-05-14 05:37:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066941)
>dRF getting very large

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:38:01)

[ CCM_1 LHCOP 24-05-14_05:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589674/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:38:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589674/content)


***

### [2024-05-14 05:37:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066940)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:37.png  
2.5e10 p/b, 2 deg kick  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:38:26)

[Beam profile analysis 24-05-14_05:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589672/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:37:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589672/content)

[Beam profile analysis 24-05-14_05:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589680/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:46:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589680/content)


***

### [2024-05-14 05:45:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066942)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:45.png  

```

```
2.5e10 p/b, 5 deg kick
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:47:29)

[Beam profile analysis 24-05-14_05:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589676/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:45:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589676/content)


***

### [2024-05-14 05:45:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066943)
>Event created from ScreenShot Client.  
RF Trim Warning 24-05-14\_05:45.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:45:33)

[RF Trim Warning 24-05-14_05:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589678/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:45:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589678/content)


***

### [2024-05-14 05:52:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066946)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_05:52.png  
fresh bunch 2.5e10 p/b, maybe large energy error  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:57:10)

[Beam profile analysis 24-05-14_05:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589682/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 05:52:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589682/content)


***

### [2024-05-14 05:53:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066947)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 05:53:15)


***

### [2024-05-14 05:57:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066948)
>Event created from ScreenShot Client.  
RF Trim Warning 24-05-14\_05:57.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:58:13)

[RF Trim Warning 24-05-14_05:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589684/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:58:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589684/content)


***

### [2024-05-14 05:58:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066949)
>Event created from ScreenShot Client.  
RF Trim Warning 24-05-14\_05:58.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:58:27)

[RF Trim Warning 24-05-14_05:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589686/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/14 05:58:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589686/content)


***

### [2024-05-14 06:02:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066952)
>LHC RUN CTRL: BEAM MODE changed to INJECTION SETUP BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/14 06:02:55)


***

### [2024-05-14 06:04:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066954)
>masked COD PC interlocks to retrieve a good situation with the pilot. Unmasked after correction

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 06:06:19)

[ LHC SIS GUI 24-05-14_06:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589688/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 06:06:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589688/content)


***

### [2024-05-14 06:06:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066955)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_06:06.png  
Fresh bunch after orbit correction  
2.5e10 p/b, 2 deg kick  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:07:47)

[Beam profile analysis 24-05-14_06:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589690/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:06:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589690/content)

[Beam profile analysis 24-05-14_06:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589692/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:07:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589692/content)


***

### [2024-05-14 06:12:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066960)
>CODs back to large current for the INDIVs

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 06:12:25)

[ Power Converter Interlock GUI 24-05-14_06:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589699/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/14 06:12:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589699/content)


***

### [2024-05-14 06:15:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066961)
>BIRK EMIL KARLSEN-BAECK(BKARLSEN) assigned RBAC Role: RF-LHC-Piquet

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/14 06:15:05)


***

### [2024-05-14 06:16:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066962)
>Event created from ScreenShot Client.  
Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-14\_06:16.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 06:16:30)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-14_06:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589705/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 06:16:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589705/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-14_06:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589707/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 06:16:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589707/content)


***

### [2024-05-14 06:22:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066963)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_06:22.png  

```

```
Bunch #2, 2.5e10 p/b, 2 deg kick
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:23:03)

[Beam profile analysis 24-05-14_06:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589709/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:22:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589709/content)

[Beam profile analysis 24-05-14_06:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589711/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:22:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589711/content)


***

### [2024-05-14 06:30:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066964)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_06:30.png  
New beam, 2e10 p/b, 2 deg kick  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:31:45)

[Beam profile analysis 24-05-14_06:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589713/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:30:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589713/content)

[Beam profile analysis 24-05-14_06:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589715/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:31:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589715/content)


***

### [2024-05-14 06:39:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066965)
>
```
Event created from ScreenShot Client.  
Beam profile analysis 24-05-14_06:39.png  

```

```
2e10 p/b, 0 deg kick
```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:40:52)

[Beam profile analysis 24-05-14_06:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589717/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:39:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589717/content)

[Beam profile analysis 24-05-14_06:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589719/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/14 06:40:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589719/content)


***

### [2024-05-14 06:49:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066966)
>blind at 2e10 -> will go back to injection probe beam for the next step

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 06:49:28)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-14_06:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589721/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/14 06:49:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3589721/content)


