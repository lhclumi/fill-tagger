# FILL 9899
**start**:		 2024-07-14 04:18:57.753988525+02:00 (CERN time)

**end**:		 2024-07-14 19:18:48.922613525+02:00 (CERN time)

**duration**:	 0 days 14:59:51.168625


***

### [2024-07-14 04:18:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105911)
>LHC RUN CTRL: New FILL NUMBER set to 9899

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 04:18:58)


***

### [2024-07-14 04:19:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105912)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 04:19:26)


***

### [2024-07-14 04:25:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105914)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 04:25:48)


***

### [2024-07-14 05:00:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105916)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:00:28)


***

### [2024-07-14 05:00:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105917)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:00:38)


***

### [2024-07-14 05:00:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105918)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:00:42)


***

### [2024-07-14 05:00:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105919)
>LHC Injection Complete

Number of injections actual / planned: 53 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:34:55 / 0:33:48 (103.3 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 05:00:43)


***

### [2024-07-14 05:02:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105920)
>Intensity in prepare ramp and also emittances.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:03:05)

[ LHC Fast BCT v1.3.2 24-07-14_05:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677225/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:02:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677225/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_05:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677227/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:02:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677227/content)


***

### [2024-07-14 05:05:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105923)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:05:21)


***

### [2024-07-14 05:05:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105921)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:05:20)


***

### [2024-07-14 05:23:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105930)
>Beam 2 vertical starts jumping from time to time which turns off the tune 
 feed-back.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 05:23:30)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-14_05:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677237/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 05:23:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677237/content)


***

### [2024-07-14 05:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105932)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:26:38)


***

### [2024-07-14 05:26:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105934)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:26:48)


***

### [2024-07-14 05:35:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105937)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:35:10)


***

### [2024-07-14 05:35:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105939)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:35:52)


***

### [2024-07-14 05:36:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105943)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:36:41)


***

### [2024-07-14 05:40:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105945)
>optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:40:37)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_05:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677239/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:40:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677239/content)


***

### [2024-07-14 05:42:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105946)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:42:28)


***

### [2024-07-14 05:48:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105947)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:48:51)


***

### [2024-07-14 05:50:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105948)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:50:55)


***

### [2024-07-14 05:50:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105948)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 05:50:55)


***

### [2024-07-14 05:54:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105949)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:54:45)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_05:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677241/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:54:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677241/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_05:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677243/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 05:54:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677243/content)


***

### [2024-07-14 06:12:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105951)
>Increasing the horizonal tune improves the lifetime for both beam 1 and 
 beam 2.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 06:13:16)

[ LHC Beam Losses Lifetime Display 24-07-14_06:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677248/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 06:13:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677248/content)


***

### [2024-07-14 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105952)
>
```
**SHIFT SUMMARY:**  
  
-Dumped the beam around 02.00 after a through check in the injectors that everything was fine.  
-Started filling but after ~500 bunches PS lost a power supply. I dumped when it was clear it would be at least 1h.  
-For the second attemt a train for beam 2 didn't arrive but wasn't clear for the automatic system so it asked, slipped with the mouse and said it was injected. This gave a mismatch and since we hadn't more than 2 trains injected many trains I decided to just dump and start over.  
-3rd fill was successful and the only issue was with the BBQ for b2v which jumps from time-to-time which then turns of the tune feedback.   
-Had to a bit of steering for beam 2 and also energy matching that was quit off.  
-Leaving in stable beams!  
  
  
  
* To be noted:

  
The QPS in sector23 for the RB flickers from time-to-time.  
  
/Tobias  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 06:54:06)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677250/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 06:54:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677250/content)


***

### [2024-07-14 13:16:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105988)
>
```
Levelling finished in IP1 and fine, switching the BBLR ON. Nodrop in lifetime this time
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 13:23:51)

[ LHC Beam Losses Lifetime Display 24-07-14_13:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677453/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 13:16:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677453/content)

[ LHC BBLR Wire App v0.4.1 24-07-14_13:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677455/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 13:16:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677455/content)


***

### [2024-07-14 13:24:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105995)
>IP5 re-optimized before switching ON automatic levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 13:24:32)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_13:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677463/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 13:24:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677463/content)


***

### [2024-07-14 14:49:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106010)
>tried tune scan on B1, not very succesful, reverted

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 14:49:37)

[Accelerator Cockpit v0.0.37 24-07-14_14:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677491/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 14:49:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677491/content)

[ LHC Beam Losses Lifetime Display 24-07-14_14:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677493/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 14:49:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677493/content)


***

### [2024-07-14 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106016)
>
```
**SHIFT SUMMARY:**  
  
Quiet shift in stable beams  
  
Delphine  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 15:54:10)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677517/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 15:53:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677517/content)


***

### [2024-07-14 17:35:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106022)
>for Dave

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:35:39)

[ INJECTION SEQUENCER  v 5.1.7 24-07-14_17:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677554/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:35:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677554/content)


***

### [2024-07-14 17:41:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106023)
>
```
trains lose slightly more as they were injected last, but nothing special 
    to the bunches that are blown up at the very end
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:47:34)

[ LHC Fast BCT v1.3.2 24-07-14_17:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677624/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:47:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677624/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_17:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677628/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:47:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677628/content)


***

### [2024-07-14 17:48:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106025)
>
```
emittance blow up seems to have occurred gradually since 13:30. Would it be linked to the switching on of the wires?
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 17:56:46)

[24-07-14_17:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677632/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 17:48:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677632/content)

[24-07-14_17:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677634/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 17:50:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677634/content)

[24-07-14_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677636/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 17:52:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677636/content)

[24-07-14_17:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677638/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 17:53:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677638/content)


***

### [2024-07-14 18:01:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106028)
>loss warning at 17:43

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 18:02:29)

[ LHC BLM Fixed Display 24-07-14_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677650/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 18:02:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677650/content)

[ LHC Beam Losses Lifetime Display 24-07-14_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677652/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 18:02:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677652/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677656/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 18:03:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677656/content)


***

### [2024-07-14 18:17:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106031)
>Event created from ScreenShot Client.  
Abort gap cleaning control v2.0.8 
 24-07-14\_18:17.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/14 18:17:36)

[Abort gap cleaning control v2.0.8 24-07-14_18:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677694/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/14 18:17:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677694/content)

[ LHC Beam Losses Lifetime Display 24-07-14_18:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677696/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 18:17:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677696/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_18:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677700/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 18:19:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677700/content)


***

### [2024-07-14 18:53:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106039)
>
```
pressures/temperatures at LHCb velo before and after the intervention are similar (one should account for the SMOG2 gas injections)
```


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 19:00:31)

[Mozilla Firefox 24-07-14_18:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677722/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 18:54:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677722/content)

[Mozilla Firefox 24-07-14_19:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677732/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 19:00:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677732/content)

[Mozilla Firefox 24-07-14_19:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677734/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/14 19:00:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677734/content)


***

### [2024-07-14 19:03:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106043)
>status before beam dump  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:04:09)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_19:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677752/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:04:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677752/content)

[ LHC Fast BCT v1.3.2 24-07-14_19:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677754/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:04:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677754/content)

[ LHC Beam Losses Lifetime Display 24-07-14_19:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677756/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 19:04:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677756/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_19:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677758/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 19:04:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677758/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_19:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677760/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:06:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677760/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_19:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677762/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:05:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677762/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_19:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677764/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:06:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677764/content)

[Desktop 24-07-14_19:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677768/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:06:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677768/content)


***

### [2024-07-14 19:06:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106045)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:06:32)


***

### [2024-07-14 19:06:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106046)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:06:37)


***

### [2024-07-14 19:06:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106047)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:06:48)


***

### [2024-07-14 19:11:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106048)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:11:48)


***

### [2024-07-14 19:11:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106054)
>Global Post Mortem Event

Event Timestamp: 14/07/24 19:11:57.330
Fill Number: 9899
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799680 [MeV]
Intensity B1/B2: 21821 / 22560 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/14 19:14:48)


***

### [2024-07-14 19:11:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106073)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: bsalvant / programmed dump after 13.5h of stable beams


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/14 19:28:53)


***

### [2024-07-14 19:12:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106050)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:12:51)


***

### [2024-07-14 19:13:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106051)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:13:01)


***

### [2024-07-14 19:13:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106052)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:13:18)


***

### [2024-07-14 19:13:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106053)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:13:29)


