# FILL 9650
**start**:		 2024-05-19 15:57:50.403613525+02:00 (CERN time)

**end**:		 2024-05-19 16:49:31.843863525+02:00 (CERN time)

**duration**:	 0 days 00:51:41.440250


***

### [2024-05-19 15:57:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070806)
>LHC RUN CTRL: New FILL NUMBER set to 9650

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:57:51)


***

### [2024-05-19 15:59:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070812)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:59:08)


***

### [2024-05-19 15:59:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070812)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 15:59:08)


***

### [2024-05-19 16:01:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070814)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:01:26)


***

### [2024-05-19 16:02:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070815)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:02:29)


***

### [2024-05-19 16:03:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070817)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:03:50)


***

### [2024-05-19 16:04:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070819)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:04:01)


***

### [2024-05-19 16:04:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070821)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:04:10)


***

### [2024-05-19 16:04:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070822)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:04:12)


***

### [2024-05-19 16:05:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070824)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:05:53)


***

### [2024-05-19 16:06:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070828)
>feed-forward of IP8 corrections in beta\* levelling

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 16:06:44)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-19_16:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597814/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 16:06:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597814/content)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-19_16:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597823/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 16:12:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597823/content)

[ LHC Lumi Commissioning.MD Toolkit 0.18.0 24-05-19_16:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597825/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 16:12:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597825/content)


***

### [2024-05-19 16:06:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070826)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:06:32)


***

### [2024-05-19 16:07:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070829)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:07:10)


***

### [2024-05-19 16:07:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070830)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:07:57)


***

### [2024-05-19 16:08:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070832)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:08:03)


***

### [2024-05-19 16:08:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070835)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:08:18)


***

### [2024-05-19 16:08:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070833)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:08:18)


***

### [2024-05-19 16:08:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070837)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:08:19)


***

### [2024-05-19 16:08:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070841)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:08:34)


***

### [2024-05-19 16:10:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070845)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:10:33)


***

### [2024-05-19 16:12:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070849)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:12:17)


***

### [2024-05-19 16:25:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070862)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:25:20)


***

### [2024-05-19 16:36:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070869)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:36:57)


***

### [2024-05-19 16:44:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070871)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:44:08)


***

### [2024-05-19 16:46:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070872)
>B2 trajectory - send correction

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:46:40)

[Desktop 24-05-19_16:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597842/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:46:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597842/content)

[OpenYASP DV LHCB2Transfer . LHC_INDIV_4Inj_Q20_2024_V1 24-05-19_16:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597844/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:46:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597844/content)


***

### [2024-05-19 16:48:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070873)
>OK after corrections

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:48:28)

[OpenYASP DV LHCB2Transfer . LHC_INDIV_4Inj_Q20_2024_V1 24-05-19_16:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597846/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:48:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597846/content)


***

### [2024-05-19 16:48:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070874)
>B1 trajectory OK

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:48:44)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_4Inj_Q20_2024_V1 24-05-19_16:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597848/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/19 16:48:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597848/content)


