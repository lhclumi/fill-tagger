# FILL 9676
**start**:		 2024-05-27 15:54:33.905488525+02:00 (CERN time)

**end**:		 2024-05-27 16:19:59.165363525+02:00 (CERN time)

**duration**:	 0 days 00:25:25.259875


***

### [2024-05-27 15:54:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076273)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/27 15:54:33)


***

### [2024-05-27 15:54:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076274)
>LHC RUN CTRL: New FILL NUMBER set to 9676

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/27 15:54:34)


***

### [2024-05-27 15:54:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076275)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/27 15:54:43)


***

### [2024-05-27 16:02:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076281)
>don't menage to open valves called piquet

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/27 16:02:54)

[LHC vac 24-05-27_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613188/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/27 16:02:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613188/content)


***

### [2024-05-27 16:02:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076282)
>Creting on\_disp knob per beam.

creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/27 16:02:56)

[ Optics Management Application v18.3.1 connected to server LHC 24-05-27_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613190/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/27 16:02:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613190/content)


***

### [2024-05-27 16:04:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076284)
>called Giuseppe, gauges were off after cryo issue. All good now.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/27 16:05:25)

[LHC vac 24-05-27_16:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613216/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/27 16:05:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3613216/content)


***

### [2024-05-27 16:19:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4076298)
>LHC RUN CTRL: New FILL NUMBER set to 9677

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/27 16:19:59)


