# FILL 10363
**start**:		 2024-11-14 06:29:57.025488525+01:00 (CERN time)

**end**:		 2024-11-14 09:01:58.889863525+01:00 (CERN time)

**duration**:	 0 days 02:32:01.864375


***

### [2024-11-14 06:30:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183617)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:30:35)


***

### [2024-11-14 06:31:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183618)
>missing signature in cloned BPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:31:30)

[24-11-14_06:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864070/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:31:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864070/content)


***

### [2024-11-14 06:32:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183619)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:32:41)


***

### [2024-11-14 06:33:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183621)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:33:58)


***

### [2024-11-14 06:35:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183622)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: ERROR
Number of failed crates: 2 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:35:34)


***

### [2024-11-14 06:36:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183624)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:36:19)


***

### [2024-11-14 06:36:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183625)
>BLM sanity check failed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:36:36)

[24-11-14_06:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864072/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:36:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864072/content)


***

### [2024-11-14 06:37:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183626)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:37:26)


***

### [2024-11-14 06:38:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183628)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:38:47)


***

### [2024-11-14 06:39:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183630)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:39:28)


***

### [2024-11-14 06:39:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183631)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:39:30)


***

### [2024-11-14 06:41:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183633)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:41:55)


***

### [2024-11-14 06:41:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183635)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:41:56)


***

### [2024-11-14 06:41:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183637)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:41:58)


***

### [2024-11-14 06:42:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183639)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:42:05)


***

### [2024-11-14 06:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183643)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:42:13)


***

### [2024-11-14 06:43:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183645)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:43:53)


***

### [2024-11-14 06:44:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183646)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:44:12)


***

### [2024-11-14 06:45:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183648)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:45:56)


***

### [2024-11-14 06:46:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183650)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:46:00)


***

### [2024-11-14 06:46:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183652)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:46:08)


***

### [2024-11-14 06:53:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183654)
>LHC RUN CTRL: ACCELERATOR MODE changed to MACHINE DEVELOPMENT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:53:13)


***

### [2024-11-14 06:53:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183655)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 06:53:25)


***

### [2024-11-14 06:53:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183656)
>Event created from ScreenShot Client.  
LHC Sequencer Execution GUI (PRO) 
 : 12.33.11 24-11-14\_06:53.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:53:31)

[ LHC Sequencer Execution GUI (PRO) : 12.33.11  24-11-14_06:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864074/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 06:53:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864074/content)


***

### [2024-11-14 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183662)
>
```
**SHIFT SUMMARY:**  
  
- got the machine in SB  
- checked beam quality in injectors and decided to dump after ~4h in SB to try maximize physics production before MD start  
- indeed, after a bit worse than tested SPS-BQM quality the injection was smooth as tested beforehand  
- smooth cycle as well, allowing to achieve ~2h35min turnaround time and yield integrated lumi as much as in previous fill in ~3.5h of SB  
- dumped at ~6:15 and prepared for MD  
- leaving the machine ready to MD14364 (RF flat bottomo optimization for ion debunching)  
  

```
  
Daniele  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/14 07:06:56)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864077/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/14 07:06:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864077/content)


***

### [2024-11-14 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183662)
>
```
**SHIFT SUMMARY:**  
  
- got the machine in SB  
- checked beam quality in injectors and decided to dump after ~4h in SB to try maximize physics production before MD start  
- indeed, after a bit worse than tested SPS-BQM quality the injection was smooth as tested beforehand  
- smooth cycle as well, allowing to achieve ~2h35min turnaround time and yield integrated lumi as much as in previous fill in ~3.5h of SB  
- dumped at ~6:15 and prepared for MD  
- leaving the machine ready to MD14364 (RF flat bottomo optimization for ion debunching)  
  

```
  
Daniele  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/14 07:06:56)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864077/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/14 07:06:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864077/content)


***

### [2024-11-14 07:00:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183660)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:00:15)


***

### [2024-11-14 07:15:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183665)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:15:20)


***

### [2024-11-14 07:16:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183668)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:16:36)


***

### [2024-11-14 07:18:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183669)
>Calibrating for 50 ns beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 07:18:46)

[ LHC Sequencer Execution GUI (PRO) : 12.33.11  24-11-14_07:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864078/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 07:18:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864078/content)


***

### [2024-11-14 07:20:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183670)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:20:34)


***

### [2024-11-14 07:22:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183672)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:22:04)


***

### [2024-11-14 07:25:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183675)
>
```
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:36:57)

[Module QPS_23:A23.RB.A23.DR2H: (NoName) 24-11-14_07:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864080/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 07:25:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864080/content)

[Circuit_RB_A23:   24-11-14_07:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864082/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 07:25:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864082/content)

[-= DQAmx qps-expert-gui - 1.9.0 =- DQAMCNMB.MB.A12R2 24-11-14_08:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864120/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:34:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864120/content)

[ -= DQAmx qps-expert-gui - 1.9.0 =-  Explorer 24-11-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864124/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:35:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864124/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864130/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:36:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864130/content)


***

### [2024-11-14 07:28:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183677)
>Event created from ScreenShot Client.  
LHC SIS GUI 24-11-14\_07:29.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/14 07:29:01)

[ LHC SIS GUI 24-11-14_07:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864084/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/14 07:29:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864084/content)


***

### [2024-11-14 07:35:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183678)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:35:00)


***

### [2024-11-14 07:48:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183689)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 
 24-11-14\_07:48.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:33:59)

[ LHC Fast BCT v1.3.2 24-11-14_07:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864116/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:34:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864116/content)


***

### [2024-11-14 07:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183679)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 07:51:53)


***

### [2024-11-14 08:01:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183680)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 08:01:56)

[ -= DQAmx qps-expert-gui - 1.9.0 =-  Explorer 24-11-14_08:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864118/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 08:35:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864118/content)


***

### [2024-11-14 08:19:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183682)
>
```
**MD14364**  
Wirescanner measurements; Every 10 minutes for 30 minutes, with Vrf = 6 MV.  

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:49:37)

[ LHC WIRESCANNER APP 24-11-14_08:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864086/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:19:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864086/content)

[ LHC WIRESCANNER APP 24-11-14_08:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864088/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:19:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864088/content)

[ LHC WIRESCANNER APP 24-11-14_08:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864090/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:19:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864090/content)

[ LHC WIRESCANNER APP 24-11-14_08:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864092/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:20:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864092/content)

[ LHC WIRESCANNER APP 24-11-14_08:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864096/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:27:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864096/content)

[ LHC WIRESCANNER APP 24-11-14_08:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864098/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:27:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864098/content)

[ LHC WIRESCANNER APP 24-11-14_08:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864100/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:27:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864100/content)

[ LHC WIRESCANNER APP 24-11-14_08:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864102/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:28:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864102/content)

[ LHC WIRESCANNER APP 24-11-14_08:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864104/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:28:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864104/content)

[ LHC WIRESCANNER APP 24-11-14_08:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864106/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:28:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864106/content)

[ LHC WIRESCANNER APP 24-11-14_08:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864108/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:28:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864108/content)

[ LHC WIRESCANNER APP 24-11-14_08:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864110/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:29:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864110/content)

[ LHC WIRESCANNER APP 24-11-14_08:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864131/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:39:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864131/content)

[ LHC WIRESCANNER APP 24-11-14_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864133/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:39:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864133/content)

[ LHC WIRESCANNER APP 24-11-14_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864135/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:39:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864135/content)

[ LHC WIRESCANNER APP 24-11-14_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864137/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:39:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864137/content)


***

### [2024-11-14 08:22:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183685)
>PZ33 in maintenance mode

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/11/14 08:22:25)


***

### [2024-11-14 08:35:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183690)
>6 MV after 30 minutes

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 08:35:50)

[ LHC Beam Losses Lifetime Display 24-11-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864122/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 08:35:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864122/content)

[ Beam Intensity - v1.5.0 24-11-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864126/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 08:35:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864126/content)


***

### [2024-11-14 08:36:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183691)
>BQM with 6 MV after 30 min

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/14 08:36:23)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-14_08:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864128/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/14 08:36:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864128/content)


***

### [2024-11-14 08:39:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183694)
>Event created from ScreenShot Client.  
LHC Beam Losses Lifetime Display 
 24-11-14\_08:39.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/14 08:39:53)

[ LHC Beam Losses Lifetime Display 24-11-14_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864141/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/14 08:39:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864141/content)


***

### [2024-11-14 08:41:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183695)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 08:41:24)


***

### [2024-11-14 08:43:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183700)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 08:43:05)


***

### [2024-11-14 08:53:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183706)
>
```
**MD14364**  
Wirescanner measurements; Every 10 minutes for 30 minutes, with Vrf = 8 MV.  
  
Beam dump at 9:00, after one set of measurements. New injection B1 at 9:12, B2 at 9:16.  

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:49:23)

[ LHC WIRESCANNER APP 24-11-14_08:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864149/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:54:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864149/content)

[ LHC WIRESCANNER APP 24-11-14_08:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864151/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:54:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864151/content)

[ LHC WIRESCANNER APP 24-11-14_08:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864153/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:54:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864153/content)

[ LHC WIRESCANNER APP 24-11-14_08:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864155/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 08:54:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864155/content)

[ LHC WIRESCANNER APP 24-11-14_09:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864189/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:15:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864189/content)

[ LHC WIRESCANNER APP 24-11-14_09:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864191/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:15:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864191/content)

[ LHC WIRESCANNER APP 24-11-14_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864197/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:18:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864197/content)

[ LHC WIRESCANNER APP 24-11-14_09:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864199/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:19:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864199/content)

[ LHC WIRESCANNER APP 24-11-14_09:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864207/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:23:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864207/content)

[ LHC WIRESCANNER APP 24-11-14_09:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864211/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:24:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864211/content)

[ LHC WIRESCANNER APP 24-11-14_09:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864217/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:28:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864217/content)

[ LHC WIRESCANNER APP 24-11-14_09:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864219/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:28:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864219/content)

[ LHC WIRESCANNER APP 24-11-14_09:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864225/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:33:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864225/content)

[ LHC WIRESCANNER APP 24-11-14_09:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864227/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:34:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864227/content)

[ LHC WIRESCANNER APP 24-11-14_09:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864231/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:37:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864231/content)

[ LHC WIRESCANNER APP 24-11-14_09:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864233/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:37:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864233/content)

[ LHC WIRESCANNER APP 24-11-14_09:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864261/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:47:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864261/content)

[ LHC WIRESCANNER APP 24-11-14_09:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864263/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:47:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864263/content)

[ LHC WIRESCANNER APP 24-11-14_09:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864265/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:47:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864265/content)

[ LHC WIRESCANNER APP 24-11-14_09:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864273/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/11/14 09:48:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864273/content)


***

### [2024-11-14 08:59:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183716)
>Global Post Mortem Event

Event Timestamp: 14/11/24 08:59:18.213
Fill Number: 10363
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 101 / 102 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: A T -> F on CIB.UA27.R2.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/14 09:02:10)


***

### [2024-11-14 09:00:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183713)
>8 MV for 10 minutes

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 09:01:15)

[ LHC Beam Losses Lifetime Display 24-11-14_09:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864169/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 09:01:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864169/content)


***

### [2024-11-14 09:00:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183712)
>PC fault

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 09:01:01)

[Circuit_RSD1_A23B2:   24-11-14_09:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864165/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 09:01:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864165/content)

[2 - RPMBB.UA27.RSD1.A23B2 Power Converter for the Circuit RSD1.A23B2    24-11-14_09:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864167/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 09:01:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864167/content)

[ Beam Intensity - v1.5.0 24-11-14_09:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864173/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 09:01:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864173/content)


***

### [2024-11-14 09:01:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183714)
>8 MV bunch length at dump

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/14 09:01:19)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-14_09:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864171/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/14 09:01:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864171/content)

[ Beam Intensity - v1.5.0 24-11-14_09:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864175/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/14 09:01:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3864175/content)


