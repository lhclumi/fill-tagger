# FILL 9863
**start**:		 2024-07-06 00:49:40.192863525+02:00 (CERN time)

**end**:		 2024-07-06 01:18:34.866738525+02:00 (CERN time)

**duration**:	 0 days 00:28:54.673875


***

### [2024-07-06 00:49:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101149)
>LHC RUN CTRL: New FILL NUMBER set to 9863

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:49:40)


***

### [2024-07-06 00:50:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101155)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:50:47)


***

### [2024-07-06 00:53:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101156)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:53:00)


***

### [2024-07-06 00:53:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101157)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:53:13)


***

### [2024-07-06 00:55:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101160)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:55:30)


***

### [2024-07-06 00:55:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101161)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:55:49)


***

### [2024-07-06 00:56:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101163)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:56:23)


***

### [2024-07-06 00:58:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101164)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:58:45)


***

### [2024-07-06 00:59:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101166)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:59:19)


***

### [2024-07-06 00:59:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101167)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:59:44)


***

### [2024-07-06 00:59:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101169)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 00:59:50)


***

### [2024-07-06 01:00:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101170)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:00:58)


***

### [2024-07-06 01:02:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101172)
>Superlocked RSSs "Unused"

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:02:29)

[1 - CIP.UL16.AR1 Powering interlock controller for the long arc cryostat A12, odd side     24-07-06_01:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665837/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:06:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665837/content)

[1 - CIP.UJ33.AL3 Powering interlock controller for the long arc cryostat A23, odd side     24-07-06_01:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665839/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:03:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665839/content)

[1 - CIP.UJ33.AR3 Powering interlock controller for the long arc cryostat A34, odd side     24-07-06_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665841/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:04:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665841/content)

[1 - CIP.USC55.AL5 Powering interlock controller for the long arc cryostat A45, odd side     24-07-06_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665843/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:04:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665843/content)

[1 - CIP.UL557.AR5 Powering interlock controller for the long arc cryostat A56, odd side     24-07-06_01:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665845/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:05:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665845/content)

[1 - CIP.TZ76.AL7 Powering interlock controller for the long arc cryostat A67, odd side    24-07-06_01:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665847/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:05:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665847/content)

[1 - CIP.UL14.AL1 Powering interlock controller for the long arc cryostat A81, odd side     24-07-06_01:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665849/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:07:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665849/content)

[1 - CIP.TZ76.AR7 Powering interlock controller for the long arc cryostat A78, odd side    24-07-06_01:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665851/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/06 01:07:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3665851/content)


***

### [2024-07-06 01:03:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101173)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:03:01)


***

### [2024-07-06 01:05:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101175)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:05:26)


***

### [2024-07-06 01:05:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101177)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:05:26)


***

### [2024-07-06 01:05:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101179)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:05:28)


***

### [2024-07-06 01:05:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101183)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:05:43)


***

### [2024-07-06 01:07:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101186)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:07:41)


***

### [2024-07-06 01:09:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101192)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:09:25)


***

### [2024-07-06 01:18:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4101198)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/06 01:18:34)


