# FILL 10253
**start**:		 2024-10-19 13:01:18.753363525+02:00 (CERN time)

**end**:		 2024-10-19 20:35:29.013238525+02:00 (CERN time)

**duration**:	 0 days 07:34:10.259875


***

### [2024-10-19 13:01:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167191)
>LHC RUN CTRL: New FILL NUMBER set to 10253

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:01:20)


***

### [2024-10-19 13:01:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167198)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:01:56)


***

### [2024-10-19 13:02:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167199)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:02:35)


***

### [2024-10-19 13:04:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167201)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:04:00)


***

### [2024-10-19 13:04:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167203)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:04:38)


***

### [2024-10-19 13:05:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167205)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:05:25)


***

### [2024-10-19 13:05:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167207)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:05:33)


***

### [2024-10-19 13:06:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167208)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:06:47)


***

### [2024-10-19 13:07:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167210)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:07:29)


***

### [2024-10-19 13:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167213)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:07:59)


***

### [2024-10-19 13:08:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167214)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:08:33)


***

### [2024-10-19 13:09:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167215)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:09:54)


***

### [2024-10-19 13:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167217)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:09:56)


***

### [2024-10-19 13:09:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167219)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:09:58)


***

### [2024-10-19 13:10:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167223)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:10:13)


***

### [2024-10-19 13:10:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167225)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:10:42)


***

### [2024-10-19 13:10:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167227)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:10:49)


***

### [2024-10-19 13:12:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167228)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:12:11)


***

### [2024-10-19 13:13:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167231)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:13:55)


***

### [2024-10-19 13:22:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167235)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 13:22:15)


***

### [2024-10-19 13:52:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167245)
>
```
Arkady found the CMS BCM2 front-end (cfv-3524-cmsbcm2) stuck in a reboot loop. Indeed it is down according to COSMOS since today ~10:00.  
  
Tried a power cycle via Diamon, but the the IPMI does not respond.  
  
Called Stephen Jackson who will check with Arkady and then possibly ring up BE-CEM in case of a hardware issue.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 14:01:48)

[Mozilla Firefox 24-10-19_13:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830592/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 13:53:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830592/content)

[Mozilla Firefox 24-10-19_13:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830594/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 13:53:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830594/content)

[Reboot cfv-3524-cmsbcm2 24-10-19_13:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830596/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 13:53:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830596/content)


***

### [2024-10-19 14:41:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167268)
>BWS B2V OK after InitHw

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/19 14:41:29)

[ LHC WIRESCANNER APP 24-10-19_14:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830643/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/19 14:41:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830643/content)


***

### [2024-10-19 14:48:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167273)
>
```
The investigation by Arkady and Stephen on the BCM2 rack lead to the network switch in the rack to be the most probable cause. It seems to be crashed (all LEDs lit up, even those on disconnected ports), and no port has connectivity.  
  
This leads to the BCM2 FEC not booting due to the missing network connectivity and in turn to the system being unconfigured and emitting a permanent (unmaskable) BIS interlock.  
  
TI checked with IT, Mattia Ponziani had a look. In fact the ownership of the switch is not fully clear, IT says it's an user switch (belonging to BI or CMS), while they don't know it.  
Arkady tried to power-cycle the switch in any case, but it is coming back in the same bad state. Finally IT (Mattia) agreed to loan a switch of the same type to CMS as a courtesy for the time being, he is now going to USC55 to have a look.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 15:39:11)

[screenShot_October_19th_2024_14_48_55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830658/content)
creator:	 mihostet  @194.12.163.134 (2024/10/19 14:48:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830658/content)


***

### [2024-10-19 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167293)
>**\*\*\* SHIFT SUMMARY \*\*\***  
Following another QPS triggered FPA of S23 at 6.2 TeV during the RF test ramp, QPS/MPE decided to swap the new boards in 10R2 by their old counterparts trying to resolve the issue. The intervention was carried out by the MPE piquets (Samer and Lukas).  
However, after pre-cycling and filling 2 pilots for MD13883, the next ramp attempt again got dumped at 6.2 TeV by a QPS FPA in S23.  
The subsequent investigation by Reiner and Jens showed (preliminary):  
- The initial FPA during the ramp-down of yesterday afternoon is understood - it was due to the missing inductive corrections in the new QDS/BS boards.  
- The following trip of RB.A23 when going to standby is also understood - wrong configuration.  
- The dumps at 6.2 TeV were not triggered by the new boards in 10R2 (therefore swapping back did not have an effect), but instead by a misconfiguration of the boards in 11R2; details why this happened and if it is linked to the 10R2 configuration are to be understood.  
In general, this hints that there is no fundamental problem with the new boards (needed for the ion run) but instead there was a configuration issue in the system following the board swap.  
  
In parallel, around ~10:00 a breaker for a CMS interlock rack in USC55 tripped. This fired all CMS interlocks (Totem\_Mov, OP switch, BCM2), but as the BPL was not closed, there was no effect. However after restoring power, the CMS BCM2 system did not come back and it's (non maskable) interlock is permanently firing. The CMS responsible (Arkady) together with BI (Stephen) and IT (Mattia) identified a broken switch in the CMS BCM2 rack. The ownership of the switch is not fully clear, but IT kindly agreed to lend a new switch of the same type to be installed. The intervention is planned for ~16:30 in USC55.  
  
Notes:  
- BWS B2V1 recovered by InitHw command on the FESA class.  
- ALICE dipole PC won't be repaired before Monday (fan replacement needs EPC experts)  
  
-- Michi --  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/19 16:24:41)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830736/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/19 16:24:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830736/content)


***

### [2024-10-19 16:53:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167296)
>CMS switch replaced with the help of IT. Waiting for CMS to confirm everything is good on their side. We still have the BIC interlock

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 16:53:27)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-10-19_16:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830741/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 16:58:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830741/content)


***

### [2024-10-19 17:27:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167299)
>for ref  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/19 17:27:14)


***

### [2024-10-19 17:33:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167300)
>BCM CMS fixed

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 17:33:13)

[BIC Details CIB.USC55.R5.B1 24-10-19_17:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830743/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 17:33:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830743/content)

[BIC Details CIB.USC55.R5.B2 24-10-19_17:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830745/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/19 17:33:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830745/content)


***

### [2024-10-19 17:37:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167301)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:37:46)


***

### [2024-10-19 17:37:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167301)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:37:46)


***

### [2024-10-19 17:51:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167303)
>Event created from ScreenShot Client.  
Desktop 24-10-19\_17:51.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/19 17:51:34)

[Desktop 24-10-19_17:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830747/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/19 17:51:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830747/content)

[Desktop 24-10-19_17:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830749/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/19 17:51:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830749/content)

[Desktop 24-10-19_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830751/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/19 17:52:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830751/content)

[Desktop 24-10-19_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830753/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/19 17:52:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830753/content)


***

### [2024-10-19 17:54:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167304)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:54:55)


***

### [2024-10-19 17:55:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167305)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:55:07)


***

### [2024-10-19 17:55:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167306)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:55:17)


***

### [2024-10-19 17:57:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167307)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:57:19)


***

### [2024-10-19 17:57:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167309)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 17:57:20)


***

### [2024-10-19 18:18:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167312)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 18:18:38)


***

### [2024-10-19 18:18:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167314)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 18:18:47)


***

### [2024-10-19 18:27:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167318)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 18:27:32)


***

### [2024-10-19 18:28:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167320)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 18:28:17)


***

### [2024-10-19 18:28:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167322)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 18:28:59)


***

### [2024-10-19 18:46:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167327)
>Arrived at 60cm with B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:46:18)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-19_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830790/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:46:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830790/content)


***

### [2024-10-19 18:54:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167329)
>Machine is flat

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:54:16)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-19_18:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830794/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:54:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830794/content)


***

### [2024-10-19 18:56:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167331)
>Event created from ScreenShot Client.  
 LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-19\_18:56.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:56:17)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-19_18:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830802/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:56:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830802/content)


***

### [2024-10-19 18:56:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167333)
>Event created from ScreenShot Client.  
Error 24-10-19\_18:56.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:56:59)

[Error 24-10-19_18:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 18:56:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830806/content)


***

### [2024-10-19 19:02:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167335)
>Inverted HV crossings, IP1 +160, IP5 -160

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 19:02:42)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-19_19:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830816/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 19:03:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830816/content)


***

### [2024-10-19 19:20:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167341)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.6.34) 24-10-19\_19:20.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 19:20:48)

[ LSA Applications Suite (v 16.6.34) 24-10-19_19:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830822/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 19:20:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830822/content)


***

### [2024-10-19 19:50:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167343)
>Miracously arrived at 60\_18 !

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 19:50:46)

[Beam Feedbacks - Dashboard - v4.99.0 - BFC.LHC 24-10-19_20:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830826/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/19 20:27:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830826/content)


***

### [2024-10-19 20:23:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167345)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L1 | RPHFC.UL14.RQX.L1 | Sinusoidal | 60 | 1 | 0.0 | COMPLETED |
|  | RPMBC.UL14.RTQX1.L1 | Sinusoidal | 60 | 1 | 0.3923736974420535 | COMPLETED |
|  | RPHGC.UL14.RTQX2.L1 | Sinusoidal | 60 | 1 | 0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/19 20:23:34)

[tmpScreenshot_1729362214.1005864.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830828/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/19 20:23:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3830828/content)


***

### [2024-10-19 20:24:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167349)
>Global Post Mortem Event

Event Timestamp: 19/10/24 20:24:51.475
Fill Number: 10253
Accelerator / beam mode: MACHINE DEVELOPMENT / ADJUST
Energy: 6799440 [MeV]
Intensity B1/B2: 0 / 0 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/19 20:27:41)


***

### [2024-10-19 20:24:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167365)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: acalia / end of MD13883


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/19 20:39:57)


***

### [2024-10-19 20:25:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167347)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:25:00)


***

### [2024-10-19 20:25:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167348)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:25:07)


***

### [2024-10-19 20:35:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167350)
>LHC RUN CTRL: New FILL NUMBER set to 10254

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/19 20:35:29)


