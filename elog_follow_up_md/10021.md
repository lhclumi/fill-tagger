# FILL 10021
**start**:		 2024-08-19 15:49:28.175238525+02:00 (CERN time)

**end**:		 2024-08-19 17:41:08.256613525+02:00 (CERN time)

**duration**:	 0 days 01:51:40.081375


***

### [2024-08-19 15:49:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128218)
>LHC RUN CTRL: New FILL NUMBER set to 10021

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:49:28)


***

### [2024-08-19 15:50:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128224)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:50:10)


***

### [2024-08-19 15:52:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128228)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:52:18)


***

### [2024-08-19 15:52:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128229)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:52:38)


***

### [2024-08-19 15:53:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128236)
>missing signature for lumi scan MCS parameters

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 15:54:25)

[ LHC Sequencer Execution GUI (PRO) : 12.33.0  24-08-19_15:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735267/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 15:54:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735267/content)


***

### [2024-08-19 15:53:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128232)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:53:59)


***

### [2024-08-19 15:54:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128237)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:54:47)


***

### [2024-08-19 15:55:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128238)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:55:02)


***

### [2024-08-19 15:56:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128240)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:56:02)


***

### [2024-08-19 15:58:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128243)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:27)


***

### [2024-08-19 15:58:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128245)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:28)


***

### [2024-08-19 15:58:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128247)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:29)


***

### [2024-08-19 15:58:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128247)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:29)


***

### [2024-08-19 15:58:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128251)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:44)


***

### [2024-08-19 15:58:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128253)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:58:57)


***

### [2024-08-19 15:59:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128255)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:59:04)


***

### [2024-08-19 16:00:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128256)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:00:43)


***

### [2024-08-19 16:02:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128258)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:02:27)


***

### [2024-08-19 16:07:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128260)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:07:45)


***

### [2024-08-19 16:08:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128262)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:08:06)


***

### [2024-08-19 16:08:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128263)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:08:40)


***

### [2024-08-19 16:23:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128275)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:23:26)


***

### [2024-08-19 16:27:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128277)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 16:27:10)


***

### [2024-08-19 16:35:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128286)
>I created TAGs for RF settings with the nominal intensity for NON\_MULTIPLEXEX, DISCRETE\_LHCRING\_RF\_PROTONS, injection and ramp BP. The name of the tag is always the same :  
nominal\_Int\_settings\_bef\_MD\_12743  
  
When the high intensity MDs are over and we are back to nominal bunch intensity, one should reload the TAGs for all the parameters that changed.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 16:37:43)


***

### [2024-08-19 16:56:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128299)
>can't measure coupling B1 as the mode is set to EXPERT

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 16:57:06)

[Accelerator Cockpit v0.0.38 24-08-19_16:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735342/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:03:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735342/content)


***

### [2024-08-19 17:03:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128305)
>Daniel Valuch changed the ADTAcDipole mode for B1H, it was a left over form the previous MD.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:03:55)


***

### [2024-08-19 17:04:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128306)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 17:04:53)


***

### [2024-08-19 17:07:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128312)
>12 bunches trajectories. Sending small correction

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:08:13)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-08-19_17:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735379/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:08:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735379/content)

[OpenYASP V4.9.2   LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-08-19_17:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735381/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:08:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735381/content)

[Desktop 24-08-19_17:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735383/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:08:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735383/content)


***

### [2024-08-19 17:15:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128318)
>  
  
Screenshot attached: Global - 25/06/2024 11:25:38 (B1V)  
  
Collimator used: TCTPV.4L2.B1  
First bottleneck identified: None  
BBA ≅ -1.0 (σ)  
Start time measurement: 06/25/2024 11:20:56  
End time measurement: 06/25/2024 11:25:36  
  
*Sent From pyAperture*

creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/08/19 17:15:21)

[tmpScreenshot_1724080521.6594517.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735400/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/08/19 17:15:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735400/content)


***

### [2024-08-19 17:15:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128320)
>Global Post Mortem Event

Event Timestamp: 19/08/24 17:15:48.677
Fill Number: 10021
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449760 [MeV]
Intensity B1/B2: 176 / 186 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/19 17:18:42)


***

### [2024-08-19 17:19:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128321)
>B2 dumped at injection with BCCM interlock, not clear why...

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:20:28)

[ PM ONLINE PRO GUI : 8.6.3 24-08-19_17:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735406/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:20:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735406/content)


***

### [2024-08-19 17:29:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128322)
>Event created from ScreenShot Client.  
Desktop 24-08-19\_17:29.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:29:57)

[Desktop 24-08-19_17:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735408/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/19 17:29:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735408/content)


***

### [2024-08-19 17:31:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128323)
>Global Post Mortem Event

Event Timestamp: 19/08/24 17:31:29.477
Fill Number: 10021
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 174 / 198 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/19 17:34:23)


***

### [2024-08-19 17:35:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128326)
>Global Post Mortem Event

Event Timestamp: 19/08/24 17:35:58.277
Fill Number: 10021
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449760 [MeV]
Intensity B1/B2: 174 / 0 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 1: B T -> F on CIB.UA47.R4.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/19 17:38:52)


***

### [2024-08-19 17:37:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128324)
>B2 dumped again with BCCM

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:38:04)

[ PM ONLINE PRO GUI : 8.6.3 24-08-19_17:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735410/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:38:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735410/content)


***

### [2024-08-19 17:38:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128325)
>Then when trying with B1 we also have a dump due to BCCM

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:38:37)

[ PM ONLINE PRO GUI : 8.6.3 24-08-19_17:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735412/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 17:38:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735412/content)


***

### [2024-08-19 17:41:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128327)
>LHC RUN CTRL: New FILL NUMBER set to 10022

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 17:41:09)


