# FILL 10203
**start**:		 2024-10-06 18:14:44.044238525+02:00 (CERN time)

**end**:		 2024-10-06 20:10:29.633363525+02:00 (CERN time)

**duration**:	 0 days 01:55:45.589125


***

### [2024-10-06 18:14:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157067)
>LHC RUN CTRL: New FILL NUMBER set to 10203

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:14:45)


***

### [2024-10-06 18:15:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157073)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:15:30)


***

### [2024-10-06 18:17:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157074)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:17:41)


***

### [2024-10-06 18:17:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157075)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:17:44)


***

### [2024-10-06 18:18:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157087)
>following the manual I will try to restart them before calling the piquet 
 (only 600A ones. )

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:19:25)

[Desktop 24-10-06_18:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805409/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:19:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805409/content)


***

### [2024-10-06 18:19:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157084)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:19:06)


***

### [2024-10-06 18:19:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157085)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:19:06)


***

### [2024-10-06 18:20:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157088)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:20:12)


***

### [2024-10-06 18:20:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157089)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:20:21)


***

### [2024-10-06 18:21:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157091)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:21:10)


***

### [2024-10-06 18:21:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157094)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:21:33)


***

### [2024-10-06 18:22:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157095)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:22:07)


***

### [2024-10-06 18:23:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157096)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:23:35)


***

### [2024-10-06 18:23:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157098)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:23:37)


***

### [2024-10-06 18:23:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157100)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:23:39)


***

### [2024-10-06 18:23:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157104)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:23:54)


***

### [2024-10-06 18:24:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157106)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:24:16)


***

### [2024-10-06 18:24:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157108)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:24:23)


***

### [2024-10-06 18:25:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157109)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:25:53)


***

### [2024-10-06 18:25:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157109)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:25:53)


***

### [2024-10-06 18:27:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157112)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:27:37)


***

### [2024-10-06 18:31:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157120)
>Not able to reset the MOs. I will try to power cycle them.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:31:46)

[Desktop 24-10-06_18:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805413/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:31:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805413/content)


***

### [2024-10-06 18:32:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157121)
>Event created from ScreenShot Client.  
qps-lhc-swisstool 24-10-06\_18:32.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:33:57)

[qps-lhc-swisstool 24-10-06_18:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805415/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:33:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805415/content)


***

### [2024-10-06 18:34:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157122)
>power cycle seems to have helped.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:34:20)

[Desktop 24-10-06_18:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805417/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:34:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805417/content)


***

### [2024-10-06 18:42:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157124)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:42:01)


***

### [2024-10-06 18:48:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157132)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:48:58)


***

### [2024-10-06 19:01:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157136)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 19:01:38)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-06_19:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805433/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 19:01:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805433/content)

[Accelerator Cockpit v0.0.38 24-10-06_19:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805435/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 19:01:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805435/content)

[Accelerator Cockpit v0.0.38 24-10-06_19:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805437/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 19:02:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805437/content)


***

### [2024-10-06 19:04:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157137)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:04:13)


***

### [2024-10-06 19:38:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157144)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:38:29)


***

### [2024-10-06 19:38:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157145)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:38:39)


***

### [2024-10-06 19:38:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157146)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:38:48)


***

### [2024-10-06 19:38:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157147)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:34:37 / 0:33:48 (102.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 19:38:49)


***

### [2024-10-06 19:40:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157148)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:40:34)


***

### [2024-10-06 19:40:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157150)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 19:40:35)


***

### [2024-10-06 19:40:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157151)
>Beginning of ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:40:44)

[ LHC Fast BCT v1.3.2 24-10-06_19:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805457/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:40:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805457/content)


***

### [2024-10-06 19:41:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157156)
>Global Post Mortem Event

Event Timestamp: 06/10/24 19:41:45.607
Fill Number: 10203
Accelerator / beam mode: PROTON PHYSICS / RAMP
Energy: 497640 [MeV]
Intensity B1/B2: 38301 / 38357 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.UA63.L6.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/06 19:44:35)


***

### [2024-10-06 19:41:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157212)
>Global Post Mortem Event Confirmation

Dump Classification: QPS trigger
Operator / Comment: tpersson / QPS trigger,this time also in sector56 but not the same crate. The QPS experts thinks it is a coincident and that we can continue.


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/06 20:57:52)


***

### [2024-10-06 19:42:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157153)
>These ones tripped again. Will call the piquet this time.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/06 19:42:35)

[Set SECTOR56 24-10-06_19:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805459/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/06 19:42:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805459/content)


***

### [2024-10-06 19:48:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157157)
>This time it was only the RSD1 and RSF but on the other hand this really 
 looks like a communication issue to me.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:48:59)

[Circuit_RSD1_A56B2:   24-10-06_19:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805463/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:48:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805463/content)


***

### [2024-10-06 19:50:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157158)
>Strange also that this one didn't open the switches.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:50:53)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-10-06_19:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805465/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 19:50:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805465/content)


***

### [2024-10-06 19:56:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157162)
>Event created from ScreenShot Client.  
LHC Multiturn 5.6.0 24-10-06\_19:56.png

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/10/06 19:56:21)

[LHC Multiturn 5.6.0 24-10-06_19:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805467/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/10/06 19:56:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805467/content)


***

### [2024-10-06 20:02:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157164)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:02:26)


***

### [2024-10-06 20:07:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157165)
>Event created from ScreenShot Client.  
Circuit\_RSD1\_A56B2: 24-10-06\_20:07.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:07:50)

[Circuit_RSD1_A56B2:   24-10-06_20:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805471/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:07:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805471/content)


***

### [2024-10-06 20:08:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157166)
>MATHIEU FAVRE(MFAVRE) assigned RBAC Role: QPS-Piquet and will expire on: 07-OCT-24 02.08.42.815000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/06 20:08:49)


