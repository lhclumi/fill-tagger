# FILL 10371
**start**:		 2024-11-14 14:45:43.488363525+01:00 (CERN time)

**end**:		 2024-11-14 16:32:14.843863525+01:00 (CERN time)

**duration**:	 0 days 01:46:31.355500


***

### [2024-11-14 14:45:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4183995)
>LHC RUN CTRL: New FILL NUMBER set to 10371

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:45:45)


***

### [2024-11-14 14:48:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184003)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:48:45)


***

### [2024-11-14 14:49:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184005)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:49:00)


***

### [2024-11-14 14:51:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184008)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:51:15)


***

### [2024-11-14 14:51:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184010)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_50NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:51:20)


***

### [2024-11-14 14:52:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184014)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:52:38)


***

### [2024-11-14 14:53:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184015)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:53:25)


***

### [2024-11-14 14:55:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184017)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:55:12)


***

### [2024-11-14 14:55:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184020)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:55:19)


***

### [2024-11-14 14:55:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184021)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:55:50)


***

### [2024-11-14 14:55:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184023)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:55:52)


***

### [2024-11-14 14:55:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184025)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:55:55)


***

### [2024-11-14 14:56:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184029)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:56:10)


***

### [2024-11-14 14:58:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184033)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:58:07)


***

### [2024-11-14 14:59:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184035)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 14:59:51)


***

### [2024-11-14 15:09:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184042)
>RF loop stability check is failing

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 15:10:04)

[ LHC Sequencer Execution GUI (PRO) : 12.33.11  24-11-14_15:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3865086/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 15:10:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3865086/content)


***

### [2024-11-14 15:18:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184049)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 15:18:07)


***

### [2024-11-14 15:46:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184066)
>Andy will go to SR4 to fix the RF loops stability issue

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 15:46:53)


***

### [2024-11-14 16:06:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184080)
>Switched RB.A23 OFF at the request of Hugues

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 16:06:32)

[ Equip State 24-11-14_16:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3865236/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/14 16:06:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3865236/content)


***

### [2024-11-14 16:24:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184092)
>NIKOLAI VASILEV BEEV(NBEEV) assigned RBAC Role: PO-LHC-Piquet and will expire on: 14-NOV-24 11.24.11.357000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/14 16:24:14)


***

### [2024-11-14 16:30:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184102)
>RF loops VCO voltage adjusted to correct value.  
Andy  


creator:	 butterwo  @cwe-sr4-acscr1.cern.ch (2024/11/14 16:42:34)


***

### [2024-11-14 16:32:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4184093)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/14 16:32:12)


