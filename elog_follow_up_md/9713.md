# FILL 9713
**start**:		 2024-06-05 17:25:11.707488525+02:00 (CERN time)

**end**:		 2024-06-05 18:05:16.666863525+02:00 (CERN time)

**duration**:	 0 days 00:40:04.959375


***

### [2024-06-05 17:25:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081834)
>LHC RUN CTRL: New FILL NUMBER set to 9713

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:25:11)


***

### [2024-06-05 17:26:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081840)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:26:00)


***

### [2024-06-05 17:27:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081841)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:27:53)


***

### [2024-06-05 17:28:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081842)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:28:19)


***

### [2024-06-05 17:29:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081844)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:29:40)


***

### [2024-06-05 17:30:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081846)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:30:06)


***

### [2024-06-05 17:30:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081847)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:30:23)


***

### [2024-06-05 17:30:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081848)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: ERRORNumber of failed crates: 1 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:30:55)


***

### [2024-06-05 17:31:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081850)
>LHC SEQ: BctDc210 tests finished. Overall result: ERRORFifo Average Test FAILED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:31:19)


***

### [2024-06-05 17:31:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081850)
>LHC SEQ: BctDc210 tests finished. Overall result: ERRORFifo Average Test FAILED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:31:19)


***

### [2024-06-05 17:32:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081855)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:32:29)


***

### [2024-06-05 17:33:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081856)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:33:03)


***

### [2024-06-05 17:33:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081857)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:33:20)


***

### [2024-06-05 17:34:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081859)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:34:38)


***

### [2024-06-05 17:36:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081863)
>BLM connectivity test failed for 1 sector, but relaunching it got it through  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 17:37:02)


***

### [2024-06-05 17:36:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081861)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:36:51)


***

### [2024-06-05 17:38:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081864)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:38:54)


***

### [2024-06-05 17:41:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081867)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:00)


***

### [2024-06-05 17:41:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081869)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:07)


***

### [2024-06-05 17:41:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081870)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:20)


***

### [2024-06-05 17:41:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081872)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:21)


***

### [2024-06-05 17:41:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081874)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:22)


***

### [2024-06-05 17:41:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081879)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:41:37)


***

### [2024-06-05 17:43:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081882)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:43:35)


***

### [2024-06-05 17:45:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081884)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:45:19)


***

### [2024-06-05 17:45:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081886)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:45:45)


***

### [2024-06-05 17:50:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081887)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 17:50:13)


***

### [2024-06-05 17:51:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081888)
>we skipped the QPS reset  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/05 17:51:54)


***

### [2024-06-05 17:57:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081890)
>Event created from ScreenShot Client.  
RF Trim Warning 24-06-05\_17:57.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 17:57:47)

[RF Trim Warning 24-06-05_17:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626610/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 17:57:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626610/content)


***

### [2024-06-05 17:58:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081891)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-05\_17:58.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 17:58:45)

[Accelerator Cockpit v0.0.37 24-06-05_17:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626612/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 17:58:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626612/content)

[Accelerator Cockpit v0.0.37 24-06-05_17:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626614/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 17:59:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626614/content)


***

### [2024-06-05 18:00:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081893)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-05\_18:00.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 18:00:32)

[Accelerator Cockpit v0.0.37 24-06-05_18:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626616/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 18:00:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626616/content)


***

### [2024-06-05 18:01:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081894)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-05\_18:01.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 18:01:17)

[Accelerator Cockpit v0.0.37 24-06-05_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626618/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/05 18:01:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3626618/content)


***

### [2024-06-05 18:02:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081895)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 18:02:01)


***

### [2024-06-05 18:05:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4081896)
>LHC RUN CTRL: New FILL NUMBER set to 9714

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/05 18:05:16)


