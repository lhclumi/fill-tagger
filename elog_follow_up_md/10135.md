# FILL 10135
**start**:		 2024-09-21 17:32:25.819363525+02:00 (CERN time)

**end**:		 2024-09-22 06:35:24.716863525+02:00 (CERN time)

**duration**:	 0 days 13:02:58.897500


***

### [2024-09-21 17:32:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147878)
>LHC RUN CTRL: New FILL NUMBER set to 10135

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:32:26)


***

### [2024-09-21 17:33:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147884)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:33:24)


***

### [2024-09-21 17:35:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147888)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:35:34)


***

### [2024-09-21 17:36:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147889)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:36:05)


***

### [2024-09-21 17:37:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147891)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:37:14)


***

### [2024-09-21 17:38:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147892)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:38:04)


***

### [2024-09-21 17:38:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147893)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:38:17)


***

### [2024-09-21 17:38:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147895)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:38:23)


***

### [2024-09-21 17:39:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147897)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:39:39)


***

### [2024-09-21 17:40:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147899)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:40:13)


***

### [2024-09-21 17:40:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147900)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:40:26)


***

### [2024-09-21 17:42:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147902)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:42:13)


***

### [2024-09-21 17:42:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147904)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:42:20)


***

### [2024-09-21 17:42:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147907)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:42:51)


***

### [2024-09-21 17:42:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147905)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:42:51)


***

### [2024-09-21 17:42:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147909)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:42:53)


***

### [2024-09-21 17:43:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147913)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:43:07)


***

### [2024-09-21 17:45:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147915)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:45:06)


***

### [2024-09-21 17:46:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147917)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 17:46:50)


***

### [2024-09-21 18:11:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147921)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 18:11:11)


***

### [2024-09-21 22:41:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147943)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 22:41:42)


***

### [2024-09-21 22:49:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147950)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/21 22:49:06)


***

### [2024-09-21 22:49:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147951)
>This one is flickering.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/21 22:50:24)

[Circuit_RCBXH2_R8:   24-09-21_22:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782456/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/21 22:50:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782456/content)


***

### [2024-09-21 22:50:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147986)
>Event created from ScreenShot Client.  
8 - RCBXH2.R8 DQAMG N type A for 
 circuit RCBXH2.R8 24-09-21\_22:50.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:13:25)

[8 - RCBXH2.R8 DQAMG N type A for circuit RCBXH2.R8    24-09-21_22:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782510/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:13:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782510/content)


***

### [2024-09-21 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147958)
>
```
**SHIFT SUMMARY:**  
  
-Arrived in stable beams. Dumped at 17.30. Access started for cryo just before 18.00.   
-The exchange of the pump was quick but then they got issues with some reading. They had to change a power supply to get it working. After that there was some temperature issue that had to be solved. They were opening and closing some vales and in the end the situation stabilized and we could close the machine around 22.35.  
-Forced the RCBXH2.R8 to card B  
-Issue injecting beam 1, seems to be an issue with the FMCM in SPS  
  
  
* To be noted:

  
  
0.9 inverse femotobarn left to reach the target for the year!   
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/21 23:35:08)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782471/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/21 23:19:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782471/content)


***

### [2024-09-21 23:37:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147964)
>
```
FMCM_RBIH.29314 seems to be preventing injection on beam 1. First Line and BIC expert called in SPS
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/21 23:39:55)

[(null) 24-09-21_23:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782482/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/21 23:37:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782482/content)


***

### [2024-09-22 00:23:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147973)
>masking that interlock (#12) in SPS BIS, allowed to confirm that the issue is coming from RBIH.29314  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 00:24:26)


***

### [2024-09-22 01:11:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147982)
>MKI2\_VAC\_INTERCONNECT came back

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 01:12:01)

[ LHC SIS GUI 24-09-22_01:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782508/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 01:12:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782508/content)

[History: MKI2 24-09-22_01:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782512/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:22:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782512/content)


***

### [2024-09-22 01:14:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147988)
>test with FMCM masked in SPS BIS

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 01:15:31)

[BIC Details 24-09-22_01:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782520/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 01:15:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782520/content)


***

### [2024-09-22 01:17:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147989)
>VGPB.176.5L2.B increased since midnight

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:17:45)

[History: MKI2 24-09-22_01:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782524/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:17:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782524/content)


***

### [2024-09-22 01:18:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147990)
>Event created from ScreenShot Client.  
History: MKI2 24-09-22\_01:18.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:18:20)

[History: MKI2 24-09-22_01:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782526/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:18:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782526/content)


***

### [2024-09-22 01:22:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147992)
>history of vacuum pressures

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:22:58)

[History: MKI2 24-09-22_01:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782528/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:23:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782528/content)


***

### [2024-09-22 01:23:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4147998)
>
```
calling ABT piquet to inform them of the pressure excursion. Number 72010 rings but noone picks up. I used the edms list and got the piquet on the phone right away. He advised me to call Giorgia, which I did. She will have a look.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 01:30:07)


***

### [2024-09-22 02:01:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148001)
>masked the interlock following discussion with Giorgia. If we see that the 
 vacuum signal goes down below the threshold, we should unmask the interlock

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:02:20)

[ LHC SIS GUI 24-09-22_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782544/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:02:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782544/content)


***

### [2024-09-22 02:04:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148002)
>issue with SPS FMCM seems solved now

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:04:41)

[BIC Details 24-09-22_02:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782546/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:04:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782546/content)


***

### [2024-09-22 02:14:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148004)
>that error persists

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:14:18)

[(null) 24-09-22_02:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782550/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/22 02:14:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782550/content)


***

### [2024-09-22 02:21:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148007)
>corrections on pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:21:19)

[RF Trim Warning 24-09-22_02:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782556/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:21:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782556/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782558/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:22:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782558/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782560/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:22:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782560/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782562/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:22:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782562/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782564/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:23:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782564/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782566/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:24:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782566/content)

[Accelerator Cockpit v0.0.38 24-09-22_02:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782570/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:25:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782570/content)


***

### [2024-09-22 02:29:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148008)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/22 02:29:50)


***

### [2024-09-22 02:32:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148009)
>Event created from ScreenShot Client.  
OpenYASP V4.9.2 LHCB1Transfer . 
 LHC\_3inj\_Nom\_48b\_Q20\_2024\_V1 . SPS.USER.LHC1 24-09-22\_02:32.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:32:39)

[OpenYASP V4.9.2   LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-09-22_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782572/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:32:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782572/content)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-09-22_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782574/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:32:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782574/content)

[Send HW 24-09-22_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782576/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:32:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782576/content)

[Confirmation required 24-09-22_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782578/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/22 02:32:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782578/content)


***

### [2024-09-22 02:36:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148010)
>Event created from ScreenShot Client.  
OpenYASP DV LHCB1Transfer . 
 LHC\_3inj\_BCMS\_Q20\_2024\_V1 24-09-22\_02:37.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:37:05)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-09-22_02:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782580/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/22 02:37:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782580/content)


***

### [2024-09-22 02:38:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148011)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:02)

[tmpScreenshot_1726965482.7088974.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782582/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782582/content)


***

### [2024-09-22 02:38:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148012)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:05)

[tmpScreenshot_1726965485.7488894.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782584/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782584/content)


***

### [2024-09-22 02:38:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148013)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:09)

[tmpScreenshot_1726965489.4288712.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782586/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782586/content)


***

### [2024-09-22 02:38:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148014)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:14)

[tmpScreenshot_1726965494.389189.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782588/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/22 02:38:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782588/content)


***

### [2024-09-22 02:52:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148016)
>Global Post Mortem Event

Event Timestamp: 22/09/24 02:52:54.595
Fill Number: 10135
Accelerator / beam mode: PROTON PHYSICS / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 20176 / 21355 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.US15.L1.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/22 02:55:46)


***

### [2024-09-22 02:52:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148113)
>Global Post Mortem Event Confirmation

Dump Classification: Failure in Magnet Powering System
Operator / Comment: mihostet / RB.A81 EE problem (power supply in RR13)


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/22 07:14:40)


***

### [2024-09-22 02:53:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148017)
>Event created from ScreenShot Client.  
Set SECTOR81 24-09-22\_02:53.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/22 02:58:31)

[Set SECTOR81 24-09-22_02:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782592/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/22 02:58:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782592/content)


***

### [2024-09-22 02:55:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148015)
>seems QPS related

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 02:55:41)

[History Buffer 24-09-22_02:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782590/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:03:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782590/content)


***

### [2024-09-22 02:55:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148015)
>seems QPS related

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 02:55:41)

[History Buffer 24-09-22_02:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782590/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:03:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782590/content)


***

### [2024-09-22 02:59:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148018)
>Event created from ScreenShot Client.  
Equip State 24-09-22\_02:59.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:03:40)

[ Equip State 24-09-22_02:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782594/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:03:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782594/content)


***

### [2024-09-22 03:03:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148019)
>Event created from ScreenShot Client.  
Circuit\_RB\_A81: 24-09-22\_03:03.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:03:48)

[Circuit_RB_A81:   24-09-22_03:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782596/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:07:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782596/content)


***

### [2024-09-22 03:10:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148021)
>Event created from ScreenShot Client.  
Mozilla Firefox 24-09-22\_03:10.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:10:35)

[Mozilla Firefox 24-09-22_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782598/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:14:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782598/content)


***

### [2024-09-22 03:14:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148022)
>called QPS piquet as FAST POWER ABORT  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:14:20)


***

### [2024-09-22 03:14:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148023)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/22 03:14:48)


***

### [2024-09-22 03:14:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148024)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/22 03:14:59)


***

### [2024-09-22 03:27:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148025)
>Event created from ScreenShot Client.  
Circuit\_RB\_A81: 24-09-22\_03:27.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:27:20)

[Circuit_RB_A81:   24-09-22_03:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782600/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:28:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782600/content)

[1 - UA87.RB.A81 DQAMS N type RB 13KA for circuit UA87.RB.A81    24-09-22_03:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782602/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:27:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782602/content)

[1 - RR13.RB.A81 DQAMS N type RB 13KA for circuit RR13.RB.A81    24-09-22_03:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782604/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:27:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782604/content)


***

### [2024-09-22 03:29:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148026)
>Event created from ScreenShot Client.  
pm-beam-loss-evaluation >> Version: 1.0.6 Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-09-22\_03:29.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:29:04)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-09-22_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782606/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:29:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782606/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-09-22_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782608/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:29:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782608/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-09-22_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782610/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:29:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782610/content)


***

### [2024-09-22 03:35:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148027)
>JENS STECKERT(JSTECKER) assigned RBAC Role: QPS-Piquet and will expire on: 22-SEP-24 05.35.37.925000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/09/22 03:35:41)


***

### [2024-09-22 03:49:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148029)
>Event created from ScreenShot Client.  
 PM POWERING PLAYBACK PRO GUI : 8.5.4 24-09-22\_03:49.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:49:34)

[ PM POWERING PLAYBACK PRO GUI : 8.5.4 24-09-22_03:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782620/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:49:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782620/content)

[qps-13kA >> Version: 5.1.11  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-09-22_03:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782622/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:49:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782622/content)


***

### [2024-09-22 03:56:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148030)
>Event created from ScreenShot Client.  
Circuit\_RB\_A81: 24-09-22\_03:56.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 03:56:13)

[Circuit_RB_A81:   24-09-22_03:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782624/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:54:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782624/content)

[Set SECTOR81 24-09-22_04:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782628/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/22 04:16:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782628/content)


***

### [2024-09-22 04:25:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148039)
>Event created from ScreenShot Client.  
 Equip State 24-09-22\_04:25.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:25:46)

[ Equip State 24-09-22_04:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782631/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:28:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782631/content)


***

### [2024-09-22 04:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148040)
>Event created from ScreenShot Client.  
 Equip State 24-09-22\_04:26.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:26:30)

[ Equip State 24-09-22_04:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782633/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:26:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782633/content)


***

### [2024-09-22 04:31:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148041)
>Event created from ScreenShot Client.  
 Equip State 24-09-22\_04:31.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:31:55)

[ Equip State 24-09-22_04:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782635/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:31:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782635/content)


***

### [2024-09-22 04:37:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148042)
>Fast abort still there on RBs in PIC, despite resets of power converter and signal inits  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:38:50)

[1 - CIP.UA87.AR8 Powering interlock controller for the long arc cryostat A81, even side     24-09-22_04:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782637/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:37:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782637/content)

[1 - CIP.UL14.AL1 Powering interlock controller for the long arc cryostat A81, odd side     24-09-22_04:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782639/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:37:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782639/content)


***

### [2024-09-22 04:49:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148046)
>
```
The QPS piquet saw that there is a possible issue with the energy extraction, which prevents him from closing the loop and he suggested to call Bozhidar  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 05:03:35)


***

### [2024-09-22 04:51:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148043)
>BOZHIDAR IVANOV PANEV(PANEV) assigned RBAC Role: QPS-Piquet and will expire on: 22-SEP-24 06.51.51.477000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/09/22 04:51:53)


***

### [2024-09-22 04:59:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148044)
>
```
Matteo and Bozhidar confirm that there is an issue with energy extraction. Bozhidar said that there is likely no power to DJBC1, and that one would need to access if the remote reset does not work.   
  
sending all the machine to standby in view of possible access  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 05:05:00)

[ LHC Sequencer Execution GUI (PRO) : 12.33.2  24-09-22_04:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782641/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 04:59:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782641/content)


***

### [2024-09-22 05:08:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148047)
>
```
calling fire brigade to get RP for the access in RR13  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 05:12:41)


***

### [2024-09-22 05:26:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148052)
>UL14 VACUUM VALVES CLOSED FOR ACCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/22 05:26:02)


***

### [2024-09-22 06:11:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148059)
>
```
access started in PM15 (Bozhidar, QPS piquet and RP)  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:22:38)


***

### [2024-09-22 06:27:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148064)
>Bozhidar called back. they replaced a module and now the loop can be closed. Switches are closed for RBs. They are exiting  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:28:18)


***

### [2024-09-22 06:32:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148065)
>Event created from ScreenShot Client.  
Circuit\_RB\_A81: 24-09-22\_06:32.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:32:40)

[Circuit_RB_A81:   24-09-22_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782692/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:32:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782692/content)

[Circuit_RQF_A81:   24-09-22_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782694/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:32:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782694/content)

[Circuit_RQD_A81:   24-09-22_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782696/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/22 06:32:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3782696/content)


***

### [2024-09-22 06:35:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4148066)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/22 06:35:20)


