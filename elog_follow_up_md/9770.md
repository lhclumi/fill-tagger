# FILL 9770
**start**:		 2024-06-09 19:21:50.297863525+02:00 (CERN time)

**end**:		 2024-06-09 19:42:25.827988525+02:00 (CERN time)

**duration**:	 0 days 00:20:35.530125


***

### [2024-06-09 19:21:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084831)
>LHC RUN CTRL: New FILL NUMBER set to 9770

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 19:21:50)


***

### [2024-06-09 19:22:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084832)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 19:22:20)


***

### [2024-06-09 19:25:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084833)
>re-correcting

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:25:24)

[Accelerator Cockpit v0.0.37 24-06-09_19:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632073/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:25:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632073/content)

[Accelerator Cockpit v0.0.37 24-06-09_19:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632075/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:26:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632075/content)

[Accelerator Cockpit v0.0.37 24-06-09_19:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632077/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:26:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632077/content)


***

### [2024-06-09 19:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084834)
>moving to MD working point (fill 17/18)

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:26:48)

[Accelerator Cockpit v0.0.37 24-06-09_19:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632079/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:26:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632079/content)

[Accelerator Cockpit v0.0.37 24-06-09_19:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632081/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:27:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632081/content)

[ LSA Applications Suite (v 16.6.1) 24-06-09_19:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632083/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:27:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632083/content)


***

### [2024-06-09 19:28:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084835)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 19:28:03)


***

### [2024-06-09 19:29:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084836)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:29:18)

[tmpScreenshot_1717954158.1339993.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632085/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:29:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632085/content)


***

### [2024-06-09 19:29:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084837)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:29:48)

[tmpScreenshot_1717954188.1829615.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632087/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:29:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632087/content)


***

### [2024-06-09 19:30:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084838)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:30:12)

[tmpScreenshot_1717954212.3999524.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632089/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:30:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632089/content)


***

### [2024-06-09 19:30:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084839)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:30:31)

[tmpScreenshot_1717954231.3913777.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632091/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:30:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632091/content)


***

### [2024-06-09 19:38:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084841)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:38:44)

[tmpScreenshot_1717954723.9954224.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632097/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:38:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632097/content)


***

### [2024-06-09 19:39:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084842)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:04)

[tmpScreenshot_1717954743.8414452.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632099/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632099/content)


***

### [2024-06-09 19:39:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084843)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:21)

[tmpScreenshot_1717954761.101029.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632101/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632101/content)


***

### [2024-06-09 19:39:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084844)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:37)

[tmpScreenshot_1717954777.5370288.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632103/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 19:39:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632103/content)


***

### [2024-06-09 19:40:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084845)
>MO = 0

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/09 19:40:30)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-06-09_19:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632105/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/09 19:40:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632105/content)


***

### [2024-06-09 19:41:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084846)
>Q' measured

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:41:07)

[Accelerator Cockpit v0.0.37 24-06-09_19:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632107/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:41:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632107/content)

[Accelerator Cockpit v0.0.37 24-06-09_19:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632109/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 19:41:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632109/content)


***

### [2024-06-09 19:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084847)
>LHC RUN CTRL: New FILL NUMBER set to 9771

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 19:42:25)


