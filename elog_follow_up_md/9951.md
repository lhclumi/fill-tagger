# FILL 9951
**start**:		 2024-07-30 20:46:02.397488525+02:00 (CERN time)

**end**:		 2024-07-31 11:33:26.840613525+02:00 (CERN time)

**duration**:	 0 days 14:47:24.443125


***

### [2024-07-30 20:46:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116075)
>LHC RUN CTRL: New FILL NUMBER set to 9951

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 20:46:03)


***

### [2024-07-30 20:46:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116077)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 20:46:46)


***

### [2024-07-30 20:49:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116080)
>HELGA TIMKO(HTIMKO) assigned RBAC Role: RF-LHC-Piquet and will expire on: 30-JUL-24 10.49.06.120000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/07/30 20:49:07)


***

### [2024-07-30 20:56:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116083)
>
```
Helga recalibrated the tuner motor steps for cavities 5B1 and 6B1 via the LLRF control.   
It might even have fixed the problem.   
For now, if it works, we can stay with the partitioning though and further checks will be performed tomorrow morning.
```
  


creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/07/30 21:34:44)

[Screenshot 2024-07-30 at 21.10.24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705320/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/07/30 21:35:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705320/content)

[Screenshot 2024-07-30 at 21.10.05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705322/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/07/30 21:35:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705322/content)

[Screenshot 2024-07-30 at 21.02.12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705324/content)
creator:	 dmirarch  @cwe-513-vml006.cern.ch (2024/07/30 21:35:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705324/content)


***

### [2024-07-30 20:57:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116084)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 20:57:47)


***

### [2024-07-30 20:59:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116085)
>restarting

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 20:59:34)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-30_20:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705250/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 20:59:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705250/content)

[Accelerator Cockpit v0.0.37 24-07-30_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705252/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:00:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705252/content)

[Accelerator Cockpit v0.0.37 24-07-30_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705254/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:00:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705254/content)

[Accelerator Cockpit v0.0.37 24-07-30_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705256/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:00:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705256/content)

[Accelerator Cockpit v0.0.37 24-07-30_21:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705258/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:01:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705258/content)

[Accelerator Cockpit v0.0.37 24-07-30_21:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705260/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:01:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705260/content)


***

### [2024-07-30 21:03:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116087)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:03:22)


***

### [2024-07-30 21:03:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116088)
>12b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:03:59)

[OpenYASP DV LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-07-30_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705262/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:03:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705262/content)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-07-30_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705264/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:04:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705264/content)


***

### [2024-07-30 21:05:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116090)
>il looks like we've a not negligible energy drift

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:05:25)

[Desktop 24-07-30_21:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705266/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:05:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705266/content)


***

### [2024-07-30 21:08:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116091)
>72b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:08:50)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705268/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:08:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705268/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705272/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:09:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705272/content)


***

### [2024-07-30 21:09:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116092)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:09:11)

[tmpScreenshot_1722366550.989962.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705270/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:09:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705270/content)


***

### [2024-07-30 21:09:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116093)
>little steering B2h

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:09:57)

[Desktop 24-07-30_21:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705274/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:09:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705274/content)

[Confirmation required 24-07-30_21:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705276/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/30 21:10:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705276/content)


***

### [2024-07-30 21:10:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116094)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:20)

[tmpScreenshot_1722366620.3346927.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705278/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705278/content)


***

### [2024-07-30 21:10:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116095)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:27)

[tmpScreenshot_1722366627.110814.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705280/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705280/content)


***

### [2024-07-30 21:10:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116096)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:35)

[tmpScreenshot_1722366635.4630926.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705282/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/30 21:10:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705282/content)


***

### [2024-07-30 21:11:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116097)
>108b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:11:44)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705284/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:11:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705284/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705286/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:12:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705286/content)


***

### [2024-07-30 21:12:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116098)
>at limit of energy matching

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:12:44)

[Desktop 24-07-30_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705288/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:12:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705288/content)


***

### [2024-07-30 21:14:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116099)
>something not ideal on beam arriving - TL are good but larger emittance 
 than usual and not uniform

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:15:20)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-30_21:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705292/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:27:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705292/content)


***

### [2024-07-30 21:28:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116102)
>still significant structure in injected beam (middle batch with slightly 
 larger emittance) - SPS confirms they get not stable beams as well

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:30:13)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-30_21:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705306/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:30:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705306/content)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705310/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:30:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705310/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-30_21:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705312/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/30 21:30:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705312/content)


***

### [2024-07-30 21:50:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116109)
>injection completed, quiet painful and worse beam quality than usual

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:51:14)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-30_21:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705338/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:51:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705338/content)

[ LHC Fast BCT v1.3.2 24-07-30_21:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705340/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:51:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705340/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-30_21:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705342/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/30 21:51:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705342/content)


***

### [2024-07-30 21:51:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116108)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:51:12)


***

### [2024-07-30 21:51:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116110)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:51:24)


***

### [2024-07-30 21:51:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116111)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:51:28)


***

### [2024-07-30 21:51:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116112)
>LHC Injection Complete

Number of injections actual / planned: 62 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:48:07 / 0:33:48 (142.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/30 21:51:29)


***

### [2024-07-30 21:53:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116116)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:53:20)


***

### [2024-07-30 21:53:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116119)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 21:53:21)


***

### [2024-07-30 21:53:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116121)
>46% max loss/thl at start of ramp (looks like FBCT needs a new calibration 
 again)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:54:42)

[ LHC BLM Fixed Display 24-07-30_21:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705346/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 21:53:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705346/content)

[ Beam Intensity - v1.4.0 24-07-30_21:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705348/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 21:54:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705348/content)


***

### [2024-07-30 22:14:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116133)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:14:41)


***

### [2024-07-30 22:14:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116135)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:14:46)


***

### [2024-07-30 22:15:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116138)
>cleaning AGAP at FT

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/30 22:15:28)

[Abort gap cleaning control v2.1.0 24-07-30_22:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705382/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/30 22:15:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705382/content)


***

### [2024-07-30 22:21:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116146)
>ramp

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 22:21:55)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-30_22:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705398/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 22:21:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705398/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-30_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705400/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 22:22:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705400/content)

[ LHC Beam Losses Lifetime Display 24-07-30_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705402/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/30 22:22:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705402/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-30_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705404/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/30 22:22:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705404/content)


***

### [2024-07-30 22:24:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116150)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:24:12)


***

### [2024-07-30 22:24:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116154)
>before collisions

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:24:54)

[ LHC Fast BCT v1.3.2 24-07-30_22:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705420/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:24:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705420/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-30_22:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705422/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:24:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705422/content)


***

### [2024-07-30 22:24:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116152)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:24:49)


***

### [2024-07-30 22:26:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116157)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:26:22)


***

### [2024-07-30 22:28:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116162)
>collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/30 22:29:02)

[ LHC Beam Losses Lifetime Display 24-07-30_22:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705448/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/30 22:29:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705448/content)

[Power Loss Monitoring 24-07-30_22:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705454/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/30 22:29:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705454/content)


***

### [2024-07-30 22:29:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116165)
>optimizations

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:30:01)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705460/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:30:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705460/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705470/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:31:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705470/content)


***

### [2024-07-30 22:31:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116168)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:31:24)


***

### [2024-07-30 22:32:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116171)
>IP2 landed a bit more separated than usual

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:32:50)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705480/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:32:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705480/content)


***

### [2024-07-30 22:37:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116178)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:37:38)


***

### [2024-07-30 22:39:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116179)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:39:37)


***

### [2024-07-30 22:39:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116179)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/30 22:39:37)


***

### [2024-07-30 22:43:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116183)
>emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:43:09)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705518/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:43:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705518/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705520/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:43:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705520/content)


***

### [2024-07-30 22:51:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116195)
>barely on target at 60cm!

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:51:28)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705562/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 23:22:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705562/content)


***

### [2024-07-30 22:52:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116198)
>all IPs on target

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:14)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705570/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705570/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705574/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705574/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705576/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705576/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705578/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705578/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-30_22:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705580/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/30 22:52:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705580/content)


***

### [2024-07-30 22:54:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116202)
>AGAP cleaning running constantly

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/30 22:54:27)

[Abort gap cleaning control v2.1.0 24-07-30_22:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705592/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/30 22:54:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705592/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-30_22:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705598/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/30 22:54:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705598/content)


***

### [2024-07-30 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116237)
>
```

```
**SHIFT SUMMARY:**  
  
- got the machine in SB  
- lost cryo conditions in 78 due loss of communication and got dumped after ~7h of SB  
- recovered the machine and waited for green light from cryo to restart (even though origin of loss of communication not identified yet)  
- precycled and prepared for physics  
- significant steering needed (energy matching at limit as well, may be needed in next filling)  
- significant time needed in PS to tune splitting and not the easiest injection  
- smooth cycle thereafter and leaving the machine in SB  
  
* To be noted:

  
- changed partition of RF lines on B1, briging down 5B1 to 3. It can be brought up to 6 if other lines overloaded (line 8B1 tripped at injection, arc detected, but not clear if correlated). Helga has re-calibrated motor and potenziometer and the issue might have been fixed. Further checks planned tomorrow morning before restoring equal partition among lines.  

```
- FBCT may need to be calibrated again because >0 unbunched beam from DCBCT-FBCT during ramp.   
  
Daniele  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/30 23:26:38)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705722/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/30 23:24:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705722/content)


***

### [2024-07-31 04:27:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116317)
>30cm

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/31 04:27:48)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-31_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705884/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/31 04:27:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705884/content)


***

### [2024-07-31 06:28:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116348)
>RF

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:28:40)

[Mozilla Firefox 24-07-31_06:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705969/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:28:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705969/content)

[Mozilla Firefox 24-07-31_06:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705971/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:28:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705971/content)


***

### [2024-07-31 06:31:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116349)
>RF last 5 days

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:32:05)

[Mozilla Firefox 24-07-31_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705973/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:32:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705973/content)

[Mozilla Firefox 24-07-31_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705975/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:32:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705975/content)

[Mozilla Firefox 24-07-31_06:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705977/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 06:32:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705977/content)


***

### [2024-07-31 06:47:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116350)
>After tune summary.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/31 06:48:05)

[ LHC Beam Losses Lifetime Display 24-07-31_06:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705979/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/31 06:48:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705979/content)


***

### [2024-07-31 06:48:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116351)
>Very stable beams  
  
AC  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/31 06:48:55)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705981/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/07/31 06:48:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3705981/content)


***

### [2024-07-31 09:26:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116413)
>Changed to flattop for the RF test.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:27:42)

[ Generation Application connected to server LHC (PRO database) 24-07-31_09:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706125/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:27:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706125/content)


***

### [2024-07-31 09:29:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116415)
>Started to change the partion function.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:38:45)

[ LSA Applications Suite (v 16.6.13) 24-07-31_09:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706133/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 09:29:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706133/content)

[ LHC RF CONTROL 24-07-31_09:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706165/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:38:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706165/content)


***

### [2024-07-31 09:33:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116421)
>Last step. Power looks reasonable, no sign of saturation. We will trim it into the ramp and injection. No need to access for the moment.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:40:01)

[ LSA Applications Suite (v 16.6.13) 24-07-31_09:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706137/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/07/31 09:33:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706137/content)

[ LHC RF CONTROL 24-07-31_09:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706169/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:40:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706169/content)


***

### [2024-07-31 09:34:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116426)
>Back to the physics beam proccess

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:35:02)

[ Generation Application connected to server LHC (PRO database) 24-07-31_09:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706149/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/31 09:35:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3706149/content)


***

### [2024-07-31 11:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116491)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:26:38)


***

### [2024-07-31 11:30:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116494)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:30:38)


***

### [2024-07-31 11:30:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116495)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:30:41)


***

### [2024-07-31 11:31:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116497)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:31:39)


***

### [2024-07-31 11:31:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116505)
>Global Post Mortem Event

Event Timestamp: 31/07/24 11:31:45.517
Fill Number: 9951
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 22413 / 23275 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/31 11:34:34)


***

### [2024-07-31 11:31:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116565)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: tpersson / Programmed dump


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/31 12:00:35)


***

### [2024-07-31 11:32:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116499)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:32:29)


***

### [2024-07-31 11:32:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116500)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:32:36)


***

### [2024-07-31 11:32:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116501)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:32:41)


***

### [2024-07-31 11:32:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4116502)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/31 11:32:43)


