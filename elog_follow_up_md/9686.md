# FILL 9686
**start**:		 2024-05-29 07:22:51.894988525+02:00 (CERN time)

**end**:		 2024-05-29 10:14:16.578988525+02:00 (CERN time)

**duration**:	 0 days 02:51:24.684000


***

### [2024-05-29 07:22:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077780)
>LHC RUN CTRL: New FILL NUMBER set to 9686

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 07:22:52)


***

### [2024-05-29 07:23:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077781)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 07:23:20)


***

### [2024-05-29 07:32:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077784)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 07:32:51)


***

### [2024-05-29 08:05:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077795)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:05:29)


***

### [2024-05-29 08:05:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077796)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:05:39)


***

### [2024-05-29 08:05:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077797)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:05:48)


***

### [2024-05-29 08:05:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077798)
>LHC Injection CompleteNumber of injections actual / planned: 49 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:32:57 / 0:33:48 (97.5 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 08:05:49)


***

### [2024-05-29 08:05:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077798)
>LHC Injection CompleteNumber of injections actual / planned: 49 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:32:57 / 0:33:48 (97.5 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 08:05:49)


***

### [2024-05-29 08:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077802)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:07:58)


***

### [2024-05-29 08:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077800)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:07:58)


***

### [2024-05-29 08:08:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077804)
>Beginning of ramp intensity.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:08:28)

[ LHC Fast BCT v1.3.2 24-05-29_08:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616486/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:08:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616486/content)


***

### [2024-05-29 08:08:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077805)
>Losses at start of ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/29 08:08:44)

[ LHC BLM Fixed Display 24-05-29_08:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616488/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/29 08:08:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616488/content)


***

### [2024-05-29 08:08:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077806)
>Event created from ScreenShot Client.  
 BSRT-vs-Emit v0.1.2 - January 2024 - INCA DISABLED - PRO CCDA 24-05-29\_08:08.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:09:00)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-29_08:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616490/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:09:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616490/content)


***

### [2024-05-29 08:09:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077807)
>emittance

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:09:11)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-29_08:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616492/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:09:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616492/content)


***

### [2024-05-29 08:29:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077816)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:29:19)


***

### [2024-05-29 08:29:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077818)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:29:33)


***

### [2024-05-29 08:38:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077824)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:38:04)


***

### [2024-05-29 08:38:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077826)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:38:43)


***

### [2024-05-29 08:39:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077828)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:39:27)


***

### [2024-05-29 08:43:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077832)
>Losses up ot 47% going to collisions.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/29 08:44:17)

[ LHC BLM Fixed Display 24-05-29_08:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616524/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/29 08:44:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616524/content)


***

### [2024-05-29 08:44:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077833)
>Optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:44:30)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-29_08:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616526/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:44:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616526/content)


***

### [2024-05-29 08:45:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077834)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:45:18)


***

### [2024-05-29 08:51:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077837)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:51:44)


***

### [2024-05-29 08:53:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077839)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 08:53:42)


***

### [2024-05-29 08:54:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077841)
>Emittance scan done in IP5 but we have IP1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:54:54)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-29_08:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616542/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 08:54:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616542/content)


***

### [2024-05-29 09:05:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077848)
>Still bad lifetime for beam 2. I tried to decrease the vertical tune a bit but the effect was not so great. Will try again at 60cm.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 09:06:20)

[ LHC Beam Losses Lifetime Display 24-05-29_09:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616554/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 09:06:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616554/content)


***

### [2024-05-29 09:18:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077859)
>Now the lifetime for beam 2 look fine.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 09:18:20)

[ LHC Beam Losses Lifetime Display 24-05-29_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616566/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/29 09:18:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616566/content)


***

### [2024-05-29 09:19:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077861)
>Emittance scan taken at 1.2m for IP1.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 09:19:35)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-29_09:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616571/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/29 09:19:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616571/content)


***

### [2024-05-29 10:01:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077883)
>Global Post Mortem EventEvent Timestamp: 29/05/24 10:01:46.257Fill Number: 9686Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799320 [MeV]Intensity B1/B2: 35460 / 34150 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 8-RF-b1: B T -> F on CIB.UA47.R4.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/29 10:04:35)


***

### [2024-05-29 10:03:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077880)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 10:03:33)


***

### [2024-05-29 10:03:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077881)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/29 10:03:35)


***

### [2024-05-29 10:10:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4077884)
>This is the one that tripped

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/29 10:10:24)

[ LHC RF CONTROL 24-05-29_10:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616642/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/05/29 10:10:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3616642/content)


