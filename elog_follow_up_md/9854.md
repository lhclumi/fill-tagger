# FILL 9854
**start**:		 2024-07-03 17:26:23.484363525+02:00 (CERN time)

**end**:		 2024-07-03 17:36:39.459488525+02:00 (CERN time)

**duration**:	 0 days 00:10:15.975125


***

### [2024-07-03 17:26:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099781)
>LHC RUN CTRL: New FILL NUMBER set to 9854

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 17:26:24)


***

### [2024-07-03 17:36:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099793)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 17:36:10)


***

### [2024-07-03 17:36:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4099794)
>LHC RUN CTRL: New FILL NUMBER set to 9855

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/03 17:36:39)


