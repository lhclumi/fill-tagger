# FILL 10204
**start**:		 2024-10-06 20:10:29.633363525+02:00 (CERN time)

**end**:		 2024-10-07 10:51:42.230363525+02:00 (CERN time)

**duration**:	 0 days 14:41:12.597000


***

### [2024-10-06 20:10:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157167)
>LHC RUN CTRL: New FILL NUMBER set to 10204

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:10:30)


***

### [2024-10-06 20:11:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157173)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:11:15)


***

### [2024-10-06 20:13:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157174)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:13:20)


***

### [2024-10-06 20:13:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157176)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:13:22)


***

### [2024-10-06 20:14:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157177)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:14:51)


***

### [2024-10-06 20:15:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157178)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:15:53)


***

### [2024-10-06 20:16:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157179)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:16:05)


***

### [2024-10-06 20:16:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157181)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:16:21)


***

### [2024-10-06 20:17:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157184)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:17:17)


***

### [2024-10-06 20:17:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157185)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:17:51)


***

### [2024-10-06 20:18:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157186)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:18:24)


***

### [2024-10-06 20:20:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157188)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:20:03)


***

### [2024-10-06 20:20:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157190)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:20:10)


***

### [2024-10-06 20:20:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157191)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:20:50)


***

### [2024-10-06 20:20:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157193)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:20:51)


***

### [2024-10-06 20:20:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157195)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:20:53)


***

### [2024-10-06 20:21:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157199)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:21:08)


***

### [2024-10-06 20:26:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157201)
>The QPS piquet power cycled the crate and now it all looks fine.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:27:06)

[Desktop 24-10-06_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805473/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:27:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805473/content)


***

### [2024-10-06 20:27:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157202)
>Summary of the QPS issue so far:
\*At stable beams several circuits in sector56 tripped. All of the ones that 
 tripped were on 3 different QPS. I could restore all by reset except for 
 the ROF/ROD. After power cycle I was able to restart. 
\*Restarted the machine. In the ramp the RSF1/RSD1 tripped and the QPS 
 controller got this orange color. I called the piquet that did a power 
 cycle. It is now back but I am afraid the issue will come back. QPS piquet 
 is currently investigating with colleuges to try to understand.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:31:09)


***

### [2024-10-06 20:32:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157203)
>Trying to send them to some different values to see if it holds.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:37:05)

[ Equip State 24-10-06_20:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805475/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:37:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805475/content)


***

### [2024-10-06 20:40:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157204)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:40:45)


***

### [2024-10-06 20:42:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157206)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:42:29)

[ Equip State 24-10-06_20:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805477/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:42:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805477/content)


***

### [2024-10-06 20:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157208)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:50:50)


***

### [2024-10-06 20:54:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157211)
>Now these tripped during the reset

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:54:26)

[Circuit_RQTL11_L5B1:   24-10-06_20:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805480/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 20:54:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805480/content)


***

### [2024-10-06 20:59:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157213)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 20:59:28)


***

### [2024-10-06 21:03:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157214)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 21:03:16)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-06_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805482/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 21:03:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805482/content)

[Accelerator Cockpit v0.0.38 24-10-06_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805484/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 21:03:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805484/content)


***

### [2024-10-06 21:05:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157215)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:05:27)


***

### [2024-10-06 21:43:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157227)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:43:17)


***

### [2024-10-06 21:43:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157228)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:43:29)


***

### [2024-10-06 21:43:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157229)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:43:38)


***

### [2024-10-06 21:43:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157230)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:38:11 / 0:33:48 (113.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 21:43:39)


***

### [2024-10-06 21:45:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157231)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:45:38)


***

### [2024-10-06 21:45:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157233)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 21:45:40)


***

### [2024-10-06 21:46:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157235)
>some losses in this ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 21:46:12)

[ LHC BLM Fixed Display 24-10-06_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805486/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 21:46:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805486/content)


***

### [2024-10-06 22:06:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157237)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:06:57)


***

### [2024-10-06 22:07:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157240)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:07:11)


***

### [2024-10-06 22:15:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157242)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:15:32)


***

### [2024-10-06 22:16:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157243)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:16:11)


***

### [2024-10-06 22:17:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157245)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:17:15)


***

### [2024-10-06 22:22:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157246)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:22:57)


***

### [2024-10-06 22:29:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157248)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:29:17)


***

### [2024-10-06 22:31:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157250)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 22:31:15)


***

### [2024-10-06 22:35:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157251)
>emittance scan ip1 and ip5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 22:35:31)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_22:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805492/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 22:35:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805492/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_22:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805494/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 22:35:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805494/content)


***

### [2024-10-06 22:46:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157253)
>Lifetime

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 22:46:26)

[ LHC Beam Losses Lifetime Display 24-10-06_22:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805496/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 22:46:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805496/content)

[ LHC Beam Losses Lifetime Display 24-10-06_22:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805498/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 22:46:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805498/content)


***

### [2024-10-06 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157255)
>
```
**SHIFT SUMMARY:**  
  
  

```

```
*Arrived in stable beams and dumped around 15.20. It was slightly delayed due to issues in the SPS.   
*Went back to stable beams without any major issues, a bit large variation of the intensity between the injections in the SPS but no major problme.  
*ALICE did some test with high luminosity in this fill and we reached 35% of dump threshold, which stopped the levelling. We discussed a bit with them and they said that this level was fine (initially they wanted to go 10% higher). I said it was possible to go to their original value but that it started to be a bit uncomfortable in case we have drifts or other losses.    
*After 45 min in stable beams I got a trip of several 600A circuits in sector56, in total 3 different QPS crates. Not all came at the same time.  I could recover all of them by simple reset excep the ROF/ROD crate which needed a pre-cycle. The expert says this was the main one to trip but for me it looked like the RSF1 went at the same time.   
*Recovered and filled again. This time I got dumped beginning of the ramp. This time it was only RSF1 sextupoles that tripped. This time the QPS controller had this orange color which is a sign of issues with the connection. Note that this crate also tripped before but wasn't the first one to do so and at that time it could be recovered without issues. This time I called the piquet and he also contact some other colleges and the final outcome was that it seems to have been two independent events. A power cycle was done and I continued.   
*Did the QPS reset and this tripped RQT11 and RQT12 in sector45. Could restart without any issue.   
*Again a bit large variation of the intensity and in the end we got some shots that were a bit high. We had some losses in the beginning of the ramp ~40%.   
*ALICE is continuing with their high luminsoity test. I am separating them not to overshot when we do the large beta* step 1.2m->60cm. They went to61 Hz/ub. The tests have now finished and they are back to normal levels.   
*Leaving in stable beams.  
  
/Tobias
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 23:09:14)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805500/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 23:09:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805500/content)


***

### [2024-10-06 23:34:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157258)
>Event created from ScreenShot Client.  
cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-10-06\_23:34.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/06 23:34:24)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-10-06_23:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805501/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/06 23:34:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805501/content)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-10-06_23:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805503/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/10/06 23:38:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805503/content)


***

### [2024-10-07 04:27:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157265)
>Event created from ScreenShot Client.  
Abort gap cleaning control v2.1.0 
 24-10-07\_04:27.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/07 04:27:08)

[Abort gap cleaning control v2.1.0 24-10-07_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805505/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/07 04:27:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805505/content)

[Mozilla Firefox 24-10-07_04:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805507/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/07 04:27:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805507/content)

[Abort gap cleaning control v2.1.0 24-10-07_04:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805509/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/07 04:28:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805509/content)

[ LHC Beam Losses Lifetime Display 24-10-07_04:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805511/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 04:30:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805511/content)

[ LHC Beam Losses Lifetime Display 24-10-07_04:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805513/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 04:30:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805513/content)

[Monitoring application. Currently monitoring : LHC - [2 subscriptions ] 24-10-07_04:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805515/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/07 04:36:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805515/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-07_06:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805615/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/07 06:59:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805615/content)


***

### [2024-10-07 06:33:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157279)
>Head on in IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:33:50)

[LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805557/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:33:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805557/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805559/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:34:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805559/content)


***

### [2024-10-07 06:33:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157279)
>Head on in IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:33:50)

[LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805557/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:33:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805557/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805559/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:34:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805559/content)


***

### [2024-10-07 06:44:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157280)
>Head on in IP1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:44:36)

[LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805571/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:44:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805571/content)

[LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805577/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:45:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805577/content)


***

### [2024-10-07 06:45:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157281)
>Machine status

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:45:21)

[udp:..multicast-bevlhc1:1234 - VLC media player 24-10-07_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805575/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:45:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805575/content)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-10-07_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805579/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:45:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805579/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-07_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805581/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:46:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805581/content)

[ LHC Fast BCT v1.3.2 24-10-07_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805583/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:46:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805583/content)

[ LHC Fast BCT v1.3.2 24-10-07_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805585/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:46:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805585/content)

[ LHC Beam Losses Lifetime Display 24-10-07_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805587/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 06:46:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805587/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-10-07_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805589/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/07 06:46:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805589/content)

[Abort gap cleaning control v2.1.0 24-10-07_06:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805593/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/07 06:47:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805593/content)

[ LHC MKI Temperature Display 24-10-07_07:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805625/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/10/07 07:06:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805625/content)


***

### [2024-10-07 06:51:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157282)
>BBLR turned ON

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:51:49)

[ LHC BBLR Wire App v0.5.2 24-10-07_06:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805601/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 06:51:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805601/content)

[ LHC Beam Losses Lifetime Display 24-10-07_06:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805603/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 06:51:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805603/content)


***

### [2024-10-07 08:29:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157295)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P3 - UJ33 |
| Peoples : | * Christophe Martin (TE/MPE/MI)169505 / 77981 - <christophe.martin@cern.ch> |
| Duration | 1h - **Access Needed** - |
| Lost redundancy on CIBU BTV | |



creator:	 lhcop  @paas-standard-avz-a-smrst.cern.ch (2024/10/07 08:29:01)


***

### [2024-10-07 09:39:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157334)
>Access team starting maintenance in PM25

creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/10/07 09:39:51)


***

### [2024-10-07 09:45:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157342)
>PM25 in access mode

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/07 09:45:29)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805897/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/07 09:45:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805897/content)


***

### [2024-10-07 10:00:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157356)
>Q' back to 20/20 for low-mu test of CMS and emittance scan

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 10:00:29)

[Accelerator Cockpit v0.0.38 24-10-07_10:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805973/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 10:00:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805973/content)

[ LHC Beam Losses Lifetime Display 24-10-07_10:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805983/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 10:01:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805983/content)

[ LHC Beam Losses Lifetime Display 24-10-07_10:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806017/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/07 10:05:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806017/content)

[Accelerator Cockpit v0.0.38 24-10-07_10:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806021/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/07 10:05:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806021/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-07_10:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806035/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:07:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806035/content)


***

### [2024-10-07 10:08:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157363)
>CMS at mu~7

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:09:03)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_10:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806041/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:09:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806041/content)


***

### [2024-10-07 10:18:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157372)
>IP5 back head-on

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:18:54)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_10:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806151/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:18:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806151/content)


***

### [2024-10-07 10:23:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157380)
>no majour blowup during CMS low-mu test

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:24:33)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-07_10:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806185/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:24:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806185/content)


***

### [2024-10-07 10:28:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157384)
>SPS already checked beams and they're ready

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/07 10:28:10)


***

### [2024-10-07 10:36:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157393)
>emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:36:17)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_10:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806253/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:36:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806253/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-07_10:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806255/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:36:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806255/content)


***

### [2024-10-07 10:37:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157394)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:37:01)


***

### [2024-10-07 10:39:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157396)
>before dump

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:39:24)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-10-07_10:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806271/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:39:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806271/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-10-07_10:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806273/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:39:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806273/content)

[ LHC Fast BCT v1.3.2 24-10-07_10:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806277/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 10:39:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806277/content)

[STABLE Beams - Bunch Length Control 24-10-07_10:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806281/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/10/07 10:39:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806281/content)


***

### [2024-10-07 10:41:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157399)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:41:01)


***

### [2024-10-07 10:41:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157400)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:41:04)


***

### [2024-10-07 10:42:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157402)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:42:02)


***

### [2024-10-07 10:43:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157410)
>Global Post Mortem Event

Event Timestamp: 07/10/24 10:43:14.949
Fill Number: 10204
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 23750 / 24023 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/07 10:46:06)

[pm-beam-loss-evaluation >> Version: 1.0.8  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-10-07_11:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806355/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 11:01:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806355/content)


***

### [2024-10-07 10:43:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157439)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: dmirarch / OP dump at end of physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/07 11:01:08)

[pm-beam-loss-evaluation >> Version: 1.0.8  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-10-07_11:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806357/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 11:02:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806357/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-10-07_11:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806359/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 11:03:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806359/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-10-07_11:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806361/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/07 11:03:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3806361/content)


***

### [2024-10-07 10:43:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157404)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:43:59)


***

### [2024-10-07 10:44:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157405)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:44:25)


***

### [2024-10-07 10:44:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157406)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:44:30)


***

### [2024-10-07 10:44:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157407)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:44:33)


***

### [2024-10-07 10:51:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157411)
>LHC RUN CTRL: New FILL NUMBER set to 10205

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/07 10:51:44)


