# FILL 10099
**start**:		 2024-09-10 02:14:12.184613525+02:00 (CERN time)

**end**:		 2024-09-10 09:43:52.790863525+02:00 (CERN time)

**duration**:	 0 days 07:29:40.606250


***

### [2024-09-10 02:14:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140443)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:14:51)


***

### [2024-09-10 02:17:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140444)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:17:09)


***

### [2024-09-10 02:18:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140446)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:18:26)


***

### [2024-09-10 02:18:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140447)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:18:51)


***

### [2024-09-10 02:19:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140448)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:19:52)


***

### [2024-09-10 02:20:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140452)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:20:50)


***

### [2024-09-10 02:21:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140453)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:21:16)


***

### [2024-09-10 02:21:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140455)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:21:22)


***

### [2024-09-10 02:21:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140456)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:21:24)


***

### [2024-09-10 02:23:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140457)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:23:20)


***

### [2024-09-10 02:23:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140459)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:23:48)


***

### [2024-09-10 02:23:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140461)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:23:55)


***

### [2024-09-10 02:25:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140462)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:25:45)


***

### [2024-09-10 02:25:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140464)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:25:46)


***

### [2024-09-10 02:25:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140466)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:25:47)


***

### [2024-09-10 02:26:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140470)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:26:01)


***

### [2024-09-10 02:28:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140472)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:28:00)


***

### [2024-09-10 02:29:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140474)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:29:44)


***

### [2024-09-10 02:31:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140476)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:31:40)


***

### [2024-09-10 02:45:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140480)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:45:06)


***

### [2024-09-10 02:50:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140482)
>Coupling measurement look actually too nice

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 02:51:14)

[Accelerator Cockpit v0.0.38 24-09-10_02:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764780/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 02:51:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764780/content)


***

### [2024-09-10 02:54:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140483)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 02:54:36)


***

### [2024-09-10 03:00:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140485)
>Wirescans

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 03:00:44)

[ LHC WIRESCANNER APP 24-09-10_03:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764784/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 03:00:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764784/content)

[ LHC WIRESCANNER APP 24-09-10_03:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764786/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 03:00:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764786/content)

[ LHC WIRESCANNER APP 24-09-10_03:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764788/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 03:00:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764788/content)

[ LHC WIRESCANNER APP 24-09-10_03:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764790/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/10 03:00:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764790/content)


***

### [2024-09-10 03:29:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140487)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 03:29:38)

[ LHC Fast BCT v1.3.2 24-09-10_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764804/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 03:29:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764804/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-10_03:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 03:29:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764806/content)


***

### [2024-09-10 03:29:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140488)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:29:46)


***

### [2024-09-10 03:29:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140488)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:29:46)


***

### [2024-09-10 03:29:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140489)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:29:55)


***

### [2024-09-10 03:30:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140490)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:30:04)


***

### [2024-09-10 03:30:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140491)
>LHC Injection Complete

Number of injections actual / planned: 55 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:35:28 / 0:33:48 (104.9 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 03:30:05)


***

### [2024-09-10 03:31:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140492)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:31:57)


***

### [2024-09-10 03:31:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140494)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:31:58)


***

### [2024-09-10 03:53:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140499)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:53:16)


***

### [2024-09-10 03:53:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140501)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 03:53:23)


***

### [2024-09-10 03:53:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140503)
>Flat top

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 03:53:28)

[ LHC Fast BCT v1.3.2 24-09-10_03:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764816/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 03:53:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764816/content)

[ LHC Beam Losses Lifetime Display 24-09-10_03:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764818/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 03:53:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764818/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-10_03:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764820/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/10 03:53:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764820/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-10_03:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764822/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/10 03:53:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764822/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-10_03:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764824/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/10 03:54:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764824/content)


***

### [2024-09-10 04:01:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140508)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:01:42)


***

### [2024-09-10 04:02:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140510)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:02:27)


***

### [2024-09-10 04:03:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140512)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:03:22)


***

### [2024-09-10 04:06:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140513)
>Collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 04:06:09)

[ LHC Beam Losses Lifetime Display 24-09-10_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764832/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 04:06:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764832/content)

[ LHC Fast BCT v1.3.2 24-09-10_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764834/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:10:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764834/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-10_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764836/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:06:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764836/content)


***

### [2024-09-10 04:07:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140514)
>Optimization IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:07:34)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-10_04:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764838/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:07:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764838/content)


***

### [2024-09-10 04:08:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140515)
>Optimizations IP1, IP2, IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:08:33)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-10_04:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764840/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 04:08:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764840/content)


***

### [2024-09-10 04:08:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140516)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:08:53)


***

### [2024-09-10 04:09:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140517)
>B1H +1e-3 tune trim to match B2 lifetime

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 04:10:16)

[ LHC Beam Losses Lifetime Display 24-09-10_04:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764842/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/10 04:10:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764842/content)


***

### [2024-09-10 04:15:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140518)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:15:12)


***

### [2024-09-10 04:17:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140519)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 04:17:10)


***

### [2024-09-10 04:21:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140520)
>Emittance scans IP1, IP5

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/10 04:21:34)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-10_04:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764844/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/10 04:21:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764844/content)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-10_04:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764846/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/10 04:21:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764846/content)


***

### [2024-09-10 06:52:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140537)
>Started the shift in stable beams.  
  
Around 2am RF line 3B2 tripped dumping the beam.  
I managed to reset it and continued right away.  
  
Filling was smooth and I had no issues during the cycle.  
Small B1H +1e-3 tune optimization in collisions.  
  
Leaving the machine in stable beams.  
  
AC

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/10 06:52:42)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764887/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/10 06:52:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3764887/content)


***

### [2024-09-10 09:38:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140607)
>Global Post Mortem Event

Event Timestamp: 10/09/24 09:38:38.247
Fill Number: 10099
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 30920 / 30768 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 11-ALICE\_Mag: A T -> F on CIB.UA27.R2.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/10 09:41:31)


***

### [2024-09-10 09:39:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140604)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 09:39:45)


***

### [2024-09-10 09:39:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140605)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/10 09:39:48)


***

### [2024-09-10 09:42:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4140608)
>Trip of the Alice dipole, this is a cooling issue the experts are investigating.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/10 09:42:59)


