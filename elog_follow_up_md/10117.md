# FILL 10117
**start**:		 2024-09-15 14:14:10.174238525+02:00 (CERN time)

**end**:		 2024-09-15 16:45:19.102863525+02:00 (CERN time)

**duration**:	 0 days 02:31:08.928625


***

### [2024-09-15 14:14:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144041)
>LHC RUN CTRL: New FILL NUMBER set to 10117

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:14:11)


***

### [2024-09-15 14:15:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144047)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:15:15)


***

### [2024-09-15 14:17:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144048)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:17:20)


***

### [2024-09-15 14:17:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144050)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:17:28)


***

### [2024-09-15 14:18:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144052)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:18:54)


***

### [2024-09-15 14:19:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144054)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:19:57)


***

### [2024-09-15 14:20:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144055)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:20:09)


***

### [2024-09-15 14:21:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144057)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:21:19)


***

### [2024-09-15 14:21:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144059)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:21:53)


***

### [2024-09-15 14:22:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144061)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:22:53)


***

### [2024-09-15 14:24:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144063)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:24:07)


***

### [2024-09-15 14:24:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144065)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:24:14)


***

### [2024-09-15 14:24:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144066)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:24:55)


***

### [2024-09-15 14:27:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144068)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:27:20)


***

### [2024-09-15 14:27:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144070)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:27:20)


***

### [2024-09-15 14:27:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144072)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:27:22)


***

### [2024-09-15 14:27:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144076)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:27:37)


***

### [2024-09-15 14:29:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144079)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:29:36)


***

### [2024-09-15 14:31:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144081)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:31:20)


***

### [2024-09-15 14:41:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144086)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:41:09)


***

### [2024-09-15 14:44:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144090)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:44:53)


***

### [2024-09-15 14:47:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144091)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 14:47:50)

[Accelerator Cockpit v0.0.38 24-09-15_14:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773769/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 14:47:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773769/content)

[Accelerator Cockpit v0.0.38 24-09-15_14:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773771/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 14:48:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773771/content)

[Accelerator Cockpit v0.0.38 24-09-15_14:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773774/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 14:51:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773774/content)


***

### [2024-09-15 14:49:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144093)
>Quiet shift in stable beams.  
Leaving the machine during injection.  
  
AC  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/15 14:49:29)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773773/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/15 14:49:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773773/content)


***

### [2024-09-15 14:50:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144094)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 14:50:45)


***

### [2024-09-15 15:24:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144097)
>full

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 15:24:03)

[ LHC Fast BCT v1.3.2 24-09-15_15:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773788/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 15:24:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773788/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-15_15:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773790/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 15:24:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773790/content)

[ Beam Intensity - v1.4.0 24-09-15_15:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773792/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 15:24:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773792/content)

[SPS Beam Quality Monitor - SPS.USER.LHC3 - SPS PRO INCA server - PRO CCDA 24-09-15_15:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773794/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/15 15:24:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773794/content)


***

### [2024-09-15 15:24:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144098)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:24:13)


***

### [2024-09-15 15:24:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144099)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:24:25)


***

### [2024-09-15 15:24:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144100)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:24:34)


***

### [2024-09-15 15:24:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144101)
>LHC Injection Complete

Number of injections actual / planned: 49 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:33:50 / 0:33:48 (100.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 15:24:35)


***

### [2024-09-15 15:26:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144105)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:26:17)


***

### [2024-09-15 15:26:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144103)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:26:16)


***

### [2024-09-15 15:26:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144107)
>Max. losses < 20%/dump

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 15:26:58)

[ LHC Beam Losses Lifetime Display 24-09-15_15:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773802/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 15:26:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773802/content)

[ Beam Intensity - v1.4.0 24-09-15_15:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773810/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 15:27:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773810/content)


***

### [2024-09-15 15:47:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144113)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:47:34)


***

### [2024-09-15 15:47:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144115)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:47:45)


***

### [2024-09-15 15:47:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144115)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:47:45)


***

### [2024-09-15 15:56:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144120)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:56:00)


***

### [2024-09-15 15:56:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144121)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:56:39)


***

### [2024-09-15 15:57:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144123)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 15:57:34)


***

### [2024-09-15 16:02:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144125)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:02:15)


***

### [2024-09-15 16:03:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144126)
>min. lifetime when going in collisions - ~20h

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:03:25)

[ LHC Beam Losses Lifetime Display 24-09-15_16:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773835/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:03:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773835/content)


***

### [2024-09-15 16:08:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144133)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:08:31)


***

### [2024-09-15 16:10:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144139)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:10:30)


***

### [2024-09-15 16:14:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144142)
>emit scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:14:35)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_16:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773859/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:14:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773859/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_16:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773861/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:14:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773861/content)


***

### [2024-09-15 16:16:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144143)
>Q' = 15 (at 1.2m)

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:16:11)

[Accelerator Cockpit v0.0.38 24-09-15_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773863/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:16:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773863/content)

[ LHC Beam Losses Lifetime Display 24-09-15_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773865/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:16:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773865/content)


***

### [2024-09-15 16:17:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144144)
>
```
Q' = 10 - no significant lifetime effect.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:18:26)

[Accelerator Cockpit v0.0.38 24-09-15_16:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773867/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:17:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773867/content)

[ LHC Beam Losses Lifetime Display 24-09-15_16:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773869/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:17:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773869/content)


***

### [2024-09-15 16:18:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144146)
>
```
back to Q' = 20 for beta* levelling  
  
going back from Q' = 10 to Q' = 20 rather seemed to have increased the lifetime slightly; on the same time, from the BBQ measurement it appears there is a tune effect (possibly feed-down, but the measurement is not reliable in collisions...)  
  
Conclusion: Q' 20 -> 10 is apparently not what is giving the big increase in lifetime when going to 60cm.  
Will do the same test in the opposite direction (increasing chroma) once the beta* levelling target is reached.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:22:56)

[Accelerator Cockpit v0.0.38 24-09-15_16:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773875/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:18:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773875/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-15_16:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773877/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/15 16:19:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773877/content)

[ LHC Beam Losses Lifetime Display 24-09-15_16:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773879/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:19:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773879/content)


***

### [2024-09-15 16:23:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144149)
>at 1.1m (with crossing angle back to 160urad)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:24:15)

[ LHC Beam Losses Lifetime Display 24-09-15_16:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773891/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:24:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773891/content)


***

### [2024-09-15 16:29:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144151)
>at 60cm

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:29:40)

[ LHC Beam Losses Lifetime Display 24-09-15_16:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773893/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:29:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773893/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-15_16:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773897/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/15 16:30:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773897/content)


***

### [2024-09-15 16:32:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144152)
>on target at 56cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:32:16)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-15_16:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773899/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:32:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773899/content)


***

### [2024-09-15 16:33:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144154)
>with all IPs on target

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:33:54)

[ LHC Beam Losses Lifetime Display 24-09-15_16:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773909/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:33:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773909/content)

[cs-513-michi9 24-09-15_16:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773911/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:34:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773911/content)

[Accelerator Cockpit v0.0.38 24-09-15_16:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773913/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:34:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773913/content)


***

### [2024-09-15 16:35:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144156)
>Q' = 10 - effect on B2 lifetime

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:35:53)

[Accelerator Cockpit v0.0.38 24-09-15_16:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773917/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/15 16:35:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773917/content)

[ LHC Beam Losses Lifetime Display 24-09-15_16:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773919/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:35:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773919/content)


***

### [2024-09-15 16:38:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144157)
>
```
Q' = 13, another drop in B2 lifetime. Stopping here.  
B2 lifetime can be recovered by raising B2H tune (+1e-3 equivalent to 3-5 units of Q')
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:39:35)

[ LHC Beam Losses Lifetime Display 24-09-15_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773921/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:38:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773921/content)


***

### [2024-09-15 16:39:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144158)
>
```
Back to Q' = 6.   
  
At beta* = 56cm (with the feed-forwards we have in), B2 is sensitive to chroma at the level of ~5h lifetime drop for 8 units of Q', while B1 is not.  
Lifetime can be recovered by moving the tunes. The effect of chroma in B2 is equivalent to ~1e-3 change in tune.  
  
Evidently, the Q' change can not explain the rise in B1 lifetime going from 1.2m to 60cm ... it is probably the Q feed-forward in combination with the applied beam separation.  

```


creator:	 mihostet  @194.12.157.136 (2024/09/15 17:06:58)

[ LHC Beam Losses Lifetime Display 24-09-15_16:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773927/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:39:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773927/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-15_16:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773931/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/15 16:40:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773931/content)

[ LHC Beam Losses Lifetime Display 24-09-15_16:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773945/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/15 16:43:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773945/content)


***

### [2024-09-15 16:42:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144166)
>Global Post Mortem Event

Event Timestamp: 15/09/24 16:42:58.966
Fill Number: 10117
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799320 [MeV]
Intensity B1/B2: 36591 / 36924 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 7-WIC: B T -> F on CIB.UJ33.U3.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/15 16:45:47)


***

### [2024-09-15 16:42:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144219)
>Global Post Mortem Event Confirmation

Dump Classification: Power converter fault(s)
Operator / Comment: acalia / RQT5.R3 tripped with SKSUB CABLE; fault resettable.


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/15 17:03:57)


***

### [2024-09-15 16:43:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144160)
>
```
RQT5.R3 tripped

16:46:31 Executing command DIAG.FAULTS on [RPMC.UJ33.RQT5.R3] on context(s)[_NON_MULTIPLEXED_LHC] with user(s) [] 
16:46:31 Result for device RPMC.UJ33.RQT5.R3 :
(String[]:3) -> VS    :EXTERNAL CROWBAR               :FAULT  :UT:  , VS    :SKSUB CABLE                    :FAULT  :UT:LT, VS    :VOUT OVER VOLTAGE              :FAULT  :UT:  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:47:16)

[Set SECTOR34 24-09-15_16:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773947/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/15 16:43:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773947/content)

[ Equip State 24-09-15_16:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773953/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 16:46:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3773953/content)

[Screenshot from 2024-09-15 16-50-58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3774021/content)
creator:	 mihostet  @194.12.157.136 (2024/09/15 17:01:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3774021/content)

[fgc-data-red >> Version: 5.0.11  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-09-15_17:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3774029/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/15 17:04:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3774029/content)


***

### [2024-09-15 16:43:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144161)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:43:34)


***

### [2024-09-15 16:43:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144162)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:43:36)


***

### [2024-09-15 16:45:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4144164)
>LHC RUN CTRL: New FILL NUMBER set to 10118

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/15 16:45:19)


