# FILL 9971
**start**:		 2024-08-03 12:29:46.952613525+02:00 (CERN time)

**end**:		 2024-08-03 23:36:14.994238525+02:00 (CERN time)

**duration**:	 0 days 11:06:28.041625


***

### [2024-08-03 12:29:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118553)
>LHC RUN CTRL: New FILL NUMBER set to 9971

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:29:47)


***

### [2024-08-03 12:30:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118559)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:30:52)


***

### [2024-08-03 12:33:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118560)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:33:04)


***

### [2024-08-03 12:33:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118561)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:33:17)


***

### [2024-08-03 12:34:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118564)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:34:32)


***

### [2024-08-03 12:34:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118565)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:34:39)


***

### [2024-08-03 12:35:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118567)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:35:34)


***

### [2024-08-03 12:35:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118568)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:35:49)


***

### [2024-08-03 12:36:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118570)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:36:42)


***

### [2024-08-03 12:36:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118573)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:36:58)


***

### [2024-08-03 12:37:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118574)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:37:32)


***

### [2024-08-03 12:39:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118575)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:07)


***

### [2024-08-03 12:39:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118577)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:08)


***

### [2024-08-03 12:39:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118579)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:09)


***

### [2024-08-03 12:39:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118583)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:24)


***

### [2024-08-03 12:39:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118585)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:44)


***

### [2024-08-03 12:39:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118587)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:39:50)


***

### [2024-08-03 12:41:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118589)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:41:24)


***

### [2024-08-03 12:43:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118592)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:43:08)


***

### [2024-08-03 12:47:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118594)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:47:08)


***

### [2024-08-03 12:54:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118596)
>MKI B1 soft start did more pulses then expected (-4min on timer). Result 
 is valid so I continue

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 12:54:36)

[ LHC Injection Kicker - LHC PRO INCA server - PRO CCDA - Version: 3.2.1  - LHC.USER.ALL 24-08-03_12:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711165/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 12:54:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711165/content)


***

### [2024-08-03 12:58:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118597)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 12:58:56)


***

### [2024-08-03 13:10:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118601)
>Big radial offset with pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 13:10:36)

[OpenYASP V4.9.0   LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-03_13:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711189/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 13:10:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711189/content)


***

### [2024-08-03 13:13:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118602)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:13:37)


***

### [2024-08-03 13:15:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118604)
>Radial offset with 12b, roughly half of the pilot in the other direction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 13:16:11)

[OpenYASP V4.9.0   LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-03_13:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711193/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 13:16:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711193/content)


***

### [2024-08-03 13:20:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118605)
>Error with wirescan B2V

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:20:21)

[24-08-03_13:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711197/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:20:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711197/content)


***

### [2024-08-03 13:22:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118606)
>Steering B1H 72b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:22:58)

[ LHC TDIS Temperature Monitoring App v0.0.3 24-08-03_13:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711199/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:22:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711199/content)


***

### [2024-08-03 13:23:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118607)
>Steering B2H 72b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:23:27)

[LINAC4.SPS.LHC BIS Monitor V12.5.1 24-08-03_13:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711201/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:23:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711201/content)


***

### [2024-08-03 13:23:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118608)
>Steering B2V 72b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:23:50)

[LINAC4.SPS.LHC BIS Monitor V12.5.1 24-08-03_13:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711203/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/08/03 13:23:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711203/content)

[Confirmation required 24-08-03_13:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711205/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/03 13:24:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711205/content)


***

### [2024-08-03 13:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118609)
>Wirescans. Missing B2V. FESA class is reporting PMThigh voltage is not ON. 
 Will debug next rampdown, now I go on with injection

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:27:20)

[ LHC WIRESCANNER APP 24-08-03_13:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711207/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:55:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711207/content)

[ LHC WIRESCANNER APP 24-08-03_13:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711209/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:27:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711209/content)

[ LHC WIRESCANNER APP 24-08-03_13:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711211/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:27:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711211/content)


***

### [2024-08-03 13:55:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118615)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:55:46)


***

### [2024-08-03 13:55:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118616)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:55:53)

[ LHC Fast BCT v1.3.2 24-08-03_13:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711213/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:55:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711213/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-03_13:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711215/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 13:56:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711215/content)


***

### [2024-08-03 13:55:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118617)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:55:55)


***

### [2024-08-03 13:55:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118618)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:55:59)


***

### [2024-08-03 13:56:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118619)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:42:23 / 0:33:48 (125.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 13:56:00)


***

### [2024-08-03 13:58:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118621)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:58:45)


***

### [2024-08-03 13:58:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118623)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 13:58:46)


***

### [2024-08-03 14:16:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118625)
>switched off durinmg the ramp

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:17:02)

[ TRANSVERSE DAMPER CONTROL 24-08-03_14:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711217/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:17:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711217/content)

[Inspector 3.5.53 - ADT Control 24-08-03_14:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711219/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:20:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711219/content)

[Inspector 3.5.53 - ADT Control 24-08-03_14:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711221/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:21:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711221/content)


***

### [2024-08-03 14:20:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118626)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:20:04)


***

### [2024-08-03 14:22:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118628)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:22:42)


***

### [2024-08-03 14:22:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118628)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:22:42)


***

### [2024-08-03 14:22:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118630)
>flat top

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 14:22:51)

[ LHC Beam Losses Lifetime Display 24-08-03_14:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711223/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 14:22:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711223/content)

[ LHC Fast BCT v1.3.2 24-08-03_14:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711225/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:32:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711225/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-03_14:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711227/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:22:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711227/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-03_14:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711229/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/03 14:23:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711229/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-03_14:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711231/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/03 14:23:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711231/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-03_14:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711239/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/03 14:32:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711239/content)


***

### [2024-08-03 14:26:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118631)
>recovered after a bit of convincing...

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:26:29)

[ TRANSVERSE DAMPER CONTROL 24-08-03_14:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711233/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 14:26:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711233/content)


***

### [2024-08-03 14:31:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118632)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:31:17)


***

### [2024-08-03 14:32:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118633)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:32:00)


***

### [2024-08-03 14:32:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118635)
>squeeze + qchange

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/03 14:32:09)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-03_14:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711235/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/03 14:32:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711235/content)

[ LHC Beam Losses Lifetime Display 24-08-03_14:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711237/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 14:32:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711237/content)


***

### [2024-08-03 14:32:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118636)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:32:53)


***

### [2024-08-03 14:35:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118639)
>lifetime dip in collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 14:35:26)

[ LHC Beam Losses Lifetime Display 24-08-03_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711241/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 14:35:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711241/content)


***

### [2024-08-03 14:35:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118641)
>collisions

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:35:38)

[ LHC Fast BCT v1.3.2 24-08-03_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711243/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:35:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711243/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-03_14:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711245/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:35:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711245/content)


***

### [2024-08-03 14:36:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118642)
>optimization IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:36:58)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711249/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:38:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711249/content)


***

### [2024-08-03 14:37:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118643)
>optimizations IP1, 2, 5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:37:56)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_14:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711251/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:37:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711251/content)


***

### [2024-08-03 14:38:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118644)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:38:16)


***

### [2024-08-03 14:44:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118646)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:44:34)


***

### [2024-08-03 14:46:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118647)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 14:46:31)


***

### [2024-08-03 14:50:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118649)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:50:53)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711259/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:54:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711259/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-03_14:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711261/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:51:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711261/content)


***

### [2024-08-03 14:54:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118650)
>Started shift during ramp down.  
XPOC Viewer did not received latest dump data/analysis. After checking with ABT piquet (and N.Magnin) the analysis was re-done and passed correctly.  
A dry dump confirmed the correct functioning of the LBDS system.  
  
Injecting the pilot I noticed a problem with the injected bucket reported by the BQM, it was off by 1.   
During the rampdown I corrected the injection phase that was overdue since a few injections. I could see the trim of the injection phase in LSA but not the bucket offset for the BQM. I reverted the injection phase trim. Re-corrected at injection the phase and resynchronized the frequency with the sequence and it worked correctly.  
  
Filled the machine and went to stable beams without problems (minor TL steering B1H). Beam was dumped by SIS reporting communication issue from FGC COD crate cfc-sr7-rr7h. The CODs did not trip, so most probably the crate stop publishing data for a very short while.  
  
Started again filling. MKI B1 soft start took a bit more pulses then expected (-4 min reported) but in the end it was fine so I continued.  
I did TL steering for B1H, B2H and B2V as the oscillations were a bit big with first train.  
During the ramp, ADT module v2b1 and h1b2 switched OFF. Managed to recover them.  
  
Leaving the machine in stable beams.  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:54:49)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711263/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 14:54:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711263/content)


***

### [2024-08-03 16:24:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118674)
>Damper tripped again.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:24:54)

[ TRANSVERSE DAMPER CONTROL 24-08-03_16:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711308/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:24:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711308/content)


***

### [2024-08-03 16:29:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118676)
>back on

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:29:48)

[ TRANSVERSE DAMPER CONTROL 24-08-03_16:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711340/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:29:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711340/content)


***

### [2024-08-03 16:35:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118677)
>Tripped again.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:35:11)

[ TRANSVERSE DAMPER CONTROL 24-08-03_16:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711370/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/03 16:35:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711370/content)


***

### [2024-08-03 22:17:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118713)
>1/5 head on. Turning on the wire.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 22:17:51)

[Desktop 24-08-03_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711444/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 22:17:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711444/content)

[ LHC BBLR Wire App v0.4.1 24-08-03_22:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711446/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/03 22:18:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711446/content)


***

### [2024-08-03 22:21:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118714)
>Best setting I found for beam 1.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 22:21:59)

[Accelerator Cockpit v0.0.37 24-08-03_22:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711448/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/03 22:21:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711448/content)

[ LHC Beam Losses Lifetime Display 24-08-03_22:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711450/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/03 22:24:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711450/content)


***

### [2024-08-03 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118717)
>
```
**SHIFT SUMMARY:**  
  
-Arrived in stable beams.  
-Two modules for the damper tripped again. Same as in the ramp and yesterday. I could restart them after some resets.  
-Turned on the wires when we were head-on and optimized the tunes.   
  
To note:  
CMS need an access. We have a number to call as soon as we dump but they will be ready in case we manage to dump according to plan.  
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/03 23:00:22)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711452/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/03 23:00:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3711452/content)


***

### [2024-08-03 23:34:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118723)
>Global Post Mortem Event

Event Timestamp: 03/08/24 23:34:04.223
Fill Number: 9971
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 26421 / 26446 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 4-BLM\_UNM: B T -> F on CIB.US15.L1.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/03 23:36:55)


***

### [2024-08-03 23:34:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118720)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 23:34:38)


***

### [2024-08-03 23:34:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118721)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/03 23:34:40)


