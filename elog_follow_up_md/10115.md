# FILL 10115
**start**:		 2024-09-14 07:57:20.553488525+02:00 (CERN time)

**end**:		 2024-09-14 20:33:17.479363525+02:00 (CERN time)

**duration**:	 0 days 12:35:56.925875


***

### [2024-09-14 07:57:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143260)
>LHC RUN CTRL: New FILL NUMBER set to 10115

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 07:57:21)


***

### [2024-09-14 07:58:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143268)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 07:58:42)


***

### [2024-09-14 08:00:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143270)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:00:58)


***

### [2024-09-14 08:02:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143272)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:02:03)


***

### [2024-09-14 08:02:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143274)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:02:23)


***

### [2024-09-14 08:02:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143276)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:02:52)


***

### [2024-09-14 08:03:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143277)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:03:42)


***

### [2024-09-14 08:04:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143280)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:04:26)


***

### [2024-09-14 08:04:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143282)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:04:35)


***

### [2024-09-14 08:05:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143283)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:05:16)


***

### [2024-09-14 08:05:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143285)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:05:51)


***

### [2024-09-14 08:06:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143286)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:06:51)


***

### [2024-09-14 08:06:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143288)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:06:52)


***

### [2024-09-14 08:06:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143290)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:06:53)


***

### [2024-09-14 08:07:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143294)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:07:08)


***

### [2024-09-14 08:07:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143296)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:07:36)


***

### [2024-09-14 08:07:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143298)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:07:43)


***

### [2024-09-14 08:09:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143300)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:09:07)


***

### [2024-09-14 08:10:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143302)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:10:51)


***

### [2024-09-14 08:12:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143306)
>Event created from ScreenShot Client.  
fgc-data-red >> Version: 5.0.11 Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-09-14\_08:12.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:12:28)

[fgc-data-red >> Version: 5.0.11  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-09-14_08:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770991/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:17:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770991/content)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-09-14_08:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770993/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:13:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770993/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-09-14_08:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770995/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:13:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3770995/content)


***

### [2024-09-14 08:15:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143308)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:15:20)


***

### [2024-09-14 08:17:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143309)
>the same corrector had tripped already on 17.07.2024 : <https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4107599>  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:18:05)


***

### [2024-09-14 08:23:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143311)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:23:14)


***

### [2024-09-14 08:25:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143312)
>corrections on pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:25:32)

[RF Trim Warning 24-09-14_08:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771009/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:25:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771009/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771011/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:26:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771011/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771013/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:26:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771013/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771015/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:26:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771015/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771017/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:26:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771017/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771019/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:28:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771019/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771021/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:29:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771021/content)

[Accelerator Cockpit v0.0.38 24-09-14_08:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771023/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 08:31:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771023/content)


***

### [2024-09-14 08:32:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143313)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 08:32:36)


***

### [2024-09-14 08:35:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143314)
>Event created from ScreenShot Client.  
Send HW 24-09-14\_08:35.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:35:08)

[Send HW 24-09-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771025/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:35:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771025/content)

[OpenYASP V4.9.2   LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-09-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771027/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:35:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771027/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-09-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771029/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:35:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771029/content)

[Confirmation required 24-09-14_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771031/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/14 08:35:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771031/content)


***

### [2024-09-14 08:39:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143315)
>injections of first 72b trains

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:40:13)

[OpenYASP V4.9.2   LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 . SPS.USER.LHC3 24-09-14_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771033/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/14 08:40:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771033/content)


***

### [2024-09-14 08:42:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143316)
>
```
asked to scrape a bit more in SPS
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 08:47:33)

[ LHC Fast BCT v1.3.2 24-09-14_08:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771035/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:06:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771035/content)


***

### [2024-09-14 09:07:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143320)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-09-14\_09:07.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:07:54)

[ LHC Fast BCT v1.3.2 24-09-14_09:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771037/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:07:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771037/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-14_09:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771039/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:08:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771039/content)

[ LHC Beam Losses Lifetime Display 24-09-14_09:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771041/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 09:08:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771041/content)


***

### [2024-09-14 09:08:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143321)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:08:27)

[SPS Beam Quality Monitor - SPS.USER.LHC3 - SPS PRO INCA server - PRO CCDA 24-09-14_09:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771043/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/14 09:08:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771043/content)


***

### [2024-09-14 09:08:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143322)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:08:38)


***

### [2024-09-14 09:08:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143323)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:08:47)


***

### [2024-09-14 09:08:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143324)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:36:11 / 0:33:48 (107.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 09:08:48)


***

### [2024-09-14 09:09:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143325)
>
```
losses on beam 1 before start ramp
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:09:40)

[ Beam Intensity - v1.4.0 24-09-14_09:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771045/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:09:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771045/content)


***

### [2024-09-14 09:10:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143328)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:10:29)


***

### [2024-09-14 09:10:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143330)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:10:29)


***

### [2024-09-14 09:10:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143331)
>
```
losses when launching ramp
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:11:00)

[ Beam Intensity - v1.4.0 24-09-14_09:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771047/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:10:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771047/content)

[ Beam Intensity - v1.4.0 24-09-14_09:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771055/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:11:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771055/content)


***

### [2024-09-14 09:31:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143338)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:31:47)


***

### [2024-09-14 09:31:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143340)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:31:55)


***

### [2024-09-14 09:40:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143348)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:40:18)


***

### [2024-09-14 09:41:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143350)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:41:17)


***

### [2024-09-14 09:42:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143352)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:42:11)


***

### [2024-09-14 09:42:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143353)
>
```
went from 7 to 6 in chromaticity target
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:43:24)

[ LSA Applications Suite (v 16.6.26) 24-09-14_09:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771113/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:42:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771113/content)


***

### [2024-09-14 09:45:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143354)
>
```
23 hours lifetime in collisions! no instability so far
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:57:19)

[ LHC Beam Losses Lifetime Display 24-09-14_09:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771115/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 09:45:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771115/content)


***

### [2024-09-14 09:45:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143356)
>
```
IP optimization
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:06:32)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_09:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771117/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:46:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771117/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_09:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771125/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:48:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771125/content)


***

### [2024-09-14 09:47:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143359)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:47:45)


***

### [2024-09-14 09:54:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143362)
>
```
emittance scan in IP5
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:06:12)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_09:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771151/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:54:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771151/content)


***

### [2024-09-14 09:54:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143363)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:54:15)


***

### [2024-09-14 09:56:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143364)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:56:13)


***

### [2024-09-14 09:56:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143364)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 09:56:13)


***

### [2024-09-14 09:58:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143367)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:15)

[tmpScreenshot_1726300695.2248843.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771163/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771163/content)


***

### [2024-09-14 09:58:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143368)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:17)

[tmpScreenshot_1726300697.1767411.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771165/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771165/content)


***

### [2024-09-14 09:58:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143369)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:20)

[tmpScreenshot_1726300700.0407915.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771169/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 09:58:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771169/content)


***

### [2024-09-14 09:59:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143372)
>did not wire scan in B2V  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 09:59:23)


***

### [2024-09-14 09:59:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143373)
>
```
emittance scan IP1
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:59:51)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_09:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771179/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:00:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771179/content)


***

### [2024-09-14 10:19:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143393)
>
```
overshoot on ATLAS lumi at 10:17, also a bummp on orbit B1H and B2H  
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:23:23)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_10:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771299/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:20:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771299/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_10:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771301/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:21:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771301/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-14_10:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771303/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 10:20:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771303/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_10:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771313/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:21:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771313/content)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_10:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771317/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:21:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771317/content)


***

### [2024-09-14 10:26:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143395)
>
```
status at 60 cm
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:38:29)

[ LHC Fast BCT v1.3.2 24-09-14_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771327/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:38:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771327/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-14_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771329/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 10:26:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771329/content)

[ LHC Beam Losses Lifetime Display 24-09-14_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771331/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 10:26:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771331/content)

[ LHC Beam Losses Lifetime Display 24-09-14_10:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771333/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 10:26:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771333/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-14_10:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771335/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/14 10:27:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771335/content)

[ LHC RF Beam Spectra Display v0.3.5 24-09-14_10:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771337/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/14 10:27:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771337/content)


***

### [2024-09-14 12:02:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143436)
>orbit change at 11:24 with impact on lumi

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 12:02:30)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-14_12:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771498/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 12:02:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771498/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-14_12:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771500/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 12:02:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771500/content)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_12:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771502/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:02:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771502/content)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_12:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771504/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 12:03:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771504/content)


***

### [2024-09-14 12:58:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143463)
>some spikes

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 12:58:29)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-14_12:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771694/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/14 12:58:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771694/content)


***

### [2024-09-14 14:04:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143495)
>
```
chroma target reduced from 7 to 6 at and below 60 cm
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:06:24)

[Accelerator Cockpit v0.0.38 24-09-14_14:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771840/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/14 14:05:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771840/content)


***

### [2024-09-14 14:49:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143511)
>
```
impact of chroma decrease on lifetime not very clear (but it is a small decrease) 
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:49:52)

[Mozilla Firefox 24-09-14_14:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771926/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 14:58:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771926/content)


***

### [2024-09-14 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143527)
>**SHIFT SUMMARY:**  
  
Arrived in stable beams.  
  
Beams dumped following a corrector trip (RCBXH3.L8).  
  
Ramped down, refilled (very good quality beam from injectors), ramped, squeezed and brought back to stable beams.  
  
Leaving the machine in stable beams.  
  
  
* To be noted:

  
- Chroma target reduced (from Qp=7 to Qp=6) at beta\*=60 cm and below   
- Sequencer was slow and got stuck when sending the parallelized preparation. With the help of Michi (thanks!), we switched to the TEST sequencer, and sent the tasks one by one. The TEST sequencer also seemed slower than usual.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:03:20)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771960/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:03:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3771960/content)


***

### [2024-09-14 15:12:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143539)
>CMS is pushing the target further to mu = 64

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:12:21)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_15:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772007/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:12:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772007/content)


***

### [2024-09-14 15:22:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143552)
>for fabio

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:22:40)

[Monitoring rda3:..UCAP-NODE-LHC-SIS.LHCSIS-INJ.SPSBCT.InterlockState @ no-selector 24-09-14_15:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772055/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 15:22:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772055/content)


***

### [2024-09-14 16:59:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143599)
>CMS is pushing even more, to mu = 65 ...

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 17:00:17)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_16:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772299/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 17:00:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772299/content)


***

### [2024-09-14 17:52:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143639)
>IP5 levelling (on mu=65) out of steam after 8h of collisions - autopilot ON

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 17:53:19)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_17:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772461/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 17:53:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772461/content)


***

### [2024-09-14 18:07:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143650)
>IP1 levelling out of steam after 08:20h in SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 18:07:37)

[LHC Luminosity Scan Client 0.62.3 [pro] 24-09-14_18:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772517/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 18:07:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772517/content)


***

### [2024-09-14 18:10:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143651)
>BBLR CW on and tune compensated (+2e-3 B1H and +1e-3 B1V)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 18:10:37)

[ LHC Beam Losses Lifetime Display 24-09-14_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772519/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 18:10:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772519/content)

[ LHC BBLR Wire App v0.4.1 24-09-14_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772521/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 18:10:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772521/content)


***

### [2024-09-14 20:29:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143700)
>Global Post Mortem Event

Event Timestamp: 14/09/24 20:29:46.053
Fill Number: 10115
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 25210 / 25642 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: SW Permit: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/14 20:32:35)


***

### [2024-09-14 20:29:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143750)
>Global Post Mortem Event Confirmation

Dump Classification: Controls system problem
Operator / Comment: mihostet / communication lost with CODs S81 (cfc-sr8-rr8i dead); MKB earth switches faulty after the dump (dilution pattern looks normal)


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/14 22:00:32)


***

### [2024-09-14 20:30:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143696)
>
```
COD comm lost in S81 - FGC gateway cfc-sr8-rr8i dead.  
  
Tried to power cycle via DIAMON, timeout.  
  
Calling EPC piquet.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:39:46)

[Set SECTOR81 24-09-14_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772636/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/14 20:30:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772636/content)

[ Power Converter Interlock GUI 24-09-14_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772638/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 20:30:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772638/content)

[PC Interlock Status 24-09-14_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772640/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 20:30:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772640/content)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-09-14_20:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772646/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:38:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772646/content)

[An error occured 24-09-14_20:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772650/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:39:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772650/content)


***

### [2024-09-14 20:30:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143697)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:30:40)


***

### [2024-09-14 20:31:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143698)
>
```
LBDS B1 earth switches ... same as yesterday. Dump looks OK though.  
  
Called kicker piquet (Christophe)  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:43:51)

[(null) 24-09-14_20:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772642/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 20:31:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772642/content)

[(null) 24-09-14_20:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772644/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/14 20:31:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772644/content)

[ WinCC - Operation 24-09-14_20:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772654/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:40:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772654/content)

[ WinCC - Operation 24-09-14_20:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772662/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:42:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772662/content)

[ XPOC Viewer - PRO 24-09-14_20:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772664/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/14 20:42:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3772664/content)


***

### [2024-09-14 20:31:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4143699)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/14 20:31:53)


