# FILL 9750
**start**:		 2024-06-09 07:40:03.187238525+02:00 (CERN time)

**end**:		 2024-06-09 07:55:43.172238525+02:00 (CERN time)

**duration**:	 0 days 00:15:39.985000


***

### [2024-06-09 07:40:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084377)
>LHC RUN CTRL: New FILL NUMBER set to 9750

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 07:40:03)


***

### [2024-06-09 07:53:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084379)
>cryo recovered, preparing to restart

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 07:53:35)


***

### [2024-06-09 07:55:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084380)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 07:55:29)


***

### [2024-06-09 07:55:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084382)
>LHC RUN CTRL: New FILL NUMBER set to 9751

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 07:55:43)


***

### [2024-06-09 07:55:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084381)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 07:55:43)


