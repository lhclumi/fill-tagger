# FILL 9931
**start**:		 2024-07-25 00:19:50.159738525+02:00 (CERN time)

**end**:		 2024-07-25 16:55:39.899238525+02:00 (CERN time)

**duration**:	 0 days 16:35:49.739500


***

### [2024-07-25 00:22:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112141)
>These two tripped going to standby.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/25 00:22:58)

[Set SECTOR23 24-07-25_00:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694579/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/25 00:22:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694579/content)


***

### [2024-07-25 00:26:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112144)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 00:26:18)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-25_01:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694591/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:03:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694591/content)


***

### [2024-07-25 00:57:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112147)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 00:57:28)


***

### [2024-07-25 01:07:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112156)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:07:12)


***

### [2024-07-25 01:11:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112158)
>The check in the sequencer says it is not fine but in the XPOC application 
 it says that beam is permitted even though there is one which is not ok 
 but this is yellow.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:13:15)

[ XPOC Viewer - PRO 24-07-25_01:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694625/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:13:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694625/content)


***

### [2024-07-25 01:13:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112159)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:13:36)


***

### [2024-07-25 01:14:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112161)
>I do a quick dry dump to see if it shows any issues.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:15:02)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-25_01:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694633/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:15:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694633/content)


***

### [2024-07-25 01:19:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112164)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 01:20:05)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-25_01:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694637/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 01:20:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694637/content)

[Accelerator Cockpit v0.0.37 24-07-25_01:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694661/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 01:21:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694661/content)

[Accelerator Cockpit v0.0.37 24-07-25_01:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694663/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 01:21:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694663/content)


***

### [2024-07-25 01:21:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112166)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-07-25\_01:21.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 01:21:34)

[Accelerator Cockpit v0.0.37 24-07-25_01:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694659/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/25 01:21:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694659/content)


***

### [2024-07-25 01:23:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112168)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:23:51)


***

### [2024-07-25 01:25:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112169)
>steering with the 12 looks good

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/25 01:26:05)

[OpenYASP V4.9.2   LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-07-25_01:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694665/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/25 01:26:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694665/content)


***

### [2024-07-25 01:28:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112170)
>message when trying to do scan in beam2 vertical.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:28:40)

[24-07-25_01:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694667/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:28:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694667/content)


***

### [2024-07-25 01:56:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112172)
>Intensity in prepare ramp.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:56:35)

[ LHC Fast BCT v1.3.2 24-07-25_01:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694677/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 01:56:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694677/content)


***

### [2024-07-25 01:56:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112173)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:56:37)


***

### [2024-07-25 01:56:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112174)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:56:46)


***

### [2024-07-25 01:56:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112175)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 01:56:50)


***

### [2024-07-25 01:56:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112176)
>LHC Injection Complete

Number of injections actual / planned: 50 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:32:59 / 0:33:48 (97.6 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 01:56:51)


***

### [2024-07-25 02:00:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112177)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:00:45)


***

### [2024-07-25 02:00:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112179)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:00:46)


***

### [2024-07-25 02:01:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112182)
>Warning in start of ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 02:01:17)

[ LHC BLM Fixed Display 24-07-25_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694681/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/25 02:01:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694681/content)


***

### [2024-07-25 02:22:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112186)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:22:04)


***

### [2024-07-25 02:22:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112188)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:22:18)


***

### [2024-07-25 02:30:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112190)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:30:43)


***

### [2024-07-25 02:31:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112191)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:31:25)


***

### [2024-07-25 02:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112193)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:32:17)


***

### [2024-07-25 02:32:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112193)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:32:17)


***

### [2024-07-25 02:35:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112194)
>Optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:35:59)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_02:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694683/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:35:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694683/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694685/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:36:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694685/content)


***

### [2024-07-25 02:37:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112195)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:37:34)


***

### [2024-07-25 02:43:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112196)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:43:52)


***

### [2024-07-25 02:45:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112197)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 02:45:55)


***

### [2024-07-25 02:49:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112198)
>Emittance scan in IP1 and IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:49:38)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_02:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694689/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:49:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694689/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_02:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694691/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 02:49:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694691/content)


***

### [2024-07-25 05:22:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112205)
>Tried to optimize the tunes for beam 1 but no improvements..

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 05:22:35)

[ LHC Beam Losses Lifetime Display 24-07-25_05:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694709/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 05:22:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694709/content)


***

### [2024-07-25 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112218)
>
```
**SHIFT SUMMARY:**  
  
-QPS intervention in sector 23 ongoing when I arrived. They left just after midnight. There is still an issue so the QPS reset should be avoided for this sector. The reset for sector23 is commented out in the sequence.  
-Got a XPOC error from beam 2 in the sequencer. When I opened the XPOC application it said beam was permitted. Some modules were red but they were all masked, as seen by the yellow color. I decided to do a dry dump to see if the problem would re-appear but this time it was all fine also from the sequencer this time.    
-wire scanner for beam 2 vertical gave an error when I tried to scan after the second train. I didn't investigate any reason but just continued.   
-40% losses in beginning of the ramp.   
-Leaving in stable beams.   
  
* To be noted:

  
No reset for the QPS of the RBs in sector 23 (commented out in the sequence).   
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 07:00:10)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694738/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 07:00:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694738/content)


***

### [2024-07-25 08:16:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112223)
>
```
30cm reached after ~5.5h in SB (2 steps cut short by longitudinal blow-up)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 08:17:09)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_08:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694757/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 08:16:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694757/content)


***

### [2024-07-25 08:23:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112226)
>B1 Lifetime drop going to 30cm. Tune scan, improvement not obvious.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 08:23:50)

[ LHC Beam Losses Lifetime Display 24-07-25_08:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694763/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 08:23:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694763/content)

[ LHC Beam Losses Lifetime Display 24-07-25_08:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694765/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 08:27:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694765/content)


***

### [2024-07-25 09:12:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112251)
>SR4 temperature - on the rise again!

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 09:12:18)

[Mozilla Firefox 24-07-25_09:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694783/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 09:12:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694783/content)


***

### [2024-07-25 09:51:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112277)
>IP5 levelling out of steam after > 7h in SB

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 09:51:33)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_09:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694823/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 09:51:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694823/content)


***

### [2024-07-25 10:29:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112312)
>IP1 levelling out of steam (almost 8h in SB!)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 10:29:36)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_10:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694888/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 10:29:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694888/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_10:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694892/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 10:30:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694892/content)


***

### [2024-07-25 10:32:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112314)
>
```
BBLR CW ON  
  
Usual +2e-3 Q1H trim to recover the B1 lifetime.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 10:34:24)

[ LHC BBLR Wire App v0.4.1 24-07-25_10:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 10:32:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694902/content)

[ LHC Beam Losses Lifetime Display 24-07-25_10:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694908/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 10:33:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694908/content)

[ LHC Beam Losses Lifetime Display 24-07-25_10:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694920/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/25 10:35:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3694920/content)


***

### [2024-07-25 14:50:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112501)
>optimized ALICE crossing plane

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 14:50:50)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-25_14:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695577/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/25 14:50:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695577/content)

[Mozilla Firefox 24-07-25_14:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695585/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 14:51:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695585/content)

[Mozilla Firefox 24-07-25_15:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695713/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/25 15:59:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3695713/content)


***

### [2024-07-25 16:45:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112596)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:45:33)


***

### [2024-07-25 16:49:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112605)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:49:36)


***

### [2024-07-25 16:49:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112606)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:49:38)


***

### [2024-07-25 16:50:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112609)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:50:34)


***

### [2024-07-25 16:52:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112620)
>Global Post Mortem Event

Event Timestamp: 25/07/24 16:52:05.299
Fill Number: 9931
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799680 [MeV]
Intensity B1/B2: 21344 / 22054 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/25 16:54:56)


***

### [2024-07-25 16:52:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112673)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: acalia / Dumped after physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/25 17:17:19)


***

### [2024-07-25 16:53:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112614)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:53:06)


***

### [2024-07-25 16:53:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112615)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:53:13)


***

### [2024-07-25 16:53:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112616)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:53:14)


***

### [2024-07-25 16:53:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112617)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/25 16:53:16)


