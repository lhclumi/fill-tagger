# FILL 9934
**start**:		 2024-07-26 01:46:46.646988525+02:00 (CERN time)

**end**:		 2024-07-26 05:43:23.055363525+02:00 (CERN time)

**duration**:	 0 days 03:56:36.408375


***

### [2024-07-26 01:46:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112906)
>LHC RUN CTRL: New FILL NUMBER set to 9934

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 01:46:46)


***

### [2024-07-26 01:46:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112905)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 01:46:46)


***

### [2024-07-26 01:47:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112907)
>Tripped going to standby.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/26 01:47:33)

[Set SECTOR23 24-07-26_01:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696610/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/26 01:47:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696610/content)


***

### [2024-07-26 01:48:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112908)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 01:48:53)


***

### [2024-07-26 01:49:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112909)
>Able to restart iwthout issues.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 01:49:33)

[ Equip State 24-07-26_01:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696612/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 01:49:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696612/content)


***

### [2024-07-26 01:52:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112910)
>The main console got completley stuck. I can't even ssh into it and the 
 mouse is not reacting.. I will reboot it.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 01:53:41)

[Terminal - lhcop@cwo-ccc-d2lc:.opt.home.lhcop 24-07-26_01:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696614/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 01:53:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696614/content)


***

### [2024-07-26 02:02:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112911)
>I think I understand what happened with the nQPS reset. The nQPS got started but I thought it hadn't since the machine crashed at the same moment so when I tried to restart it from the other computer it couldn't do it because it was already running.    


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 02:03:37)


***

### [2024-07-26 02:20:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112918)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 02:20:03)


***

### [2024-07-26 02:26:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112921)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 02:26:25)


***

### [2024-07-26 02:27:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112922)
>injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/26 02:27:34)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-26_02:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696654/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/26 02:27:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696654/content)

[Accelerator Cockpit v0.0.37 24-07-26_02:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696656/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 02:28:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696656/content)

[Accelerator Cockpit v0.0.37 24-07-26_02:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696658/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/26 02:28:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696658/content)


***

### [2024-07-26 02:30:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112923)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 02:30:33)


***

### [2024-07-26 02:33:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112924)
>Good injection trajectories.

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/26 02:33:21)

[Desktop 24-07-26_02:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696660/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/26 02:33:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696660/content)


***

### [2024-07-26 03:03:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112926)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:03:07)


***

### [2024-07-26 03:03:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112927)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:03:17)


***

### [2024-07-26 03:03:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112927)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:03:17)


***

### [2024-07-26 03:03:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112928)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:03:21)


***

### [2024-07-26 03:03:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112929)
>LHC Injection Complete

Number of injections actual / planned: 50 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:32:49 / 0:33:48 (97.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/26 03:03:22)


***

### [2024-07-26 03:05:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112930)
>Prepare for ramp.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:05:23)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-26_03:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696682/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:05:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696682/content)

[ LHC Fast BCT v1.3.2 24-07-26_03:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696684/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:05:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696684/content)


***

### [2024-07-26 03:07:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112931)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:07:05)


***

### [2024-07-26 03:07:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112933)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:07:06)


***

### [2024-07-26 03:07:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112935)
>losses start of ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/26 03:07:42)

[ LHC BLM Fixed Display 24-07-26_03:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696686/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/26 03:07:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696686/content)


***

### [2024-07-26 03:28:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112939)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:28:25)


***

### [2024-07-26 03:28:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112941)
>Got a warning that the abort gab should be cleaned..

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:29:12)

[Abort gap cleaning control v2.1.0 24-07-26_03:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696689/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:29:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696689/content)


***

### [2024-07-26 03:31:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112942)
>I cleaned it but hard was just as we reached end of ramp so strange..

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:31:57)

[Abort gap cleaning control v2.1.0 24-07-26_03:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696691/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:31:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696691/content)


***

### [2024-07-26 03:39:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112944)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:39:23)


***

### [2024-07-26 03:39:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112945)
>Triggered a bit more.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:39:58)

[Abort gap cleaning control v2.1.0 24-07-26_03:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696693/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/26 03:39:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696693/content)


***

### [2024-07-26 03:40:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112946)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:40:20)


***

### [2024-07-26 03:41:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112948)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:41:11)


***

### [2024-07-26 03:43:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112949)
>going to collision.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/26 03:44:03)

[ LHC Beam Losses Lifetime Display 24-07-26_03:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696695/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/26 03:44:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696695/content)


***

### [2024-07-26 03:44:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112950)
>optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:44:54)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-26_03:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696697/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:44:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696697/content)


***

### [2024-07-26 03:46:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112951)
>Rather big optimization this time too.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:46:22)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-26_03:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696699/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:46:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696699/content)


***

### [2024-07-26 03:46:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112952)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:46:43)


***

### [2024-07-26 03:53:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112953)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:53:05)


***

### [2024-07-26 03:55:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112954)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 03:55:06)


***

### [2024-07-26 03:58:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112955)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:58:44)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-26_03:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696701/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:58:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696701/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-07-26_03:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696703/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/26 03:58:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3696703/content)


***

### [2024-07-26 05:40:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112961)
>Global Post Mortem Event

Event Timestamp: 26/07/24 05:40:29.560
Fill Number: 9934
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 34361 / 34645 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/26 05:43:24)


***

### [2024-07-26 05:41:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112958)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 05:41:22)


***

### [2024-07-26 05:41:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112959)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 05:41:24)


***

### [2024-07-26 05:43:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4112960)
>LHC RUN CTRL: New FILL NUMBER set to 9935

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/26 05:43:23)


