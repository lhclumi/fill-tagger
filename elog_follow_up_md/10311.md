# FILL 10311
**start**:		 2024-11-03 10:13:39.825863525+01:00 (CERN time)

**end**:		 2024-11-04 02:35:19.967613525+01:00 (CERN time)

**duration**:	 0 days 16:21:40.141750


***

### [2024-11-03 10:13:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176365)
>LHC RUN CTRL: New FILL NUMBER set to 10311

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:13:40)


***

### [2024-11-03 10:20:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176374)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:20:18)


***

### [2024-11-03 10:22:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176375)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:22:17)


***

### [2024-11-03 10:22:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176376)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:22:21)


***

### [2024-11-03 10:23:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176379)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:23:54)


***

### [2024-11-03 10:23:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176380)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:24:00)


***

### [2024-11-03 10:24:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176382)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:24:52)


***

### [2024-11-03 10:25:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176383)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:25:13)


***

### [2024-11-03 10:26:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176385)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:26:03)


***

### [2024-11-03 10:26:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176388)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:26:19)


***

### [2024-11-03 10:26:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176389)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:26:53)


***

### [2024-11-03 10:28:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176392)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:28:28)


***

### [2024-11-03 10:28:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176394)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:28:29)


***

### [2024-11-03 10:28:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176396)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:28:30)


***

### [2024-11-03 10:28:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176400)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:28:45)


***

### [2024-11-03 10:29:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176402)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:29:10)


***

### [2024-11-03 10:29:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176404)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:29:16)


***

### [2024-11-03 10:30:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176405)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:30:44)


***

### [2024-11-03 10:32:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176408)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:32:30)


***

### [2024-11-03 10:36:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176412)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:36:14)


***

### [2024-11-03 10:48:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176419)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:48:09)


***

### [2024-11-03 10:55:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176420)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 10:55:04)


***

### [2024-11-03 11:00:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176423)
>wire scans

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/03 11:00:50)

[ LHC WIRESCANNER APP 24-11-03_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850693/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/03 11:00:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850693/content)

[ LHC WIRESCANNER APP 24-11-03_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850697/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/03 11:00:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850697/content)

[ LHC WIRESCANNER APP 24-11-03_11:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850699/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/03 11:00:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850699/content)

[ LHC WIRESCANNER APP 24-11-03_11:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850701/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/03 11:01:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850701/content)


***

### [2024-11-03 11:22:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176427)
>injected beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:22:33)

[ LHC Fast BCT v1.3.2 24-11-03_11:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850719/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:22:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850719/content)

[ Beam Intensity - v1.5.0 24-11-03_11:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850757/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 12:09:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850757/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-11-03_11:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850759/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 12:09:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850759/content)


***

### [2024-11-03 11:23:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176428)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:23:02)


***

### [2024-11-03 11:23:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176429)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:23:12)


***

### [2024-11-03 11:23:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176430)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:23:17)


***

### [2024-11-03 11:23:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176431)
>LHC Injection Complete

Number of injections actual / planned: 51 / 48
SPS SuperCycle length: 28.8 [s]
Actual / minimum time: 0:28:15 / 0:28:02 (100.7 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/03 11:23:18)


***

### [2024-11-03 11:24:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176432)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:24:47)


***

### [2024-11-03 11:24:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176434)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:24:48)


***

### [2024-11-03 11:34:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176437)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:34:01)


***

### [2024-11-03 11:34:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176437)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:34:01)


***

### [2024-11-03 11:34:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176439)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:34:48)


***

### [2024-11-03 11:35:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176440)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:35:25)


***

### [2024-11-03 11:36:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176442)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:36:08)


***

### [2024-11-03 11:40:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176444)
>Alice is not publishing the lumi, trying to optimize with the BRANs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:41:11)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-11-03_11:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850733/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 12:05:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850733/content)


***

### [2024-11-03 11:41:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176445)
>IP1/5/8 optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:41:54)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-11-03_11:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850735/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:41:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850735/content)


***

### [2024-11-03 11:42:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176446)
>Alice optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:42:10)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-11-03_11:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850737/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 11:42:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850737/content)


***

### [2024-11-03 11:42:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176447)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:42:30)


***

### [2024-11-03 11:50:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176452)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/03 11:50:47)


***

### [2024-11-03 11:54:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176453)
>apply +0.004 in Qh B1 and B2

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/03 11:54:40)

[ LHC Beam Losses Lifetime Display 24-11-03_11:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850741/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/03 11:54:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850741/content)


***

### [2024-11-03 12:05:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176457)
>Alice still have an issue with one server that is provoding the luminosity. I can't start the levelling.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 12:06:13)


***

### [2024-11-03 12:18:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176460)
>Alice lumi is now OK, start the levelling  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 12:19:13)


***

### [2024-11-03 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176520)
>
```
**SHIFT SUMMARY:**  
  
Started and ended the shift in stable beams.  
The plan was to sump after 22 hours of stable beams. Atlas requested to run at low mu for 1h at the end of the fill and then went back to head-on.  
The dump was a bit delayed by an issue in SPS RF. Once they were OK, I dumped and refill.  
Very smooth cycle, tune shift of +0.004 for Qh B1 and B2 for life time improvement.  
  
At the beginning of stable beams Alice had an issue with the lumi publication during ~40 mins. Once solved I could their lumi to target.  
Leaving with IP1/2/8 still in levelling and IP5 head-on.  
  
Delphine  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 14:56:10)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850910/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 14:56:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850910/content)


***

### [2024-11-03 17:02:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176550)
>abort gap got a bit high for beam 1.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:03:02)

[Abort gap cleaning control v2.1.1 24-11-03_17:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850987/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:03:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850987/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-11-03_17:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850989/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:03:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850989/content)


***

### [2024-11-03 17:04:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176552)
>The sequence for beam 1 had stop so it didn't take any step, that is why 
 the bunch became so long and also increased the abort gap.

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/03 17:05:39)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-03_17:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850991/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/03 17:05:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850991/content)


***

### [2024-11-03 17:12:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176554)
>Much improved but still not perfect.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:12:21)

[Abort gap cleaning control v2.1.1 24-11-03_17:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850995/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:12:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850995/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-11-03_17:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850997/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:12:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850997/content)


***

### [2024-11-03 17:15:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176555)
>All good witht he abortgap now after some more cleaning.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:15:40)

[Abort gap cleaning control v2.1.1 24-11-03_17:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850999/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:15:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3850999/content)


***

### [2024-11-03 17:58:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176566)
>Got one of these spikes again.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:58:40)

[Abort gap cleaning control v2.1.1 24-11-03_17:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851014/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/03 17:58:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851014/content)


***

### [2024-11-03 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176635)
>
```
**SHIFT SUMMARY:**  
  
Stable beams the entire shift. The only minor issue was that the sequence that was increasing the voltage for beam 1 had stopped so we didn't increase the voltage -> long bunches -> beam in the abort gap. After cleaning and increasing voltage the problem disappeared.  
  
/Tobias   
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 22:57:20)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851158/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/03 22:57:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851158/content)


***

### [2024-11-04 02:29:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176679)
>Global Post Mortem Event

Event Timestamp: 04/11/24 02:29:59.103
Fill Number: 10311
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 2679480 [MeV]
Intensity B1/B2: 28217 / 29531 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-BCCM A + B Beam 2: B T -> F on CIB.UA47.R4.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/04 02:32:49)


***

### [2024-11-04 02:30:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176717)
>Global Post Mortem Event Confirmation

Dump Classification: RF fault
Operator / Comment: gtrad / Trip of M1B2 in Stable beams (ppref Run) with loss of Cryo conditions in cavities: 2R4


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/04 02:48:33)


***

### [2024-11-04 02:30:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176677)
>Event created from ScreenShot Client.  
LHC RF CONTROL 24-11-04\_02:30.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/04 02:30:29)

[ LHC RF CONTROL 24-11-04_02:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851257/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/04 02:30:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851257/content)

[ LSA Applications Suite (v 16.6.35) 24-11-04_02:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851259/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/04 02:31:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851259/content)

[Monitoring application. Currently monitoring : LHC - [2 subscriptions ] 24-11-04_02:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851261/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/04 02:31:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851261/content)

[BEAM DUMP INTERLOCK B2 24-11-04_02:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851263/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/04 02:31:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851263/content)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-11-04_02:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851265/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/11/04 02:32:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851265/content)

[ LHC Beam Losses Lifetime Display 24-11-04_02:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851273/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/04 02:34:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851273/content)


***

### [2024-11-04 02:30:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176678)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/04 02:30:55)


***

### [2024-11-04 02:33:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176680)
>Power Converter Fault  
  
Device: RPTK.SR4.RA47U2.R4 (RPTK.SR4.RA47U2.R4)  
Time: 2024-11-04 02:29:59,608  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1732614:6519295?archive=1732614:6519296?archive=1732614:6519297?archive=1732614:6519298?archive=1732614:6519299?archive=1732614:6519300)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/04 02:33:27)


***

### [2024-11-04 02:33:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4176681)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.62.5 
 [pro] 24-11-04\_02:33.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/04 02:33:43)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-11-04_02:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851267/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/04 02:33:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851267/content)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-11-04_02:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851269/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/04 02:33:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851269/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-04_02:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851271/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/04 02:34:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851271/content)

[ LHC Fast BCT v1.3.2 24-11-04_02:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851275/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/04 02:34:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851275/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-04_02:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851277/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/04 02:34:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3851277/content)


