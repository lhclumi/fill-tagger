# FILL 10167
**start**:		 2024-09-28 03:36:29.866738525+02:00 (CERN time)

**end**:		 2024-09-28 08:22:50.462238525+02:00 (CERN time)

**duration**:	 0 days 04:46:20.595500


***

### [2024-09-28 03:36:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151786)
>LHC RUN CTRL: New FILL NUMBER set to 10167

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 03:36:30)


***

### [2024-09-28 03:36:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151787)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 03:36:59)


***

### [2024-09-28 03:41:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151790)
>
```
re-configuring the cycle for high intensity (chroma, MO, Q-trim during collisions BP)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 03:47:19)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-09-28_03:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790379/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 03:46:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790379/content)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-09-28_03:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790381/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 03:46:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790381/content)

[ LSA Consistency Check App v0.9.3 connected to server LHC 24-09-28_03:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790383/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 03:46:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790383/content)


***

### [2024-09-28 03:52:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151793)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 03:52:32)


***

### [2024-09-28 04:06:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151797)
>Injection Complete  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:07:54)

[ LHC Fast BCT v1.3.2 24-09-28_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790387/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:06:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790387/content)

[ INJECTION SEQUENCER  v 5.1.11 24-09-28_04:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790389/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:06:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790389/content)


***

### [2024-09-28 04:07:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151798)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:07:53)


***

### [2024-09-28 04:08:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151800)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:08:07)


***

### [2024-09-28 04:08:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151801)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:08:16)


***

### [2024-09-28 04:08:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151802)
>LHC Injection Complete

Number of injections actual / planned: 14 / 12
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:15:45 / 0:12:12 (129.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:08:17)


***

### [2024-09-28 04:10:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151803)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:10:06)


***

### [2024-09-28 04:10:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151805)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:10:07)


***

### [2024-09-28 04:10:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151807)
>BBLR PCInterlock masked

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:11:07)

[ LHC SIS GUI 24-09-28_04:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790391/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:11:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790391/content)


***

### [2024-09-28 04:13:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151808)
>
```
big (~2-3e-2) tune RT trim during the snapback following a long injection plateau
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:16:00)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-28_04:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790403/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/28 04:14:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790403/content)

[ LHC Beam Losses Lifetime Display 24-09-28_04:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790405/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:14:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790405/content)


***

### [2024-09-28 04:31:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151810)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:31:25)


***

### [2024-09-28 04:31:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151812)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:31:45)


***

### [2024-09-28 04:40:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151814)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:40:10)


***

### [2024-09-28 04:40:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151815)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:40:16)


***

### [2024-09-28 04:40:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151816)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:40:55)


***

### [2024-09-28 04:41:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151818)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 04:41:37)


***

### [2024-09-28 04:44:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151819)
>min lifetime = ~50h

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:45:02)

[ LHC Beam Losses Lifetime Display 24-09-28_04:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790419/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 04:45:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790419/content)


***

### [2024-09-28 04:47:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151820)
>optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:47:48)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_04:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790421/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:48:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790421/content)


***

### [2024-09-28 04:48:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151823)
>non-colliding bunches shaking, in particular in B1  
  
increasing the ADT gain in the witness region to the nominal one (0.015 -> 0.04)  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:52:24)

[ TuneViewer Light V5.7.2 24-09-28_04:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790427/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/28 04:48:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790427/content)

[ LSA Applications Suite (v 16.6.29) 24-09-28_04:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790429/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:49:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790429/content)

[ ADT Activity Monitor 24-09-28_04:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790431/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:51:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790431/content)

[ TuneViewer Light V5.7.2 24-09-28_04:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790433/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 04:51:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790433/content)


***

### [2024-09-28 05:01:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151824)
>Unstable bunches of first 12b reduced in bunch length (longitudinal shaving)  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:02:14)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-28_05:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790435/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/28 05:01:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790435/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-09-28_05:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790437/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/28 05:01:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790437/content)

[ LHC Beam Losses Lifetime Display 24-09-28_05:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790439/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 05:02:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790439/content)


***

### [2024-09-28 05:05:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151825)
>at 45cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:05:46)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790441/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:05:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790441/content)


***

### [2024-09-28 05:08:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151826)
>Optmimzed IP1/5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:08:11)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790443/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:08:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790443/content)


***

### [2024-09-28 05:10:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151827)
>
```
increasing tune separation: -3e-3 in B1H and B2H
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:13:35)

[ LHC Beam Losses Lifetime Display 24-09-28_05:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790445/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 05:10:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790445/content)

[Accelerator Cockpit v0.0.38 24-09-28_05:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790453/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/28 05:13:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790453/content)


***

### [2024-09-28 05:12:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151829)
>Event created from ScreenShot Client.  
TuneViewer Light V5.7.2 
 24-09-28\_05:12.png

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/28 05:12:56)

[ TuneViewer Light V5.7.2 24-09-28_05:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790447/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/28 05:12:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790447/content)


***

### [2024-09-28 05:12:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151830)
>Event created from ScreenShot Client.  
TuneViewer Light V5.7.2 
 24-09-28\_05:13.png

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/28 05:13:02)

[ TuneViewer Light V5.7.2 24-09-28_05:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790449/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/09/28 05:13:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790449/content)


***

### [2024-09-28 05:13:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151831)
>pileup > 100 in CMS!

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:13:17)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790451/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:13:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790451/content)


***

### [2024-09-28 05:20:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151833)
>losses - 12b unstable

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:20:54)

[ LHC Fast BCT v1.3.2 24-09-28_05:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790455/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:21:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790455/content)

[ LHC Fast BCT v1.3.2 24-09-28_05:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790457/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:21:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790457/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-28_05:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790459/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:25:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790459/content)


***

### [2024-09-28 05:25:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151834)
>
```
ATLAS lumi at 0 - called them, they are checking
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:31:14)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790461/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:25:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790461/content)


***

### [2024-09-28 05:31:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151837)
>
```
at 30cm  
  
no sign of hierarchy breakage in B2  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:32:06)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790463/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:31:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790463/content)

[ LHC Beam Losses Lifetime Display 24-09-28_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790465/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 05:31:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790465/content)

[ LHC Beam Losses Lifetime Display 24-09-28_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790467/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 05:31:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790467/content)


***

### [2024-09-28 05:34:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151838)
>optimized at 30cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:34:47)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_05:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790469/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:34:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790469/content)


***

### [2024-09-28 05:35:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151839)
>updated physics-end BP with collimator gaps/optics from end of B\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:36:13)

[ LSA Applications Suite (v 16.6.29) 24-09-28_05:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790471/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:36:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790471/content)


***

### [2024-09-28 05:45:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151843)
>ATLAS bunch lumi dropping to zero for certain bunches as well

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:45:59)

[NavPy 1.5.2 24-09-28_05:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790473/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 05:59:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790473/content)


***

### [2024-09-28 06:12:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151850)
>ATLAS total lumi back (with a somewhat questionable scale)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:12:20)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_06:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790494/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:20:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790494/content)


***

### [2024-09-28 06:22:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151854)
>
```
ATLAS still working on lumi - now absolute value looks better, bbb still glitchy
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:12:41)

[ LHC Luminosity Scan Client 0.62.3 [pro] 24-09-28_06:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790510/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:23:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790510/content)


***

### [2024-09-28 06:24:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151855)
>
```
BBLR CW at 75%  
  
Significant tune effect, in particular in H ... is there a problem with the compensation?  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:12:33)

[ Power Converter Interlock GUI 24-09-28_06:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790512/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 06:24:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790512/content)

[ LHC Beam Losses Lifetime Display 24-09-28_06:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790514/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 06:25:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790514/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-28_06:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790516/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/28 06:26:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790516/content)


***

### [2024-09-28 06:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151859)
>|\*\*\* XPOC error has been reset by user 'ydutheil' at 28.09.2024 06:37:11
| Comment: missing data due to crashed FESA class, all OK otherwise
|
| Dump info: BEAM 2 at 28.09.2024 01:45:08
| Beam info: Energy= 449.64 GeV ; Intensity= 5.65E13 p+ ; #Bunches= 348
| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR


creator:	 ydutheil  @cs-ccr-pm3.cern.ch (2024/09/28 06:37:11)


***

### [2024-09-28 06:45:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151860)
>
```
The tune effect is understood. It is a "feature" of how settings are driven during beta* levelling. LumiServer was trimming the BBLR-KNOB level (as well as IPQ K). Since the BBLR-KNOB is flat after 42cm, nothing was done once the wire was switched on. However, unlike standard KNOBs, the BBLR-K still change due to changing TCT gaps even if the KNOB is constant. This was therefore not trimmed on the PHYSICS-END BP (but the correct compensation was always driven, since the IPQ-K were driven), leading to an inconsistency in the LSA BBLR settings hierarchy.  
  
Since the BBLR-K -> IPQ-K MR uses the (relative) KNOB MR, the effect is comparable to the "ghost knobs" we observed when KNOBs are not re-generated but K-level is: The moment the BBLR-K was set to the correct value, it changed the IPQ-K in relative removing part of the tune compensation (the difference between the tune compensation at 42cm and 30cm, ~15% of the total effect) which lead to the observed tune effect.  
  
IPQs settings from end-of-BetaStarLevelling reloaded and BBLR-K set to the correct value wihout propagation -> LSA BBLR consistency re-established. Tune effect fixed.  
  
For a possible next fill, made a release of LumiServer to also trim BBLR-K level (in addition to BBLR-KNOB) on the Physics-END BP. To be deployed during the ramp-down...  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:38:16)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-28_06:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790532/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/28 06:45:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790532/content)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790536/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:46:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790536/content)

[Parameter Hierarchy 24-09-28_06:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790542/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:41:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790542/content)

[ LSA Applications Suite (v 16.6.29) 24-09-28_07:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790554/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:16:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790554/content)

[ LSA Applications Suite (v 16.6.29) 24-09-28_07:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790562/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:19:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790562/content)


***

### [2024-09-28 06:48:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151861)
>BBLR at 0.75

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:48:13)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790538/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:49:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790538/content)


***

### [2024-09-28 06:48:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151861)
>BBLR at 0.75

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:48:13)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790538/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:49:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790538/content)


***

### [2024-09-28 06:49:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151862)
>BBLR at 0.5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:49:43)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790540/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:49:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790540/content)


***

### [2024-09-28 06:50:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151863)
>BBLR at 0.25

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:50:31)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790544/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:50:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790544/content)


***

### [2024-09-28 06:51:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151864)
>BBLR at 0

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 06:52:03)

[ LSA Applications Suite (v 16.6.29) 24-09-28_06:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790546/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 07:03:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790546/content)


***

### [2024-09-28 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151781)
>
```
**MD6925 Summary**  
  
**Electron cloud coupled-bunch tune shifts at injection**  
  
MD was a big success, with the trains of 2.3e11 p/b being happily stable with chromaticities of 10, octupoles at -2 and a reduced adt gain (half).   
  
Several fills were made at the originally planned intensities: 2.3, 2.0, 1.6, 1.2 10^11 protons per bunch. Two of the fills were dumped from RF trips and another 3 of them were dumped from unusual instabilities when doing the kicks. Nevertheless, all of the planned data could be recorded:  
  
Bunches were kicked:    
- Train-by-train  
- Batch-by-batch  
- Individually  
- Two at the time (keeping one of them always the same)  
- With windows increasing in length  
- With increasing kick amplitudes (through the number of turns of the excitation)  
Detailed analysis of the bunch-by-bunch tunes is to follow.  
  
During the last fill, the ADTACDipole buffers of the adtobsbox stopped recording. They restarted after a reboot of the adtobs2dev.  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/28 03:34:33)


***

### [2024-09-28 07:04:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151865)
>opened TCT limits for crossing angle reduction

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 07:04:38)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-28_07:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790548/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 07:04:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790548/content)


***

### [2024-09-28 07:06:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151866)
>Lifetime with xing at 140urad

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:06:52)

[ LHC Beam Losses Lifetime Display 24-09-28_07:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790550/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:06:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790550/content)


***

### [2024-09-28 07:08:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151867)
>lifetime at xing 130urad

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:08:16)

[ LHC Beam Losses Lifetime Display 24-09-28_07:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790552/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:08:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790552/content)


***

### [2024-09-28 07:16:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151868)
>lifetime after switching ON BBLR at 130urad

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:16:49)

[ LHC Beam Losses Lifetime Display 24-09-28_07:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790556/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:16:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790556/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-28_07:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790558/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/28 07:16:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790558/content)


***

### [2024-09-28 07:29:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151870)
>lifetime at 130urad wires OFF

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:29:50)

[ LHC Beam Losses Lifetime Display 24-09-28_07:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790564/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:29:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790564/content)


***

### [2024-09-28 07:43:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151871)
>
```
Hierarchy barely broken with tight settings
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:45:41)

[ LHC BLM Fixed Display 24-09-28_07:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790566/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/28 07:44:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790566/content)

[ LHC Beam Losses Lifetime Display 24-09-28_07:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790568/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:44:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790568/content)


***

### [2024-09-28 07:43:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151872)
>no drastic change in lifetime after tightening the collimators

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:44:12)

[ LHC Beam Losses Lifetime Display 24-09-28_07:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790570/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:44:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790570/content)


***

### [2024-09-28 07:47:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151873)
>copied coll settings from col10 tophysics end, no MR no drive

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 07:47:29)

[ LSA Applications Suite (v 16.6.29) 24-09-28_07:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790572/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/28 07:47:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790572/content)


***

### [2024-09-28 07:49:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151874)
>Slight improvement after putting the wire on

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/28 07:49:19)

[ LHC BLM Fixed Display 24-09-28_07:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790574/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/28 07:49:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790574/content)


***

### [2024-09-28 07:49:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151875)
>lifetime after switching the wires ON

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:49:25)

[ LHC Beam Losses Lifetime Display 24-09-28_07:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790576/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 07:49:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790576/content)


***

### [2024-09-28 07:49:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151876)
>hierarchy with BBLR ON

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:49:58)

[ LHC Beam Losses Lifetime Display 24-09-28_07:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790578/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 07:50:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790578/content)


***

### [2024-09-28 08:00:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151877)
>lifetime wires OFF

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 08:00:18)

[ LHC Beam Losses Lifetime Display 24-09-28_08:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790580/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/28 08:00:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790580/content)


***

### [2024-09-28 08:09:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151878)
>TCSG.D4R7.B2 retracted by 0.5 sigma

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 08:09:36)

[ LHC Beam Losses Lifetime Display 24-09-28_08:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790582/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 08:09:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790582/content)


***

### [2024-09-28 08:11:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151879)
>
```
Both jaws of TCSG.D4 were put 100um out, then scanned the bottom jaw until 
    warning (then moved to nominal settings). Afterwards (after this screenshot) did the same for the top jaw. Hierarchy breaking appears at 6.22 sigma.
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 08:28:37)

[LHC Collimator Control Application -  INCA DISABLED - PRO CCDA 24-09-28_08:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790584/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/28 08:12:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790584/content)


***

### [2024-09-28 08:17:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151883)
>Global Post Mortem Event

Event Timestamp: 28/09/24 08:17:26.065
Fill Number: 10167
Accelerator / beam mode: MACHINE DEVELOPMENT / ADJUST
Energy: 6799440 [MeV]
Intensity B1/B2: 1669 / 1720 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/28 08:20:17)


***

### [2024-09-28 08:17:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151919)
>Global Post Mortem Event Confirmation

Dump Classification: Machine Development
Operator / Comment: acalia / programmed dump after fill for MD12663


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/28 08:35:37)


***

### [2024-09-28 08:17:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151880)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 08:17:45)


***

### [2024-09-28 08:17:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151881)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 08:17:47)


***

### [2024-09-28 08:17:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151882)
>LumiServer with the fix for the BBLR-K trim deployed  
  
-- Michi  


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/09/28 08:18:12)


***

### [2024-09-28 08:22:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151884)
>LHC RUN CTRL: New FILL NUMBER set to 10168

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/28 08:22:51)


