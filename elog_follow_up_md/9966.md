# FILL 9966
**start**:		 2024-08-01 17:20:02.274738525+02:00 (CERN time)

**end**:		 2024-08-01 21:30:10.512488525+02:00 (CERN time)

**duration**:	 0 days 04:10:08.237750


***

### [2024-08-01 17:20:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117560)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 17:20:31)


***

### [2024-08-01 17:32:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117571)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 17:32:56)


***

### [2024-08-01 17:57:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117584)
>GEORGES TRAD(GTRAD) assigned RBAC Role: CO-Timing-PIQUET and will expire on: 01-AUG-24 11.57.03.165000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/01 17:57:04)


***

### [2024-08-01 18:04:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117589)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:04:44)


***

### [2024-08-01 18:05:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117590)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:05:34)


***

### [2024-08-01 18:08:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117593)
>B2 scan A at injection

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:08:42)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708817/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:08:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708817/content)


***

### [2024-08-01 18:10:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117594)
>Beam 1 scan A at injection

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:10:53)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708819/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:10:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708819/content)


***

### [2024-08-01 18:13:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117597)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:13:43)


***

### [2024-08-01 18:13:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117598)
>LHC Injection Complete

Number of injections actual / planned: 29 / 24
SPS SuperCycle length: 28.8 [s]
Actual / minimum time: 0:40:48 / 0:16:31 (247.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 18:13:44)


***

### [2024-08-01 18:17:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117600)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:17:26)


***

### [2024-08-01 18:17:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117602)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:17:26)


***

### [2024-08-01 18:38:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117611)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:38:44)


***

### [2024-08-01 18:46:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117614)
>Beam 1 Scan B at Injection

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:17)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708842/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708842/content)


***

### [2024-08-01 18:46:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117615)
>Beam 2 Scan B at Injection

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:28)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708844/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708844/content)


***

### [2024-08-01 18:46:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117616)
>Beam 2 Scan A at FlatTop

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:41)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708846/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708846/content)


***

### [2024-08-01 18:46:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117617)
>Beam 1 Scan A at FlatTop

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:55)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708848/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:46:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708848/content)


***

### [2024-08-01 18:47:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117618)
>Beam 2 Scan B at FlatTop

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:47:47)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-08-01_18:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708850/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/01 18:47:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708850/content)


***

### [2024-08-01 18:51:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117620)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 18:51:14)


***

### [2024-08-01 19:12:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117627)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:12:25)


***

### [2024-08-01 19:13:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117628)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:13:35)


***

### [2024-08-01 19:14:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117630)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:14:27)


***

### [2024-08-01 19:16:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117633)
>In collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:16:58)

[ LHC Beam Losses Lifetime Display 24-08-01_19:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708868/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:16:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708868/content)


***

### [2024-08-01 19:18:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117635)
>OPtimizing all IPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:19:07)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_19:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708872/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:19:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708872/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_19:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708874/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:19:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708874/content)


***

### [2024-08-01 19:20:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117636)
>aborted optimization of IP2 as we are getting close to COD interlock

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:21:10)

[ Power Converter Interlock GUI 24-08-01_19:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708876/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:21:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708876/content)


***

### [2024-08-01 19:21:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117637)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:21:28)


***

### [2024-08-01 19:21:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117637)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:21:28)


***

### [2024-08-01 19:27:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117638)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:27:57)


***

### [2024-08-01 19:29:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117639)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 19:29:55)


***

### [2024-08-01 19:33:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117640)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.61.1 
 [pro] 24-08-01\_19:33.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:34:00)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_19:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708878/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:34:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708878/content)


***

### [2024-08-01 19:34:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117641)
>XRPs inserted

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:34:36)

[COLLIMATORS_VISTAR_MOV 24-08-01_19:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708880/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 19:34:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708880/content)


***

### [2024-08-01 19:41:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117642)
>Emittance scan completed in ATLAS

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:41:51)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_19:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708882/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:41:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708882/content)


***

### [2024-08-01 19:54:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117643)
>scan completed in IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:54:26)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708884/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 19:54:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708884/content)


***

### [2024-08-01 20:23:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117649)
>Optimized IP 1 and IP 5 atbeta\* 39cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:23:23)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:23:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708902/content)


***

### [2024-08-01 20:29:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117650)
>beta\* 30 cm reached

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:29:37)


***

### [2024-08-01 20:30:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117651)
>Increasing Chroma to avoid further non colliding bunches instabilities

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:30:50)

[ LSA Applications Suite (v 16.6.13) 24-08-01_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708914/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:30:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708914/content)


***

### [2024-08-01 20:32:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117654)
>Optimized IP1 and IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:32:14)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_20:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708934/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:32:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708934/content)


***

### [2024-08-01 20:32:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117655)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.61.1 
 [pro] 24-08-01\_20:32.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:32:52)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_20:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708938/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:32:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708938/content)


***

### [2024-08-01 20:55:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117659)
>Emittance scan in IP1 completed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:55:41)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_20:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708964/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 20:55:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708964/content)


***

### [2024-08-01 21:14:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117660)
>Emittance scan completed in IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 21:14:44)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-01_21:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708968/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/01 21:14:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708968/content)


***

### [2024-08-01 21:19:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117663)
>
```

```
**Summary of BSRT calibration fill**Calibration fill started at 17:30 after beam tuning in the injectors.  
Injected 11 bunches (intensity 1.2e11) with emittance in range 1um, 2um, 3um and one INDIV with nominal intensity (1.6e11) and 2um emittance.  
  

```
Scan of BSRT lens position at injection energy. Ramp and several lens scans at FT and SB with beta* 120cm, 60cm and 30cm. Continuous wire scanner measurements performed during the fill. Online analysis hints drift of the focusing position of B1 and new calibration factors for B2, to be computed in offline analysis.  
  
In parallel, emittance scans, tests with the coronagraph and the new wire scanners.  
  
Daniele, Georges and Jan  
  
  
  

```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/08/01 21:36:43)

[cwe-513-vpl878.cern.ch:1 (dbutti) - TigerVNC 24-08-01_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708972/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/08/01 21:20:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708972/content)


***

### [2024-08-01 21:24:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117664)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:24:02)


***

### [2024-08-01 21:25:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117665)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:25:33)


***

### [2024-08-01 21:28:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117666)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:28:00)


***

### [2024-08-01 21:28:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117667)
>XRP out

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 21:28:56)

[COLLIMATORS_VISTAR_MOV 24-08-01_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708976/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/01 21:28:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3708976/content)


***

### [2024-08-01 21:29:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117668)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:29:02)


***

### [2024-08-01 21:29:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117680)
>Global Post Mortem Event

Event Timestamp: 01/08/24 21:29:16.225
Fill Number: 9966
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 140 / 142 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/01 21:32:04)


***

### [2024-08-01 21:29:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117669)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:29:32)


***

### [2024-08-01 21:29:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117670)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:29:35)


***

### [2024-08-01 21:29:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117671)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:29:37)


***

### [2024-08-01 21:29:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4117672)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/01 21:29:38)


