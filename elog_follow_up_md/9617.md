# FILL 9617
**start**:		 2024-05-12 18:02:41.004613525+02:00 (CERN time)

**end**:		 2024-05-12 18:14:47.139238525+02:00 (CERN time)

**duration**:	 0 days 00:12:06.134625


***

### [2024-05-12 18:02:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065899)
>LHC RUN CTRL: New FILL NUMBER set to 9617

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/12 18:02:41)


***

### [2024-05-12 18:05:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065900)
>Event created from ScreenShot Client.  
 LHC Sequencer Execution GUI (PRO) : 12.32.0 24-05-12\_18:05.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/12 18:05:27)


***

### [2024-05-12 18:05:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065901)
>
```
odd state for current check, replaying the precycle sequence from the start
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/12 18:09:43)

[ LHC Sequencer Execution GUI (PRO) : 12.32.0  24-05-12_18:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587017/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/12 18:09:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587017/content)


***

### [2024-05-12 18:07:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065902)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/12 18:07:50)


***

### [2024-05-12 18:14:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4065904)
>LHC RUN CTRL: New FILL NUMBER set to 9618

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/12 18:14:47)


