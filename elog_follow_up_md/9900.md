# FILL 9900
**start**:		 2024-07-14 19:18:48.922613525+02:00 (CERN time)

**end**:		 2024-07-15 11:41:26.955488525+02:00 (CERN time)

**duration**:	 0 days 16:22:38.032875


***

### [2024-07-14 19:18:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106055)
>LHC RUN CTRL: New FILL NUMBER set to 9900

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:18:50)


***

### [2024-07-14 19:21:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106056)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:21:30)


***

### [2024-07-14 19:24:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106062)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:24:27)


***

### [2024-07-14 19:24:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106064)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:24:52)


***

### [2024-07-14 19:26:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106065)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:26:22)


***

### [2024-07-14 19:27:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106066)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:27:05)


***

### [2024-07-14 19:27:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106068)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:27:22)


***

### [2024-07-14 19:28:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106069)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:28:12)


***

### [2024-07-14 19:28:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106072)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:28:44)


***

### [2024-07-14 19:29:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106074)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:29:18)


***

### [2024-07-14 19:30:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106075)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:30:15)


***

### [2024-07-14 19:31:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106077)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:31:01)


***

### [2024-07-14 19:31:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106079)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:31:07)


***

### [2024-07-14 19:31:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106081)
>Event created from ScreenShot Client.  
Cannot copy results 
 24-07-14\_19:31.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:31:31)

[Cannot copy results  24-07-14_19:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677796/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:31:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677796/content)


***

### [2024-07-14 19:32:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106082)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:32:40)


***

### [2024-07-14 19:32:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106084)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:32:41)


***

### [2024-07-14 19:32:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106086)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:32:42)


***

### [2024-07-14 19:32:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106090)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:32:57)


***

### [2024-07-14 19:34:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106092)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:34:55)


***

### [2024-07-14 19:36:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106094)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:36:39)


***

### [2024-07-14 19:37:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106096)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:37:07)


***

### [2024-07-14 19:38:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106097)
>will skip QPS reset

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/14 19:38:58)

[ QpsLastResetApp 24-07-14_19:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677800/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/14 19:38:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677800/content)


***

### [2024-07-14 19:54:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106099)
>resetting QPS board of MB.A9R8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:54:43)

[H - MB.A9R8 DQAMC N type MB for dipole MB.A9R8    24-07-14_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677806/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:54:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677806/content)


***

### [2024-07-14 19:55:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106100)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 19:55:13)


***

### [2024-07-14 19:55:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106101)
>reset OK

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:55:47)

[ LHC CIRCUIT SUPERVISION v9.0 24-07-14_19:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677808/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 19:55:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677808/content)


***

### [2024-07-14 19:57:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106102)
>corrections on pilots

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 19:58:06)

[RF Trim Warning 24-07-14_19:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677810/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 19:58:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677810/content)

[Accelerator Cockpit v0.0.37 24-07-14_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677812/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 19:58:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677812/content)

[Accelerator Cockpit v0.0.37 24-07-14_19:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677814/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 19:59:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677814/content)

[Accelerator Cockpit v0.0.37 24-07-14_20:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677816/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 20:00:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677816/content)

[Accelerator Cockpit v0.0.37 24-07-14_20:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677818/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 20:01:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677818/content)

[Accelerator Cockpit v0.0.37 24-07-14_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677820/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 20:02:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677820/content)


***

### [2024-07-14 20:09:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106103)
>issue with SPS TT40  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:09:42)


***

### [2024-07-14 20:10:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106104)
>after large phase correction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 20:11:02)

[Accelerator Cockpit v0.0.37 24-07-14_20:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677822/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/14 20:11:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677822/content)


***

### [2024-07-14 20:12:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106105)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 20:12:12)


***

### [2024-07-14 20:14:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106106)
>Event created from ScreenShot Client.  
OpenYASP DV LHCB1Transfer . 
 LHC\_3inj\_Nom\_48b\_Q20\_2024\_V1 24-07-14\_20:14.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:14:15)

[OpenYASP DV LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 24-07-14_20:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677824/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:14:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677824/content)


***

### [2024-07-14 20:18:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106107)
>TL correction on B2

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:18:55)

[OpenYASP V4.8.17   LHCB2Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-07-14_20:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677826/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:18:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677826/content)


***

### [2024-07-14 20:20:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106108)
>Event created from ScreenShot Client.  
OpenYASP DV LHCB2Transfer . 
 LHC\_3inj\_BCMS\_Q20\_2024\_V1 24-07-14\_20:20.png

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:20:04)

[OpenYASP DV LHCB2Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-14_20:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677828/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/14 20:20:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677828/content)


***

### [2024-07-14 20:20:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106109)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:20:48)

[tmpScreenshot_1720981248.5282853.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677830/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:20:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677830/content)


***

### [2024-07-14 20:21:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106110)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:05)

[tmpScreenshot_1720981265.2404974.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677832/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677832/content)


***

### [2024-07-14 20:21:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106111)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:30)

[tmpScreenshot_1720981290.5546055.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677834/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677834/content)


***

### [2024-07-14 20:21:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106112)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:44)

[tmpScreenshot_1720981304.3448865.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677836/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:21:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677836/content)


***

### [2024-07-14 20:23:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106190)
>Event created from ScreenShot Client.  
RF Trim Warning 24-07-14\_20:23.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/15 06:39:03)

[RF Trim Warning 24-07-14_20:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677953/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/15 06:39:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677953/content)


***

### [2024-07-14 20:50:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106120)
>issue with RF cavity 800 MHz in SPS  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:50:15)


***

### [2024-07-14 20:57:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106121)
>status end of injection

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:57:54)

[ LHC Fast BCT v1.3.2 24-07-14_20:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677854/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:58:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677854/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_20:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677856/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 20:57:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677856/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_20:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677858/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 20:58:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677858/content)

[SPS Beam Quality Monitor - SPS.USER.LHC4 - SPS PRO INCA server - PRO CCDA 24-07-14_20:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677860/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 20:58:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677860/content)

[Mozilla Firefox 24-07-14_21:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677866/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:18:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677866/content)


***

### [2024-07-14 20:58:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106122)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 20:58:07)


***

### [2024-07-14 20:58:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106123)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 20:58:16)


***

### [2024-07-14 20:58:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106124)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 20:58:20)


***

### [2024-07-14 20:58:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106125)
>LHC Injection Complete

Number of injections actual / planned: 68 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:46:09 / 0:33:48 (136.5 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 20:58:21)


***

### [2024-07-14 21:01:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106126)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:01:57)


***

### [2024-07-14 21:01:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106128)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:01:58)


***

### [2024-07-14 21:12:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106131)
>BLM warning at start ramp in IR3 (35%)

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:13:13)

[ LHC BLM Fixed Display 24-07-14_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677864/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:13:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677864/content)


***

### [2024-07-14 21:21:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106132)
>tune FB B2V off at ~6.3 TeV

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:22:04)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-14_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677868/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:22:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677868/content)


***

### [2024-07-14 21:23:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106133)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:23:18)


***

### [2024-07-14 21:23:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106135)
>turning FB on again for B2V as signal better

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:23:55)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-14_21:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677870/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/14 21:23:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677870/content)


***

### [2024-07-14 21:24:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106136)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:24:02)


***

### [2024-07-14 21:24:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106138)
>status flat top

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:24:11)

[ LHC Fast BCT v1.3.2 24-07-14_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677872/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:24:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677872/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677874/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 21:24:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677874/content)

[ LHC Beam Losses Lifetime Display 24-07-14_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677876/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 21:24:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677876/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_21:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677878/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:25:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677878/content)


***

### [2024-07-14 21:39:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106139)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:39:12)


***

### [2024-07-14 21:39:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106140)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:39:56)


***

### [2024-07-14 21:40:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106142)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:40:44)


***

### [2024-07-14 21:44:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106143)
>Event created from ScreenShot Client.  
LHC Luminosity Scan Client 0.60.1 
 [pro] 24-07-14\_21:44.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:45:53)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677880/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:48:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677880/content)


***

### [2024-07-14 21:47:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106144)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:47:16)


***

### [2024-07-14 21:48:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106145)
>tune trims +2E-3 in B1h and B1V -> beneficial -> kept

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 21:48:28)

[ LHC Beam Losses Lifetime Display 24-07-14_21:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677882/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 21:48:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677882/content)

[Accelerator Cockpit v0.0.37 24-07-14_21:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677884/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:48:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677884/content)


***

### [2024-07-14 21:49:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106148)
>IP optimizations

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:49:14)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677886/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:53:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677886/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_21:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677888/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:49:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677888/content)


***

### [2024-07-14 21:53:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106151)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:53:35)


***

### [2024-07-14 21:55:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106152)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:55:33)


***

### [2024-07-14 21:55:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106152)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 21:55:33)


***

### [2024-07-14 21:57:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106153)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:58:05)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677890/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 21:58:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677890/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_22:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677892/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:02:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677892/content)


***

### [2024-07-14 22:24:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106156)
>status at 56 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:25:06)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-14_22:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677896/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:25:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677896/content)

[Desktop 24-07-14_22:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677898/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:25:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677898/content)

[ LHC Fast BCT v1.3.2 24-07-14_22:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677900/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:31:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677900/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-14_22:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/14 22:25:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677902/content)

[ LHC Beam Losses Lifetime Display 24-07-14_22:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677904/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/14 22:25:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677904/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-14_22:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677906/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/14 22:26:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677906/content)


***

### [2024-07-14 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106162)
>
```
**SHIFT SUMMARY:**  
  
Arrived in stable beams, program dumped, refilled, ramped, squeezed and left the machine in stable beams.
```
  

```
  
  
  
* To be noted:

  
- emittance blow up at the end of the last train of B1 (due to wires turned ON?)  
- again bunch flattening caused losses and the need to perform several abort gap cleaning (losses reached 32%)  
- had to do a reset of QPS on MB.A9R8  
- Tune feedback B2V switched off at the end of the ramp  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/14 23:02:45)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677920/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/14 23:02:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677920/content)


***

### [2024-07-15 01:26:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106169)
>went down with the horizontal tune imporved lifetime of beam 1.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/15 01:26:38)

[ LHC Beam Losses Lifetime Display 24-07-15_01:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677929/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/15 01:26:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677929/content)


***

### [2024-07-15 03:55:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106177)
>Several abort gap cleaning needed after the latest longitudinal blowup

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/15 03:55:37)

[Abort gap cleaning control v2.0.8 24-07-15_03:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677936/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/15 03:55:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677936/content)


***

### [2024-07-15 04:28:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106178)
>Optimization didn't do much in IR5.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 04:28:59)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-15_04:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677938/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 04:28:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677938/content)


***

### [2024-07-15 05:03:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106179)
>Around 1.4 inverse femtobarn the last 24hours!

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 05:04:10)

[udp:..multicast-bevlhc3:1234 - VLC media player 24-07-15_05:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677940/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 05:04:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677940/content)


***

### [2024-07-15 05:07:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106180)
>LRBB wire is o. Optimized tunes. Going up with horinzontal to 0.316 for 
 beam 1 was the only thing that gave significant improvement.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/15 05:08:07)

[ LHC Beam Losses Lifetime Display 24-07-15_05:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677942/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/15 05:08:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677942/content)


***

### [2024-07-15 06:57:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106194)
>
```
B1 QH feed-forward - ~40% from 39cm -> 30cm  
  
Total trim was -3e-3, FF is just above -1e-3.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 07:16:18)

[ LSA Applications Suite (v 16.6.7) 24-07-15_07:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677956/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 07:13:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677956/content)


***

### [2024-07-15 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106193)
>
```
**SHIFT SUMMARY:**  
-Arrived in stable beams.  
-Reduced horizontal tune at 36cm for beam1 h and then increased it again after the LRBB wires were turned on. Part of the tune optimization is now feed-forward for the next fill.   
-Very good last 24h with around 1.4 inverse femtobarns!  
-Leaving in stable beams  
/Tobias  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 06:58:32)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677955/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 06:58:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677955/content)


***

### [2024-07-15 07:46:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106197)
>
```
another good back-to-back fill and MKI temperature threshold will be hit...
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 07:48:27)

[ LHC MKI Temperature Display 24-07-15_07:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677962/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/07/15 07:47:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677962/content)


***

### [2024-07-15 09:50:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106242)
>QFB debugging for Diogo

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 09:51:02)

[  24-07-15_09:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678140/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 09:51:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678140/content)

[  24-07-15_09:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678144/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 09:53:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678144/content)

[  24-07-15_09:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678156/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 09:57:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678156/content)

[  24-07-15_10:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678180/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 10:09:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678180/content)


***

### [2024-07-15 11:31:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106296)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:31:24)


***

### [2024-07-15 11:31:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106297)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:31:25)


***

### [2024-07-15 11:33:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106299)
>emit scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 11:33:36)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-15_11:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678374/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 11:33:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678374/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-15_11:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678376/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/15 11:33:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3678376/content)


***

### [2024-07-15 11:33:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106300)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:33:53)


***

### [2024-07-15 11:38:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106310)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:38:53)


***

### [2024-07-15 11:39:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106319)
>Global Post Mortem Event

Event Timestamp: 15/07/24 11:39:03.307
Fill Number: 9900
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799680 [MeV]
Intensity B1/B2: 21496 / 22313 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/15 11:41:52)


***

### [2024-07-15 11:39:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106370)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: mihostet / end of physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/15 11:58:19)


***

### [2024-07-15 11:39:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106311)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:39:47)


***

### [2024-07-15 11:39:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106313)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:39:54)


***

### [2024-07-15 11:40:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106314)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:40:02)


***

### [2024-07-15 11:40:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106315)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:40:04)


***

### [2024-07-15 11:40:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4106316)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/15 11:40:34)


