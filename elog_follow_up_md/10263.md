# FILL 10263
**start**:		 2024-10-20 12:12:30.573488525+02:00 (CERN time)

**end**:		 2024-10-20 14:15:20.440488525+02:00 (CERN time)

**duration**:	 0 days 02:02:49.867000


***

### [2024-10-20 12:12:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167712)
>LHC RUN CTRL: New FILL NUMBER set to 10263

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 12:12:31)


***

### [2024-10-20 12:12:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167713)
>
```
LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM  
  
Pilots in slots 1 and 32  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 12:22:18)


***

### [2024-10-20 12:15:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167715)
>tunes with probes

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:15:41)

[ Accelerator Cockpit v0.0.41 24-10-20_12:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831637/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:15:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831637/content)


***

### [2024-10-20 12:16:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167716)
>after correction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:16:12)

[ Accelerator Cockpit v0.0.41 24-10-20_12:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831639/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:16:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831639/content)


***

### [2024-10-20 12:16:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167717)
>chroma

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:16:44)

[ Accelerator Cockpit v0.0.41 24-10-20_12:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831641/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:16:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831641/content)


***

### [2024-10-20 12:17:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167718)
>after correction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:17:17)

[ Accelerator Cockpit v0.0.41 24-10-20_12:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831643/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:17:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831643/content)


***

### [2024-10-20 12:18:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167719)
>coupling

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:18:09)

[ Accelerator Cockpit v0.0.41 24-10-20_12:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831645/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:18:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831645/content)


***

### [2024-10-20 12:19:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167720)
>after corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:19:19)

[ Accelerator Cockpit v0.0.41 24-10-20_12:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831647/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/20 12:19:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831647/content)


***

### [2024-10-20 12:21:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167721)
>after all corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/20 12:21:14)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-20_12:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831649/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/20 12:21:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831649/content)


***

### [2024-10-20 12:23:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167723)
>pilot B1 H

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/20 12:24:03)

[Figure 0 24-10-20_12:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831653/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/20 12:24:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831653/content)


***

### [2024-10-20 12:24:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167722)
>
```
second kick with ADT not damping
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/10/20 12:25:20)

[ TRANSVERSE DAMPER CONTROL 24-10-20_12:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831651/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/10/20 12:23:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831651/content)

[Figure 0 24-10-20_12:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831657/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/20 12:27:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831657/content)

[Figure 0 24-10-20_12:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831659/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/20 12:28:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831659/content)

[Figure 0 24-10-20_12:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831661/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/20 12:35:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831661/content)


***

### [2024-10-20 13:09:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167731)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 13:09:07)


***

### [2024-10-20 13:09:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167732)
>Q' = 10, MO = -2 for nominals (and later 8b4e trains)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:09:33)

[ LSA Applications Suite (v 16.6.34) 24-10-20_13:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831669/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:09:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831669/content)


***

### [2024-10-20 13:12:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167734)
>first indivs of 1.75e11 in

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:12:35)

[ LHC Fast BCT v1.3.2 24-10-20_13:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831671/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:40:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831671/content)


***

### [2024-10-20 13:40:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167737)
>INDIVs of 1.62e11

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:40:52)

[ LHC Fast BCT v1.3.2 24-10-20_13:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831676/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 13:44:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831676/content)


***

### [2024-10-20 14:00:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167743)
>
```
high intensity indivs (~2e11 + 2.2e11)  
  
ADT bunch intensity interlock masked.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 14:01:24)

[ LHC Fast BCT v1.3.2 24-10-20_14:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831682/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/20 14:01:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831682/content)

[ LHC SIS GUI 24-10-20_14:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831684/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/20 14:01:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3831684/content)


***

### [2024-10-20 14:15:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167750)
>LHC RUN CTRL: New FILL NUMBER set to 10264

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 14:15:21)


