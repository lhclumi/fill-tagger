# FILL 9976
**start**:		 2024-08-05 16:13:54.478738525+02:00 (CERN time)

**end**:		 2024-08-05 18:33:16.439988525+02:00 (CERN time)

**duration**:	 0 days 02:19:21.961250


***

### [2024-08-05 16:13:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119440)
>LHC RUN CTRL: New FILL NUMBER set to 9976

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 16:13:56)


***

### [2024-08-05 16:16:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119442)
>
```
Tripped RB.A78 and RQS.R2B2 starting the precycle
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 16:22:53)

[Circuit_RB_A78:   24-08-05_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713139/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 16:16:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713139/content)

[8 - RPTE.UA83.RB.A78 Power Converter for the Circuit RB.A78    24-08-05_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713141/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 16:16:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713141/content)

[ Equip State 24-08-05_16:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713143/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 16:16:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713143/content)

[Set SECTOR78 24-08-05_16:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713153/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/05 16:22:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713153/content)

[Set SECTOR23 24-08-05_16:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713155/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/05 16:22:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713155/content)


***

### [2024-08-05 17:20:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119484)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 17:20:32)


***

### [2024-08-05 17:20:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119485)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 17:20:39)


***

### [2024-08-05 17:47:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119498)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P8 - service areas |
| Peoples : | * Joao Carlos Letra Simoes (EN/EL/CBS)165168 / 71875 - <joao.simoes@cern.ch> |
| Duration | 2h - **Access Needed** - |
| Access to US 85 for intervention on ETZ101/85 | |



creator:	 mblanger  @paas-standard-avz-a-m5xvl.cern.ch (2024/08/05 17:47:59)


***

### [2024-08-05 18:27:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119502)
>EPC piquet access in finished. RQS.R2B2 is repaired (RB.A78 was reset by them remotely)  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 18:28:14)

[Circuit_RQS_R2B2:   24-08-05_18:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713350/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 18:28:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713350/content)

[Circuit_RB_A78:   24-08-05_18:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713352/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 18:28:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713352/content)


***

### [2024-08-05 18:33:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119503)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:33:15)


***

### [2024-08-05 18:33:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119504)
>LHC RUN CTRL: New FILL NUMBER set to 9977

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:33:17)


