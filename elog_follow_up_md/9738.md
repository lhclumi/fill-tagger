# FILL 9738
**start**:		 2024-06-08 18:56:06.354988525+02:00 (CERN time)

**end**:		 2024-06-08 21:29:13.190613525+02:00 (CERN time)

**duration**:	 0 days 02:33:06.835625


***

### [2024-06-08 18:56:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084065)
>LHC RUN CTRL: New FILL NUMBER set to 9738

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 18:56:06)


***

### [2024-06-08 18:58:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084071)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 18:58:57)


***

### [2024-06-08 19:01:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084072)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:01:26)


***

### [2024-06-08 19:01:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084072)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:01:26)


***

### [2024-06-08 19:01:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084074)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:01:49)


***

### [2024-06-08 19:02:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084075)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:02:49)


***

### [2024-06-08 19:03:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084097)
>
```
**MD11243 -- HL-LHC cycle test Summary**(A. Donadon,L. Giacomel, M. Hofstettler, B. Lindstrom, R. De Maria, D. Mirarchi, M. Monikowska, A. Vella, D. Veres, F. Van Der Veken)  
  
Second part of MD on new HL-LHC optics. Focus was on collimator alignment and measurement of cleaning, as well as checking the combined ramp and desqueeze with tight colilmator settings.  
  
Alignment at injection showed consistent results with nominal optics. Ramp to FT with nominal bunch followed, with another alignment. This took longer than anticipated, but was completed, showing consistent results with nominal optics.  
  
Then we dumped and reinjected for loss maps during the energy ramp. Due to a crab cavity interlock, we could not inject B1 and had to proceed with only B2. Loss maps were done at 1, 1.5, 3, 4, 5, 6 and 6.8 TeV. Loss maps at FT show an improvement by 55% in both planes, as well as negligible leakage to the TCTs. In B2V, one collimator seemed a bit misaligned (TCLA.A6L7.B2). Loss maps during the ramp will be analyzed in detail offline.  
  
No issues were observed during the ramp, such as spurious loss spikes in IR7.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 20:57:15)


***

### [2024-06-08 19:03:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084077)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:03:31)


***

### [2024-06-08 19:04:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084078)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:04:18)


***

### [2024-06-08 19:04:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084079)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:04:52)


***

### [2024-06-08 19:05:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084081)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:05:50)


***

### [2024-06-08 19:06:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084083)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:06:24)


***

### [2024-06-08 19:07:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084084)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:07:16)


***

### [2024-06-08 19:07:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084086)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:07:16)


***

### [2024-06-08 19:07:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084088)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:07:18)


***

### [2024-06-08 19:07:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084092)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:07:32)


***

### [2024-06-08 19:09:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084095)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:09:32)


***

### [2024-06-08 19:10:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084098)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:10:42)


***

### [2024-06-08 19:11:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084099)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 19:11:16)


***

### [2024-06-08 21:15:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084120)
>SPS dump issue fixed, starting MD now  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/08 21:15:20)


***

### [2024-06-08 21:15:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084121)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:15:25)


***

### [2024-06-08 21:17:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084122)
>corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/08 21:17:09)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-06-08_21:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630509/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/06/08 21:17:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630509/content)

[Accelerator Cockpit v0.0.37 24-06-08_21:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630511/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:17:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630511/content)

[Accelerator Cockpit v0.0.37 24-06-08_21:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630513/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:19:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630513/content)

[Accelerator Cockpit v0.0.37 24-06-08_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630515/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:20:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630515/content)

[Accelerator Cockpit v0.0.37 24-06-08_21:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630517/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:21:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630517/content)


***

### [2024-06-08 21:22:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084123)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-08\_21:22.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:22:53)

[Accelerator Cockpit v0.0.37 24-06-08_21:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630519/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:22:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630519/content)


***

### [2024-06-08 21:23:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084125)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-08\_21:23.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:23:07)

[Accelerator Cockpit v0.0.37 24-06-08_21:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630521/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:23:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630521/content)


***

### [2024-06-08 21:23:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084124)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:23:03)


***

### [2024-06-08 21:27:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084127)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-08\_21:27.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:27:30)

[Accelerator Cockpit v0.0.37 24-06-08_21:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630523/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:27:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630523/content)


***

### [2024-06-08 21:28:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084128)
>Event created from ScreenShot Client.  
RF Trim Warning 24-06-08\_21:28.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:28:13)

[RF Trim Warning 24-06-08_21:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630525/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/08 21:28:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630525/content)


***

### [2024-06-08 21:29:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084129)
>LHC RUN CTRL: New FILL NUMBER set to 9739

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/08 21:29:13)


