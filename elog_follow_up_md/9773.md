# FILL 9773
**start**:		 2024-06-09 20:40:12.393238525+02:00 (CERN time)

**end**:		 2024-06-09 21:05:25.405738525+02:00 (CERN time)

**duration**:	 0 days 00:25:13.012500


***

### [2024-06-09 20:40:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084893)
>LHC RUN CTRL: New FILL NUMBER set to 9773

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 20:40:12)


***

### [2024-06-09 20:43:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084894)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 20:43:01)


***

### [2024-06-09 20:44:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084895)
>re-correcting

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:44:29)

[Accelerator Cockpit v0.0.37 24-06-09_20:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632215/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:44:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632215/content)

[Accelerator Cockpit v0.0.37 24-06-09_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632217/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:45:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632217/content)

[Accelerator Cockpit v0.0.37 24-06-09_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632219/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:45:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632219/content)

[Accelerator Cockpit v0.0.37 24-06-09_20:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632221/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:45:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632221/content)


***

### [2024-06-09 20:46:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084897)
>moving to MD targets (fill 23/24)

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:46:27)

[Accelerator Cockpit v0.0.37 24-06-09_20:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632223/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:46:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632223/content)

[Accelerator Cockpit v0.0.37 24-06-09_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632225/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:47:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632225/content)

[ LSA Applications Suite (v 16.6.1) 24-06-09_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632227/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 20:47:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632227/content)


***

### [2024-06-09 20:47:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084898)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 20:47:23)


***

### [2024-06-09 20:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084899)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:48:33)

[tmpScreenshot_1717958913.380727.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632229/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:48:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632229/content)


***

### [2024-06-09 20:48:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084900)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:48:50)

[tmpScreenshot_1717958930.3417535.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632231/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:48:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632231/content)


***

### [2024-06-09 20:49:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084901)
>nominals in

creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/06/09 20:49:39)

[ TuneViewer Light V5.7.2 24-06-09_20:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632233/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/06/09 20:49:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632233/content)

[ TuneViewer Light V5.7.2 24-06-09_20:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632235/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/06/09 20:50:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632235/content)


***

### [2024-06-09 20:51:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084902)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:51:06)

[tmpScreenshot_1717959066.3629775.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632237/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:51:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632237/content)


***

### [2024-06-09 20:51:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084903)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:51:25)

[tmpScreenshot_1717959084.7730906.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632239/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:51:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632239/content)


***

### [2024-06-09 20:54:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084904)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:54:33)

[tmpScreenshot_1717959272.9052103.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632241/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:54:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632241/content)


***

### [2024-06-09 20:54:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084905)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:54:49)

[tmpScreenshot_1717959289.2564182.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632243/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:54:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632243/content)


***

### [2024-06-09 20:55:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084906)
>blowup R1 0.05  


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:55:15)


***

### [2024-06-09 20:56:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084907)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:56:23)

[tmpScreenshot_1717959383.2676036.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632245/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:56:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632245/content)


***

### [2024-06-09 20:56:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084908)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:56:40)

[tmpScreenshot_1717959400.1930504.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632247/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 20:56:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632247/content)


***

### [2024-06-09 21:01:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084909)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:01:44)

[tmpScreenshot_1717959704.2327938.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632249/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:01:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632249/content)


***

### [2024-06-09 21:01:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084910)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:01:59)

[tmpScreenshot_1717959719.183857.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632251/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:01:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632251/content)


***

### [2024-06-09 21:02:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084911)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:02:13)

[tmpScreenshot_1717959733.2099235.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632253/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:02:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632253/content)


***

### [2024-06-09 21:02:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084912)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:02:34)

[tmpScreenshot_1717959754.6740599.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632255/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/06/09 21:02:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632255/content)


***

### [2024-06-09 21:04:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084913)
>Q' remeasured

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 21:04:52)

[Accelerator Cockpit v0.0.37 24-06-09_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632257/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 21:04:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3632257/content)


***

### [2024-06-09 21:05:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084914)
>LHC RUN CTRL: New FILL NUMBER set to 9774

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 21:05:25)


