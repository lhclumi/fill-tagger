# FILL 9975
**start**:		 2024-08-05 11:56:29.565738525+02:00 (CERN time)

**end**:		 2024-08-05 16:13:54.478738525+02:00 (CERN time)

**duration**:	 0 days 04:17:24.913000


***

### [2024-08-05 11:56:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119278)
>LHC RUN CTRL: New FILL NUMBER set to 9975

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 11:56:30)


***

### [2024-08-05 12:19:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119298)
>
```
RQ7.L1 tested with EPC (Ludovic) on site after their intervention in S81.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:19:52)

[Circuit_RQ7_L1:   24-08-05_12:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712712/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:19:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712712/content)


***

### [2024-08-05 12:20:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119303)
>
```
PRZEMYSLAW TOMASZ PLUTECKI(PPLUTECK) assigned RBAC Role: PO-LHC-Piquet and will expire on: 05-AUG-24 06.20.25.158000 PM
  
He restarted a few FGC gateways in the shadow of the access and needs to configure them.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:21:13)


***

### [2024-08-05 12:24:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119306)
>
```
At the request of Thomasz, un-skipped the QPS RESET RBs task by default  
  
The task can still fail sporadically, but it won't trip RB.A23 anymore. The failure is due to a timeout in the macro of ICS which is at the edge of being too short. Thomasz will follow up.  
  
Also, the sequencer task does not transport the error message from RDA (CIRCUIT.HWD_CONFIG_SEND/RESULT) back to the GUI but just shows a generic message...  
  
The real message is  
**QPS_HWD_CFG_SEND_SEQ_TASK_1722852824887 -> Macro not executed successfully on one (many) QPS system(s)** 
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:28:04)

[ LHC Sequence Editor (DEV) : 12.32.13  24-08-05_12:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712724/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:25:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712724/content)

[Terminal - lhcop@cwo-ccc-d3lc:.opt.home.lhcop 24-08-05_12:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712740/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 12:27:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712740/content)


***

### [2024-08-05 13:25:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119343)
>
```
LHCb access already finished!  
  
We still have SND and AFP in Point 1, ALICE in Point 2, and CV in Point 3.  

```


creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/08/05 13:27:00)

[ Access conditions for powering 24-08-05_13:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712874/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/08/05 13:27:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712874/content)

[ LHC Access Interlock App v0.2.2 24-08-05_13:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712876/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/08/05 13:27:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712876/content)


***

### [2024-08-05 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119489)
>**\*\*\* SHIFT SUMMARY \*\*\***  
Stable Beams until ~10:01, when the fill was dumped by SIS on communication loss with FGCs in point 7 (3 crates, similar event happened last week; CEM and IT are investigating).  
  
After, access for SND (emulsion exchange), LHCb (muon chamber issues) and a lot of teams in the shadow (all experiments, CV, EPC).  
  
Notes  
- LHCb initial separation increased by 2\*5um (once again)  
- pre-detuning phase changed 25 deg -> 27.5 deg on the request of RF  
- QPS RESET RBs task fixed by Thomasz (QPS configuration problem) and put back in the sequence; it can still fail intermittently (timeout too short, to be updated by ICS) but will not trip RB.A23 anymore  
- TIM communication with LASS endpoints lost for ~5 min in Stable Beams; no effect on the beam.  
  
-- Michi --  


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 17:25:00)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713287/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 17:25:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713287/content)


***

### [2024-08-05 15:30:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119408)
>New version of accpit with correct trimming of inj phase + bucket offset

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 15:30:37)

[ LSA Applications Suite (v 16.6.15) 24-08-05_15:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713101/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 15:30:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713101/content)

[ LSA Applications Suite (v 16.6.15) 24-08-05_15:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713103/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 15:31:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713103/content)

[ LSA Applications Suite (v 16.6.15) 24-08-05_15:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713105/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 15:31:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713105/content)

[Accelerator Cockpit v0.0.38 24-08-05_15:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713107/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 15:31:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713107/content)


***

### [2024-08-05 16:03:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119432)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 16:03:54)


***

### [2024-08-05 16:08:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119435)
>Access finished, precycle

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 16:08:56)


***

### [2024-08-05 16:13:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119439)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 16:13:54)


***

### [2024-08-05 16:13:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119440)
>LHC RUN CTRL: New FILL NUMBER set to 9976

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 16:13:56)


