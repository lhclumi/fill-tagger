# FILL 9749
**start**:		 2024-06-09 05:37:11.685738525+02:00 (CERN time)

**end**:		 2024-06-09 07:40:03.187238525+02:00 (CERN time)

**duration**:	 0 days 02:02:51.501500


***

### [2024-06-09 05:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084348)
>LHC RUN CTRL: New FILL NUMBER set to 9749

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 05:37:11)


***

### [2024-06-09 05:38:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084349)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 05:38:03)


***

### [2024-06-09 05:41:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084351)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-06-09\_05:41.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 05:41:58)

[Accelerator Cockpit v0.0.37 24-06-09_05:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630879/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 05:51:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630879/content)

[Accelerator Cockpit v0.0.37 24-06-09_05:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630881/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 05:42:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630881/content)

[Accelerator Cockpit v0.0.37 24-06-09_05:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630883/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 05:44:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630883/content)

[ XPOC Viewer - PRO 24-06-09_07:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630945/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 07:08:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630945/content)

[ XPOC Viewer - PRO 24-06-09_07:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630947/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 07:08:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630947/content)


***

### [2024-06-09 05:45:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084352)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 05:45:23)


***

### [2024-06-09 06:00:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084354)
>
```
e-cloud instabilities at injection  
  
Injectors couldn't produce the standard beam, so we had to switch to BCMS which has too small emittances and is unstable for these intensities and this configuration.  

```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/09 06:06:59)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-09_06:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630885/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/09 06:06:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630885/content)

[ LHC Fast BCT v1.3.2 24-06-09_06:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630887/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/09 06:00:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630887/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-06-09_06:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630889/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/06/09 06:00:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630889/content)


***

### [2024-06-09 06:05:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084355)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 06:05:55)


***

### [2024-06-09 06:35:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084358)
>Event created from ScreenShot Client.  
LHC Collimator Control Application - INCA DISABLED - PRO CCDA 24-06-09\_06:35.png

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/06/09 06:35:27)

[LHC Collimator Control Application -  INCA DISABLED - PRO CCDA 24-06-09_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630893/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/06/09 06:35:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630893/content)

[LHC Collimator Control Application -  INCA DISABLED - PRO CCDA 24-06-09_06:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630895/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/06/09 06:35:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630895/content)


***

### [2024-06-09 06:37:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084359)
>Global Post Mortem EventEvent Timestamp: 09/06/24 06:37:33.651Fill Number: 9749Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAMEnergy: 449640 [MeV]Intensity B1/B2: 3350 / 3069 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/09 06:40:27)


***

### [2024-06-09 06:37:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084368)
>
```
Global Post Mortem Event ConfirmationDump Classification: Programmed DumpOperator / Comment: bsalvant / high losses in dump region after programmed dump
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 07:10:55)


***

### [2024-06-09 06:57:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084363)
>
```
RF off to allow intervention on harmonic filter in point 4, as requested by TI
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 07:10:22)

[ LHC RF CONTROL 24-06-09_06:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630921/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/09 06:58:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630921/content)


***

### [2024-06-09 06:58:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084364)
>
```
sending all converters of sector 34 and 45 to standby for the EPC intervention on SEQ4, as requested by TI
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 07:10:38)

[ Equip State 24-06-09_06:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630923/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 06:59:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630923/content)


***

### [2024-06-09 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084378)
>
```
**SHIFT SUMMARY:**  
  
Arrived during longitudinal halo scraping MD.  
  
TI operator informed that the harmonic filter SEQ4 for the powering station LHC4 is in fault, and according to the procedure we need to intervene quickly. The intervention requires setting RF and converters of sector 34 and 45 to OFF. After discussion with Jorg and inspection by the EPC piquet on-site, we planned it for 07:00.  
  
Since the MD had lost quite some time, I stopped the MD shortly before 04:00, and MD9544 could start at 04:30 (instead of 04:00). There were quite some injection issues, and large corrections were needed (in particular the bucket was off by +1 in Beam 1).   
  
Once corrected, it was not possible to get 48b standard from the injectors, and one needed to use the 48b BCMS instead. Large emittance blow-up affected the first fill, and we could not reinject quickly after the first scraping before the intervention on the SEQ4 filter as there was an XPOC BLM error.
```

```
  
Switching back ON the harmonic filter at 07:00 caused a loss of cryo conditions at point 4.   
  
Leaving the machine with cryo recovery almost completed.  
  
  
  
* To be noted:

  
- Nicolas Magnin had to reset a RETRIG error on beam 1 XPOC that occurred when arming the LBDS. It is not clear to him what happened. Following that, a SIS interlock needed to be masked (POST_MORTEM_MACH_PROT_OK) after checking with Jorg.  
- Wire scanner B1H high voltage did not work. Worked after hypercycle change.  
- Beam 1 pilot got dumped twice on COLLBPM_A4L5_V_B1_INTOL, the first time following an attempt to inject the wrong pattern from SPS, the second time when injecting the pilot.   
- There were again cavity issues in the PS as yesterday.  
  
  
Benoit  
  
  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 07:47:43)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630959/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/09 07:47:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630959/content)


***

### [2024-06-09 07:05:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084365)
>S34 and S45 off to harmonicx filter internvention

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/09 07:05:56)

[Set SECTOR34 24-06-09_07:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630925/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/09 07:05:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630925/content)


***

### [2024-06-09 07:07:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084367)
>XPOC latches because of BLMs in the dump line

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 07:07:56)

[ XPOC Viewer - PRO 24-06-09_07:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630943/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 07:07:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630943/content)


***

### [2024-06-09 07:13:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084369)
>lost point 4 during intervention

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/06/09 07:13:35)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-06-09_07:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630949/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/06/09 07:13:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630949/content)


***

### [2024-06-09 07:40:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084377)
>LHC RUN CTRL: New FILL NUMBER set to 9750

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 07:40:03)


