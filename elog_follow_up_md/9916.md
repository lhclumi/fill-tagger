# FILL 9916
**start**:		 2024-07-19 22:44:34.434613525+02:00 (CERN time)

**end**:		 2024-07-20 01:07:40.780488525+02:00 (CERN time)

**duration**:	 0 days 02:23:06.345875


***

### [2024-07-19 22:44:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109399)
>LHC RUN CTRL: New FILL NUMBER set to 9916

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 22:44:35)


***

### [2024-07-19 22:46:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109401)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 22:46:29)


***

### [2024-07-19 22:49:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109402)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 22:49:15)


***

### [2024-07-19 22:49:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109403)
>
```
All sectors patrolled - closing & precycling.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/19 22:51:35)

[ LHC Access Interlock App v0.2.2 24-07-19_22:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686581/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/07/19 22:50:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686581/content)

[ Equip State 24-07-19_22:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686583/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/19 22:51:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686583/content)


***

### [2024-07-19 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109415)
>
```

```
**SHIFT SUMMARY:**  
  
- got the machine during recovery from ATLAS access, which finished shortly after  
- issue on PS LLRF prevented taking trains  
- asked SPS to start taking pilots to check everything fine, and 12b as soon as they were ready from PS  
- this allowed to spot same issue with TI2 on same PC as yesterday night  
- while waiting for TI2 issue to be fixed, lost PLC of access system that caused loss of patrol in zone 1, 4, and 6 in point 1 and zone 7 in point 2   
- organized patrol and went on site with help of Dwane from SPS and RP piquet at ~8pm  
- while underground at point 2 cryo asked for an access as well in point 2, therefore I gave the ok but when they arrived on site they discovered that the access was needed in UJ27 and the RP piquet propagated the patrol loss to zone 1 while going out from UJ23 to remove veto in UJ27.  
- leaving the machine during precycle  
  

```
  
Daniele  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/19 23:15:13)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686635/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/19 23:14:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686635/content)


***

### [2024-07-19 23:01:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109420)
>
```
TI (Matteo) informed us that they got again an alarm on the cooling of the DQR (QPS EE resistors) in R34. It appears the intervention done on Thursday was not successful.  
  
After discussing with Mirko, this is not a blocking problem (just the EE cooldown will be slower in case of an FPA) and we can continue until the next planned access.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/19 23:24:30)


***

### [2024-07-19 23:20:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109419)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 23:20:23)


***

### [2024-07-19 23:24:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109421)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 23:24:50)


***

### [2024-07-19 23:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109422)
>
```
CMS not giving injection BIS permit - called them, now OK
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/19 23:27:42)

[LHC-EXPTS Handshakes 24-07-19_23:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686648/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/19 23:26:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686648/content)


***

### [2024-07-19 23:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109422)
>
```
CMS not giving injection BIS permit - called them, now OK
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/19 23:27:42)

[LHC-EXPTS Handshakes 24-07-19_23:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686648/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/19 23:26:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686648/content)


***

### [2024-07-19 23:28:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109424)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:28:39)

[Accelerator Cockpit v0.0.37 24-07-19_23:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686652/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:28:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686652/content)

[Accelerator Cockpit v0.0.37 24-07-19_23:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686654/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:29:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686654/content)

[Accelerator Cockpit v0.0.37 24-07-19_23:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686656/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:29:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686656/content)


***

### [2024-07-19 23:30:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109425)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/19 23:30:40)


***

### [2024-07-19 23:31:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109426)
>correction with trains

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:31:55)

[RF Trim Warning 24-07-19_23:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686658/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:31:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686658/content)


***

### [2024-07-19 23:35:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109429)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:35:11)

[tmpScreenshot_1721424911.8125517.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686676/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:35:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686676/content)


***

### [2024-07-19 23:35:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109430)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:35:14)

[tmpScreenshot_1721424914.0683043.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686678/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:35:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686678/content)


***

### [2024-07-19 23:36:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109431)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:36:00)

[tmpScreenshot_1721424960.1811273.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686680/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:36:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686680/content)


***

### [2024-07-19 23:36:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109432)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:36:02)

[tmpScreenshot_1721424962.1251268.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686682/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/19 23:36:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686682/content)


***

### [2024-07-20 00:00:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109436)
>
```
MKI2 interconnect (2D-Q5) vacuum interlock  
  
It appears the vacuum was on the rise since ~21:30 with a peak at ~23:00 (well before beam!) which did not fully recover before starting to inject...  
Nearby gauges don't see the any significant pressure rise - is gauge VGPB.176.5L2.B working correctly?  
  
Called kicker piquet (Omer) who is checking with Tobias.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:27:48)

[ LHC-INJ-SIS Status 24-07-20_00:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686692/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/20 00:00:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686692/content)

[ LHC Fast BCT v1.3.2 24-07-20_00:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686694/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:01:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686694/content)

[History: MKI2 24-07-20_00:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686696/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:03:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686696/content)

[History: MKI2 24-07-20_00:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686698/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:07:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686698/content)

[Profile LHC 24-07-20_00:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686700/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:16:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686700/content)


***

### [2024-07-20 00:38:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109441)
>
```
Omer agrees the interlock is probably due to a problem with gauge VGPB.176.5L2.B.  
Since the magnet D tank vacuum does not show any significant activity, he agreed to mask the interlock for the remaining 5 injections in B1.  
  
To be observed how the vacuum behaves after the next dump.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:42:00)

[ LHC SIS GUI 24-07-20_00:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686712/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/20 00:38:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686712/content)

[History: MKI2 24-07-20_00:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686720/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:41:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686720/content)


***

### [2024-07-20 00:40:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109443)
>will try to ramp a somewhat degraded beam...

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:40:47)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-20_00:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686714/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:40:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686714/content)

[ Beam Intensity - v1.4.0 24-07-20_00:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686716/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:41:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686716/content)

[ LHC Fast BCT v1.3.2 24-07-20_00:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686718/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:41:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686718/content)


***

### [2024-07-20 00:40:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109442)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 00:40:44)


***

### [2024-07-20 00:40:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109444)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 00:40:54)


***

### [2024-07-20 00:40:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109445)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 00:40:58)


***

### [2024-07-20 00:40:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109446)
>LHC Injection Complete

Number of injections actual / planned: 56 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 1:10:18 / 0:33:48 (208.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/20 00:40:58)


***

### [2024-07-20 00:43:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109447)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 00:43:07)


***

### [2024-07-20 00:43:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109449)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 00:43:08)


***

### [2024-07-20 00:43:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109453)
>Global Post Mortem Event

Event Timestamp: 20/07/24 00:43:18.777
Fill Number: 9916
Accelerator / beam mode: PROTON PHYSICS / RAMP
Energy: 450600 [MeV]
Intensity B1/B2: 36759 / 36762 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 4-BLM\_UNM: B T -> F on CIB.SR3.S3.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/20 00:46:10)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-07-20_00:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686733/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:56:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686733/content)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-07-20_00:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686735/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:56:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686735/content)


***

### [2024-07-20 00:43:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109458)
>Global Post Mortem Event Confirmation

Dump Classification: Beam Loss
Operator / Comment: mihostet / losses in IR3 at start of ramp following ~30 min delay at injection due to MKI2 vacuum interconnect issue


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/20 01:01:00)


***

### [2024-07-20 00:48:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109454)
>|\*\*\* XPOC error has been reset by user 'mihostet' at 20.07.2024 00:48:03
| Comment: debunched beam - ~1.5h at injection
|
| Dump info: BEAM 1 at 20.07.2024 00:43:18
| Beam info: Energy= 450.60 GeV ; Intensity= 3.63E14 p+ ; #Bunches= 2352
| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR


creator:	 mihostet  @cs-ccr-pm3.cern.ch (2024/07/20 00:48:03)


***

### [2024-07-20 00:48:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109455)
>|\*\*\* XPOC error has been reset by user 'mihostet' at 20.07.2024 00:48:06
| Comment: debunched beam - ~1.5h at injection
|
| Dump info: BEAM 2 at 20.07.2024 00:43:18
| Beam info: Energy= 450.60 GeV ; Intensity= 3.60E14 p+ ; #Bunches= 2352
| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR


creator:	 mihostet  @cs-ccr-pm3.cern.ch (2024/07/20 00:48:06)


***

### [2024-07-20 00:56:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109457)
>
```
After an initial drop following the dump, VGPB.176.5L2.B keeps rising even though there is no beam...  
  
After talking to Omer again, we agreed that for the moment:  
- we unmask the MKI2 VAC INTERCONNECT interlock in SIS  
- we increase the SIS threshold for MKI2D-Q5 (VGPB.176.5L2.B) interconnect so it does not interlock  
... so at least all the other interconnects are interlocked.  
  
Tomorrow Omer will check with Tobias and Giorgia if we should add some complementary measures, e.g. tightening the Magnet D tank vacuum threshold (currently 2e-9 but vacuum level is < 1e-10)  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 01:26:27)

[History: MKI2 24-07-20_00:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686737/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 00:56:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686737/content)

[ LSA Applications Suite (v 16.6.7) 24-07-20_01:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686773/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 01:22:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686773/content)

[Profile LHC 24-07-20_01:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686775/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/20 01:28:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3686775/content)


***

### [2024-07-20 01:04:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109460)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 01:04:28)


***

### [2024-07-20 01:04:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109462)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 01:04:38)


***

### [2024-07-20 01:04:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4109463)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/20 01:04:40)


