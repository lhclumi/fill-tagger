# FILL 9630
**start**:		 2024-05-15 03:57:16.121488525+02:00 (CERN time)

**end**:		 2024-05-15 04:28:13.862988525+02:00 (CERN time)

**duration**:	 0 days 00:30:57.741500


***

### [2024-05-15 03:57:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067644)
>LHC RUN CTRL: New FILL NUMBER set to 9630

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:57:16)


***

### [2024-05-15 03:57:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067645)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 03:57:54)


***

### [2024-05-15 04:00:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067646)
>starting MD11723  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 04:00:43)


***

### [2024-05-15 04:02:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067647)
>Event created from ScreenShot Client.  
RF Trim Warning 24-05-15\_04:02.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:02:36)

[RF Trim Warning 24-05-15_04:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591187/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:02:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591187/content)


***

### [2024-05-15 04:04:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067648)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-15\_04:04.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:04:55)

[Accelerator Cockpit v0.0.35 24-05-15_04:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591189/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:04:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591189/content)


***

### [2024-05-15 04:07:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067649)
>chromaticities to 20

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:07:25)

[Accelerator Cockpit v0.0.35 24-05-15_04:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591197/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:07:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591197/content)


***

### [2024-05-15 04:20:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067650)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-15\_04:20.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:20:27)

[Accelerator Cockpit v0.0.35 24-05-15_04:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591205/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 04:20:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3591205/content)


***

### [2024-05-15 04:25:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067652)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 04:25:34)


