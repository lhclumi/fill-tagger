# FILL 10061
**start**:		 2024-08-26 19:51:08.725488525+02:00 (CERN time)

**end**:		 2024-08-27 03:38:25.104363525+02:00 (CERN time)

**duration**:	 0 days 07:47:16.378875


***

### [2024-08-26 19:51:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132241)
>LHC RUN CTRL: New FILL NUMBER set to 10061

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 19:51:09)


***

### [2024-08-26 19:51:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132242)
>warning in XPOC context

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:51:32)

[ XPOC Viewer - PRO 24-08-26_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743176/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:54:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743176/content)


***

### [2024-08-26 19:51:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132243)
>
```
B2 orbit had not been corrected and went above the threshold on too many BPMs, causing the dump. Thanks Jorg for pointing me to the reason of the dump. 
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/26 23:34:47)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-08-26_19:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743178/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:51:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743178/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-08-26_19:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743180/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 19:52:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743180/content)

[pm-sis-analysis >> Version: 2.2.5  Responsible: Jorg Wenninger 24-08-26_20:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743210/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/08/26 20:39:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743210/content)

[pm-sis-analysis >> Version: 2.2.5  Responsible: Jorg Wenninger 24-08-26_20:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743212/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/08/26 20:39:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743212/content)


***

### [2024-08-26 19:53:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132244)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 19:53:40)


***

### [2024-08-26 20:00:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132246)
>Event created from ScreenShot Client.  
pm-beam-loss-evaluation >> 
 Version: 1.0.6 Responsible: TE-MPE-CB Software Team 
 (mpe-software-coord@cern.ch) 24-08-26\_20:00.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 20:00:45)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-08-26_20:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743184/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 20:02:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743184/content)


***

### [2024-08-26 20:01:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132247)
>dampers were off in beam 1

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/26 20:01:11)

[ TRANSVERSE DAMPER CONTROL 24-08-26_20:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743186/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/26 20:01:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743186/content)


***

### [2024-08-26 20:02:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132250)
>Event created from ScreenShot Client.  
LHC BLM Fixed Display 
 24-08-26\_20:02.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 20:02:52)

[ LHC BLM Fixed Display 24-08-26_20:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743190/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 20:02:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743190/content)


***

### [2024-08-26 20:03:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132251)
>Event created from ScreenShot Client.  
RF Trim Warning 24-08-26\_20:03.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:03:57)

[RF Trim Warning 24-08-26_20:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743192/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:03:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743192/content)

[Accelerator Cockpit v0.0.38 24-08-26_20:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743194/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:04:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743194/content)

[Accelerator Cockpit v0.0.38 24-08-26_20:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743196/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:05:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743196/content)

[Accelerator Cockpit v0.0.38 24-08-26_20:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743198/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:08:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743198/content)

[Accelerator Cockpit v0.0.38 24-08-26_20:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743200/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:08:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743200/content)


***

### [2024-08-26 20:09:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132252)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:09:03)


***

### [2024-08-26 20:13:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132253)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:47)

[tmpScreenshot_1724696026.8865151.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743202/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743202/content)


***

### [2024-08-26 20:13:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132254)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:50)

[tmpScreenshot_1724696030.2862477.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743204/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743204/content)


***

### [2024-08-26 20:13:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132255)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:52)

[tmpScreenshot_1724696032.7664149.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743206/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743206/content)


***

### [2024-08-26 20:13:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132256)
>
*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:56)

[tmpScreenshot_1724696036.7342777.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743208/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/26 20:13:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743208/content)


***

### [2024-08-26 20:41:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132257)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:41:48)


***

### [2024-08-26 20:41:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132258)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:41:57)


***

### [2024-08-26 20:42:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132259)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:42:01)


***

### [2024-08-26 20:42:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132260)
>LHC Injection Complete

Number of injections actual / planned: 48 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:32:58 / 0:33:48 (97.5 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 20:42:02)


***

### [2024-08-26 20:44:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132261)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:44:58)


***

### [2024-08-26 20:44:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132263)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 20:44:58)


***

### [2024-08-26 21:00:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132265)
>tune feedback OFF for B2V again

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 21:00:14)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-26_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743214/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 21:00:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743214/content)


***

### [2024-08-26 21:00:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132266)
>turned it back ON when the signal came back

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 21:01:06)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-26_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743216/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 21:01:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743216/content)

[ TuneViewer Light V5.7.2 24-08-26_21:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743218/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/08/26 21:01:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743218/content)

[ TuneViewer Light V5.7.2 24-08-26_21:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743220/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/08/26 21:01:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743220/content)

[ TuneViewer Light V5.7.2 24-08-26_21:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743222/content)
creator:	 lhcop  @cwo-ccc-d1lf.cern.ch (2024/08/26 21:01:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743222/content)


***

### [2024-08-26 21:04:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132267)
>status during the ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:04:23)

[ LHC Fast BCT v1.3.2 24-08-26_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743224/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:04:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743224/content)


***

### [2024-08-26 21:06:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132269)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:06:16)


***

### [2024-08-26 21:09:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132271)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:09:01)


***

### [2024-08-26 21:10:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132273)
>
```
set Tune FB B2 to OD gated as last fill
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:11:03)

[Beam Feedbacks - Mission Control 24-08-26_21:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743226/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/26 21:10:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743226/content)


***

### [2024-08-26 21:12:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132274)
>status in the squeeze

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:12:33)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743228/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:12:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743228/content)

[ LHC Fast BCT v1.3.2 24-08-26_21:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743230/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:12:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743230/content)


***

### [2024-08-26 21:18:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132275)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:18:07)


***

### [2024-08-26 21:18:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132276)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:18:52)


***

### [2024-08-26 21:19:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132278)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:19:43)


***

### [2024-08-26 21:20:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132279)
>Event created from ScreenShot Client.  
BSRT-vs-Emit v0.1.2 - January 
 2024 - INCA DISABLED - PRO CCDA 24-08-26\_21:20.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:20:38)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743232/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:20:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743232/content)


***

### [2024-08-26 21:22:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132280)
>before optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:22:35)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743234/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:22:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743234/content)


***

### [2024-08-26 21:24:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132281)
>
```
optimization IP1/IP5. Quite off for IP5 in V.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:45:34)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743236/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:24:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743236/content)


***

### [2024-08-26 21:24:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132282)
>after optimization IP1/IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:24:46)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743238/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:24:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743238/content)


***

### [2024-08-26 21:26:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132283)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:26:37)


***

### [2024-08-26 21:26:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132284)
>after IP2/IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:26:47)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743240/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:29:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743240/content)


***

### [2024-08-26 21:27:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132285)
>optimization IP2/IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:27:14)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743242/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:27:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743242/content)


***

### [2024-08-26 21:30:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132286)
>Event created from ScreenShot Client.  
 LHC Fast BCT v1.3.2 24-08-26\_21:30.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:30:15)

[ LHC Fast BCT v1.3.2 24-08-26_21:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743244/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:30:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743244/content)


***

### [2024-08-26 21:33:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132287)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:33:10)


***

### [2024-08-26 21:35:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132288)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/26 21:35:07)


***

### [2024-08-26 21:39:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132289)
>tune trim +1e-3 in B1H

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:39:55)

[ LHC Beam Losses Lifetime Display 24-08-26_21:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743246/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:39:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743246/content)


***

### [2024-08-26 21:43:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132291)
>status at start of beta\* levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:43:46)

[ LHC Fast BCT v1.3.2 24-08-26_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743250/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743250/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743252/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:43:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743252/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743254/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:43:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743254/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743256/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743256/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743258/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743258/content)


***

### [2024-08-26 21:44:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132292)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:17)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743260/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743260/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743262/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:44:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743262/content)


***

### [2024-08-26 21:55:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132293)
>status at 56 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:55:31)

[ LHC Fast BCT v1.3.2 24-08-26_21:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743264/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 22:12:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743264/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-26_21:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743266/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:55:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743266/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743268/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:55:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743268/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743270/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:55:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743270/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743272/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:55:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743272/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743274/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:56:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743274/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-26_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743276/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/26 21:56:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743276/content)

[ LHC Beam Losses Lifetime Display 24-08-26_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743278/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:56:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743278/content)


***

### [2024-08-26 21:56:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132294)
>comparing lifetime in collisions for the last fill that had large B2 blow 
 up and the current fill. As Michi was saying, a very good lifetime in 
 collision could mean that H and V tunes are too close to each other and 
 could lead to instabilities in ADJUST

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:58:39)

[ LHC Beam Losses Lifetime Display 24-08-26_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743280/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:58:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743280/content)


***

### [2024-08-26 21:56:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132294)
>comparing lifetime in collisions for the last fill that had large B2 blow 
 up and the current fill. As Michi was saying, a very good lifetime in 
 collision could mean that H and V tunes are too close to each other and 
 could lead to instabilities in ADJUST

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:58:39)

[ LHC Beam Losses Lifetime Display 24-08-26_21:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743280/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/26 21:58:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743280/content)


***

### [2024-08-26 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132300)
>
```
**SHIFT SUMMARY:**  
  
Arrived during filling.  
  
Some losses at start of ramp (41%) as filling was longer than usual, especially for the first batch of beam 1.  
  
Tune feedback signal was bad for beam 2V and we had to switch to OD_GATED.  

```

```
  
Significant emittance blow-up occurred on beam 2 during the collision beam process. Michi also noted that the beam 2 lifetime was particularly good also, which could mean that H and V tunes could have been too close.    
  
Beams were dumped by a trip of a warm corrector (RCBWH5.L3).  
  
I ramped down and refilled, but beams were dumped as the beam 2 orbit had drifted too much while waiting for the RF cavities of the PS, and I had not corrected it.  
  
I refilled after asking for a bit more intensity (I had 1.58e11 p/b in the last fill). It went smoothly. Tune feedback signal was lost again for beam 2V at the end of the ramp and I had to switch to OD_GATED before the squeeze as for the previous fill.  
  
Lifetime in collisions was not as good as that fill, and also we got no emittance blow-up in B2. IP5 was quite off before optimization (0.017mm in V).  
  
Leaving the machine in stable beams.  
   
  
* To be noted:

  
- ALICE requested a much higher luminosity for 15' at the start of stable beams for the first fill.  
- If RCBWH5.L3 trips again, EPC will need to access to replace power modules.  
  
Benoit  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/26 23:41:00)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743286/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/26 23:40:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743286/content)


***

### [2024-08-26 23:08:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132298)
>line 3B2

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/26 23:09:04)

[Monitoring application. Currently monitoring : LHC - [2 subscriptions ] 24-08-26_23:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743282/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/26 23:09:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743282/content)

[ LHC RF CONTROL 24-08-26_23:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743284/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/26 23:09:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743284/content)


***

### [2024-08-27 02:27:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132303)
>cryo issues, preparing to dump

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:27:12)


***

### [2024-08-27 02:27:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132304)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:27:42)


***

### [2024-08-27 02:31:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132305)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:31:46)


***

### [2024-08-27 02:31:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132306)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:31:47)


***

### [2024-08-27 02:32:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132307)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:32:42)


***

### [2024-08-27 02:32:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132312)
>Global Post Mortem Event

Event Timestamp: 27/08/24 02:32:53.788
Fill Number: 10061
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 31465 / 31335 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/27 02:35:43)


***

### [2024-08-27 02:32:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132328)
>Global Post Mortem Event Confirmation

Dump Classification: Loss of Cryogenics
Operator / Comment: dmirarch / Lost KURC in point 8 


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/27 02:54:47)


***

### [2024-08-27 02:33:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132308)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:33:37)


***

### [2024-08-27 02:33:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132309)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:33:44)


***

### [2024-08-27 02:33:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132310)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:33:47)


***

### [2024-08-27 02:33:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132311)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:33:49)


***

### [2024-08-27 02:35:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132313)
>cryo conditions lost

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/27 02:36:00)

[Set SECTOR81 24-08-27_02:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743289/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/27 02:36:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743289/content)


***

### [2024-08-27 02:36:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132314)
>several circuits in slow abort, including mains.
Calling EPC piquet to recover them.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:38:36)

[ Equip State 24-08-27_02:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743291/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:37:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743291/content)

[ Equip State 24-08-27_02:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743293/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:38:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743293/content)

[Monitoring application. Currently monitoring : LHC - [3 subscriptions ] 24-08-27_02:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743295/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:39:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743295/content)

[ Equip State 24-08-27_02:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743297/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 02:42:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743297/content)


***

### [2024-08-27 02:44:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132315)
>OLIVIER EMMANUEL FOURNIER(OFOURNIE) assigned RBAC Role: PO-LHC-Piquet and will expire on: 27-AUG-24 04.44.48.174000 AM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/27 02:44:52)


***

### [2024-08-27 02:45:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132316)
>
```
Called ALICE and LHCb to inform them about the possibility to ramp down their magnets given the foreseen long stop.   
They'll call back once checked with run respective managers.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 03:22:35)


***

### [2024-08-27 02:50:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132320)
>
```
LHCb called back reporting to switch their magnet off only if more than 10h are foreseen.   
Present information (from today's morning meetings) is that loss of KURC in 81 will take between half a day and five days to repair.   
Switching off LHCb magnet.
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 03:22:27)


***

### [2024-08-27 02:50:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132323)
>LHC SEQ: ramping down LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 02:50:49)


***

### [2024-08-27 02:51:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132324)
>RF off

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/27 02:51:31)

[ LHC RF CONTROL 24-08-27_02:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743303/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/27 02:51:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743303/content)


***

### [2024-08-27 03:19:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132332)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 03:20:18)


***

### [2024-08-27 03:21:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132333)
>ALICE will keep magnets ON for cosmic run  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/27 03:22:10)


***

### [2024-08-27 03:23:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132334)
>machine OFF

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/27 03:24:05)

[ CCM_1 LHCOP 24-08-27_03:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743305/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/27 03:24:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3743305/content)


***

### [2024-08-27 03:38:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4132338)
>LHC RUN CTRL: New FILL NUMBER set to 10062

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/27 03:38:26)


