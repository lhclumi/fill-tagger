# FILL 9744
**start**:		 2024-06-09 03:27:01.486988525+02:00 (CERN time)

**end**:		 2024-06-09 03:47:31.747238525+02:00 (CERN time)

**duration**:	 0 days 00:20:30.260250


***

### [2024-06-09 03:27:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084275)
>LHC RUN CTRL: New FILL NUMBER set to 9744

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 03:27:01)


***

### [2024-06-09 03:27:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084276)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 03:27:20)


***

### [2024-06-09 03:30:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084277)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 03:30:31)


***

### [2024-06-09 03:33:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084278)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:11)

[tmpScreenshot_1717896791.0519304.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630785/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630785/content)


***

### [2024-06-09 03:33:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084279)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:16)

[tmpScreenshot_1717896796.77895.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630787/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630787/content)


***

### [2024-06-09 03:33:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084280)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:39)

[tmpScreenshot_1717896819.787499.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630789/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630789/content)


***

### [2024-06-09 03:33:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084281)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:54)

[tmpScreenshot_1717896834.0595572.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630791/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630791/content)


***

### [2024-06-09 03:33:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084282)
>*Sent By LHC WS APP*

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:57)

[tmpScreenshot_1717896837.6755455.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630793/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/09 03:33:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3630793/content)


***

### [2024-06-09 03:47:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4084285)
>LHC RUN CTRL: New FILL NUMBER set to 9745

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/09 03:47:31)


