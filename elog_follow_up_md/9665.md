# FILL 9665
**start**:		 2024-05-24 10:04:01.191863525+02:00 (CERN time)

**end**:		 2024-05-24 10:51:32.008988525+02:00 (CERN time)

**duration**:	 0 days 00:47:30.817125


***

### [2024-05-24 10:04:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074083)
>LHC RUN CTRL: New FILL NUMBER set to 9665

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:04:01)


***

### [2024-05-24 10:05:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074084)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:05:02)


***

### [2024-05-24 10:08:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074092)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:08:25)


***

### [2024-05-24 10:09:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074094)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:09:03)


***

### [2024-05-24 10:09:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074095)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:09:47)


***

### [2024-05-24 10:10:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074098)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:10:39)


***

### [2024-05-24 10:10:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074099)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:10:45)


***

### [2024-05-24 10:11:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074101)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:11:05)


***

### [2024-05-24 10:11:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074103)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:11:33)


***

### [2024-05-24 10:11:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074104)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:11:49)


***

### [2024-05-24 10:11:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074104)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:11:49)


***

### [2024-05-24 10:13:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074107)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:13:00)


***

### [2024-05-24 10:13:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074108)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:13:39)


***

### [2024-05-24 10:14:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074109)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:14:14)


***

### [2024-05-24 10:14:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074111)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:14:15)


***

### [2024-05-24 10:14:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074113)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:14:16)


***

### [2024-05-24 10:14:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074117)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:14:31)


***

### [2024-05-24 10:15:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074119)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:15:00)


***

### [2024-05-24 10:15:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074121)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:15:07)


***

### [2024-05-24 10:16:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074122)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:16:30)


***

### [2024-05-24 10:17:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074124)
>RF voltage increased to 5.5 MV

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:17:49)

[ LSA Applications Suite (v 16.5.36) 24-05-24_10:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607295/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:17:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607295/content)


***

### [2024-05-24 10:18:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074126)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:18:14)


***

### [2024-05-24 10:19:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074128)
>RF increase propagated to ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:19:19)

[ LSA Applications Suite (v 16.5.36) 24-05-24_10:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607301/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:19:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607301/content)

[ LSA Applications Suite (v 16.5.36) 24-05-24_10:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607303/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:19:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607303/content)

[ LSA Applications Suite (v 16.5.36) 24-05-24_10:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607307/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:19:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607307/content)

[ LSA Applications Suite (v 16.5.36) 24-05-24_10:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607309/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:20:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607309/content)


***

### [2024-05-24 10:28:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074133)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:28:23)


***

### [2024-05-24 10:29:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074134)
>RB QPS A45 not OK

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/24 10:30:05)

[ LHC-INJ-SIS Status 24-05-24_10:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607326/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/24 10:30:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607326/content)

[K - MB.B11L5 DQAMC N type MB for dipole MB.B11L5    24-05-24_10:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607328/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:30:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607328/content)


***

### [2024-05-24 10:35:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074135)
>
```
SPI bus reset - didn't work as well  
  
calling QPS piquet  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:37:03)

[qps-lhc-swisstool 24-05-24_10:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607332/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:36:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607332/content)


***

### [2024-05-24 10:38:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074137)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 24-MAY-24 12.38.19.748000 PM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/24 10:38:21)


***

### [2024-05-24 10:39:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074138)
>sewnding RB.A45 to standby

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:40:07)

[ Equip State 24-05-24_10:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607336/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:40:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607336/content)


***

### [2024-05-24 10:41:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074143)
>switches opened

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:42:26)

[Circuit_RB_A45:   24-05-24_10:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607340/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:42:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607340/content)


***

### [2024-05-24 10:44:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074145)
>PM killed as agreed with piquet agreement to close switches without waiting

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:45:22)

[5 - UA47.RB.A45 DQAMS N type RB 13KA for circuit UA47.RB.A45    24-05-24_10:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607348/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:45:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607348/content)


***

### [2024-05-24 10:47:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074146)
>Event created from ScreenShot Client.  
Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-05-24\_10:47.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:47:23)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-05-24_10:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607354/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:47:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607354/content)


***

### [2024-05-24 10:49:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074147)
>RB recovered

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:49:57)

[Circuit_RB_A45:   24-05-24_10:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607358/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:49:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607358/content)

[Circuit_RB_A45:   24-05-24_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607362/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/24 10:51:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3607362/content)


***

### [2024-05-24 10:50:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074149)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:50:57)


***

### [2024-05-24 10:51:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074150)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:51:31)


***

### [2024-05-24 10:51:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4074151)
>LHC RUN CTRL: New FILL NUMBER set to 9666

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/24 10:51:32)


