# FILL 10194
**start**:		 2024-10-03 10:00:32.401738525+02:00 (CERN time)

**end**:		 2024-10-03 11:34:48.428988525+02:00 (CERN time)

**duration**:	 0 days 01:34:16.027250


***

### [2024-10-03 10:00:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155099)
>LHC RUN CTRL: New FILL NUMBER set to 10194

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:00:33)


***

### [2024-10-03 10:03:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155107)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:03:02)


***

### [2024-10-03 10:03:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155108)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:03:19)


***

### [2024-10-03 10:04:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155110)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:04:51)


***

### [2024-10-03 10:05:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155111)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:05:07)


***

### [2024-10-03 10:06:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155112)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:06:06)


***

### [2024-10-03 10:06:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155115)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_125NSEC (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:06:57)


***

### [2024-10-03 10:07:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155119)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:07:18)


***

### [2024-10-03 10:09:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155122)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:09:01)


***

### [2024-10-03 10:10:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155125)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:10:02)


***

### [2024-10-03 10:10:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155127)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:10:09)


***

### [2024-10-03 10:10:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155128)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:10:16)


***

### [2024-10-03 10:11:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155129)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:11:30)


***

### [2024-10-03 10:11:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155131)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:11:32)


***

### [2024-10-03 10:11:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155131)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:11:32)


***

### [2024-10-03 10:11:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155133)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:11:34)


***

### [2024-10-03 10:11:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155137)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:11:49)


***

### [2024-10-03 10:12:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155139)
>landau to 0 in ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 10:13:05)

[ LSA Applications Suite (v 16.6.29) 24-10-03_10:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799038/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 10:13:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799038/content)


***

### [2024-10-03 10:13:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155141)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:13:46)


***

### [2024-10-03 10:15:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155143)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:15:30)


***

### [2024-10-03 10:25:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155152)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:25:11)


***

### [2024-10-03 10:34:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155160)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 10:34:35)


***

### [2024-10-03 10:36:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155162)
>Bis interlock from CMS. I called them and asked to clear it.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/03 10:36:27)

[LHC-EXPTS Handshakes 24-10-03_10:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799159/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/03 10:36:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799159/content)


***

### [2024-10-03 10:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155165)
>Forced card b

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 10:44:08)

[Circuit_RCBXH2_R8:   24-10-03_10:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799163/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 10:44:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799163/content)


***

### [2024-10-03 10:44:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155167)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/03 10:45:02)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-03_10:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799167/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/03 10:45:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799167/content)


***

### [2024-10-03 10:48:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155170)
>Event created from ScreenShot Client.  
OpenYASP V4.9.3 LHCRING . RAMP-SQUEEZE-2.68TeV-ATS-3.1m-ppref-2024\_V1@0\_[START] 24-10-03\_10:48.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:48:12)

[OpenYASP V4.9.3   LHCRING . RAMP-SQUEEZE-2.68TeV-ATS-3.1m-ppref-2024_V1@0_[START] 24-10-03_10:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799171/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:48:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799171/content)


***

### [2024-10-03 10:48:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155171)
>before changing the alice compensation knob.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:48:30)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-2.68TeV-ATS-3.1m-ppref-2024_V1@0_[START] 24-10-03_10:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799173/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:48:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799173/content)


***

### [2024-10-03 10:48:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155173)
>after compensation knob is changed.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:49:08)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-2.68TeV-ATS-3.1m-ppref-2024_V1@0_[START] 24-10-03_10:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799175/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:49:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799175/content)

[ LSA Applications Suite (v 16.6.29) 24-10-03_10:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799179/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:49:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799179/content)


***

### [2024-10-03 10:51:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155175)
>Included in the ramp

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:51:16)

[ LSA Applications Suite (v 16.6.29) 24-10-03_10:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799181/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/03 10:51:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799181/content)


***

### [2024-10-03 11:09:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155185)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:09:29)


***

### [2024-10-03 11:09:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155186)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:09:39)


***

### [2024-10-03 11:09:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155187)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:09:45)


***

### [2024-10-03 11:13:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155189)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:13:16)


***

### [2024-10-03 11:13:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155191)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:13:18)


***

### [2024-10-03 11:22:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155197)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:22:30)


***

### [2024-10-03 11:26:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155201)
>Global Post Mortem Event

Event Timestamp: 03/10/24 11:26:08.754
Fill Number: 10194
Accelerator / beam mode: PROTON PHYSICS / FLAT TOP
Energy: 2679600 [MeV]
Intensity B1/B2: 3 / 3 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/03 11:28:59)


***

### [2024-10-03 11:26:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155199)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:26:31)


***

### [2024-10-03 11:33:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155214)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.6.29) 24-10-03\_11:33.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 11:36:48)

[ LSA Applications Suite (v 16.6.29) 24-10-03_11:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799234/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 11:36:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3799234/content)


***

### [2024-10-03 11:34:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155204)
>LHC RUN CTRL: New FILL NUMBER set to 10195

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 11:34:52)


