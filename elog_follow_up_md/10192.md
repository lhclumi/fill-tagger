# FILL 10192
**start**:		 2024-10-03 07:39:35.703863525+02:00 (CERN time)

**end**:		 2024-10-03 09:51:10.175363525+02:00 (CERN time)

**duration**:	 0 days 02:11:34.471500


***

### [2024-10-03 07:39:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155025)
>LHC RUN CTRL: New FILL NUMBER set to 10192

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 07:39:36)


***

### [2024-10-03 07:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155026)
>Switch to 2.68TeV cycle.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 07:42:20)

[ Generation Application connected to server LHC (PRO database) 24-10-03_07:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798659/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 07:42:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798659/content)


***

### [2024-10-03 07:57:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155029)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 07:57:16)


***

### [2024-10-03 08:02:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155030)
>Not able to close the machine due to the issue in point 6 with lost communcation. I have talked to Vitor and he will talk to the team. 

creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/03 08:02:34)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798665/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/10/03 08:02:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798665/content)


***

### [2024-10-03 08:10:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155046)
>They will access PM65 for the access system and potentially UJ63 so RP is also going to come with them.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 08:39:58)


***

### [2024-10-03 08:25:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155035)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:


creator:	 copera  @cs-ccr-inca1.cern.ch (2024/10/03 08:25:57)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798679/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/10/03 08:25:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798679/content)


***

### [2024-10-03 08:40:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155047)
>Called the BLM piquet about the strange behaving BLM that caused the dump.   


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 08:40:37)


***

### [2024-10-03 09:03:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155056)
>BLMBI.11R5.B0T20\_MBB-LEGR is suspiciously close to BLMQI.11R5.B1E21\_MQ which once gave unphysical readings a year ago...  
<https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/3749121>  
<https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/3741565>  


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/03 09:08:17)


***

### [2024-10-03 09:16:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155059)
>
```
the problem of the BBLR BP mismatch is understood - it has the same root cause as the problem at FT during the MD, the laslett tune trims differ and create a K-level mismatch for RQTD/F.
```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/03 09:27:43)

[ LSA Applications Suite (v 16.6.29) 24-10-03_09:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798732/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/03 09:17:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798732/content)

[ LSA Applications Suite (v 16.6.29) 24-10-03_09:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798734/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/10/03 09:17:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3798734/content)


***

### [2024-10-03 09:44:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155081)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 09:44:38)


***

### [2024-10-03 09:46:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155083)
>They can't find any issue on the
```
BLMBI.11R5.B0T20_MBB-LEGR_11R5. We continue for now but to be followed up in case this comes back. 
```
  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/03 09:46:46)


***

### [2024-10-03 09:51:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155085)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 09:51:09)


***

### [2024-10-03 09:51:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4155086)
>LHC RUN CTRL: New FILL NUMBER set to 10193

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/03 09:51:11)


