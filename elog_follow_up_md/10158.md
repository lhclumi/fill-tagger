# FILL 10158
**start**:		 2024-09-27 21:33:23.944363525+02:00 (CERN time)

**end**:		 2024-09-27 22:17:28.768363525+02:00 (CERN time)

**duration**:	 0 days 00:44:04.824000


***

### [2024-09-27 21:33:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151655)
>LHC RUN CTRL: New FILL NUMBER set to 10158

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 21:33:24)


***

### [2024-09-27 21:33:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151656)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 21:33:47)


***

### [2024-09-27 21:36:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151657)
>injection of pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 21:36:26)

[Accelerator Cockpit v0.0.38 24-09-27_21:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790049/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 21:36:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790049/content)

[Accelerator Cockpit v0.0.38 24-09-27_21:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790051/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 21:36:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790051/content)

[Accelerator Cockpit v0.0.38 24-09-27_21:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790053/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/27 21:36:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790053/content)


***

### [2024-09-27 21:37:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151658)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 21:37:21)


***

### [2024-09-27 21:45:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151663)
>beam re-injected with 2e11p/b

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 21:46:06)

[ LHC Fast BCT v1.3.2 24-09-27_21:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790055/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 21:46:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790055/content)

[ Beam Intensity - v1.5.0 24-09-27_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790057/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 21:46:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790057/content)

[ Beam Intensity - v1.5.0 24-09-27_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790059/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 21:46:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790059/content)

[ LHC BLM Fixed Display 24-09-27_21:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790061/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/27 21:47:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790061/content)


***

### [2024-09-27 21:46:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151664)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/27 21:46:09)


***

### [2024-09-27 21:57:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151668)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 
 24-09-27\_21:57.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/27 21:57:36)

[ LHC Fast BCT v1.3.2 24-09-27_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790063/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/09/27 21:57:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3790063/content)


***

### [2024-09-27 22:16:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151675)
>Global Post Mortem Event

Event Timestamp: 27/09/24 22:16:48.125
Fill Number: 10158
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 6862 / 6898 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 22:19:38)


