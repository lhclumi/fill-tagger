# FILL 9694
**start**:		 2024-05-31 14:25:14.089238525+02:00 (CERN time)

**end**:		 2024-06-01 03:32:50.022988525+02:00 (CERN time)

**duration**:	 0 days 13:07:35.933750


***

### [2024-05-31 14:25:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079104)
>LHC RUN CTRL: New FILL NUMBER set to 9694

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 14:25:15)


***

### [2024-05-31 14:26:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079105)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 14:26:17)


***

### [2024-05-31 14:32:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079112)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 14:32:36)


***

### [2024-05-31 14:42:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079118)
>Beneficial effect of the energy trim

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/31 14:42:21)

[IQC Dp.p Monitoring 24-05-31_14:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619440/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/31 14:42:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619440/content)


***

### [2024-05-31 14:43:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079121)
>Emittance fo first trains

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 14:43:44)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-31_14:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619446/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 14:49:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619446/content)


***

### [2024-05-31 15:08:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079131)
>MAchine filled

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:08:42)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-31_15:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619504/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:08:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619504/content)

[ LHC Fast BCT v1.3.2 24-05-31_15:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619520/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:14:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619520/content)

[ LHC Fast BCT v1.3.2 24-05-31_15:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619522/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:14:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619522/content)

[ LHC Fast BCT v1.3.2 24-05-31_15:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619524/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:14:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619524/content)


***

### [2024-05-31 15:09:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079132)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:09:08)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-05-31_15:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619508/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/31 15:09:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619508/content)


***

### [2024-05-31 15:09:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079133)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:09:18)


***

### [2024-05-31 15:09:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079134)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:09:27)


***

### [2024-05-31 15:09:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079135)
>LHC Injection CompleteNumber of injections actual / planned: 54 / 48SPS SuperCycle length: 36.0 [s]Actual / minimum time: 0:36:51 / 0:33:48 (109.0 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/31 15:09:28)

[ LHC Fast BCT v1.3.2 24-05-31_15:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619607/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:33:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619607/content)


***

### [2024-05-31 15:12:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079138)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:12:12)


***

### [2024-05-31 15:12:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079136)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:12:11)


***

### [2024-05-31 15:12:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079140)
>56% dump threshold at start of RAMP

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:12:50)

[ LHC BLM Fixed Display 24-05-31_15:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619518/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:12:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619518/content)


***

### [2024-05-31 15:33:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079158)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:33:30)


***

### [2024-05-31 15:33:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079162)
>FT

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:33:48)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-31_15:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619605/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:33:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619605/content)

[ LHC Fast BCT v1.3.2 24-05-31_15:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619609/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:34:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619609/content)

[ LHC Fast BCT v1.3.2 24-05-31_15:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619611/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:34:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619611/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-05-31_15:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619615/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/31 15:35:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619615/content)


***

### [2024-05-31 15:33:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079160)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:33:45)


***

### [2024-05-31 15:42:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079167)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:42:21)


***

### [2024-05-31 15:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079168)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:42:25)


***

### [2024-05-31 15:42:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079168)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:42:25)


***

### [2024-05-31 15:43:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079169)
>QChange

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:43:08)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-31_15:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619621/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:43:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619621/content)


***

### [2024-05-31 15:43:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079170)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:43:17)


***

### [2024-05-31 15:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079172)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:44:04)


***

### [2024-05-31 15:47:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079173)
>Losses going into collisions.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/31 15:47:13)

[ LHC Beam Losses Lifetime Display 24-05-31_15:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619623/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/31 15:47:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619623/content)


***

### [2024-05-31 15:48:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079174)
>Optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:49:02)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_15:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619625/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 15:49:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619625/content)


***

### [2024-05-31 15:49:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079175)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:49:43)


***

### [2024-05-31 15:53:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079179)
>LHCb arrived at ~10 Hz/ubApproached them with IP steering to ~200 Hz/ub (-1.3 sigma)

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 15:54:32)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_15:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619681/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 15:54:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619681/content)


***

### [2024-05-31 15:56:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079180)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:56:45)


***

### [2024-05-31 15:57:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079182)
>Losses going into collisions again.

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/31 15:58:45)

[ LHC Beam Losses Lifetime Display 24-05-31_15:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619707/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/31 15:58:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619707/content)


***

### [2024-05-31 15:58:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079181)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/31 15:58:24)


***

### [2024-05-31 15:58:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079183)
>No warnings for the losses going into collisions this time.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:59:07)

[ LHC BLM Fixed Display 24-05-31_15:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619709/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/31 15:59:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619709/content)


***

### [2024-05-31 16:02:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079184)
>Emittance scan finished in IP1 and IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 16:02:32)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619711/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 16:02:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619711/content)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_16:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619715/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 16:02:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619715/content)


***

### [2024-05-31 16:33:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079200)
>incorporated corrections for IP8 back into the PHYSICS BP (with 60um initial separation)

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 16:33:48)

[ LSA Applications Suite (v 16.5.44) 24-05-31_16:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619781/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 16:33:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619781/content)


***

### [2024-05-31 16:48:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079214)
>Optimizing IR8 in crossing plane.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 16:48:27)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_16:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619847/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 16:48:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619847/content)


***

### [2024-05-31 17:07:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079223)
>small optimization in the crossing plane for IR8.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 17:07:54)

[ LHC Luminosity Scan Client 0.59.2 [pro] 24-05-31_17:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619899/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 17:07:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619899/content)


***

### [2024-05-31 17:13:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079225)
>feed-forward of LHCb separation

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 17:13:55)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-05-31_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619915/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 17:13:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619915/content)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-05-31_17:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619925/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 17:14:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3619925/content)


***

### [2024-05-31 22:42:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079297)
>Incorporating the last part of the feed-forsward (the rest was already done).

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 22:44:04)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-05-31_22:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620193/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/31 22:44:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620193/content)


***

### [2024-05-31 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079306)
>
```

```
Summary draft:  
Arrived during the injection.  We had losses of 45% in the beginning of the ramp but after that we didn't have any warnings (not even going to collisions!)  
We did every beta* step down in order to optimize IP8 in between. There were big differences in the beginning of the beta* leveling but much smaller later on.  The optimization is feed-forwarded so next fill we should be fine. 
```
  
/Tobias  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 23:07:57)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620223/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/31 23:07:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620223/content)


***

### [2024-06-01 03:23:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079323)
>Global Post Mortem EventEvent Timestamp: 01/06/24 03:23:01.834Fill Number: 9694Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799200 [MeV]Intensity B1/B2: 24302 / 24381 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 8-RF-b1: B T -> F on CIB.UA47.R4.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/01 03:25:52)


***

### [2024-06-01 03:23:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079320)
>M1b1 lost the RF, beam dumped

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/01 03:23:43)

[ LHC RF CONTROL 24-06-01_03:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620232/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/06/01 03:23:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620232/content)


***

### [2024-06-01 03:25:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079321)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/01 03:25:03)


***

### [2024-06-01 03:25:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079322)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/01 03:25:20)


***

### [2024-06-01 03:31:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079327)
>task failed for LBDS, but all looks good on the BETS explorer for B2...

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/01 03:32:23)

[ LHC Sequencer Execution GUI (PRO) : 12.32.5  24-06-01_03:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620236/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/01 03:43:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620236/content)

[ BETS Explorer 24-06-01_03:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620238/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/01 03:32:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3620238/content)


***

### [2024-06-01 03:32:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4079328)
>LHC RUN CTRL: New FILL NUMBER set to 9695

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/01 03:32:50)


