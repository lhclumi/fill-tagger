# FILL 9896
**start**:		 2024-07-13 10:24:17.098863525+02:00 (CERN time)

**end**:		 2024-07-14 02:07:35.804863525+02:00 (CERN time)

**duration**:	 0 days 15:43:18.706000


***

### [2024-07-13 10:24:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105594)
>LHC RUN CTRL: New FILL NUMBER set to 9896

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:24:17)


***

### [2024-07-13 10:25:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105601)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:25:30)


***

### [2024-07-13 10:27:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105602)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:27:37)


***

### [2024-07-13 10:27:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105604)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:27:44)


***

### [2024-07-13 10:29:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105606)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:29:03)


***

### [2024-07-13 10:30:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105608)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:30:13)


***

### [2024-07-13 10:30:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105609)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:30:20)


***

### [2024-07-13 10:30:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105611)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:30:41)


***

### [2024-07-13 10:31:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105612)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:31:06)


***

### [2024-07-13 10:33:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105614)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:03)


***

### [2024-07-13 10:33:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105616)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:30)


***

### [2024-07-13 10:33:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105618)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:31)


***

### [2024-07-13 10:33:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105620)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:32)


***

### [2024-07-13 10:33:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105622)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:36)


***

### [2024-07-13 10:33:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105625)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:33:47)


***

### [2024-07-13 10:34:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105627)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:34:15)


***

### [2024-07-13 10:34:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105629)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:34:21)


***

### [2024-07-13 10:35:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105630)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:35:46)


***

### [2024-07-13 10:37:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105632)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:37:30)


***

### [2024-07-13 10:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105638)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 10:48:43)


***

### [2024-07-13 11:02:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105642)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:02:30)


***

### [2024-07-13 11:10:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105643)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:10:10)


***

### [2024-07-13 11:11:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105646)
>a bit of steering with the 12b

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/13 11:12:01)

[OpenYASP V4.8.10   LHCB1Transfer . LHC_3inj_Nom_48b_Q20_2024_V1 . SPS.USER.LHC1 24-07-13_11:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676542/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/13 11:12:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676542/content)


***

### [2024-07-13 11:13:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105647)
>trajectory after correction

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/13 11:13:52)

[OpenYASP DV LHCB1Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-13_11:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676544/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/13 11:13:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676544/content)

[OpenYASP DV LHCB2Transfer . LHC_3inj_BCMS_Q20_2024_V1 24-07-13_11:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676546/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/07/13 11:14:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676546/content)


***

### [2024-07-13 11:15:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105648)
>wire scans

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 11:15:29)

[ LHC WIRESCANNER APP 24-07-13_11:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676548/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 11:15:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676548/content)

[ LHC WIRESCANNER APP 24-07-13_11:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676550/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 11:15:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676550/content)

[ LHC WIRESCANNER APP 24-07-13_11:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676552/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 11:15:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676552/content)

[ LHC WIRESCANNER APP 24-07-13_11:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676554/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 11:15:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676554/content)


***

### [2024-07-13 11:49:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105651)
>injected beam

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 11:49:30)

[ LHC Fast BCT v1.3.2 24-07-13_11:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676578/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 11:49:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676578/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_11:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676580/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 11:49:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676580/content)

[ Beam Intensity - v1.4.0 24-07-13_11:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676582/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 11:49:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676582/content)


***

### [2024-07-13 11:49:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105652)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:49:37)


***

### [2024-07-13 11:49:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105653)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:49:47)


***

### [2024-07-13 11:49:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105654)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:49:50)


***

### [2024-07-13 11:49:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105655)
>LHC Injection Complete

Number of injections actual / planned: 58 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:39:41 / 0:33:48 (117.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 11:49:51)


***

### [2024-07-13 11:52:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105656)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:52:45)


***

### [2024-07-13 11:52:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105658)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 11:52:46)


***

### [2024-07-13 12:14:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105662)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:14:06)


***

### [2024-07-13 12:14:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105664)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:14:12)


***

### [2024-07-13 12:17:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105668)
>emittances after the ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:17:42)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_12:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676598/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:17:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676598/content)


***

### [2024-07-13 12:22:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105671)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:22:29)


***

### [2024-07-13 12:23:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105672)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:23:05)


***

### [2024-07-13 12:23:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105674)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:23:56)


***

### [2024-07-13 12:28:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105679)
>All Ips optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:28:57)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_12:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676614/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:28:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676614/content)


***

### [2024-07-13 12:29:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105680)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:29:23)


***

### [2024-07-13 12:35:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105683)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:35:40)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_12:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676630/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:35:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676630/content)


***

### [2024-07-13 12:35:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105684)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:35:50)


***

### [2024-07-13 12:37:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105685)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/13 12:37:47)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_12:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676646/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:42:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676646/content)


***

### [2024-07-13 12:44:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105687)
>Re-optimized IP1-5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:44:25)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_12:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676650/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 12:44:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676650/content)


***

### [2024-07-13 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105718)
>
```
**SHIFT SUMMARY:**  
  
Arrived at the start of stable beams with 375b.  
The check of LHCb velo following their intervention was successful. After 3:30h in stable beams, with LHCb agreement, I dumped and refilled with 2352b.  
Very smooth cycle, no losses in the ramp or in collisions. Very good lifetime.  
  
Leaving the machine in stable beams.  
  
Delphine  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 15:02:46)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676758/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 15:02:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676758/content)


***

### [2024-07-13 15:04:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105719)
>the rota file for jeune genevois

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 15:06:08)

[SPS Beam Quality Monitor - SPS.USER.LHC3 - SPS PRO INCA server - PRO CCDA 24-07-13_15:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676763/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 15:06:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676763/content)


***

### [2024-07-13 16:41:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105741)
>status at 36cm  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:18)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676863/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676863/content)

[ LHC Fast BCT v1.3.2 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676865/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676865/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676867/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676867/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676869/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:12:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676869/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676871/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676871/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676873/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676873/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676875/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676875/content)


***

### [2024-07-13 16:41:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105741)
>status at 36cm  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:18)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676863/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676863/content)

[ LHC Fast BCT v1.3.2 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676865/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676865/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676867/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:41:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676867/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676869/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:12:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676869/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676871/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676871/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676873/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676873/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_16:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676875/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 16:42:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676875/content)


***

### [2024-07-13 17:12:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105749)
>status at 33 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:12:40)

[ LHC Fast BCT v1.3.2 24-07-13_17:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676943/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:12:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676943/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_17:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676945/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:12:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676945/content)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_17:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676947/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:46:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676947/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676949/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:13:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676949/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676951/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:13:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676951/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676953/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:13:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676953/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676955/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 17:13:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676955/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676957/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 17:13:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676957/content)

[ LHC Bunch Length 24-07-13_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676959/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 17:13:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676959/content)


***

### [2024-07-13 17:46:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105763)
>lifetime at 30cm

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 17:46:42)

[ LHC Beam Losses Lifetime Display 24-07-13_17:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676989/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 17:46:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676989/content)


***

### [2024-07-13 17:50:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105765)
>tune trims
B1H +1e-3 -> very bad -> reverted 
B1H -1e-3 -> a bit better -> kept
B1V -1e-3 -> better -> kept

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 17:51:41)

[ LHC Beam Losses Lifetime Display 24-07-13_17:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676991/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 17:51:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676991/content)

[Accelerator Cockpit v0.0.37 24-07-13_17:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676993/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/07/13 17:51:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676993/content)


***

### [2024-07-13 17:53:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105767)
>B1 tunes lower than B2 following beta\* to 30 cm

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/13 17:54:11)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-07-13_17:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676995/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/07/13 17:54:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3676995/content)


***

### [2024-07-13 18:55:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105780)
>status at 30 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:55:51)

[ LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_18:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677039/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:55:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677039/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_18:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677041/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:55:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677041/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_18:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677043/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:55:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677043/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_18:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677045/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:55:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677045/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_18:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677047/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:56:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677047/content)

[ LHC Fast BCT v1.3.2 24-07-13_18:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677049/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:56:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677049/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-07-13_18:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677051/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 18:56:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677051/content)


***

### [2024-07-13 19:32:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105791)
>separation levelling not efficient anymore in IP5. Waiting for IP1 
 separation levelling to to turn on the wires.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 19:33:32)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_19:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677074/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 19:33:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677074/content)

[LHC Luminosity Scan Client 0.60.1 [pro] 24-07-13_19:33.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677076/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 19:33:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677076/content)


***

### [2024-07-13 20:04:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105796)
>starting wires

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 20:04:44)

[ LHC BBLR Wire App v0.4.1 24-07-13_20:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677080/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 20:04:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677080/content)


***

### [2024-07-13 20:05:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105797)
>wires ON

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 20:05:05)

[ LHC BBLR Wire App v0.4.1 24-07-13_20:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677082/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/13 20:05:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677082/content)


***

### [2024-07-13 20:06:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105798)
>impact of wires and tune trims (+1e-3 in B1H, +1e-3 in B1V, +1e-3 in B1H)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 20:07:30)

[ LHC Beam Losses Lifetime Display 24-07-13_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677084/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 20:07:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677084/content)


***

### [2024-07-13 20:43:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105805)
>
```
abort gap cleaning required repeatedly on beam 1 following bunch flattening
```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/13 20:45:34)

[Abort gap cleaning control v2.0.8 24-07-13_20:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677086/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/13 20:44:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677086/content)

[Mozilla Firefox 24-07-13_20:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677092/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/07/13 20:45:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677092/content)

[ LHC Beam Losses Lifetime Display 24-07-13_20:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677094/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 20:47:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677094/content)


***

### [2024-07-13 21:34:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105814)
>abort gap cleaning again

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/13 21:34:30)

[Abort gap cleaning control v2.0.8 24-07-13_21:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677100/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/13 21:34:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677100/content)

[ LHC Beam Losses Lifetime Display 24-07-13_21:34.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677102/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 21:34:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677102/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-13_21:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677104/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 21:35:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677104/content)


***

### [2024-07-13 22:56:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105830)
>regular loss spikes on B1 when bunch flattening is triggered (together 
 with abort gap being filled). However, it is not systematic

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 22:57:24)

[ LHC Beam Losses Lifetime Display 24-07-13_22:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677120/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/13 22:57:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677120/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-07-13_22:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677122/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/07/13 22:57:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677122/content)


***

### [2024-07-13 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105832)
>
```
**SHIFT SUMMARY:**  
  
  
Arrived in stable beams, left in stable beams!
```
  
  

```
  
* To be noted:

  
- measured B1 tunes went down when beta* reached 30 cm, and this was correlated with lower lifetime. Tune trims could recover part of that.    
- wires were powered 30' after beams were head-on in IP1 and IP5 (there was an injector kicker problem beam in PSB).  
- loss spikes and abort gap filling occurred when bunch flattening was triggered for beam 1 (not every time, but regularly).     
  
  

```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/13 23:09:57)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677124/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/13 23:09:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677124/content)


***

### [2024-07-14 00:26:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105835)
>Several abort gap cleaning needed after the last blow up.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/14 00:26:31)

[Abort gap cleaning control v2.0.8 24-07-14_00:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677125/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/14 00:26:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3677125/content)


***

### [2024-07-14 01:56:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105840)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 01:56:32)


***

### [2024-07-14 02:00:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105843)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:00:20)


***

### [2024-07-14 02:00:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105844)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:00:22)


***

### [2024-07-14 02:01:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105845)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:01:34)


***

### [2024-07-14 02:01:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105850)
>Global Post Mortem Event

Event Timestamp: 14/07/24 02:01:51.254
Fill Number: 9896
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799680 [MeV]
Intensity B1/B2: 21910 / 21930 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/14 02:04:44)


***

### [2024-07-14 02:01:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105892)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: tpersson / Programmed dump


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/07/14 02:41:22)


***

### [2024-07-14 02:02:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105846)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:02:35)


***

### [2024-07-14 02:02:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105847)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:02:41)


***

### [2024-07-14 02:02:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105848)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:02:44)


***

### [2024-07-14 02:02:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4105849)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/14 02:02:45)


