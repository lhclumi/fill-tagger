# FILL 10020
**start**:		 2024-08-19 11:38:47.485238525+02:00 (CERN time)

**end**:		 2024-08-19 15:49:28.175238525+02:00 (CERN time)

**duration**:	 0 days 04:10:40.690000


***

### [2024-08-19 11:38:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128098)
>LHC RUN CTRL: New FILL NUMBER set to 10020

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:38:48)


***

### [2024-08-19 11:39:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128105)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:39:32)


***

### [2024-08-19 11:41:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128107)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:41:33)


***

### [2024-08-19 11:41:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128109)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:41:40)


***

### [2024-08-19 11:43:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128110)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:43:31)


***

### [2024-08-19 11:44:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128113)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:44:12)


***

### [2024-08-19 11:44:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128114)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:44:25)


***

### [2024-08-19 11:45:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128116)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:45:34)


***

### [2024-08-19 11:47:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128119)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:47:59)


***

### [2024-08-19 11:48:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128121)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:48:00)


***

### [2024-08-19 11:48:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128123)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:48:04)


***

### [2024-08-19 11:48:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128127)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:48:18)


***

### [2024-08-19 11:48:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128129)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:48:20)


***

### [2024-08-19 11:48:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128131)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:48:26)


***

### [2024-08-19 11:50:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128135)
>Changed bunch length blow up target in the RAMP to get shorter bunches for 
 MD12783
To be reverted at the end of the MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 11:50:39)

[ LSA Applications Suite (v 16.6.19) 24-08-19_11:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735094/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 11:50:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735094/content)


***

### [2024-08-19 11:50:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128133)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:50:16)


***

### [2024-08-19 11:52:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128136)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:52:00)


***

### [2024-08-19 11:59:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128141)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:59:24)


***

### [2024-08-19 11:59:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128141)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 11:59:24)


***

### [2024-08-19 12:03:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128142)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:03:27)


***

### [2024-08-19 12:03:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128144)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:03:48)


***

### [2024-08-19 12:04:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128145)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:04:22)


***

### [2024-08-19 12:13:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128146)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:13:27)


***

### [2024-08-19 12:17:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128148)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:17:33)


***

### [2024-08-19 12:33:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128149)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:33:15)


***

### [2024-08-19 12:33:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128150)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:33:25)


***

### [2024-08-19 12:33:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128151)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:33:29)


***

### [2024-08-19 12:33:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128152)
>LHC Injection Complete

Number of injections actual / planned: 28 / 28
SPS SuperCycle length: 28.8 [s]
Actual / minimum time: 0:15:56 / 0:18:26 (86.4 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/19 12:33:30)


***

### [2024-08-19 12:35:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128154)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:35:15)


***

### [2024-08-19 12:35:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128156)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:35:16)


***

### [2024-08-19 12:35:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128158)
>Event created from ScreenShot Client.  
LHC Fast BCT v1.3.2 
 24-08-19\_12:35.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 12:35:50)

[ LHC Fast BCT v1.3.2 24-08-19_12:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735128/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 12:35:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735128/content)

[ LHC WIRESCANNER APP 24-08-19_12:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735130/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/19 12:35:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735130/content)

[ LHC WIRESCANNER APP 24-08-19_12:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735132/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/19 12:36:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735132/content)

[ LHC WIRESCANNER APP 24-08-19_12:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735134/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/19 12:36:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735134/content)

[ LHC WIRESCANNER APP 24-08-19_12:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735136/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/19 12:36:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735136/content)


***

### [2024-08-19 12:56:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128161)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 12:56:33)


***

### [2024-08-19 12:56:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128163)
>FLATTOP

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/19 12:56:50)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-19_12:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735140/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/19 12:56:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735140/content)

[ LHC Beam Losses Lifetime Display 24-08-19_12:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735142/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/19 12:57:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735142/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-19_12:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735144/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/19 12:57:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735144/content)


***

### [2024-08-19 13:38:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128177)
>Event created from ScreenShot Client.  
 Head-Tail Viewer - LHC Head-Tail Beam 1 24-08-19\_13:38.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:38:06)

[ Head-Tail Viewer - LHC Head-Tail Beam 1 24-08-19_13:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735164/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:38:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735164/content)


***

### [2024-08-19 13:48:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128181)
>Event created from ScreenShot Client.  
 Head-Tail Viewer - LHC Head-Tail Beam 1 24-08-19\_13:48.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:48:41)

[ Head-Tail Viewer - LHC Head-Tail Beam 1 24-08-19_13:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735168/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:48:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735168/content)

[ Head-Tail Viewer - LHC Head-Tail Beam 1 24-08-19_13:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735176/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:51:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735176/content)

[ Head-Tail Viewer - LHC Head-Tail Beam 2 24-08-19_13:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735178/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/19 13:56:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735178/content)


***

### [2024-08-19 13:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128183)
>Event created from ScreenShot Client.  
LHC High-resolution Bunch 
 Profiles 24-08-19\_13:50.png

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/19 13:50:52)

[LHC High-resolution Bunch Profiles 24-08-19_13:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735170/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/19 13:50:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735170/content)

[Beam profile analysis 24-08-19_13:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735172/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/19 13:50:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735172/content)

[BEAM 1 VTU and Scope Settings 24-08-19_13:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735174/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/19 13:50:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735174/content)


***

### [2024-08-19 15:02:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128207)
>Reverted the RAMP fuction target length to nominal after the end of this MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 15:02:50)

[ LSA Applications Suite (v 16.6.19) 24-08-19_15:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735246/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/19 15:02:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735246/content)


***

### [2024-08-19 15:11:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128212)
>
```

```
MD12783 summary (octupole threshold at the sweet spot):  
  
- First fill: 14 bunches @ 1.6e11 p+/b, with decreasing transverse emittances along the train. Ramp to flat top (bunch length ~1.3 ns). The first bunch was transversely blown up from an injection mis-steering.   
    * Went to Ioct = 60 A (in two steps) with Q'=20 -> many bunches got unstable in both beams.  
    * Moved back to 80 A and went down in Q'x (steps of -1 unit per minute - keeping the Q' in the other plane constant): instabilities for B1/B2 at resp. Q'x= 15 / 16  
    * Same procedure for Q'y -> unstable B1/B2 for Q'y = 18 / 19  
    * Same procedure, going up in Q'x -> unstable B1 for Q'x = 27. N2 was still not unstable at Q'x=36.  
    * The beams were dumped due to a sextupole trip (unrelated to the MD).  
  
- Second fill: same bunch configuration (except that the first bunch was not blown-up), and same procedure.  
    * went down to Ioct=80 A (in two steps) with Q'=22 -> many bunches got unstable in both beams.  
    * Went up to 100 A and down / up in Q' by step of 1 unit (keeping the other plane Q' at 22). Results for the lower bound of the sweet spot in Q' are (B1/B2):  
        100 A: Q'x = 14/12  
        120 A: Q'x = 14/9 (but the witness bunch got unstable for B1)  
        140 A: Q'x = 13/not measured  
        160 A: Q'x = 14/not measured  
        200 A: Q'x = 11/not measured  
    * Before dumping, a Q' measurement was performed (show no offset), and a damper gain calibration (first at Q'=20, then at Q'=5)  
  
- Third fill: same beam, but the RF blow-up was adjusted in the ramp to reach ~1.05 ns at flat top.  
    * Started with an octupole scan at Q'=20 -> B1/B2 unstable at 10 A  
    * Then same procedure (up/down in Q' by steps of 1 unit per minute, at various octupole current). Results (B1/B2):  
        200 A: Q'x=16/10  
        300 A: not unstable when going down (going to Q'x=3/5)  
        250 A: Q'x=12/(not unstable down to 5)  
        150 A B2: unstable for Q'x=12, Q'y=10, not unstable when going up in Q'y (up to 30)  
        275 A B1: still stable down to Q'x=3  
        140 A B1: still stable down to Q'y=3  
        100 A B2: still stable up to Q'y=30  
        80 A B1: unstable for Q'y=13  
        80 A B2: unstable for Q'y=29      
        40 A B2: unstable for Q'y=26       
    * Then programmed beam dump.  
  
Overall, many new threshold limits in terms of Q', from this MD.      
  

```
Xavier, Lorenzo, Nicolas, Georges and Delphine  

```


creator:	 nimounet  @194.12.183.180 (2024/08/19 15:54:50)


***

### [2024-08-19 15:47:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128215)
>Event created from ScreenShot Client.  
Figure 1 24-08-19\_15:47.png

creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/08/19 15:47:44)

[Figure 1 24-08-19_15:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735260/content)
creator:	 lhcop  @cwo-ccc-d6lf.cern.ch (2024/08/19 15:47:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3735260/content)


***

### [2024-08-19 15:48:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128227)
>Global Post Mortem Event

Event Timestamp: 19/08/24 15:48:30.075
Fill Number: 10020
Accelerator / beam mode: MACHINE DEVELOPMENT / FLAT TOP
Energy: 6799440 [MeV]
Intensity B1/B2: 202 / 184 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b2: B T -> F on CIB.CCR.LHC.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/19 15:51:25)


***

### [2024-08-19 15:49:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128216)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:49:08)


***

### [2024-08-19 15:49:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128217)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:49:10)


***

### [2024-08-19 15:49:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4128218)
>LHC RUN CTRL: New FILL NUMBER set to 10021

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/19 15:49:28)


