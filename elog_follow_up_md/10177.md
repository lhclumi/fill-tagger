# FILL 10177
**start**:		 2024-09-30 15:01:08.575738525+02:00 (CERN time)

**end**:		 2024-09-30 16:47:54.395988525+02:00 (CERN time)

**duration**:	 0 days 01:46:45.820250


***

### [2024-09-30 15:01:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153232)
>LHC RUN CTRL: New FILL NUMBER set to 10177

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:01:13)


***

### [2024-09-30 15:01:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153238)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:01:51)


***

### [2024-09-30 15:03:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153239)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:03:52)


***

### [2024-09-30 15:05:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153241)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:05:22)


***

### [2024-09-30 15:05:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153243)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:05:28)


***

### [2024-09-30 15:06:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153245)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:06:43)


***

### [2024-09-30 15:06:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153245)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:06:43)


***

### [2024-09-30 15:07:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153247)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:07:27)


***

### [2024-09-30 15:07:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153250)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:07:56)


***

### [2024-09-30 15:09:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153251)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:09:54)


***

### [2024-09-30 15:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153253)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:09:57)


***

### [2024-09-30 15:09:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153255)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:10:00)


***

### [2024-09-30 15:10:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153259)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:10:15)


***

### [2024-09-30 15:10:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153262)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:10:39)


***

### [2024-09-30 15:10:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153264)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:10:46)


***

### [2024-09-30 15:12:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153265)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:12:14)


***

### [2024-09-30 15:13:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153267)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:13:58)


***

### [2024-09-30 15:20:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153269)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:20:53)


***

### [2024-09-30 15:21:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153270)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:22:01)


***

### [2024-09-30 15:22:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153271)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:22:58)


***

### [2024-09-30 15:23:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153272)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:23:36)


***

### [2024-09-30 15:27:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153274)
>Event created from ScreenShot Client.  
 TRANSVERSE DAMPER CONTROL 24-09-30\_15:27.png

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/30 15:27:30)

[ TRANSVERSE DAMPER CONTROL 24-09-30_15:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3792777/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/30 15:27:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3792777/content)


***

### [2024-09-30 15:41:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153292)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 15:41:57)


***

### [2024-09-30 16:22:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153310)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:22:31)


***

### [2024-09-30 16:22:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153311)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:22:43)


***

### [2024-09-30 16:22:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153312)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:22:51)


***

### [2024-09-30 16:45:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153318)
>CCM has created an event for the LSA Connsistency Checks application.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/30 16:45:29)

[LSA Connsistency Checks_0-2024-09-30-16.45.28.984.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3792906/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/30 16:45:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3792906/content)


***

### [2024-09-30 16:47:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153319)
>LHC SEQ: B1&B2 injection protection coll at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:47:11)


***

### [2024-09-30 16:47:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153320)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:47:14)


***

### [2024-09-30 16:47:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153321)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:47:54)


***

### [2024-09-30 16:47:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4153322)
>LHC RUN CTRL: New FILL NUMBER set to 10178

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/30 16:47:55)


