# FILL 9788
**start**:		 2024-06-17 02:53:56.545863525+02:00 (CERN time)

**end**:		 2024-06-17 05:29:38.450613525+02:00 (CERN time)

**duration**:	 0 days 02:35:41.904750


***

### [2024-06-17 02:53:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088369)
>LHC RUN CTRL: New FILL NUMBER set to 9788

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:53:57)


***

### [2024-06-17 02:54:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088375)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:54:58)


***

### [2024-06-17 02:55:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088377)
>|\*\*\* XPOC error has been reset by user 'delph' at 17.06.2024 02:55:42| Comment: asynch dump test|| Dump info: BEAM 1 at 17.06.2024 02:52:33| Beam info: Energy= 6799.80 GeV ; Intensity= 0.00E0 p+ ; #Bunches= 11| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR

creator:	 delph  @cs-ccr-pm3.cern.ch (2024/06/17 02:55:42)


***

### [2024-06-17 02:55:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088378)
>|\*\*\* XPOC error has been reset by user 'delph' at 17.06.2024 02:55:55| Comment: Asynch dump test|| Dump info: BEAM 2 at 17.06.2024 02:52:33| Beam info: Energy= 6799.80 GeV ; Intensity= 0.00E0 p+ ; #Bunches= 11| XPOC info: Server= 'XPOC PRO' ; Analysis= OK ; Check= ERROR

creator:	 delph  @cs-ccr-pm3.cern.ch (2024/06/17 02:55:55)


***

### [2024-06-17 02:57:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088379)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:57:06)


***

### [2024-06-17 02:57:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088381)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:57:41)


***

### [2024-06-17 02:57:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088382)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:57:55)


***

### [2024-06-17 02:59:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088383)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:59:13)


***

### [2024-06-17 02:59:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088385)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 02:59:50)


***

### [2024-06-17 03:00:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088387)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:00:50)


***

### [2024-06-17 03:00:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088388)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:00:51)


***

### [2024-06-17 03:01:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088389)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:01:16)


***

### [2024-06-17 03:03:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088392)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:04)


***

### [2024-06-17 03:03:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088393)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:22)


***

### [2024-06-17 03:03:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088394)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:38)


***

### [2024-06-17 03:03:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088395)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:40)


***

### [2024-06-17 03:03:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088397)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:41)


***

### [2024-06-17 03:03:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088399)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:43)


***

### [2024-06-17 03:03:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088401)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:45)


***

### [2024-06-17 03:03:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088403)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:51)


***

### [2024-06-17 03:03:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088406)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:03:57)


***

### [2024-06-17 03:05:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088408)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:05:55)


***

### [2024-06-17 03:07:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088410)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:07:39)


***

### [2024-06-17 03:09:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088412)
>as a preparation for B\* levelling I trim the Q' function constant at 10 units in the B\*levelling BP

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 03:10:30)

[ LSA Applications Suite (v 16.6.1) 24-06-17_03:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638366/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 03:10:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638366/content)


***

### [2024-06-17 03:17:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088414)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:17:43)


***

### [2024-06-17 03:29:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088415)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:30:00)


***

### [2024-06-17 03:34:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088416)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:34:37)


***

### [2024-06-17 03:51:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088419)
>injected beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 03:51:41)

[ LHC Fast BCT v1.3.2 24-06-17_03:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638368/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:26:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638368/content)


***

### [2024-06-17 03:51:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088418)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:51:41)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-06-17_03:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638370/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 03:51:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638370/content)


***

### [2024-06-17 03:51:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088420)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:51:51)


***

### [2024-06-17 03:51:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088421)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:51:59)


***

### [2024-06-17 03:55:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088422)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:55:11)


***

### [2024-06-17 03:55:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088424)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 03:55:12)


***

### [2024-06-17 04:16:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088432)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:16:31)


***

### [2024-06-17 04:16:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088434)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:16:37)


***

### [2024-06-17 04:25:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088436)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:25:27)


***

### [2024-06-17 04:26:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088437)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:26:06)


***

### [2024-06-17 04:26:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088439)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:26:53)


***

### [2024-06-17 04:36:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088443)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:36:13)


***

### [2024-06-17 04:38:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088444)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 04:38:09)


***

### [2024-06-17 04:38:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088445)
>AFT and TOTEM roman pots IN  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:39:07)


***

### [2024-06-17 04:41:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088446)
>XRP In  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 04:41:13  
- Background acquisitions: 10  
- Max losses timestamp: 04:41:30  
- Machine configuration: SB\_105.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:41:39)

[tmpScreenshot_1718592099.61925.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638376/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:41:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638376/content)


***

### [2024-06-17 04:42:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088447)
>Loss map B1V stuck again

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:43:09)

[ LHC LOSS MAPS 24-06-17_04:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638378/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:43:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638378/content)


***

### [2024-06-17 04:45:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088449)
>XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 04:44:16  
- Background acquisitions: 10  
- Max losses timestamp: 04:44:50  
- Machine configuration: SB\_105.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:45:06)

[tmpScreenshot_1718592306.268219.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638380/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:45:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638380/content)


***

### [2024-06-17 04:45:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088450)
>XRP IN  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 04:45:08  
- Background acquisitions: 10  
- Max losses timestamp: 04:45:26  
- Machine configuration: SB\_105.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:45:45)

[tmpScreenshot_1718592345.412786.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638382/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:45:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638382/content)


***

### [2024-06-17 04:46:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088451)
>XRP in  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 04:45:47  
- Background acquisitions: 10  
- Max losses timestamp: 04:46:03  
- Machine configuration: SB\_105.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:46:15)

[tmpScreenshot_1718592375.252825.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638384/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:46:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638384/content)


***

### [2024-06-17 04:46:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088451)
>XRP in  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 04:45:47  
- Background acquisitions: 10  
- Max losses timestamp: 04:46:03  
- Machine configuration: SB\_105.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:46:15)

[tmpScreenshot_1718592375.252825.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638384/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:46:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638384/content)


***

### [2024-06-17 04:50:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088454)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 04:50:02  
- Background acquisitions: 10  
- Max losses timestamp: 04:50:18  
- Machine configuration: SB\_93  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:50:32)

[tmpScreenshot_1718592632.303948.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638386/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:50:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638386/content)


***

### [2024-06-17 04:50:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088455)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 04:50:34  
- Background acquisitions: 10  
- Max losses timestamp: 04:50:48  
- Machine configuration: SB\_93  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:50:59)

[tmpScreenshot_1718592659.3603497.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638388/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:50:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638388/content)


***

### [2024-06-17 04:51:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088456)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 04:51:01  
- Background acquisitions: 10  
- Max losses timestamp: 04:51:16  
- Machine configuration: SB\_93  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:51:27)

[tmpScreenshot_1718592687.682801.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638390/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:51:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638390/content)


***

### [2024-06-17 04:51:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088457)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 04:51:29  
- Background acquisitions: 10  
- Max losses timestamp: 04:51:44  
- Machine configuration: SB\_93  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:51:53)

[tmpScreenshot_1718592713.72071.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638392/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:51:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638392/content)


***

### [2024-06-17 04:55:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088458)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 04:55:14  
- Background acquisitions: 10  
- Max losses timestamp: 04:55:28  
- Machine configuration: SB\_82.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:55:38)

[tmpScreenshot_1718592938.1471531.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638394/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:55:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638394/content)


***

### [2024-06-17 04:56:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088459)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 04:55:40  
- Background acquisitions: 10  
- Max losses timestamp: 04:55:55  
- Machine configuration: SB\_82.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:56:04)

[tmpScreenshot_1718592964.5715554.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638396/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:56:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638396/content)


***

### [2024-06-17 04:56:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088460)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 04:56:06  
- Background acquisitions: 10  
- Max losses timestamp: 04:56:23  
- Machine configuration: SB\_82.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:56:34)

[tmpScreenshot_1718592994.1895907.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638398/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:56:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638398/content)


***

### [2024-06-17 04:57:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088461)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 04:56:38  
- Background acquisitions: 10  
- Max losses timestamp: 04:56:56  
- Machine configuration: SB\_82.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:57:06)

[tmpScreenshot_1718593026.3335247.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638400/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 04:57:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638400/content)


***

### [2024-06-17 05:01:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088463)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 05:00:47  
- Background acquisitions: 10  
- Max losses timestamp: 05:01:11  
- Machine configuration: SB\_72.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:01:22)

[tmpScreenshot_1718593281.9430518.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638402/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:01:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638402/content)


***

### [2024-06-17 05:01:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088464)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 05:01:23  
- Background acquisitions: 10  
- Max losses timestamp: 05:01:38  
- Machine configuration: SB\_72.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:01:46)

[tmpScreenshot_1718593306.1593945.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638404/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:01:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638404/content)


***

### [2024-06-17 05:02:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088465)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 05:01:47  
- Background acquisitions: 10  
- Max losses timestamp: 05:02:02  
- Machine configuration: SB\_72.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:02:12)

[tmpScreenshot_1718593332.3195515.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638406/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:02:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638406/content)


***

### [2024-06-17 05:02:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088466)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 05:02:14  
- Background acquisitions: 10  
- Max losses timestamp: 05:02:30  
- Machine configuration: SB\_72.5  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:02:37)

[tmpScreenshot_1718593357.4444466.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638408/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:02:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638408/content)


***

### [2024-06-17 05:08:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088467)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1H  
- Start timestamp: 05:07:57  
- Background acquisitions: 10  
- Max losses timestamp: 05:08:13  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:08:21)

[tmpScreenshot_1718593701.435783.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638410/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:08:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638410/content)


***

### [2024-06-17 05:08:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088468)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B1V  
- Start timestamp: 05:08:23  
- Background acquisitions: 10  
- Max losses timestamp: 05:08:36  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:08:48)

[tmpScreenshot_1718593727.9491718.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638412/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:08:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638412/content)


***

### [2024-06-17 05:09:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088469)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2H  
- Start timestamp: 05:08:50  
- Background acquisitions: 10  
- Max losses timestamp: 05:09:05  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:09:14)

[tmpScreenshot_1718593754.0607092.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638414/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:09:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638414/content)


***

### [2024-06-17 05:09:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088470)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: B2V  
- Start timestamp: 05:09:15  
- Background acquisitions: 10  
- Max losses timestamp: 05:09:31  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:09:40)

[tmpScreenshot_1718593780.0527236.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638416/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:09:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638416/content)


***

### [2024-06-17 05:09:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088471)
>chroma to 3 for off momentum loss maps

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/17 05:10:07)

[Accelerator Cockpit v0.0.37 24-06-17_05:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638418/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/06/17 05:10:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638418/content)


***

### [2024-06-17 05:11:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088472)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: +dp/p  
- Start timestamp: 05:10:31  
- Background acquisitions: 10  
- Max losses timestamp: 05:11:19  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:11:30)

[tmpScreenshot_1718593890.7500741.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638420/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:11:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638420/content)


***

### [2024-06-17 05:12:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088473)
>  
  
Screenshot attached: Loss map preliminary analysis  
- Loss Map: -dp/p  
- Start timestamp: 05:11:32  
- Background acquisitions: 10  
- Max losses timestamp: 05:12:17  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:12:26)

[tmpScreenshot_1718593946.415452.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638422/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:12:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638422/content)


***

### [2024-06-17 05:27:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088486)
>Global Post Mortem EventEvent Timestamp: 17/06/24 05:27:55.155Fill Number: 9788Accelerator / beam mode: PROTON PHYSICS / ADJUSTEnergy: 6799800 [MeV]Intensity B1/B2: 10 / 9 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/17 05:30:49)


***

### [2024-06-17 05:28:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088476)
>  
  
Screenshot attached: Asynchronous Beam Dump overview  
- Start timestamp: 05:12:32  
- Machine configuration: SB\_60  
  
*Sent From pyLossMaps*

creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:28:03)

[tmpScreenshot_1718594883.65553.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638424/content)
creator:	 delph  @cwo-ccc-d3lc.cern.ch (2024/06/17 05:28:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3638424/content)


***

### [2024-06-17 05:28:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088477)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 05:28:58)


***

### [2024-06-17 05:28:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088478)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 05:28:59)


***

### [2024-06-17 05:29:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4088479)
>LHC RUN CTRL: New FILL NUMBER set to 9789

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/17 05:29:39)


