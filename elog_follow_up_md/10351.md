# FILL 10351
**start**:		 2024-11-11 12:37:24.324863525+01:00 (CERN time)

**end**:		 2024-11-11 16:09:08.754238525+01:00 (CERN time)

**duration**:	 0 days 03:31:44.429375


***

### [2024-11-11 12:37:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4181998)
>LHC RUN CTRL: New FILL NUMBER set to 10351

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 12:37:26)


***

### [2024-11-11 12:45:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182001)
>LHC SEQ: preparing the whole LHC for access

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 12:45:25)


***

### [2024-11-11 12:45:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182002)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 12:45:28)


***

### [2024-11-11 12:47:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182003)
>Power Converter Faults  
  
Device: RPTK.SR4.RA43U2.L4 (RPTK.SR4.RA43U2.L4)  
Time: 2024-11-11 12:43:47,708  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734686:6532462?archive=1734686:6532464?archive=1734686:6532468?archive=1734686:6532471?archive=1734686:6532474?archive=1734686:6532476)  
  
  
Device: RPTK.SR4.RA43U3.L4 (RPTK.SR4.RA43U3.L4)  
Time: 2024-11-11 12:43:48,028  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734685:6532461?archive=1734685:6532465?archive=1734685:6532467?archive=1734685:6532470?archive=1734685:6532473?archive=1734685:6532475)  
  
  
Device: RPTK.SR4.RA47U3.R4 (RPTK.SR4.RA47U3.R4)  
Time: 2024-11-11 12:43:50,793  
Faults: NO\_PC\_PERMIT FAST\_ABORT  
VS faults: EXTERNAL FAST ABORT, CLAMPED, MCB  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734687:6532463?archive=1734687:6532466?archive=1734687:6532469?archive=1734687:6532472?archive=1734687:6532477?archive=1734687:6532478)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/11 12:47:15)


***

### [2024-11-11 12:51:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182006)
>Power Converter Faults  
  
Device: RPHGC.UA87.RTQX2.R8 (RPHGC.UA87.RTQX2.R8)  
Time: 2024-11-11 12:47:33,573  
Faults: NO\_PC\_PERMIT VS\_STATE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734699:6532487?archive=1734699:6532512?archive=1734699:6532514?archive=1734699:6532516?archive=1734699:6532528?archive=1734699:6532539)  
  
  
Device: RPMBC.USC55.RTQX1.L5 (RPMBC.USC55.RTQX1.L5)  
Time: 2024-11-11 12:47:33,343  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED, AUX POWER SUPPLY / FANS  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734688:6532479?archive=1734688:6532511?archive=1734688:6532513?archive=1734688:6532515?archive=1734688:6532527?archive=1734688:6532540)  
  
  
Device: RPMBC.UA23.RTQX1.L2 (RPMBC.UA23.RTQX1.L2)  
Time: 2024-11-11 12:47:33,543  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED, PRECHARGE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734693:6532480?archive=1734693:6532499?archive=1734693:6532501?archive=1734693:6532519?archive=1734693:6532536?archive=1734693:6532542)  
  
  
Device: RPHFC.USC55.RQX.L5 (RPHFC.USC55.RQX.L5)  
Time: 2024-11-11 12:47:33,353  
Faults: NO\_PC\_PERMIT VS\_STATE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734691:6532482?archive=1734691:6532492?archive=1734691:6532502?archive=1734691:6532525?archive=1734691:6532534?archive=1734691:6532541)  
  
  
Device: RPMBC.UA83.RTQX1.L8 (RPMBC.UA83.RTQX1.L8)  
Time: 2024-11-11 12:47:33,483  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734696:6532485?archive=1734696:6532494?archive=1734696:6532504?archive=1734696:6532523?archive=1734696:6532531?archive=1734696:6532544)  
  
  
Device: RPMBC.UL16.RTQX1.R1 (RPMBC.UL16.RTQX1.R1)  
Time: 2024-11-11 12:47:33,543  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734694:6532481?archive=1734694:6532498?archive=1734694:6532509?archive=1734694:6532517?archive=1734694:6532530?archive=1734694:6532545)  
  
  
Device: RPHFC.UA83.RQX.L8 (RPHFC.UA83.RQX.L8)  
Time: 2024-11-11 12:47:33,493  
Faults: NO\_PC\_PERMIT VS\_STATE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734689:6532483?archive=1734689:6532493?archive=1734689:6532508?archive=1734689:6532522?archive=1734689:6532537?archive=1734689:6532543)  
  
  
Device: RPMBC.UA27.RTQX1.R2 (RPMBC.UA27.RTQX1.R2)  
Time: 2024-11-11 12:47:33,643  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734690:6532484?archive=1734690:6532500?archive=1734690:6532505?archive=1734690:6532521?archive=1734690:6532533?archive=1734690:6532546)  
  
  
Device: RPMBC.UL14.RTQX1.L1 (RPMBC.UL14.RTQX1.L1)  
Time: 2024-11-11 12:47:33,563  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED, PRECHARGE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734698:6532490?archive=1734698:6532496?archive=1734698:6532506?archive=1734698:6532518?archive=1734698:6532538?archive=1734698:6532548)  
  
  
Device: RPMBC.UL557.RTQX1.R5 (RPMBC.UL557.RTQX1.R5)  
Time: 2024-11-11 12:47:33,423  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734695:6532488?archive=1734695:6532495?archive=1734695:6532503?archive=1734695:6532526?archive=1734695:6532535?archive=1734695:6532547)  
  
  
Device: RPHFC.UL557.RQX.R5 (RPHFC.UL557.RQX.R5)  
Time: 2024-11-11 12:47:33,433  
Faults: NO\_PC\_PERMIT VS\_STATE  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734692:6532486?archive=1734692:6532491?archive=1734692:6532507?archive=1734692:6532524?archive=1734692:6532529?archive=1734692:6532550)  
  
  
Device: RPMBC.UA87.RTQX1.R8 (RPMBC.UA87.RTQX1.R8)  
Time: 2024-11-11 12:47:33,563  
Faults: NO\_PC\_PERMIT VS\_FAULT  
VS faults: OFF RECEIVED  
PowerSpy: [View logs](https://cern.ch/powerspy/?archive=1734697:6532489?archive=1734697:6532497?archive=1734697:6532510?archive=1734697:6532520?archive=1734697:6532532?archive=1734697:6532549)  


creator:	 pofgcadm  @cs-ccr-abpo1.cern.ch (2024/11/11 12:51:04)


***

### [2024-11-11 12:52:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182007)
>ALL LHC VACUUM VALVES CLOSED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 12:52:26)


***

### [2024-11-11 12:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182008)
>LHC SEQ: UNDULATOR L4 AND R4 OFF

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/11 12:52:51)


***

### [2024-11-11 13:12:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182022)
>CMS SMP card fixed (exchanged power supply) by R.Secondo

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 13:12:30)


***

### [2024-11-11 14:02:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182046)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:


creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:02:13)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860466/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:02:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860466/content)


***

### [2024-11-11 14:03:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182047)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:


creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:03:35)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860467/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:03:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860467/content)


***

### [2024-11-11 14:06:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182050)
>Migration scheduled by cconfsrv was triggered by controls.configuration.service@cern.ch. The following device(s) were synchronized from CCS to LSA:


creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:06:50)

[devices](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860476/content)
creator:	 copera  @cs-ccr-inca1.cern.ch (2024/11/11 14:06:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860476/content)


***

### [2024-11-11 14:14:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182052)
>DAVID CHRISTIAN GLENAT(DGLENAT) assigned RBAC Role: RF-LHC-Piquet and will expire on: 11-NOV-24 09.14.26.712000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/11 14:14:29)


***

### [2024-11-11 14:19:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182055)
>ANDREA CALIA(ACALIA) assigned RBAC Role: LHC-FEEDBACK-PIQUET and will expire on: 11-NOV-24 09.19.54.490000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/11 14:19:56)


***

### [2024-11-11 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182076)
>Started the shift in stable beams.  
RF L7B2 tripped shortly after dumping the beam.  
After checking with Helga, I reset the fault and started another fill to be dumped at 12 for the scheduled access.  
  
Filling was very painful (as the previous ones). With Jorg we masked the IQC B1 because it was not working without BQM.  
Also, SIS INJECTION\_BUCKET\_B1 appear to be working by getting/setting Mode and NormalMode on the MountainRange FESA class (reference https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4181858 )  
  
Cycle was ok until physics with losses warning around 30-40% dump threshold.  
I was informed of a problem with SMP card for CMS. After checking with them, we decided to dump the fill with a trigger of CMS safety chain to check that it was correctly working.  
After the dump handshake, they triggered the dump with their BCM and it worked as expected.  
R.Secondo fixed the issue by replacing the power supply of the corresponsing SMP card.  
  
Access started after the dump and it is still ongoing in P4 (RF) and ALICE.  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 15:26:51)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860556/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 15:26:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3860556/content)


***

### [2024-11-11 15:03:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182069)
>PM25 distributor fixed by access team

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/11 15:03:24)


***

### [2024-11-11 15:51:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4182096)
>HELGA TIMKO(HTIMKO) assigned RBAC Role: RF-LHC-Piquet and will expire on: 11-NOV-24 06.51.04.019000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/11/11 15:51:05)


