# FILL 10010
**start**:		 2024-08-16 21:27:39.337738525+02:00 (CERN time)

**end**:		 2024-08-17 10:39:56.704613525+02:00 (CERN time)

**duration**:	 0 days 13:12:17.366875


***

### [2024-08-16 21:27:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126976)
>LHC RUN CTRL: New FILL NUMBER set to 10010

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 21:27:40)


***

### [2024-08-16 21:28:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126978)
>I misclicked on equip state and also put to standby the RBs.. so I'll do the precycle before the test with pilot  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 21:29:39)


***

### [2024-08-16 21:28:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126977)
>LHC SEQ: Precycle started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 21:28:57)


***

### [2024-08-16 21:59:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126984)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 21:59:58)


***

### [2024-08-16 22:04:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126989)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:04:46)


***

### [2024-08-16 22:07:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126990)
>
```
Finally feedbacks are converging !  
  
RT corrections < 5urad  

```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:08:41)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-16_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733424/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/16 22:07:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733424/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-16_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733426/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:08:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733426/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-08-16_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733428/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:07:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733428/content)


***

### [2024-08-16 22:11:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126991)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:11:12)


***

### [2024-08-16 22:17:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126992)
>Wirescans

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:17:11)

[ LHC WIRESCANNER APP 24-08-16_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733430/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:17:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733430/content)

[ LHC WIRESCANNER APP 24-08-16_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733432/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:17:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733432/content)

[ LHC WIRESCANNER APP 24-08-16_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733434/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:17:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733434/content)

[ LHC WIRESCANNER APP 24-08-16_22:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733436/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 22:17:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733436/content)


***

### [2024-08-16 22:47:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126998)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 22:47:20)

[ LHC Fast BCT v1.3.2 24-08-16_22:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733464/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:10:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733464/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-16_22:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733466/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 22:47:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733466/content)


***

### [2024-08-16 22:47:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4126999)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:47:33)


***

### [2024-08-16 22:47:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127000)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:47:42)


***

### [2024-08-16 22:47:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127001)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:47:46)


***

### [2024-08-16 22:47:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127002)
>LHC Injection Complete

Number of injections actual / planned: 54 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:36:35 / 0:33:48 (108.2 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/16 22:47:47)


***

### [2024-08-16 22:49:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127008)
>Started the shift during access.  
During the recovery I rebooted the MKI bets frontend and called Viliam and Nicolas to reset feedback tracking and trigger/retrigger FAULTY status on MKD B1.  
I also called MPE piquet to recover MQ 13L2 since it was not giving QPS OK.  
  
During the injection of the pilot, I noticed the orbit feedback not converging.....  
I checked the possible failure cases of the OFB but in the end the current sent over the RT channel was not taken into account by some converters.  
After a second attempt, it looks like the converters of the sectors around P4 were not responsing to the RT trims.  
This was confirmed by running the RT commissioning script as suggested by Jorg.  
  
I contacted then P. Plutecki from EPC to have a look on the FGC side.  
We checked a few crates but he didn't find any difference in the logs from the ones that are working, so he rebooted one.  
I checked and indeed the reboot fixed the situation, but of course tripped the converters.  
Since it was easy to miss a COD by checking crate by crate, we decided to reboot all the crates (72) connected to at least one COD.  
Unfortunately, some crates are connected to IPQs so a precycle was needed.  
  
After the precycle, injection was smooth and leaving the machine during the ramp.  
  
Notes:  
- preparation for the fill and for ramp are now in parallel in the sequencer using the new feature  
- due to the situation of the compensator in P4, we agreed with EN-EL to not switch ON the RF in case of a trip until we're at 59GeV as this could crate problems with voltage control on their side.  
- RF OFF and RF ON during the RF preparation sequence is not skipped  
  
AC

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:11:17)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733470/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:11:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733470/content)


***

### [2024-08-16 22:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127003)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:50:51)


***

### [2024-08-16 22:50:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127005)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 22:50:51)


***

### [2024-08-16 23:12:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127009)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:12:08)


***

### [2024-08-16 23:12:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127011)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:12:24)


***

### [2024-08-16 23:12:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127011)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:12:24)


***

### [2024-08-16 23:21:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127015)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:21:01)


***

### [2024-08-16 23:21:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127016)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:21:45)


***

### [2024-08-16 23:22:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127018)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:22:38)


***

### [2024-08-16 23:26:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127019)
>IP optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:26:38)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733473/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:26:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733473/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733475/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:29:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733475/content)


***

### [2024-08-16 23:28:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127020)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:28:09)


***

### [2024-08-16 23:28:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127021)
>
```
Tune trims: +2e-3 in B1 H and +1e-3 in B2H -> kept
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:37:15)

[Accelerator Cockpit v0.0.38 24-08-16_23:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733477/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/16 23:28:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733477/content)

[ LHC Beam Losses Lifetime Display 24-08-16_23:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733479/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/16 23:28:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733479/content)


***

### [2024-08-16 23:30:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127022)
>separation levelling in IP2 and IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:30:41)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733481/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:30:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733481/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733483/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:36:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733483/content)


***

### [2024-08-16 23:34:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127023)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:34:42)


***

### [2024-08-16 23:35:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127024)
>emittance scans

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:35:16)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733485/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:35:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733485/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733487/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:41:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733487/content)


***

### [2024-08-16 23:36:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127025)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/16 23:36:40)


***

### [2024-08-16 23:42:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127026)
>lieftime still below 20h despite the tune trims

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/16 23:42:55)

[ LHC Beam Losses Lifetime Display 24-08-16_23:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733489/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/16 23:42:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733489/content)


***

### [2024-08-16 23:51:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127027)
>losses

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:51:57)

[ LHC Fast BCT v1.3.2 24-08-16_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733491/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:52:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733491/content)


***

### [2024-08-16 23:52:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127028)
>status at 60 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:52:24)

[ LHC Fast BCT v1.3.2 24-08-16_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733493/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:52:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733493/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-16_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733495/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:52:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733495/content)

[ LHC Beam Losses Lifetime Display 24-08-16_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733497/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/16 23:52:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733497/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733499/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:52:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733499/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733501/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:53:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733501/content)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733503/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:53:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733503/content)


***

### [2024-08-16 23:55:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127030)
>All IPs on separation levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:55:58)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733505/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:56:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733505/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733507/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:56:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733507/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733509/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:56:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733509/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-16_23:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733511/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/16 23:56:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733511/content)


***

### [2024-08-17 03:10:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127041)
>status at 42 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:11)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733542/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733542/content)

[ LHC Fast BCT v1.3.2 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733544/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:11:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733544/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733546/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733546/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733548/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733548/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733550/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733550/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733552/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733552/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733554/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 03:10:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733554/content)

[ LHC Beam Losses Lifetime Display 24-08-17_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733556/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/17 03:10:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733556/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-17_03:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733558/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/17 03:11:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733558/content)


***

### [2024-08-17 05:03:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127066)
>again beta\* levelling steps shortened by bunch flattening in both beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:04:22)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733595/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:04:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733595/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-17_05:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733597/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/17 05:04:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733597/content)


***

### [2024-08-17 05:31:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127068)
>status at 30 cm

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:49)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733599/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733599/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733601/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733601/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733603/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733603/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733605/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733605/content)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-17_05:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733607/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:31:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733607/content)

[ LHC Fast BCT v1.3.2 24-08-17_05:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733609/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:32:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733609/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-17_05:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733611/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 05:32:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733611/content)


***

### [2024-08-17 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127077)
>
```
**SHIFT SUMMARY:**  
  
Arrived during the ramp, squeezed, brought to stable beams, and stayed there.  
  
Benoit  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 07:08:29)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733620/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 07:06:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733620/content)


***

### [2024-08-17 07:26:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127079)
>BBLR @350A, +0.001 in Qh/V B1

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/17 07:27:04)

[ LHC Beam Losses Lifetime Display 24-08-17_07:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733625/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/17 07:27:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3733625/content)


***

### [2024-08-17 10:34:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127097)
>Global Post Mortem Event

Event Timestamp: 17/08/24 10:34:49.698
Fill Number: 10010
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 24352 / 24542 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 13-LHCb\_Mag: A T -> F on CIB.UA87.R8.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/17 10:37:41)


***

### [2024-08-17 10:34:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127145)
>Global Post Mortem Event Confirmation

Dump Classification: LHC Experiments
Operator / Comment: delph / Trip of teh LHCb magnet


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/17 11:19:00)


***

### [2024-08-17 10:37:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127094)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/17 10:37:05)


***

### [2024-08-17 10:37:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127095)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/17 10:37:07)


***

### [2024-08-17 10:37:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4127096)
>LHCb magnet tripped. LHCb control room is contacting their piquet.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/17 10:37:40)


