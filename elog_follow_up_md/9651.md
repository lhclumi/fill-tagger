# FILL 9651
**start**:		 2024-05-19 16:49:31.843863525+02:00 (CERN time)

**end**:		 2024-05-20 02:31:21.921238525+02:00 (CERN time)

**duration**:	 0 days 09:41:50.077375


***

### [2024-05-19 16:49:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070875)
>LHC RUN CTRL: New FILL NUMBER set to 9651

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:49:32)


***

### [2024-05-19 16:52:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070876)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 16:52:11)


***

### [2024-05-19 16:54:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070877)
>wire scans of the fist 8b injection

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 16:54:38)

[ LHC WIRESCANNER APP 24-05-19_16:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597850/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 16:54:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597850/content)

[ LHC WIRESCANNER APP 24-05-19_16:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597852/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 16:54:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597852/content)

[ LHC WIRESCANNER APP 24-05-19_16:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597854/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 16:54:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597854/content)

[ LHC WIRESCANNER APP 24-05-19_16:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597856/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 16:54:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597856/content)


***

### [2024-05-19 17:19:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070883)
>injected beams

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 17:20:05)

[ LHC Fast BCT v1.3.2 24-05-19_17:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597878/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 17:20:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597878/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-05-19_17:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597882/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 17:20:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597882/content)


***

### [2024-05-19 17:20:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070884)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:20:40)


***

### [2024-05-19 17:21:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070885)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:21:55)


***

### [2024-05-19 17:22:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070886)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:22:06)


***

### [2024-05-19 17:22:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070887)
>LHC Injection CompleteNumber of injections actual / planned: 49 / 38SPS SuperCycle length: 43.2 [s]Actual / minimum time: 0:37:58 / 0:32:21 (117.3 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/19 17:22:07)


***

### [2024-05-19 17:25:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070890)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:25:57)


***

### [2024-05-19 17:25:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070888)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:25:57)


***

### [2024-05-19 17:42:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070895)
>MICHAEL ARTUR HOSTETTLER(MIHOSTET) assigned RBAC Role: LHC-OP-SOFTWARE-Piquet

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/05/19 17:42:36)


***

### [2024-05-19 17:47:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070896)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:47:15)


***

### [2024-05-19 17:47:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070898)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:47:30)


***

### [2024-05-19 17:56:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070900)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:56:02)


***

### [2024-05-19 17:56:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070901)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:56:57)


***

### [2024-05-19 17:57:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070903)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 17:57:54)


***

### [2024-05-19 18:02:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070906)
>IP1/5 optimization

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:03:11)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597885/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:03:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597885/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597887/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:03:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597887/content)


***

### [2024-05-19 18:03:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070907)
>Separate Atlas -4.5 sigma as requested by the procedure.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:04:00)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597889/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:04:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597889/content)


***

### [2024-05-19 18:07:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070909)
>Optimizing IP 2 and 8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:07:35)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597891/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:07:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597891/content)


***

### [2024-05-19 18:07:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070910)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 18:07:58)


***

### [2024-05-19 18:08:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070912)
>
```
LHCb with initial separation - looks OK to go for a full machine, very close to their target  
mu = ~3.7 (typical target ~3.9)  
L = ~8.5 Hz/ub with 16b colliding -> expect ~1100 Hz/ub with 2100b
```


creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 18:15:13)

[ LHC Luminosity Scan Client 0.58.4 [pro] 24-05-19_18:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597899/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/19 18:12:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597899/content)


***

### [2024-05-19 18:10:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070911)
>Start levelling for IP2 and 8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:10:48)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597895/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:10:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597895/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_18:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597897/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 18:10:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597897/content)


***

### [2024-05-19 18:16:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070913)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/19 18:16:39)


***

### [2024-05-19 20:56:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070916)
>stop the bunch length control

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/19 20:56:52)

[STABLE Beams - Bunch Length Control 24-05-19_20:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597903/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/19 20:56:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597903/content)


***

### [2024-05-19 20:59:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070917)
>OFB stopped, IP2/8 sep levelling stopped  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 20:59:43)


***

### [2024-05-19 21:00:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070918)
>start first mu scan for Atlas

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:00:16)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597905/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:00:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597905/content)


***

### [2024-05-19 21:45:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070920)
>first mu scan finished.  
Switch ON bunch length control, OFB and levelling for IP2/8  
  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:46:36)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597909/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:55:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597909/content)


***

### [2024-05-19 21:57:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070922)
>reduce separation in IP1 to prepare for the B8 levlling step

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:57:49)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597912/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:57:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597912/content)


***

### [2024-05-19 21:58:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070926)
>start B\* levelling with target IP5 pile-up = 55

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 21:59:56)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_21:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597916/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:00:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597916/content)


***

### [2024-05-19 22:20:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070930)
>target reached with B8 levelling

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:20:28)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597922/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:20:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597922/content)


***

### [2024-05-19 22:21:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070931)
>IP5 re-optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:22:06)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597924/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:22:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597924/content)


***

### [2024-05-19 22:22:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070932)
>restart levelling with CMS target = 69

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:23:17)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597926/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:23:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597926/content)


***

### [2024-05-19 22:27:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070933)
>As we are still limited at 36cm B\*, we don't manage to reach the target of 69.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:28:01)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597928/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:28:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597928/content)


***

### [2024-05-19 22:31:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070935)
>IP5 re-optimized

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:31:25)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597930/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:31:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597930/content)


***

### [2024-05-19 22:40:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070937)
>start second mu scan for Atlas

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:40:37)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_22:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597932/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 22:42:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597932/content)


***

### [2024-05-19 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070945)
>
```
**SHIFT SUMMARY:**  
When I arrived Atlas solenoid was switching ON but not ready yet, and in the meanwhile we did a cycle with 2 colliding indiv to check the beam positions in collisions point 8 after the polarity change. Atlas was also checked. We did the first B* levelling step to check, and found that the change of gap for TCL6.R1 was not done on the B* levelling BP and we had a mismatch error when trying to load the B* step.  
This was solved for now by removing the TCL6.R1 from the group of collimators to move with B* levelling (indeed its settings are constant during the levelling). It will have to be put back when Daniele will have change the settings in the B* levelling BP.  
  
After this test cycle, Atlas solenoid was OK and we could proceed with the calibration-transfer fill.  
The cycle was very smooth, mu scans ongoing in IP1...  
  
* To be noted:

  
In SPS, the RF problem is solved and no further access should be needed.  
The AGK settings needs to be reverted before injected trains and the new settings validated.  
LHCb needs an urgent access after this fill.  
  
  

```


creator:	 delph  @194.12.189.61 (2024/05/19 23:15:53)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597938/content)
creator:	 delph  @194.12.189.61 (2024/05/19 23:15:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597938/content)


***

### [2024-05-19 23:26:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070948)
>
```

```
Validation for AGK change  
  
* Get the OK from ALICE and LHCb that splashes are OK for them, they may need some time to safe their configuration  
* open wrap dashboard "AGK validation", it helps see the losses on the TDIs  
* The goal is to inject in and out of the abord gap on both extreme of the ring, with the new last allowed injection bucket at 33211  
* try to inject in the abord gap, 33251 and 35601: at each injection the beam should hit the TDI. The vistar injection should also show a red box "FIB - AGK"  
* try to inject outside the abord gap, 1 and 33211: each injection should be OK  

```
  
Yann
```


creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/20 00:15:11)

[screenShot_May_19th_2024_23_33_41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597939/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/19 23:34:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597939/content)


***

### [2024-05-19 23:35:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070949)
>starting to put back IP1 HO

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:35:30)

[ LHC Luminosity Scan Client 0.58.4 [pro] 24-05-19_23:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597941/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:35:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597941/content)


***

### [2024-05-19 23:35:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070949)
>starting to put back IP1 HO

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:35:30)

[ LHC Luminosity Scan Client 0.58.4 [pro] 24-05-19_23:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597941/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:35:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597941/content)


***

### [2024-05-19 23:35:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070950)
>
```
Reverted IQC (abort gap start, MKI min and max kick length) and SIS AGK settings to values for 3x36b operation.  
  
The change to VDM was done here (Thu 16.5 6:58):  
<https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4068719>  
  
/JW  

```


creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/19 23:43:35)

[ LSA Applications Suite (v 16.5.36) 24-05-19_23:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597943/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/19 23:35:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597943/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_23:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597945/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/19 23:37:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597945/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_23:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597947/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/19 23:37:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597947/content)

[ LSA Applications Suite (v 16.5.36) 24-05-19_23:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597949/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/05/19 23:38:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597949/content)


***

### [2024-05-19 23:39:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070951)
>IP1&5 optimized HO

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:39:58)

[ LHC Luminosity Scan Client 0.58.4 [pro] 24-05-19_23:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597951/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:39:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597951/content)


***

### [2024-05-19 23:40:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070952)
>starting 2h acquisition for ATLAS

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/19 23:40:08)


***

### [2024-05-19 23:50:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070953)
>started CMS scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 23:50:35)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-19_23:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597953/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/19 23:50:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597953/content)


***

### [2024-05-20 00:09:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070954)
>changes reverted for MKI & AGK settings of 3\*36b

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/20 00:09:21)

[ LSA Applications Suite (v 16.5.36) 24-05-20_00:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597955/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/05/20 00:09:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597955/content)


***

### [2024-05-20 01:06:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070959)
>scan completed

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:06:13)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-20_01:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597965/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:06:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597965/content)


***

### [2024-05-20 01:07:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070960)
>orbit drift during CMS scan

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/20 01:07:15)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-20_01:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597967/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/20 01:07:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597967/content)


***

### [2024-05-20 01:08:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070961)
>optmizations

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/20 01:08:17)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-20_01:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597969/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/20 01:08:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597969/content)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-20_01:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597971/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:10:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597971/content)


***

### [2024-05-20 01:16:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070962)
>long blowup enabled between scans

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/20 01:16:46)

[STABLE Beams - Bunch Length Control 24-05-20_01:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597973/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/05/20 01:16:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597973/content)


***

### [2024-05-20 01:24:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070963)
>propagating 2mm gap on TCL.6R1.B1 also in b\* levelling BP

creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/20 01:24:58)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-20_01:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597975/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/05/20 01:24:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597975/content)


***

### [2024-05-20 01:37:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070964)
>CMS emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:38:01)

[ LHC Luminosity Scan Client 0.58.3 [pro] 24-05-20_01:37.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597977/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:38:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597977/content)


***

### [2024-05-20 01:45:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070965)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:45:08)


***

### [2024-05-20 01:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070966)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:48:34)


***

### [2024-05-20 01:50:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070967)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:50:09)


***

### [2024-05-20 01:51:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070973)
>Global Post Mortem EventEvent Timestamp: 20/05/24 01:51:00.701Fill Number: 9651Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMSEnergy: 6799320 [MeV]Intensity B1/B2: 1881 / 1878 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCHFirst BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/20 01:53:50)


***

### [2024-05-20 01:51:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070968)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:51:46)


***

### [2024-05-20 01:51:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070969)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:51:52)


***

### [2024-05-20 01:52:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070970)
>Event created from ScreenShot Client.  
udp:..multicast-bevlhc1:1234 - VLC media player 24-05-20\_01:52.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:52:45)

[udp:..multicast-bevlhc1:1234 - VLC media player 24-05-20_01:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597979/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:52:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597979/content)


***

### [2024-05-20 01:52:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070971)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:52:49)


***

### [2024-05-20 01:52:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070972)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:52:51)


***

### [2024-05-20 01:56:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070974)
>LHC SEQ: preparing the LHC for access in service areas

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:56:32)


***

### [2024-05-20 01:56:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070975)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/20 01:56:50)


***

### [2024-05-20 01:59:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070976)
>Event created from ScreenShot Client.  
XPOC Viewer - PRO 24-05-20\_01:59.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:59:50)

[ XPOC Viewer - PRO 24-05-20_01:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597981/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:59:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597981/content)

[ XPOC Viewer - PRO 24-05-20_01:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597983/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 01:59:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597983/content)


***

### [2024-05-20 02:01:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4070978)
>Event created from ScreenShot Client.  
XPOC PRO GUI : 8.5.0 24-05-20\_02:01.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 02:01:59)

[ XPOC PRO GUI : 8.5.0 24-05-20_02:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597985/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 02:01:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597985/content)

[ XPOC PRO GUI : 8.5.0 24-05-20_02:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597987/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/20 02:02:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3597987/content)


