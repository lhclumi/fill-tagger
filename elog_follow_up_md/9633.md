# FILL 9633
**start**:		 2024-05-15 11:09:57.789488525+02:00 (CERN time)

**end**:		 2024-05-15 18:10:38.936363525+02:00 (CERN time)

**duration**:	 0 days 07:00:41.146875


***

### [2024-05-15 11:09:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067929)
>LHC RUN CTRL: New FILL NUMBER set to 9633

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:09:58)


***

### [2024-05-15 11:10:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067936)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:10:50)


***

### [2024-05-15 11:12:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067937)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:12:11)


***

### [2024-05-15 11:13:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067938)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:13:06)


***

### [2024-05-15 11:15:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067940)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:15:17)


***

### [2024-05-15 11:17:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067943)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:17:38)


***

### [2024-05-15 11:18:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067946)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:18:12)


***

### [2024-05-15 11:25:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067947)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:25:10)


***

### [2024-05-15 11:26:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067951)
>Event created from ScreenShot Client.  
LHC.BSRL\_PROFILES.B1 - UCAP hierarchy (beta!) 24-05-15\_11:26.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 11:26:51)

[LHC.BSRL_PROFILES.B1 - UCAP hierarchy (beta!) 24-05-15_11:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592039/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 11:26:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592039/content)


***

### [2024-05-15 11:27:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067952)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:27:14)


***

### [2024-05-15 11:29:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067955)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:29:38)


***

### [2024-05-15 11:29:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067957)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:29:39)


***

### [2024-05-15 11:29:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067959)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:29:41)


***

### [2024-05-15 11:29:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067963)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:29:55)


***

### [2024-05-15 11:31:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067966)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:31:54)


***

### [2024-05-15 11:33:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067970)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:33:40)


***

### [2024-05-15 11:33:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067972)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:33:54)


***

### [2024-05-15 11:41:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067979)
>Event created from ScreenShot Client.  
cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-05-15\_11:41.png

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 11:41:48)

[cwe-513-vpl746.cern.ch:1 (gtrad) - TigerVNC 24-05-15_11:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592082/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 11:41:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592082/content)


***

### [2024-05-15 11:49:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4067990)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 11:49:25)


***

### [2024-05-15 12:12:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068008)
>Event created from ScreenShot Client.  
udp:..multicast-bevlhc1:1234 - VLC media player 24-05-15\_12:12.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 12:12:30)

[udp:..multicast-bevlhc1:1234 - VLC media player 24-05-15_12:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592192/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 12:12:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592192/content)


***

### [2024-05-15 12:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068011)
>corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 12:21:31)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_12:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592196/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 12:21:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592196/content)

[Accelerator Cockpit v0.0.35 24-05-15_12:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592200/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 12:22:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592200/content)

[Accelerator Cockpit v0.0.35 24-05-15_12:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592204/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 12:23:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592204/content)

[Accelerator Cockpit v0.0.35 24-05-15_12:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592208/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 12:23:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592208/content)

[Accelerator Cockpit v0.0.35 24-05-15_12:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592214/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 12:24:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592214/content)


***

### [2024-05-15 12:29:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068016)
>created new filling scheme to adapt to AGK with 36btrains

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 12:29:34)

[INJECTION SCHEME EDITOR  v 5.1.65.1.6 24-05-15_12:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592218/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 12:29:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592218/content)


***

### [2024-05-15 12:32:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068018)
>SETUP flag

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 12:33:03)

[Safe Machine Parameters in CCC : Overview GUI 24-05-15_12:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592224/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/05/15 12:33:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592224/content)


***

### [2024-05-15 12:34:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068019)
>LHC RUN CTRL: BEAM MODE changed to INJECTION SETUP BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 12:34:00)


***

### [2024-05-15 13:24:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068060)
>Channeling B2H identifiedScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:24:52)

[tmpScreenshot_1715772291.9504688.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592366/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:24:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592366/content)


***

### [2024-05-15 13:28:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068065)
>B2H channeling identifiedScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:28:42)

[tmpScreenshot_1715772522.2083907.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592376/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:28:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592376/content)


***

### [2024-05-15 13:34:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068074)
>Confirmed and sentScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:34:34)

[tmpScreenshot_1715772874.2835276.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592390/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 13:34:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592390/content)


***

### [2024-05-15 14:01:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068090)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:01:21)


***

### [2024-05-15 14:13:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068098)
>LHC RUN CTRL: BEAM MODE changed to INJECTION SETUP BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:13:19)


***

### [2024-05-15 14:29:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068112)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:29:21)


***

### [2024-05-15 14:29:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068113)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:29:39)


***

### [2024-05-15 14:29:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068114)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:29:47)


***

### [2024-05-15 14:33:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068115)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 14:33:14)


***

### [2024-05-15 14:36:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068121)
>from 450 GEV to 1 TeV

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 14:36:31)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_14:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592470/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 14:36:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592470/content)

[Accelerator Cockpit v0.0.35 24-05-15_14:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592485/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 14:38:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592485/content)


***

### [2024-05-15 14:39:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068125)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.35 24-05-15\_14:39.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 14:39:33)

[Accelerator Cockpit v0.0.35 24-05-15_14:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592493/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 14:39:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592493/content)


***

### [2024-05-15 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068290)
>
```
**SHIFT SUMMARY:**  
  
- got the machine during end of MD11723 (Schottky, summary [here](https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4067825))   
- changed hypercycle and prepared for MD10343 (ramp in steps and channeling at intermediate energy)  
- leaving the machine duirng MD10343  
  
* To be reverted:

  
- BIS ring and TI masks  
- SIS goniometers, PC, TDI  
- goniometer pipe status  
  
* Miscellaneous:

  
- created new filling scheme to adapt AGK to 36b trains for crystal MDs  
  
  

```
Daniele  
  
  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 18:10:15)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593102/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 18:10:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593102/content)


***

### [2024-05-15 15:14:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068147)
>B2H channeling identifiedScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:14:19)

[tmpScreenshot_1715778858.9418263.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592579/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:14:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592579/content)


***

### [2024-05-15 15:19:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068151)
>B2H textbook channeling at 1 TeVScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:19:50)

[tmpScreenshot_1715779189.9767127.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592591/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:19:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592591/content)


***

### [2024-05-15 15:22:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068152)
>Channeling optimized B2HScreenshot attached: Optimization (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:22:03)

[tmpScreenshot_1715779322.9409065.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592593/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 15:22:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592593/content)


***

### [2024-05-15 15:44:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068168)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 15:44:07)


***

### [2024-05-15 15:44:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068170)
>ramping from 1 to 3 TeV  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 15:45:03)


***

### [2024-05-15 15:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068175)
>RT trims from 1 toi 3 TeV

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:02)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_15:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592653/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592653/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592655/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592655/content)


***

### [2024-05-15 15:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068175)
>RT trims from 1 toi 3 TeV

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:02)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_15:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592653/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592653/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_15:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592655/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 15:51:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592655/content)


***

### [2024-05-15 15:54:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068177)
>Chroma at 3 TeV

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 15:54:53)

[Accelerator Cockpit v0.0.35 24-05-15_15:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592657/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 15:54:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592657/content)


***

### [2024-05-15 15:55:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068178)
>After correction

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 15:55:37)

[Accelerator Cockpit v0.0.35 24-05-15_15:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592659/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 15:55:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592659/content)


***

### [2024-05-15 15:55:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068179)
>
```
trimmed in the -0.5mm IP8-H shift for the VdM cycle  
Offset at the TCTPH +0.6236mm (both B1 and B2 in orbit coord system)  
  
IP5-H shift +0.3mm  
Offset at TCTPH -0.5948mm (both B1 and B2 in orbit coord system)  
  
Added the knobs to the VdM ref orbit and re-generated the ref orbit for the PHYSICS BP.  
Updated and reloaded the PCInterlock BP.  

```


creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:33:51)

[ LSA Applications Suite (v 16.5.36) 24-05-15_15:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592661/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 15:56:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592661/content)

[OpenYASP DV LHCRING . B1 . PHYSICS-6.8TeV-2024RP-VdM_V1@160_[END] 24-05-15_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592665/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 15:56:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592665/content)

[OpenYASP DV LHCRING . B2 . PHYSICS-6.8TeV-2024RP-VdM_V1@160_[END] 24-05-15_15:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592667/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 15:56:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592667/content)

[Terminal - mihostet@cs-ccr-seq1:.opt.seq-lhc 24-05-15_15:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592675/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 15:59:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592675/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_16:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592769/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 16:20:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592769/content)

[OpenYASP DV LHCRING . B1 . PHYSICS-6.8TeV-2024RP-VdM_V1@160_[END] 24-05-15_16:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592779/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 16:26:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592779/content)


***

### [2024-05-15 16:29:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068209)
>B2H channeling identified at 3 TeVScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:29:10)

[tmpScreenshot_1715783350.1491375.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592797/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:29:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592797/content)


***

### [2024-05-15 16:35:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068216)
>B2H channeling identifiedScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:35:54)

[tmpScreenshot_1715783753.909079.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592816/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:35:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592816/content)


***

### [2024-05-15 16:38:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068222)
>trimming in correction to TCT centre for VdM

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:38:48)

[ LSA Applications Suite (v 16.5.36) 24-05-15_16:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592829/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:39:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592829/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_16:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592831/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:39:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592831/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_16:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592833/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:43:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592833/content)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-15_16:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592835/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:40:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592835/content)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-15_16:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592837/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:41:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592837/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_16:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592847/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/05/15 16:43:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592847/content)


***

### [2024-05-15 16:38:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068221)
>Channeling optimizedScreenshot attached: Optimization (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:38:37)

[tmpScreenshot_1715783917.158658.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592825/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 16:38:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592825/content)


***

### [2024-05-15 17:01:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068242)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 17:01:59)


***

### [2024-05-15 17:02:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068244)
>Ramping from 3 to 5 TeV  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 17:02:28)


***

### [2024-05-15 17:08:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068250)
>RT trims during rampo from 3 to 5TeV

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 17:08:50)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-15_17:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592897/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/15 17:08:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592897/content)


***

### [2024-05-15 17:09:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068251)
>Chroma at 5 TeV

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 17:09:19)

[Accelerator Cockpit v0.0.35 24-05-15_17:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592903/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/15 17:09:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592903/content)


***

### [2024-05-15 17:11:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068252)
>generating collimator settings for MD7203 (IR7 de-squeeze)

creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:11:37)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-15_17:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592905/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:11:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592905/content)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-15_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592909/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:13:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592909/content)

[ LHC Collimator Settings App v0.9.6 connected to server LHC 24-05-15_17:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592911/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:13:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592911/content)


***

### [2024-05-15 17:15:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068256)
>copying settings from nominal

creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:05)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592915/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592915/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592917/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592917/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592919/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592919/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592921/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592921/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592923/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:16:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592923/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592925/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:17:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592925/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592927/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:17:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592927/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592929/content)
creator:	 lhcop  @cwo-ccc-d9lc.cern.ch (2024/05/15 17:17:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3592929/content)


***

### [2024-05-15 17:42:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068276)
>B2H channeling identified at 5 TeVScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:42:44)

[tmpScreenshot_1715787761.3364902.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593012/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:42:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593012/content)


***

### [2024-05-15 17:45:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068278)
>trimmed IP2 crossing angle in VdM PHYSICS BP down to 145urad (as last year)

creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 17:46:00)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593035/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 17:46:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593035/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_17:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593041/content)
creator:	 mihostet  @cs-513-michi9.cern.ch (2024/05/15 17:51:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593041/content)


***

### [2024-05-15 17:47:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068279)
>B2H channeling at 5 TeVScreenshot attached: Find (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:47:18)

[tmpScreenshot_1715788038.7705874.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593037/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:47:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593037/content)


***

### [2024-05-15 17:50:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068280)
>channeling optimized 5 TeVScreenshot attached: Optimization (B2H)  
*Sent From pyCrystalCockpit*

creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:50:57)

[tmpScreenshot_1715788257.082064.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593039/content)
creator:	 dmirarch  @cwo-ccc-d5lc.cern.ch (2024/05/15 17:50:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593039/content)


***

### [2024-05-15 18:00:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068284)
>parking position settings back to 53.5mm for VdM

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:01:10)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593075/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:01:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593075/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593081/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:01:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593081/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593085/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:02:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593085/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593087/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:02:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593087/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593089/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:02:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593089/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593091/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:03:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593091/content)

[ LSA Applications Suite (v 16.5.36) 24-05-15_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593093/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:03:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593093/content)


***

### [2024-05-15 18:08:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068295)
>Global Post Mortem EventEvent Timestamp: 15/05/24 18:08:05.016Fill Number: 9633Accelerator / beam mode: MACHINE DEVELOPMENT / RAMPEnergy: 4999440 [MeV]Intensity B1/B2: 22 / 11 [e^10 charges]Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCHFirst BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B2

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/15 18:10:55)


***

### [2024-05-15 18:08:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068380)
>Global Post Mortem Event ConfirmationDump Classification: Programmed DumpOperator / Comment: dmirarch / programmed dump after MD

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/05/15 18:50:19)


***

### [2024-05-15 18:09:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068289)
>changing hypercycle for the next MD

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 18:09:31)

[ Generation Application connected to server LHC (PRO database) 24-05-15_18:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593099/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/05/15 19:09:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3593099/content)


***

### [2024-05-15 18:10:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068298)
>***Summary MD10343 (part for crystal channeling at intermediate energy)***  
  
Performed angular and linear scan with B2H crystal,  
with first observation of channeling at 1, 3, 5 TeV!  
Very nice data collected, successfull MD, lots of data to analyse.  
  
R. Cai, K. Dewhurst, C. Maccani, D. Mirarchi, S. Redaelli, G. Ricci, A. Vella

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/05/15 18:11:38)


***

### [2024-05-15 18:10:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068292)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 18:10:23)


***

### [2024-05-15 18:10:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4068293)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/15 18:10:25)


