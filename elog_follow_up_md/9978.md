# FILL 9978
**start**:		 2024-08-05 18:34:44.410488525+02:00 (CERN time)

**end**:		 2024-08-06 08:29:49.241488525+02:00 (CERN time)

**duration**:	 0 days 13:55:04.831000


***

### [2024-08-05 18:34:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119507)
>LHC RUN CTRL: New FILL NUMBER set to 9978

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:34:44)


***

### [2024-08-05 18:36:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119514)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:36:01)


***

### [2024-08-05 18:38:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119515)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:38:09)


***

### [2024-08-05 18:38:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119517)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:38:26)


***

### [2024-08-05 18:39:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119518)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:39:31)


***

### [2024-08-05 18:39:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119522)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:39:53)


***

### [2024-08-05 18:40:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119524)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:40:57)


***

### [2024-08-05 18:41:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119525)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:41:01)


***

### [2024-08-05 18:41:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119527)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:41:34)


***

### [2024-08-05 18:42:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119530)
>LHC SEQ: ramping up ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:42:19)


***

### [2024-08-05 18:42:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119532)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:42:19)


***

### [2024-08-05 18:42:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119533)
>LHC SEQ: ramping up ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:42:23)


***

### [2024-08-05 18:42:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119534)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:42:53)


***

### [2024-08-05 18:43:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119536)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:43:59)


***

### [2024-08-05 18:44:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119538)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:44:00)


***

### [2024-08-05 18:44:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119540)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:44:02)


***

### [2024-08-05 18:44:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119544)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:44:16)


***

### [2024-08-05 18:44:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119546)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:44:57)


***

### [2024-08-05 18:45:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119548)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:45:04)


***

### [2024-08-05 18:46:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119551)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:46:16)


***

### [2024-08-05 18:48:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119555)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 18:48:00)


***

### [2024-08-05 19:09:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119563)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 19:09:43)


***

### [2024-08-05 19:09:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119565)
>Waiting for beam from the PS. Will not go to injection energy for the moment  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:10:17)


***

### [2024-08-05 19:24:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119569)
>
```
Tripped going to injection, RSD1.A67B1
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:25:05)

[Set SECTOR67 24-08-05_19:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713384/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/05 19:24:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713384/content)

[Circuit_RSD1_A67B1:   24-08-05_19:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713386/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:25:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713386/content)

[1 - RPMBB.UA67.RSD1.A67B1 Power Converter for the Circuit RSD1.A67B1    24-08-05_19:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713388/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:25:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713388/content)

[ Equip State 24-08-05_19:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713390/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:27:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713390/content)


***

### [2024-08-05 19:42:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119573)
>RSD1.A67B1 recovered

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:42:26)

[Monitoring application. Currently monitoring : LHC - [1 subscription ] 24-08-05_19:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713422/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:42:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713422/content)

[Circuit_RSD1_A67B1:   24-08-05_19:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713424/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:42:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713424/content)


***

### [2024-08-05 19:44:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119574)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 19:44:49)


***

### [2024-08-05 19:52:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119579)
>
```
Failed to connect to RF scope turn measurement B1. FESA class restarted and it works (apparently)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:54:36)

[24-08-05_19:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713450/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:53:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713450/content)

[DIAMON console [PROD] 2.6.1 - UNKNOWN as LHCOP - LHC 24-08-05_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713452/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 19:54:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713452/content)


***

### [2024-08-05 19:53:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119580)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 19:53:32)


***

### [2024-08-05 19:58:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119582)
>
```
Wirescans. B2V doesn't work
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:00:30)

[24-08-05_19:58.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713480/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 19:58:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713480/content)

[ LHC WIRESCANNER APP 24-08-05_20:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713482/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 20:00:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713482/content)

[ LHC WIRESCANNER APP 24-08-05_20:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713484/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 20:00:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713484/content)

[ LHC WIRESCANNER APP 24-08-05_20:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713486/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 20:00:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713486/content)


***

### [2024-08-05 20:27:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119585)
>Before ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:27:36)

[ LHC Fast BCT v1.3.2 24-08-05_20:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713502/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:27:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713502/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-05_20:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713504/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:27:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713504/content)


***

### [2024-08-05 20:27:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119586)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:27:37)


***

### [2024-08-05 20:27:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119587)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:27:47)


***

### [2024-08-05 20:27:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119588)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:27:51)


***

### [2024-08-05 20:27:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119589)
>LHC Injection Complete

Number of injections actual / planned: 48 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:34:20 / 0:33:48 (101.6 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 20:27:52)


***

### [2024-08-05 20:30:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119590)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:30:05)


***

### [2024-08-05 20:30:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119592)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:30:05)


***

### [2024-08-05 20:30:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119595)
>losses start of ramp, 44%06L3 1.3s RS

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:30:51)

[ LHC BLM Fixed Display 24-08-05_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713520/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:30:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713520/content)


***

### [2024-08-05 20:31:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119596)
>L6B1 (M2B1) saturation?

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 20:32:06)

[Inspector 3.5.53 - ACS Control 24-08-05_20:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713522/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 20:32:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713522/content)

[Inspector 3.5.53 - ACS Control 24-08-05_20:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713524/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 20:32:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713524/content)


***

### [2024-08-05 20:51:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119602)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:51:23)


***

### [2024-08-05 20:51:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119604)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 20:51:41)


***

### [2024-08-05 20:51:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119606)
>Flat top

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:51:53)

[ LHC Fast BCT v1.3.2 24-08-05_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713546/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:51:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713546/content)

[ LHC Beam Losses Lifetime Display 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713548/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 20:52:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713548/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713550/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:52:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713550/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713552/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:52:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713552/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713554/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/05 20:52:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713554/content)


***

### [2024-08-05 20:51:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119606)
>Flat top

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:51:53)

[ LHC Fast BCT v1.3.2 24-08-05_20:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713546/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 20:51:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713546/content)

[ LHC Beam Losses Lifetime Display 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713548/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 20:52:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713548/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713550/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:52:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713550/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713552/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 20:52:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713552/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-08-05_20:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713554/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/05 20:52:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713554/content)


***

### [2024-08-05 21:00:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119612)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:00:00)


***

### [2024-08-05 21:00:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119615)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:00:47)


***

### [2024-08-05 21:00:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119617)
>squeeze + qchange

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 21:00:56)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-05_21:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713592/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 21:00:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713592/content)


***

### [2024-08-05 21:01:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119618)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:01:40)


***

### [2024-08-05 21:02:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119619)
>large UFO in 6R5, 37%

creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 21:02:47)

[UFO - BLMQI.06R5.B1E30_MQML - 2024-08-05 21:01:56.0 24-08-05_21:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713594/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 21:02:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713594/content)

[ UFO Buster GUI - v7.5.0 -  INCA DISABLED - PRO CCDA - Version: 7.5.0 24-08-05_21:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713596/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 21:02:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713596/content)

[ LHC BLM Fixed Display 24-08-05_21:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713598/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/05 21:03:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713598/content)


***

### [2024-08-05 21:04:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119620)
>Collisions

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 21:04:25)

[ LHC Beam Losses Lifetime Display 24-08-05_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713600/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 21:04:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713600/content)

[ LHC Fast BCT v1.3.2 24-08-05_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713602/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:04:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713602/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-08-05_21:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713604/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:04:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713604/content)


***

### [2024-08-05 21:05:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119622)
>optimization IP8

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:05:33)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-05_21:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713606/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:05:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713606/content)


***

### [2024-08-05 21:06:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119624)
>optimizations IP1, IP2, IP5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:06:42)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-05_21:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713612/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 21:06:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713612/content)


***

### [2024-08-05 21:06:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119625)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:06:56)


***

### [2024-08-05 21:08:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119627)
>LIfetime optimization as it was low. +1e-3 B1 and B2 H

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 21:09:05)

[ LHC Beam Losses Lifetime Display 24-08-05_21:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713620/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 21:09:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713620/content)


***

### [2024-08-05 21:14:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119628)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:14:01)


***

### [2024-08-05 21:15:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119630)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 21:15:59)


***

### [2024-08-05 21:20:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119634)
>emittance scans

creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/05 21:20:21)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713624/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/05 21:20:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713624/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_21:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713626/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/05 21:20:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713626/content)


***

### [2024-08-05 22:48:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119713)
>Started the shift during the access.  
Once SND finished, I tried to restart the machine and RB.A78 and RQS.R2B2 tripped at the start of precycle.  
EPC piquet reset the RB but needed to access (UA) for the RQS.  
  
After the access was finished, RSD1.A67B1 tripped when arming the injection functions. Manage to reset it.  
I had to wait a bit due to PS cavities piquet intervention.  
  
I had to restart the ALLBunchProfile fesa class from cfc-ccr-alldiag, sequencer tasks were failing for B1.  
  
Wirescanned B2V is not working, it complains about "PMT high voltage is not ON".  
  
Injection was ok.  
Start of ramp lossesin IR3 44%.  
During the ramp RF module L6B1 reported intermittently saturation. But it went away after a few minutes.  
  
UFO 37% after squeeze.  
  
Leaving the machine in stable beams during B\* levelling.  
  
Note:  
- new version of accpit server to fix an edge case for the injection phase trims  
  
AC  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 22:48:34)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713918/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 22:48:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3713918/content)


***

### [2024-08-06 04:29:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119749)
>Turning on wire

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 04:29:57)

[ LHC BBLR Wire App v0.4.1 24-08-06_04:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714149/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 04:29:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714149/content)

[Beam process - 0.4.2 24-08-06_04:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714155/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/06 04:31:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714155/content)


***

### [2024-08-06 04:31:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119750)
>Tune optimization.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/06 04:31:25)

[Accelerator Cockpit v0.0.38 24-08-06_04:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714151/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/06 04:31:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714151/content)

[ LHC Beam Losses Lifetime Display 24-08-06_04:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714153/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/06 04:31:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714153/content)


***

### [2024-08-06 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119778)
>
```
**SHIFT SUMMARY:**  
  
Stable beams!  
  
Turned on the wires when we were head-on at 30cm and did a tune optimization.   
/Tobias  
  

```


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/06 06:45:31)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714197/content)
creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/08/06 06:45:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714197/content)


***

### [2024-08-06 07:13:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119781)
>optimized LHCb crossing plane, ~4.5um (~0.15 sigma)

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 07:14:05)

[ LHC Luminosity Scan Client 0.62.0 [pro] 24-08-06_07:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714198/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 07:14:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714198/content)


***

### [2024-08-06 08:27:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119794)
>Global Post Mortem Event

Event Timestamp: 06/08/24 08:27:25.505
Fill Number: 9978
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 24015 / 24142 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 13-LHCb\_Mag: A T -> F on CIB.UA87.R8.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/06 08:30:18)

[LHC Luminosity Scan Client 0.62.0 [pro] 24-08-06_08:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714214/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 08:35:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714214/content)


***

### [2024-08-06 08:27:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119845)
>Global Post Mortem Event Confirmation

Dump Classification: Power converter fault(s)
Operator / Comment: mihostet / LHCb magnet PC trip (bridge B cooling temp)


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/06 08:51:33)


***

### [2024-08-06 08:28:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119790)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/06 08:28:35)


***

### [2024-08-06 08:28:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119791)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/06 08:28:37)


***

### [2024-08-06 08:29:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119792)
>
```
dumped by LHCb magnet trip

INT   :BRIDGE B COOLING TEMP          :FAULT  :UT:LT  
ACT   :MCB FAULT                      :FAULT  :UT:   
ACT   :MCB                            :FAULT  :UT:  
  
TI didn't see anything particular on their side - cooling water temperature perfectly at the setpoint (26 deg, alarm at 27 deg; temperature drop after the trip).  
  
Called EPC piquet (Ludovic) to check the converter.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 08:44:59)

[udp:..multicast-bevlhcexpmag:1234 - VLC media player 24-08-06_08:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714210/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/08/06 08:29:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714210/content)

[ Equip State 24-08-06_08:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714212/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 08:31:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714212/content)

[1 - UW_85_TT8021_2_AL1 D¿faut temp¿rature haute sortie ¿changeur secondaire Secteur LHCb    24-08-06_08:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714218/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 08:39:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714218/content)

[1 - UW_85_TT8021_3 Temp¿rature retour secondaire echangeur LHCb - %IW\2.20\0.0.0.7    24-08-06_08:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714220/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/06 08:42:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3714220/content)


***

### [2024-08-06 08:29:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119793)
>LHC RUN CTRL: New FILL NUMBER set to 9979

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/06 08:29:49)


