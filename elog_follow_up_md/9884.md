# FILL 9884
**start**:		 2024-07-10 15:32:32.327238525+02:00 (CERN time)

**end**:		 2024-07-11 01:36:34.476613525+02:00 (CERN time)

**duration**:	 0 days 10:04:02.149375


***

### [2024-07-10 15:32:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4103945)
>LHC RUN CTRL: New FILL NUMBER set to 9884

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/10 15:32:32)


***

### [2024-07-10 15:50:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4103967)
>The intervention on the RCD.A78.B2 finished. We tested with current and 
 they measured and it all looks fine.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 15:51:07)

[2 - UA83.RCD.A78B2 DQAMS N type 600A for circuit UA83.RCD.A78B2    24-07-10_15:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672388/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 15:51:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672388/content)


***

### [2024-07-10 18:35:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104083)
>
```
UFO loss pattern visible in 28L5 approximately 20 ms before the beam dump  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:39:57)

[blm_blmlhc >> Version: 2.4.7  Responsible: Fabio Follin 24-07-10_18:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:39:29)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672902/content)

[pm-beam-loss-evaluation >> Version: 1.0.6  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-07-10_18:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672904/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:39:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672904/content)


***

### [2024-07-10 18:40:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104085)
>Signature of quench heater firing on the circulating beam visible ~3ms before the dump  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:41:17)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-07-10_18:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672906/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:41:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672906/content)

[bpm_orbit >> Version: 7.0.8  Responsible: Jorg Wenniger 24-07-10_18:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672908/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:41:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672908/content)


***

### [2024-07-10 18:42:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104086)
>
```
dBLM signal of the losses ~20ms before the beam dump  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 18:46:20)

[rawBuf1 24-07-10_18:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672910/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/10 18:45:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672910/content)

[rawBuf1 24-07-10_18:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672914/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/10 18:45:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3672914/content)


***

### [2024-07-10 22:01:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104119)
>nQPS reset for RB and RQ in sector 45

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:02:12)

[ XPOC Viewer - PRO 24-07-10_22:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673059/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:02:12)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673059/content)


***

### [2024-07-10 22:07:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104120)
>I managed to recover the RB but for the RQ there is still an issue.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:08:04)

[Circuit_RQD_A45:   24-07-10_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673061/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:09:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673061/content)


***

### [2024-07-10 22:15:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104122)
>After some reset it all came back in everything is now green.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:15:43)

[Circuit_RQD_A45:   24-07-10_22:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673063/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:15:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673063/content)


***

### [2024-07-10 22:44:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104126)
>I don't manage to get the last one on. I will try with the sequencer.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/10 22:44:21)

[ LHC RF CONTROL 24-07-10_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673073/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/10 22:44:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673073/content)


***

### [2024-07-10 22:46:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104127)
>The QPS for sector 45 should be fine now so put it back in the sequence.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:46:35)

[ LHC Sequence Editor (DEV) : 12.32.13  24-07-10_22:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673083/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 22:49:26)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673083/content)

[ LHC SIS GUI 24-07-10_22:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673087/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/07/10 22:48:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673087/content)


***

### [2024-07-10 22:49:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104128)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/10 22:49:29)


***

### [2024-07-10 22:51:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104129)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/10 22:51:50)


***

### [2024-07-10 22:52:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104131)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/10 22:52:24)


***

### [2024-07-10 22:52:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104132)
>Rf is back on!

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/10 22:53:07)

[ LHC RF CONTROL 24-07-10_22:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673095/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/07/10 22:53:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673095/content)


***

### [2024-07-10 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104135)
>
```
**SHIFT SUMMARY:**  
  
*Arrived in cryo recovery. Several access.  
*The QPS for sector45 should now working fine after some replacement so I have put it back the reset in the sequence.  
*The RCD.A78.B2 has been fixed. It is now in normal operation (simulation before).   
*Got the green light from mp3 to continue. The switches are closed and QPS are reset for the RBs and RQs  
*Access for a water leak in ALICE around 22.30 now they are accessing in PM25.   
*Turned on the RF and after some reset and playing the sequence it is back.  
  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 23:08:24)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673109/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/10 23:08:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673109/content)


***

### [2024-07-11 00:00:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104137)
>Cryo Start in Arc 45 was forced True kindly by Cryo colleagues to allow us 
 resetting the circuits

creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 00:01:35)

[udp:..multicast-bevlhc2:1234 - VLC media player 24-07-11_00:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673136/content)
creator:	 lhcop  @cwo-ccc-d1lc.cern.ch (2024/07/11 00:01:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673136/content)

[Set SECTOR45 24-07-11_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673138/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/11 00:04:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673138/content)


***

### [2024-07-11 00:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104148)
>|Machine closed and all circuits prepared.

Waiting for CRYO conditions to be OK for powering (Line N stillnot ready)

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/11 00:26:03)

[Desktop 24-07-11_00:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673146/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/07/11 00:26:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673146/content)


***

### [2024-07-11 00:22:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104140)
>LHC SEQ: ramping up ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 00:22:31)


***

### [2024-07-11 00:22:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104141)
>Upon ALICE request we Ramp up their dipole

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 00:22:51)


***

### [2024-07-11 00:23:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104142)
>LHC SEQ: ramping up LHCB DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 00:23:47)


***

### [2024-07-11 00:24:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104143)
>WIth the green light from LHCb I also ramp up their dipole

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 00:24:09)


***

### [2024-07-11 00:24:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104145)
>ALL VACUUM VALVES OPEN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 00:24:46)


***

### [2024-07-11 00:24:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104146)
>Event created from ScreenShot Client.  
LHC Sequencer Execution GUI (PRO) 
 : 12.32.13 24-07-11\_00:24.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 00:24:55)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-07-11_00:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673144/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/07/11 00:24:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3673144/content)


***

### [2024-07-11 01:36:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104160)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 01:36:33)


***

### [2024-07-11 01:36:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4104161)
>LHC RUN CTRL: New FILL NUMBER set to 9885

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/07/11 01:36:34)


