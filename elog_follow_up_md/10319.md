# FILL 10319
**start**:		 2024-11-05 13:12:00.877238525+01:00 (CERN time)

**end**:		 2024-11-05 14:45:26.805238525+01:00 (CERN time)

**duration**:	 0 days 01:33:25.928000


***

### [2024-11-05 13:12:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177880)
>LHC RUN CTRL: New FILL NUMBER set to 10319

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:12:03)


***

### [2024-11-05 13:15:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177889)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:15:17)


***

### [2024-11-05 13:15:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177891)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:15:25)


***

### [2024-11-05 13:17:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177893)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:17:41)


***

### [2024-11-05 13:19:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177896)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:19:13)


***

### [2024-11-05 13:21:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177901)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:21:38)


***

### [2024-11-05 13:21:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177903)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:21:46)


***

### [2024-11-05 13:28:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177909)
>LHC RUN CTRL: PARTICLE TYPE for LHC BEAM1 changed to PROTON

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:28:33)


***

### [2024-11-05 13:28:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177910)
>LHC RUN CTRL: PARTICLE TYPE for LHC BEAM2 changed to PROTON

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:28:37)


***

### [2024-11-05 13:28:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177912)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:28:52)


***

### [2024-11-05 13:30:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177916)
>LHC SEQ: BctDc210 tests finished. Overall result: ERROR
Fifo Average Test FAILED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:30:38)


***

### [2024-11-05 13:32:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177926)
>injection kicker not OK. OK after changing particle type. Relaunching 
 preparation

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/05 13:33:48)

[ LHC Sequencer Execution GUI (PRO) : 12.33.10  24-11-05_13:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853046/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/05 13:39:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853046/content)

[ LSA Applications Suite (v 16.6.35) 24-11-05_13:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853098/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/05 13:42:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853098/content)


***

### [2024-11-05 13:34:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177930)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:34:35)


***

### [2024-11-05 13:34:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177930)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:34:35)


***

### [2024-11-05 13:35:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177933)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:35:08)


***

### [2024-11-05 13:36:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177934)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:36:14)


***

### [2024-11-05 13:37:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177938)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:37:23)


***

### [2024-11-05 13:38:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177940)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:38:18)


***

### [2024-11-05 13:38:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177942)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:38:49)


***

### [2024-11-05 13:38:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177943)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:38:49)


***

### [2024-11-05 13:40:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177945)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:40:44)


***

### [2024-11-05 13:40:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177947)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:40:48)


***

### [2024-11-05 13:40:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177949)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:40:50)


***

### [2024-11-05 13:41:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177954)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:41:08)


***

### [2024-11-05 13:41:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177956)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:41:19)


***

### [2024-11-05 13:41:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177958)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:41:25)


***

### [2024-11-05 13:41:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177960)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:41:41)


***

### [2024-11-05 13:42:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177961)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:42:41)


***

### [2024-11-05 13:43:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177966)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:43:08)


***

### [2024-11-05 13:44:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177969)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:44:52)


***

### [2024-11-05 13:46:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177971)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:46:42)


***

### [2024-11-05 13:52:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178001)
>Tomasz called back about the trip of ROD and sectupoles this morning. Something went wrong there, but he cannot see much as the power cycle crashed and erased the post mortem  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/05 14:03:42)


***

### [2024-11-05 13:54:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4177984)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 13:54:40)


***

### [2024-11-05 14:02:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178000)
>masked for TCDI tests

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/05 14:02:46)

[ LHC SIS GUI 24-11-05_14:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853180/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/05 14:02:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853180/content)

[ LHC SIS GUI 24-11-05_14:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853182/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/05 14:02:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853182/content)


***

### [2024-11-05 14:05:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178004)
>Event created from ScreenShot Client.  
RF Trim Warning 24-11-05\_14:05.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:05:17)

[RF Trim Warning 24-11-05_14:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853192/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:05:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853192/content)

[ Accelerator Cockpit v0.0.42 24-11-05_14:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853194/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:08:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853194/content)

[ Accelerator Cockpit v0.0.42 24-11-05_14:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853196/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:06:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853196/content)

[ Accelerator Cockpit v0.0.42 24-11-05_14:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853208/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:07:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853208/content)

[ Accelerator Cockpit v0.0.42 24-11-05_14:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853224/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:09:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853224/content)


***

### [2024-11-05 14:10:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178009)
>Precycle and RAMPDOWN (2.68 TeV) re-established for RQTL9.L7B2 after
Inspection that did not revealed anything unusual
MP3 confirmation that no degradation on circuit performance is expected even 
 in case of faults

creator:	 lhcop  @cwe-513-vpl956.cern.ch (2024/11/05 14:11:09)

[ LSA Applications Suite (v 16.6.36) 24-11-05_14:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853234/content)
creator:	 lhcop  @cwe-513-vpl956.cern.ch (2024/11/05 14:11:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853234/content)


***

### [2024-11-05 14:27:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178025)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/05 14:27:03)


***

### [2024-11-05 14:30:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4178031)
>Event created from ScreenShot Client.  
 LSA Applications Suite (v 16.6.36) 24-11-05\_14:30.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:30:06)

[ LSA Applications Suite (v 16.6.36) 24-11-05_14:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853296/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/05 14:30:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3853296/content)


