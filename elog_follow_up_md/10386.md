# FILL 10386
**start**:		 2024-11-18 21:38:04.130613525+01:00 (CERN time)

**end**:		 2024-11-18 23:56:03.512613525+01:00 (CERN time)

**duration**:	 0 days 02:17:59.382000


***

### [2024-11-18 21:38:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186553)
>LHC RUN CTRL: New FILL NUMBER set to 10386

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:38:06)


***

### [2024-11-18 21:39:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186559)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:39:27)


***

### [2024-11-18 21:41:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186560)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:41:31)


***

### [2024-11-18 21:41:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186562)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:41:40)


***

### [2024-11-18 21:42:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186563)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:42:55)


***

### [2024-11-18 21:44:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186565)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:44:17)


***

### [2024-11-18 21:44:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186567)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:44:59)


***

### [2024-11-18 21:45:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186569)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:45:08)


***

### [2024-11-18 21:46:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186571)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:46:05)


***

### [2024-11-18 21:47:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186572)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:47:24)


***

### [2024-11-18 21:47:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186574)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:47:26)


***

### [2024-11-18 21:47:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186576)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:47:28)


***

### [2024-11-18 21:47:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186579)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:47:35)


***

### [2024-11-18 21:47:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186583)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:47:44)


***

### [2024-11-18 21:48:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186585)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:48:14)


***

### [2024-11-18 21:48:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186587)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:48:21)


***

### [2024-11-18 21:48:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186589)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:48:36)


***

### [2024-11-18 21:49:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186592)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:49:44)


***

### [2024-11-18 21:51:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186595)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:51:28)


***

### [2024-11-18 21:55:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186599)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 21:55:13)


***

### [2024-11-18 22:07:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186607)
>Event created from ScreenShot Client.  
LHC Sequencer Execution GUI (PRO) 
 : 12.33.11 24-11-18\_22:07.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:09:40)

[ LHC Sequencer Execution GUI (PRO) : 12.33.11  24-11-18_22:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869008/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:09:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869008/content)


***

### [2024-11-18 22:09:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186608)
>Event created from ScreenShot Client.  
pm-beam-loss-evaluation >> 
 Version: 1.0.11 Responsible: TE-MPE-CB Software Team 
 (mpe-software-coord@cern.ch) 24-11-18\_22:09.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:09:48)

[pm-beam-loss-evaluation >> Version: 1.0.11  Responsible: TE-MPE-CB Software Team (mpe-software-coord@cern.ch) 24-11-18_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869010/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:09:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869010/content)


***

### [2024-11-18 22:11:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186610)
>Event created from ScreenShot Client.  
BETS Explorer 24-11-18\_22:11.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:14:15)

[ BETS Explorer 24-11-18_22:11.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869012/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:14:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869012/content)


***

### [2024-11-18 22:14:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186611)
>Event created from ScreenShot Client.  
BETS Explorer 24-11-18\_22:14.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:20:47)

[ BETS Explorer 24-11-18_22:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869014/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:20:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869014/content)


***

### [2024-11-18 22:20:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186612)
>Event created from ScreenShot Client.  
Bis Streams GUI (PRO) 
 24-11-18\_22:20.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:21:00)

[ Bis Streams GUI (PRO) 24-11-18_22:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869016/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:21:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869016/content)


***

### [2024-11-18 22:22:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186613)
>Event created from ScreenShot Client.  
CIB.CCR.LHC.B1 - BIS Device 
 Overview 24-11-18\_22:22.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:22:49)

[CIB.CCR.LHC.B1 - BIS Device Overview 24-11-18_22:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869018/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 22:22:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869018/content)


***

### [2024-11-18 22:29:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186619)
>Many thanks to Joerg, the problem was just that the operator button was not put back...  


creator:	 lhcop  @cwo-ccc-dtls.cern.ch (2024/11/18 22:49:34)


***

### [2024-11-18 22:34:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186614)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 22:34:11)


***

### [2024-11-18 22:38:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186615)
>corrections on pilot

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:38:39)

[RF Trim Warning 24-11-18_22:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869021/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:38:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869021/content)


***

### [2024-11-18 22:40:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186616)
>reinjected as OFB had trouble converging on beam 2

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:41:19)

[RF Trim Warning 24-11-18_22:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869023/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:41:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869023/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:41.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869025/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:41:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869025/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869027/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:42:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869027/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869029/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:42:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869029/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:42.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869031/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:42:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869031/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:43.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869033/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:43:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869033/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869037/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:44:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869037/content)

[ Accelerator Cockpit v0.0.42 24-11-18_22:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869041/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 22:45:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869041/content)


***

### [2024-11-18 22:47:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186629)
>Event created from ScreenShot Client.  
Safe Machine Parameters in CCC : 
 Overview GUI 24-11-18\_22:47.png

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/18 23:07:57)

[Safe Machine Parameters in CCC : Overview GUI 24-11-18_22:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869049/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/18 23:07:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869049/content)


***

### [2024-11-18 22:48:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186618)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 22:48:17)


***

### [2024-11-18 22:48:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186618)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 22:48:17)


***

### [2024-11-18 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186636)
>
```
**SHIFT SUMMARY:**  
  
Arrived during the access for experiments, EPC in sector 56, EN-CV in PM65 and STI in PM25.  
  
Once the access was completed, precycled and filled for loss maps and aperture.  
  
Loss maps and aperture measurements were successfully completed (see summary [here](https://logbook.cern.ch/elogbook-server/GET/showEventInLogbook/4186550)).  
  
Dumped and refilled for asynchronous dump.  
  
Leaving during the ramp.  
  
  
  
* To be noted:

  
- Thanks Joerg for pointing out that the operator button was not switched back, and was preventing me from arming the BIC.  
- the first filling was very smooth, the second fill had many issues on beam 2 due to rephasing.  
- CMS noted that the LHC operation vistar is delayed by a couple of minutes while the LHC page 1 is not. We also noticed it when flattening the IP1 crossing angle during aperture measurements.  Georges restarted the vistar and it is much better.  
- when looking for collisions, IP8 was difficult to find and quite off in the horizontal plane.  
  
  
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/18 23:25:27)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869051/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/11/18 23:19:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869051/content)


***

### [2024-11-18 22:59:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186621)
>difficult to get beam 2 (rephasing)

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/18 22:59:56)

[SPS Beam Quality Monitor - SPS.USER.LHCION2 - SPS PRO INCA server - PRO CCDA 24-11-18_22:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869045/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/18 22:59:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869045/content)


***

### [2024-11-18 23:06:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186622)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:06:13)


***

### [2024-11-18 23:06:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186623)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:06:23)


***

### [2024-11-18 23:06:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186624)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:06:29)


***

### [2024-11-18 23:06:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186625)
>LHC Injection Complete

Number of injections actual / planned: 12 / 20
SPS SuperCycle length: 43.2 [s]
Actual / minimum time: 0:18:15 / 0:19:24 (94.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/18 23:06:30)


***

### [2024-11-18 23:08:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186630)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:08:31)


***

### [2024-11-18 23:08:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186632)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:08:33)


***

### [2024-11-18 23:29:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186638)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:29:49)


***

### [2024-11-18 23:29:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186640)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:29:58)


***

### [2024-11-18 23:35:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186645)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:35:43)


***

### [2024-11-18 23:35:57](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186646)
>Some tunes structure in the SQUEEZE that could be fedforward.

They were also present in teh previous fill

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:36:50)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-18_23:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869053/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:36:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869053/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-18_23:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869055/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:36:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869055/content)


***

### [2024-11-18 23:39:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186648)
>Crossing angfle flip, 
worst tune excursion B1V 3e-3

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:39:23)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-18_23:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869057/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:39:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869057/content)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-11-18_23:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869059/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:39:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869059/content)


***

### [2024-11-18 23:39:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186649)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:39:51)


***

### [2024-11-18 23:40:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186651)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:40:31)


***

### [2024-11-18 23:47:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186652)
>Optimzinga ll IPs

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:47:59)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869061/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:48:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869061/content)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869063/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:48:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869063/content)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869065/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:48:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869065/content)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869067/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:49:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869067/content)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869069/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:51:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869069/content)


***

### [2024-11-18 23:51:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186653)
>BEfore Async dump

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:51:53)

[ LHC Luminosity Scan Client 0.63.5 [pro] 24-11-18_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869071/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:51:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869071/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-11-18_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869073/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:51:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869073/content)

[ LHC Fast BCT v1.3.2 24-11-18_23:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869075/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:51:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869075/content)

[LHC Beam Quality Monitor - LHC.USER.ALL -  INCA DISABLED - PRO CCDA 24-11-18_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869077/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/11/18 23:52:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869077/content)

[ LHC BLM Fixed Display 24-11-18_23:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869079/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/11/18 23:52:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869079/content)


***

### [2024-11-18 23:53:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186654)
>Bumps IN

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 23:53:17)

[OpenYASP DV LHCRING . PHYSICS-6.8TeV-IONS-2024_V1_NegPol@240_[END] 24-11-18_23:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869081/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 23:53:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869081/content)

[ LHC LOSS MAPS 24-11-18_23:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869083/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/11/18 23:53:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869083/content)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-11-18_23:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869085/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/11/18 23:53:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869085/content)


***

### [2024-11-18 23:53:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186665)
>Global Post Mortem Event

Event Timestamp: 18/11/24 23:53:40.480
Fill Number: 10386
Accelerator / beam mode: ION PHYSICS / ADJUST
Energy: 6799200 [MeV]
Intensity B1/B2: 9 / 9 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/11/18 23:56:32)


***

### [2024-11-18 23:54:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186655)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:54:15)


***

### [2024-11-18 23:54:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186656)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:54:17)


***

### [2024-11-18 23:54:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186657)
>unamsked

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/18 23:54:50)

[ LHC SIS GUI 24-11-18_23:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869087/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/11/18 23:54:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3869087/content)


***

### [2024-11-18 23:55:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186658)
>Contacted Y. Dutheil for the analysis of the Async Dump Loss Map

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/11/18 23:55:59)


***

### [2024-11-18 23:56:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4186659)
>LHC RUN CTRL: New FILL NUMBER set to 10387

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/11/18 23:56:04)


