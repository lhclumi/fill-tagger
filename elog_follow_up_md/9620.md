# FILL 9620
**start**:		 2024-05-13 09:14:49.727363525+02:00 (CERN time)

**end**:		 2024-05-13 09:41:01.785488525+02:00 (CERN time)

**duration**:	 0 days 00:26:12.058125


***

### [2024-05-13 09:14:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066265)
>LHC RUN CTRL: New FILL NUMBER set to 9620

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/13 09:14:50)


***

### [2024-05-13 09:15:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066266)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/13 09:15:16)


***

### [2024-05-13 09:16:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066267)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/13 09:16:40)


***

### [2024-05-13 09:17:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066268)
>orbit after OFB converged with INDIVs

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/13 09:17:54)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-13_09:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587596/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/13 09:17:54)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587596/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-13_09:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587598/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/13 09:17:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587598/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1_MD1@0_[START] 24-05-13_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587600/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/13 09:18:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587600/content)


***

### [2024-05-13 09:18:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066269)
>RF offset

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/13 09:18:21)

[RF Trim Warning 24-05-13_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587602/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/13 09:18:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587602/content)


***

### [2024-05-13 09:18:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066270)
>after RF trim

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/13 09:18:52)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1_MD1@0_[START] 24-05-13_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587604/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/05/13 09:18:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587604/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-13_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587606/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/13 09:18:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587606/content)

[Beam Feedbacks - Dashboard - v4.95.0 - BFC.LHC 24-05-13_09:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587608/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/05/13 09:18:57)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587608/content)


***

### [2024-05-13 09:35:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066276)
>kicke r oit

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/13 09:35:50)

[LHC Injection Quality Check 3.17.4 -  INCA DISABLED - PRO CCDA 24-05-13_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587618/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/05/13 09:35:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3587618/content)


***

### [2024-05-13 09:41:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4066283)
>LHC RUN CTRL: New FILL NUMBER set to 9621

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/05/13 09:41:01)


