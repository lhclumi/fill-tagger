# FILL 9706
**start**:		 2024-06-03 20:16:43.327488525+02:00 (CERN time)

**end**:		 2024-06-03 22:12:02.540988525+02:00 (CERN time)

**duration**:	 0 days 01:55:19.213500


***

### [2024-06-03 20:16:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080741)
>LHC RUN CTRL: New FILL NUMBER set to 9706

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:16:43)


***

### [2024-06-03 20:17:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080747)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:17:50)


***

### [2024-06-03 20:20:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080749)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:20:00)


***

### [2024-06-03 20:20:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080751)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:20:12)


***

### [2024-06-03 20:21:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080752)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESSChosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:21:23)


***

### [2024-06-03 20:21:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080754)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:21:46)


***

### [2024-06-03 20:22:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080755)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:22:42)


***

### [2024-06-03 20:22:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080756)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:22:42)


***

### [2024-06-03 20:22:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080756)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:22:42)


***

### [2024-06-03 20:23:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080758)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESSAll tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:23:26)


***

### [2024-06-03 20:24:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080761)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:24:06)


***

### [2024-06-03 20:24:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080762)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:24:46)


***

### [2024-06-03 20:25:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080763)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:25:50)


***

### [2024-06-03 20:25:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080765)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESSNumber of failed BPMs: 0 / 61(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:25:51)


***

### [2024-06-03 20:25:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080767)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESSNumber of failed devices: 0 / 67(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:25:52)


***

### [2024-06-03 20:26:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080771)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:26:07)


***

### [2024-06-03 20:26:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080773)
>PM arrived QPS is back green

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:26:24)

[Circuit_RCBXV3_R8:   24-06-03_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623213/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:26:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623213/content)

[Multiple events 24-06-03_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623215/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:26:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623215/content)

[Multiple events 24-06-03_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623217/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:26:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623217/content)

[Multiple events 24-06-03_20:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623219/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:26:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623219/content)

[Multiple events 24-06-03_20:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623221/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:28:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623221/content)

[Multiple events 24-06-03_20:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623223/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:30:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623223/content)

[pmb-lv22-7.0.1 24-06-03_20:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623225/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:31:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623225/content)

[bic_eventseq >> Version: 3.6.2  Responsible: TE-MPE-MS Software Team (167226 - mpe-software-coord@cern.ch) 24-06-03_20:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623227/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:31:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623227/content)


***

### [2024-06-03 20:26:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080774)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESSNumber of failed crates: 0 / 27(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:26:38)


***

### [2024-06-03 20:26:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080776)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:26:45)


***

### [2024-06-03 20:28:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080778)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:28:06)


***

### [2024-06-03 20:29:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080780)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:29:50)


***

### [2024-06-03 20:38:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080783)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:38:35)


***

### [2024-06-03 20:50:31](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080786)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:50:31)


***

### [2024-06-03 20:53:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080789)
>Event created from ScreenShot Client.  
Module QPS\_45:A45.RB.A45.DL5K: (NoName) 24-06-03\_20:53.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:53:13)

[Module QPS_45:A45.RB.A45.DL5K: (NoName) 24-06-03_20:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623241/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:53:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623241/content)


***

### [2024-06-03 20:54:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080790)
>JENS STECKERT(JSTECKER) assigned RBAC Role: QPS-Piquet and will expire on: 03-JUN-24 10.53.56.906000 PM

creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/06/03 20:54:03)


***

### [2024-06-03 20:54:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080791)
>Event created from ScreenShot Client.  
K - DQAMGS.RB.A45.B11L5 DQAMGS N type for circuit RB.A45 24-06-03\_20:54.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:55:01)

[K - DQAMGS.RB.A45.B11L5 DQAMGS N type for circuit RB.A45    24-06-03_20:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623243/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:55:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623243/content)

[K - QPS_45:DQAMGS.A45.B11L5 24-06-03_20:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623245/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:55:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623245/content)

[ LHC-INJ-SIS Status 24-06-03_20:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623247/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/03 20:55:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623247/content)

[ LHC CIRCUIT SUPERVISION v9.0 24-06-03_20:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623249/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 20:55:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623249/content)


***

### [2024-06-03 20:59:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080794)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 20:59:29)


***

### [2024-06-03 21:46:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080798)
>Not the best filling...

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/03 21:46:39)

[SPS Beam Quality Monitor - SPS.USER.LHC3 - SPS PRO INCA server - PRO CCDA 24-06-03_21:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623253/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/06/03 21:46:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623253/content)


***

### [2024-06-03 21:50:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080799)
>
```
Last injection did not go through because of QPS state not OK on the same circuit as before.  
  
Strange we did not dump yet  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 22:04:56)

[ LHC-INJ-SIS Status 24-06-03_21:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623255/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/06/03 21:50:14)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623255/content)


***

### [2024-06-03 21:50:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080801)
>Global Post Mortem EventEvent Timestamp: 03/06/24 21:50:14.212Fill Number: 9706Accelerator / beam mode: PROTON PHYSICS / INJECTION PHYSICS BEAMEnergy: 449640 [MeV]Intensity B1/B2: 37974 / 34527 [e^10 charges]Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMPFirst BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: A T -> F on CIB.UA83.L8.B1

creator:	 copera  @cs-ccr-pm3.cern.ch (2024/06/03 21:53:06)


***

### [2024-06-03 21:50:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080800)
>
```
Here comes the dump!  
  
RBCXV3.R8 again tripped when the filling was almost completed!
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 22:05:07)

[Set SECTOR81 24-06-03_21:50.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623257/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/03 21:51:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623257/content)


***

### [2024-06-03 21:57:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080802)
>
```
Dumped again by the same circuit  
  
QPS piquet investigating  
  
Most probably no Post Mortem will be available as teh communication was lost with the crate and will need reset.  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 22:04:15)

[History Buffer 24-06-03_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623259/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 21:57:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623259/content)

[Circuit_RCBXH3_R8:   24-06-03_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623261/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 21:57:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623261/content)

[Set SECTOR81 24-06-03_21:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623263/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/06/03 21:57:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623263/content)

[8 - RCBXH3.R8 DQAMG N type A for circuit RCBXH3.R8    24-06-03_22:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623267/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 22:03:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623267/content)

[History Buffer 24-06-03_22:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623269/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/06/03 22:09:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3623269/content)


***

### [2024-06-03 22:12:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4080811)
>LHC RUN CTRL: New FILL NUMBER set to 9707

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/06/03 22:12:02)


