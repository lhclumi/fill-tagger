# FILL 10145
**start**:		 2024-09-26 23:05:32.793863525+02:00 (CERN time)

**end**:		 2024-09-27 01:50:44.296863525+02:00 (CERN time)

**duration**:	 0 days 02:45:11.503000


***

### [2024-09-26 23:05:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150942)
>LHC RUN CTRL: New FILL NUMBER set to 10145

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:05:34)


***

### [2024-09-26 23:06:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150948)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:06:11)


***

### [2024-09-26 23:07:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150949)
>
```
problem with missing settings in injection BP  
  
It appears the Injection BP was not cloned, but re-generated from the ramp by mistake when creating the MD HC...  
  
Copied mismatching & missing settings from nominal injection BP to MD4_Generic injection BP (including MCS). Also generated the signatures missing from the PHYSICS BP.  
  
Also, MCS for collimators were missing - re-generated.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:31:14)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-26_23:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788690/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:07:44)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788690/content)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-26_23:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788694/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:07:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788694/content)

[ LSA Applications Suite (v 16.6.29) 24-09-26_23:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788696/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:08:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788696/content)

[ LSA Applications Suite (v 16.6.29) 24-09-26_23:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788698/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:12:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788698/content)

[ LSA Applications Suite (v 16.6.29) 24-09-26_23:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788702/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:16:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788702/content)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-26_23:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788704/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:15:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788704/content)

[ LHC Sequencer Execution GUI (PRO) : 12.33.5  24-09-26_23:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788720/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/26 23:27:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788720/content)


***

### [2024-09-26 23:08:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150951)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:08:27)


***

### [2024-09-26 23:11:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150953)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:11:03)


***

### [2024-09-26 23:15:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150956)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:15:00)


***

### [2024-09-26 23:15:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150958)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:15:07)


***

### [2024-09-26 23:16:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150959)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:16:18)


***

### [2024-09-26 23:17:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150961)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:17:51)


***

### [2024-09-26 23:18:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150962)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:18:21)


***

### [2024-09-26 23:19:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150964)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:19:15)


***

### [2024-09-26 23:20:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150966)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:20:47)


***

### [2024-09-26 23:20:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150968)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:20:48)


***

### [2024-09-26 23:20:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150970)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:20:50)


***

### [2024-09-26 23:21:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150974)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:21:05)


***

### [2024-09-26 23:21:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150974)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:21:05)


***

### [2024-09-26 23:21:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150976)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:21:41)


***

### [2024-09-26 23:22:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150978)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:22:19)


***

### [2024-09-26 23:23:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150979)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:23:05)


***

### [2024-09-26 23:24:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150982)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:24:49)


***

### [2024-09-26 23:30:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150984)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:30:45)


***

### [2024-09-26 23:30:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150985)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:30:59)


***

### [2024-09-26 23:40:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150988)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:40:15)


***

### [2024-09-26 23:47:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150989)
>probes in

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/26 23:47:23)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-26_23:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788732/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/26 23:47:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788732/content)


***

### [2024-09-26 23:47:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150990)
>corrections

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/26 23:48:03)

[Accelerator Cockpit v0.0.38 24-09-26_23:47.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788734/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/26 23:48:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788734/content)

[Accelerator Cockpit v0.0.38 24-09-26_23:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788736/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/26 23:48:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788736/content)

[Accelerator Cockpit v0.0.38 24-09-26_23:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788738/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/26 23:49:27)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788738/content)


***

### [2024-09-26 23:51:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150991)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/26 23:51:34)


***

### [2024-09-26 23:57:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150993)
>INDIV (with nominal TF) before steering

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/26 23:58:07)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-09-26_23:57.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788744/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/26 23:58:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788744/content)


***

### [2024-09-27 00:00:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150994)
>
```
masked IQC in SIS for MD - too tedious to unlatch continuously
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:52:41)

[ LHC SIS GUI 24-09-27_00:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788746/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 00:01:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788746/content)


***

### [2024-09-27 00:04:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150996)
>after steering

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:04:45)

[Desktop 24-09-27_00:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788750/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:04:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788750/content)


***

### [2024-09-27 00:10:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150998)
>
```
Updating calibration curves, then re-trimming K to re-calculate I.  
  
Max. Difference in I ~12 A  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:19:10)

[cs-513-michi9 24-09-27_00:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788756/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:12:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788756/content)

[cs-513-michi9 24-09-27_00:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788758/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:13:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788758/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_00:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788760/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:13:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788760/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_00:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788762/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:17:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788762/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_00:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788764/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:18:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788764/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_00:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788766/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:18:41)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788766/content)


***

### [2024-09-27 00:20:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4150999)
>masking FEI

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 00:20:20)

[(null) 24-09-27_00:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788768/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/27 00:20:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788768/content)


***

### [2024-09-27 00:22:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151001)
>trajectory with new calibration curves

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:22:36)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-09-27_00:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788770/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:22:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788770/content)

[OpenYASP V4.9.2   LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 . SPS.USER.LHC4 24-09-27_00:22.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788772/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:22:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788772/content)


***

### [2024-09-27 00:25:12](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151002)
>
```
corrected (first good bunch #6 - bucket 3001)
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 00:26:13)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-09-27_00:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788774/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:25:32)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788774/content)


***

### [2024-09-27 00:52:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151003)
>orbit and B1 trajectory corrected

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:52:55)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-09-27_00:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788776/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 00:52:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788776/content)


***

### [2024-09-27 01:02:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151005)
>
```
back to QTL_OLD calibration after 40 bunches  
  
reverted steering to the one at midnight (before calib change)  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:04:56)

[cs-513-michi9 24-09-27_01:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788780/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:04:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788780/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788782/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:04:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788782/content)


***

### [2024-09-27 01:05:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151006)
>trajectory after calibration change + steering rollback

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 01:05:19)

[OpenYASP DV LHCB1Transfer . LHC_INDIV_1inj_Q20_2024_V1 24-09-27_01:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788784/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/09/27 01:05:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788784/content)


***

### [2024-09-27 01:28:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151007)
>full

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:28:15)

[ INJECTION SEQUENCER  v 5.1.11 24-09-27_01:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788786/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/27 01:52:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788786/content)


***

### [2024-09-27 01:29:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151008)
>ADT on LEVEL2 

disabled AFT for ADT during the MD to avoid creating false faults

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 01:30:54)

[ TRANSVERSE DAMPER CONTROL 24-09-27_01:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788788/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 01:29:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788788/content)

[ LSA Applications Suite (v 16.6.29) 24-09-27_01:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788790/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/09/27 01:30:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788790/content)


***

### [2024-09-27 01:49:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151009)
>ADT back on

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 01:49:54)

[ TRANSVERSE DAMPER CONTROL 24-09-27_01:49.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788792/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/27 01:49:55)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3788792/content)


***

### [2024-09-27 01:50:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4151012)
>Global Post Mortem Event

Event Timestamp: 27/09/24 01:50:22.546
Fill Number: 10145
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 981 / 988 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/27 01:53:13)


