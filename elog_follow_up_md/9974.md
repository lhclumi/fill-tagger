# FILL 9974
**start**:		 2024-08-04 17:22:19.880363525+02:00 (CERN time)

**end**:		 2024-08-05 11:56:29.565738525+02:00 (CERN time)

**duration**:	 0 days 18:34:09.685375


***

### [2024-08-04 17:22:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118972)
>LHC RUN CTRL: New FILL NUMBER set to 9974

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:22:20)


***

### [2024-08-04 17:24:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118979)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:24:56)


***

### [2024-08-04 17:27:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118980)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:27:11)


***

### [2024-08-04 17:27:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118982)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:27:28)


***

### [2024-08-04 17:28:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118983)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:28:32)


***

### [2024-08-04 17:29:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118985)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:29:54)


***

### [2024-08-04 17:29:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118987)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:29:58)


***

### [2024-08-04 17:30:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118990)
>CMS didn't switch to thelocal clock fast enough. Gave some extra time but 
 than contniued as aggreed upon.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 17:31:02)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-08-04_17:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712031/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 17:31:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712031/content)


***

### [2024-08-04 17:30:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118988)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:30:35)


***

### [2024-08-04 17:32:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118991)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:00)


***

### [2024-08-04 17:33:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118993)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:00)


***

### [2024-08-04 17:33:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118995)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:02)


***

### [2024-08-04 17:33:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4118999)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:16)


***

### [2024-08-04 17:33:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119001)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:49)


***

### [2024-08-04 17:33:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119003)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:33:55)


***

### [2024-08-04 17:34:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119004)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:34:33)


***

### [2024-08-04 17:35:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119005)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:35:16)


***

### [2024-08-04 17:36:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119010)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:36:58)


***

### [2024-08-04 17:37:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119011)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:37:00)


***

### [2024-08-04 17:37:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119013)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:37:32)


***

### [2024-08-04 17:45:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119016)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:45:36)


***

### [2024-08-04 17:58:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119021)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 17:58:35)


***

### [2024-08-04 17:59:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119022)
>Had to force this card.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 17:59:52)

[K - MB.B9L5 DQAMC N type MB for dipole MB.B9L5    24-08-04_17:59.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712043/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 17:59:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712043/content)


***

### [2024-08-04 18:01:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119023)
>Event created from ScreenShot Client.  
Beam Feedbacks - Dashboard - 
 v4.97.2 - BFC.LHC 24-08-04\_18:01.png

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/04 18:01:48)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-04_18:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712047/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/04 18:01:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712047/content)

[Accelerator Cockpit v0.0.37 24-08-04_18:02.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712049/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/04 18:02:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712049/content)

[Accelerator Cockpit v0.0.37 24-08-04_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712053/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/04 18:03:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712053/content)

[Accelerator Cockpit v0.0.37 24-08-04_18:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712055/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/04 18:03:16)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712055/content)


***

### [2024-08-04 18:04:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119024)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:04:06)


***

### [2024-08-04 18:05:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119025)
>Event created from ScreenShot Client.  
Accelerator Cockpit v0.0.37 24-08-04\_18:05.png

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/04 18:05:35)

[Accelerator Cockpit v0.0.37 24-08-04_18:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712059/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/04 18:05:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712059/content)


***

### [2024-08-04 18:37:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119029)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:37:02)


***

### [2024-08-04 18:37:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119030)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:37:11)


***

### [2024-08-04 18:37:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119031)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:37:15)


***

### [2024-08-04 18:37:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119032)
>LHC Injection Complete

Number of injections actual / planned: 51 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:33:10 / 0:33:48 (98.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/04 18:37:16)


***

### [2024-08-04 18:39:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119033)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:39:16)


***

### [2024-08-04 18:39:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119035)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 18:39:17)


***

### [2024-08-04 18:39:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119037)
>39% in start of ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/04 18:39:53)

[ LHC BLM Fixed Display 24-08-04_18:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712061/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/04 18:39:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712061/content)


***

### [2024-08-04 19:00:34](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119038)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:00:34)


***

### [2024-08-04 19:00:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119040)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:00:39)


***

### [2024-08-04 19:08:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119045)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:08:59)


***

### [2024-08-04 19:09:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119046)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:09:33)


***

### [2024-08-04 19:10:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119048)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:10:25)


***

### [2024-08-04 19:15:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119049)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:15:04)


***

### [2024-08-04 19:15:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119050)
>IP8 arriving maybe a bit too high. If it drifts in the wrong direction we 
 will be above the requested.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:16:36)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-04_19:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712065/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:16:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712065/content)


***

### [2024-08-04 19:19:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119051)
>Optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:19:45)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-04_19:19.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712067/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:19:45)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712067/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-04_19:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712069/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:20:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712069/content)


***

### [2024-08-04 19:21:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119055)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:21:22)


***

### [2024-08-04 19:23:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119056)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/04 19:23:24)


***

### [2024-08-04 19:28:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119057)
>emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:28:58)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-04_19:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712071/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:28:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712071/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-04_19:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712073/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/04 19:29:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712073/content)


***

### [2024-08-04 22:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119077)
>
```
**SHIFT SUMMARY:**  
  
-Arrived in stable beams  
-Dumped after ~14h in stable beams  
-Had to force a card in the RB.45.  
-Quick turn-around with a good filling. Corrected the phase at injection. Losses of ~40% beginning of ramp.  
-Arrived on target in LHCb so might be good to add additional separation again.  
  
* To be noted:

LHCb needs to access. The plan is to combine it with SND. Exact timing and start time to be discussed during the morning meeting.  
  
 /Tobias  
  

```


creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/04 23:06:37)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712081/content)
creator:	 lhcop  @cwo-ccc-d4lc.cern.ch (2024/08/04 23:06:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712081/content)


***

### [2024-08-05 01:05:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119091)
>beta\* 30 cm

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 01:05:42)

[ LHC Beam Losses Lifetime Display 24-08-05_01:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712094/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 01:05:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712094/content)

[ LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_01:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712096/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 01:05:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712096/content)


***

### [2024-08-05 02:44:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119096)
>Beams Head on in IP1

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 02:44:48)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_02:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712110/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 02:44:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712110/content)


***

### [2024-08-05 02:48:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119098)
>IP5 Head on as well

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 02:48:30)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_02:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712112/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 02:48:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712112/content)


***

### [2024-08-05 02:51:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119099)
>Event created from ScreenShot Client.  
Set BBLR\_PC 24-08-05\_02:51.png

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/05 02:51:43)

[Set BBLR_PC 24-08-05_02:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712114/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/08/05 02:51:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712114/content)

[ LHC BBLR Wire App v0.4.1 24-08-05_02:51.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712116/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 02:51:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712116/content)

[ LHC Beam Losses Lifetime Display 24-08-05_02:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712118/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 02:52:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712118/content)

[Accelerator Cockpit v0.0.37 24-08-05_02:52.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712120/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 02:52:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712120/content)


***

### [2024-08-05 02:54:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119100)
>After tune optimization

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 02:54:31)

[Accelerator Cockpit v0.0.37 24-08-05_02:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712122/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/05 02:54:31)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712122/content)

[ LHC Beam Losses Lifetime Display 24-08-05_02:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712124/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 02:54:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712124/content)


***

### [2024-08-05 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119144)
>
```
***** SHIFT SUMMARY *****  
Stable Beams for the entire shift.  
  
~~George~~  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 07:19:32)


***

### [2024-08-05 07:40:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119150)
>
```
added another 2*5um to LHCb initial separation

Total trims since 20 July:
2*5um + 2*5um + 2*7um + 2*5um = 2*22um (~1.5 nom. sigma)  
  
There appears to be a general drift towards a smaller separation, combined with unregular jumps towards smaller separation - typically after which adjustments to the initial separations were made.  
The adjustments just brought the initial lumi back to the trend, indicating that the jumps were mostly persistent.  

```


creator:	 mihostet  @194.12.144.27 (2024/08/05 07:58:11)

[ LSA Applications Suite (v 16.6.13) 24-08-05_07:40.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712221/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 07:43:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712221/content)

[lhcb.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712227/content)
creator:	 mihostet  @194.12.144.27 (2024/08/05 07:55:23)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712227/content)

[lhcb_sep.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712237/content)
creator:	 mihostet  @194.12.144.27 (2024/08/05 08:32:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712237/content)


***

### [2024-08-05 08:18:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119155)
>a bit of preventive cleaning...

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 08:18:17)

[AbortGapMonitor - LHC PRO INCA server - PRO CCDA - Version: 3.0.0  - LHC.USER.ALL 24-08-05_08:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712233/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 08:18:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712233/content)

[Abort gap cleaning control v2.1.0 24-08-05_08:18.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712235/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/08/05 08:18:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712235/content)


***

### [2024-08-05 08:42:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119162)
>
```
Exterior MAD door left open in PM56 - to be closed during the access
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 08:42:45)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712259/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/08/05 08:42:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712259/content)


***

### [2024-08-05 09:20:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119176)
>Predetuning phase to be set for the next fill - trim is not yet sent

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/08/05 09:21:13)

[ LSA Applications Suite (v 16.6.13) 24-08-05_09:20.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712283/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/08/05 09:21:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712283/content)


***

### [2024-08-05 09:32:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119183)
>
```
Many alarms on lost patrols, missing tunnel keys - looks like a TIM communication issue. TIM reporting all(?) LASS tags as invalid.  
  
The messages disappeared on their own at ~09:36.  
  
No impact on beam operation as none of these signals is interlocked.  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:31:02)

[ LHC Access Interlock App v0.2.2 24-08-05_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712291/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 09:32:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712291/content)

[ Access conditions for powering 24-08-05_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712293/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 09:32:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712293/content)

[History chart for last 100 records: 145594, 136408, 145593, 145591, 145596 24-08-05_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712299/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:35:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712299/content)

[DataTag history table: 145591 24-08-05_09:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712301/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:36:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712301/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712315/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:52:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712315/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712422/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:31:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712422/content)


***

### [2024-08-05 09:32:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119183)
>
```
Many alarms on lost patrols, missing tunnel keys - looks like a TIM communication issue. TIM reporting all(?) LASS tags as invalid.  
  
The messages disappeared on their own at ~09:36.  
  
No impact on beam operation as none of these signals is interlocked.  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:31:02)

[ LHC Access Interlock App v0.2.2 24-08-05_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712291/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 09:32:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712291/content)

[ Access conditions for powering 24-08-05_09:32.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712293/content)
creator:	 lhcop  @cwo-ccc-d9lf.cern.ch (2024/08/05 09:32:22)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712293/content)

[History chart for last 100 records: 145594, 136408, 145593, 145591, 145596 24-08-05_09:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712299/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:35:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712299/content)

[DataTag history table: 145591 24-08-05_09:36.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712301/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:36:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712301/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712315/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:52:34)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712315/content)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712422/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:31:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712422/content)


***

### [2024-08-05 09:32:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119184)
>
```
LASS is OK - no dump!
```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:37:08)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712297/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/08/05 09:32:42)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712297/content)


***

### [2024-08-05 09:54:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119194)
>
```
IP5 DOROS BPMs seem to be drifting off a bit - IP1 more stable. Intensity or temperature effect?  
  
The comparison with the trim of the autopilot (very stable) shows that the readings and not the beams are drifting.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:59:59)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_09:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712318/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:55:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712318/content)

[LHC Luminosity Scan Client 0.61.1 [pro] 24-08-05_09:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712320/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 09:55:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712320/content)


***

### [2024-08-05 09:58:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119196)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 09:58:08)


***

### [2024-08-05 09:58:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119197)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 09:58:09)


***

### [2024-08-05 10:02:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119202)
>Global Post Mortem Event

Event Timestamp: 05/08/24 10:02:09.048
Fill Number: 9974
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799560 [MeV]
Intensity B1/B2: 21272 / 21254 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: SW Permit: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/05 10:04:58)


***

### [2024-08-05 10:02:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119221)
>Global Post Mortem Event Confirmation

Dump Classification: Controls system problem
Operator / Comment: mihostet / SIS communication lost with 3 FGC crates in P7 (rl7b, rr7g, rr7c)


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/05 10:33:08)


***

### [2024-08-05 10:03:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119200)
>
```
SIS cod communication loss  
  
Crates cfc-sr7-rl7b and cfc-sr7-rr7g and crc-sr7-rr7c  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:27:27)

[ LHC SIS GUI 24-08-05_10:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712336/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/08/05 10:03:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712336/content)

[pm-sis-analysis >> Version: 2.2.5  Responsible: Jorg Wenninger 24-08-05_10:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712338/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:07:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712338/content)

[pm-sis-analysis >> Version: 2.2.5  Responsible: Jorg Wenninger 24-08-05_10:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712340/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:04:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712340/content)

[pm-sis-analysis >> Version: 2.2.5  Responsible: Jorg Wenninger 24-08-05_10:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712344/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:05:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712344/content)

[Terminal - lhcop@cs-ccr-sis1:.opt.lhc-sis.lhc-sis-server.log 24-08-05_10:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712352/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:12:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712352/content)

[Terminal - lhcop@cs-ccr-sis1:.opt.lhc-sis.lhc-sis-server.log 24-08-05_10:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712354/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:14:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712354/content)

[Terminal - lhcop@cs-ccr-sis1:.opt.lhc-sis.lhc-sis-server.log 24-08-05_10:23.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712372/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:24:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712372/content)

[Terminal - lhcop@cs-ccr-sis1:.opt.lhc-sis.lhc-sis-server.log 24-08-05_10:27.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712410/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 10:27:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712410/content)


***

### [2024-08-05 10:03:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119201)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:03:56)


***

### [2024-08-05 10:06:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119203)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:06:57)


***

### [2024-08-05 10:06:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119204)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:07:01)


***

### [2024-08-05 10:25:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119216)
>LUDOVIC CHARNAY(LCHARNAY) assigned RBAC Role: PO-LHC-Piquet and will expire on: 05-AUG-24 04.25.55.609000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/05 10:25:59)


***

### [2024-08-05 10:41:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119223)
>LHC SEQ: preparing the whole LHC for access

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:41:02)


***

### [2024-08-05 10:41:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119224)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:41:05)


***

### [2024-08-05 10:49:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119230)
>LHC SEQ: UNDULATOR L4 AND R4 OFF

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:49:10)


***

### [2024-08-05 10:58:56](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119241)
>LHC SEQ: ramping down ALICE DIPOLE and COMPENSATORS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:58:56)


***

### [2024-08-05 10:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119242)
>LHC SEQ: ramp down ALICE SOLENOID

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/05 10:59:00)


***

### [2024-08-05 11:06:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119247)
>TOMASZ PAWEL PODZORNY(TPODZORN) assigned RBAC Role: QPS-Piquet and will expire on: 05-AUG-24 05.06.47.816000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/05 11:06:49)


***

### [2024-08-05 11:20:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119256)
>
```
Dosimeter reader blocked in PM56 on the inside - people can not batch out.  
  
Called Vitor who is following up with the people inside (Schindler, RP, Totem/XRP)  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:34:37)

[AccessScreenshot.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712528/content)
creator:	 lhcop  @cwo-ccc-d8wc.cern.ch (2024/08/05 11:20:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712528/content)


***

### [2024-08-05 11:20:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119257)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P6 - service areas |
| Peoples : | * Laurent Catin (EN/HE/HEM)160343 / 77105 - <laurent.catin@cern.ch> |
| Duration | 1h - **Access Needed** - |
| PZ65 lift, GSM battery replacement | |



creator:	 msolfaro  @paas-standard-avz-a-m5xvl.cern.ch (2024/08/05 11:20:48)


***

### [2024-08-05 11:21:13](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119258)
>

| **New access request** | |
| --- | --- |
| Areas to access : | * P7 - TZ76 |
| Peoples : | * Laurent Catin (EN/HE/HEM)160343 / 77105 - <laurent.catin@cern.ch> |
| Duration | 1h - **Access Needed** - |
| PM76 lift, GSM battery replacement | |



creator:	 msolfaro  @paas-standard-avz-a-m5xvl.cern.ch (2024/08/05 11:21:14)


***

### [2024-08-05 11:29:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119261)
>
```
Tested RB reset sequencer task after a fix by Thomasz  
The task failed but did not trip RB.A23. The reason for the trips that happened before was a configuration problem on the QPS side (new configuration not fully deployed).  
  
Thomasz will follow up for the failing task.  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:33:34)

[ LHC Sequencer Execution GUI (PRO) : 12.32.13  24-08-05_11:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712532/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:30:08)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712532/content)

[Circuit_RB_A23:   24-08-05_11:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712534/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:30:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712534/content)

[Circuit_RB_A12:   24-08-05_11:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712536/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:30:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712536/content)


***

### [2024-08-05 11:41:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119270)
>VITO VIZZIELLO(VIVIZZIE) assigned RBAC Role: QPS-Piquet and will expire on: 05-AUG-24 05.40.57.031000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/08/05 11:41:02)


***

### [2024-08-05 11:48:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119274)
>27.5 deg pre-detuning trim applied

creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/08/05 11:49:04)

[ LSA Applications Suite (v 16.6.13) 24-08-05_11:48.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712618/content)
creator:	 lhcop  @cwo-ccc-d5lc.cern.ch (2024/08/05 11:49:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3712618/content)


***

### [2024-08-05 11:53:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4119276)
>Vitor called back, he rebooted the card reader which brought it back, so PM56 is operational again.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/05 11:53:24)


