# FILL 10041
**start**:		 2024-08-22 08:00:06.011238525+02:00 (CERN time)

**end**:		 2024-08-22 09:05:36.751863525+02:00 (CERN time)

**duration**:	 0 days 01:05:30.740625


***

### [2024-08-22 08:00:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130057)
>LHC RUN CTRL: New FILL NUMBER set to 10041

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/22 08:00:07)


***

### [2024-08-22 08:00:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130058)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/22 08:00:36)


***

### [2024-08-22 08:03:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130060)
>corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/22 08:03:48)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-08-22_08:03.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738563/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/08/22 08:03:49)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738563/content)

[Accelerator Cockpit v0.0.38 24-08-22_08:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738565/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/22 08:04:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738565/content)

[Accelerator Cockpit v0.0.38 24-08-22_08:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738567/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/22 08:05:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738567/content)

[Accelerator Cockpit v0.0.38 24-08-22_08:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738569/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/22 08:05:09)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738569/content)

[Accelerator Cockpit v0.0.38 24-08-22_08:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738571/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/22 08:05:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738571/content)

[Accelerator Cockpit v0.0.38 24-08-22_08:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738573/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/08/22 08:07:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738573/content)


***

### [2024-08-22 08:11:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130061)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/08/22 08:11:18)


***

### [2024-08-22 08:16:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130063)
>
```
Oscillating inter-band satellites still visible in B2V 0.2 eV 1.5e11 bunch.   
  
Less visible but still present with 0.8e11.   

```


creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/08/22 08:33:00)

[Figure 1 24-08-22_08:16.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738579/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/08/22 08:16:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738579/content)

[Figure 1 24-08-22_08:31.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738611/content)
creator:	 lhcop  @cwo-ccc-d6lc.cern.ch (2024/08/22 08:31:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3738611/content)


***

### [2024-08-22 08:57:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130083)
>Global Post Mortem Event

Event Timestamp: 22/08/24 08:57:27.979
Fill Number: 10041
Accelerator / beam mode: MACHINE DEVELOPMENT / INJECTION PHYSICS BEAM
Energy: 449640 [MeV]
Intensity B1/B2: 46 / 47 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 9-MKQA-b1: B T -> F on CIB.UA43.L4.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/22 09:00:22)


***

### [2024-08-22 08:57:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130091)
>Global Post Mortem Event Confirmation

Dump Classification: Operational mistake
Operator / Comment: dmirarch / went above SBF with last injection


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/08/22 09:16:24)


***

### [2024-08-22 09:04:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4130086)
>AC dipole key back in the traka

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/08/22 09:04:27)


