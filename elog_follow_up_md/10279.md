# FILL 10279
**start**:		 2024-10-25 00:43:04.925238525+02:00 (CERN time)

**end**:		 2024-10-25 05:54:08.277488525+02:00 (CERN time)

**duration**:	 0 days 05:11:03.352250


***

### [2024-10-25 00:43:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170014)
>LHC RUN CTRL: New FILL NUMBER set to 10279

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:43:05)


***

### [2024-10-25 00:43:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170020)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:43:45)


***

### [2024-10-25 00:46:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170022)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:46:11)


***

### [2024-10-25 00:47:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170024)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_1 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:47:36)


***

### [2024-10-25 00:48:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170027)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:48:39)


***

### [2024-10-25 00:49:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170029)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:49:19)


***

### [2024-10-25 00:49:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170030)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:49:39)


***

### [2024-10-25 00:51:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170032)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:51:40)


***

### [2024-10-25 00:52:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170033)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:04)


***

### [2024-10-25 00:52:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170035)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:06)


***

### [2024-10-25 00:52:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170037)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:08)


***

### [2024-10-25 00:52:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170041)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:23)


***

### [2024-10-25 00:52:40](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170043)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:40)


***

### [2024-10-25 00:52:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170045)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:52:47)


***

### [2024-10-25 00:53:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170046)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:53:45)


***

### [2024-10-25 00:54:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170048)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:54:21)


***

### [2024-10-25 00:56:05](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170050)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:56:05)


***

### [2024-10-25 00:58:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170052)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 00:58:11)


***

### [2024-10-25 01:00:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170055)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:00:38)


***

### [2024-10-25 01:01:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170057)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:01:12)


***

### [2024-10-25 01:04:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170062)
>Found the error in the old knob. There was a sign error so made a new one.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 01:07:25)

[ Optics Management Application v18.3.1 connected to server LHC 24-10-25_01:04.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837303/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 01:07:25)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837303/content)


***

### [2024-10-25 01:13:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170065)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:13:20)


***

### [2024-10-25 01:20:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170070)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:20:18)


***

### [2024-10-25 01:24:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170073)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/25 01:25:07)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-25_01:24.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837313/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/25 01:25:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837313/content)

[ Accelerator Cockpit v0.0.41 24-10-25_01:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837315/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 01:25:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837315/content)


***

### [2024-10-25 01:28:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170076)
>beam setup

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/25 01:28:23)

[Safe Machine Parameters in CCC : Overview GUI 24-10-25_01:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837317/content)
creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/25 01:28:24)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837317/content)


***

### [2024-10-25 01:29:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170077)
>Corrected horizontal and vertical for beam 2.

creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/25 01:29:21)

[OpenYASP DV LHCB2Transfer . LHC_PILOT_Q20_2024_V1 24-10-25_01:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837319/content)
creator:	 lhcop  @cwo-ccc-d0lc.cern.ch (2024/10/25 01:29:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837319/content)


***

### [2024-10-25 01:48:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170079)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:48:29)


***

### [2024-10-25 01:48:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170079)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:48:29)


***

### [2024-10-25 01:48:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170080)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:48:39)


***

### [2024-10-25 01:48:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170081)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:48:45)


***

### [2024-10-25 01:50:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170084)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:50:50)


***

### [2024-10-25 01:50:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170086)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 01:50:52)


***

### [2024-10-25 02:12:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170095)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 02:12:08)


***

### [2024-10-25 02:15:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170098)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 02:15:16)


***

### [2024-10-25 02:29:29](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170107)
>New knob in 75%

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 02:29:42)

[ LSA Applications Suite (v 16.6.34) 24-10-25_02:29.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837341/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 02:29:43)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837341/content)


***

### [2024-10-25 02:30:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170108)
>Large coupling after the correction was trimmed in.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 02:30:43)

[ Accelerator Cockpit v0.0.41 24-10-25_02:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837343/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:15:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837343/content)


***

### [2024-10-25 02:35:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170114)
>Warning in point 8 when we kicked again.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/25 02:36:03)

[ LHC BLM Fixed Display 24-10-25_02:35.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837359/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/25 02:36:03)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837359/content)


***

### [2024-10-25 02:38:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170117)
>Reducing the crossing a bit in LHCb because we have large losses.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 02:38:49)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-25_02:38.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837371/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 02:38:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837371/content)


***

### [2024-10-25 03:04:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170133)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L2 | RPHFC.UA23.RQX.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHGC.UA23.RTQX2.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UA23.RTQX1.L2 | Sinusoidal | 90 | 2 | 9.62898285072879 | COMPLETED |
| MQXA1.R2 | RPHFC.UA27.RQX.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.UA27.RTQX1.R2 | Sinusoidal | 90 | 2 | -9.69844730807381 | COMPLETED |
|  | RPHGC.UA27.RTQX2.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:04:27)

[tmpScreenshot_1729818267.6314347.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837389/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:04:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837389/content)


***

### [2024-10-25 03:10:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170142)
>correction at -0.75

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 03:10:17)

[ LSA Applications Suite (v 16.6.34) 24-10-25_03:10.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837397/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 03:10:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837397/content)


***

### [2024-10-25 03:18:06](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170144)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L2 | RPHFC.UA23.RQX.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHGC.UA23.RTQX2.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UA23.RTQX1.L2 | Sinusoidal | 90 | 2 | 9.629368824377707 | COMPLETED |
| MQXA1.R2 | RPHFC.UA27.RQX.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.UA27.RTQX1.R2 | Sinusoidal | 90 | 2 | -9.700632213317476 | COMPLETED |
|  | RPHGC.UA27.RTQX2.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:18:06)

[tmpScreenshot_1729819085.7998943.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837403/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:18:06)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837403/content)


***

### [2024-10-25 03:34:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170159)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L2 | RPHFC.UA23.RQX.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHGC.UA23.RTQX2.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UA23.RTQX1.L2 | Sinusoidal | 90 | 2 | 9.629436415044438 | COMPLETED |
| MQXA1.R2 | RPHFC.UA27.RQX.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.UA27.RTQX1.R2 | Sinusoidal | 90 | 2 | -9.700997987873961 | COMPLETED |
|  | RPHGC.UA27.RTQX2.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:34:33)

[tmpScreenshot_1729820072.7832603.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837417/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:34:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837417/content)


***

### [2024-10-25 03:46:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170164)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L1 | RPHFC.UL14.RQX.L1 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UL14.RTQX1.L1 | Sinusoidal | 90 | 2 | 9.804658405575537 | COMPLETED |
|  | RPHGC.UL14.RTQX2.L1 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
| MQXA1.R1 | RPMBC.UL16.RTQX1.R1 | Sinusoidal | 90 | 2 | -9.785127344323882 | COMPLETED |
|  | RPHFC.UL16.RQX.R1 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPHGC.UL16.RTQX2.R1 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:46:28)

[tmpScreenshot_1729820788.1876945.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837425/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:46:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837425/content)


***

### [2024-10-25 03:56:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170176)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L5 | RPHGC.USC55.RTQX2.L5 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.USC55.RTQX1.L5 | Sinusoidal | 90 | 2 | -9.765261279749211 | COMPLETED |
|  | RPHFC.USC55.RQX.L5 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
| MQXA1.R5 | RPHGC.UL557.RTQX2.R5 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHFC.UL557.RQX.R5 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UL557.RTQX1.R5 | Sinusoidal | 90 | 2 | 9.763239005616924 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:56:20)

[tmpScreenshot_1729821380.2620516.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837437/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 03:56:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837437/content)


***

### [2024-10-25 04:24:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170201)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.R2 | RPHFC.UA27.RQX.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.UA27.RTQX1.R2 | Sinusoidal | 90 | 2 | -8.622663091578033 | COMPLETED |
|  | RPHGC.UA27.RTQX2.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
| MQXA1.L2 | RPHFC.UA23.RQX.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHGC.UA23.RTQX2.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UA23.RTQX1.L2 | Sinusoidal | 90 | 2 | 8.559578281473478 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:24:46)

[tmpScreenshot_1729823086.0641172.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837453/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:24:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837453/content)


***

### [2024-10-25 04:33:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170204)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.R2 | RPHFC.UA27.RQX.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.UA27.RTQX1.R2 | Sinusoidal | 90 | 2 | -8.622663505984747 | COMPLETED |
|  | RPHGC.UA27.RTQX2.R2 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
| MQXA1.L2 | RPHFC.UA23.RQX.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHGC.UA23.RTQX2.L2 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UA23.RTQX1.L2 | Sinusoidal | 90 | 2 | 8.559577826722489 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:33:36)

[tmpScreenshot_1729823615.90005.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837465/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:33:36)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837465/content)


***

### [2024-10-25 04:46:04](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170208)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L5 | RPHGC.USC55.RTQX2.L5 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPMBC.USC55.RTQX1.L5 | Sinusoidal | 90 | 2 | -8.680127217871814 | COMPLETED |
|  | RPHFC.USC55.RQX.L5 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
| MQXA1.R5 | RPHGC.UL557.RTQX2.R5 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPHFC.UL557.RQX.R5 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UL557.RTQX1.R5 | Sinusoidal | 90 | 2 | 8.678542855722299 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:46:04)

[tmpScreenshot_1729824364.0844865.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837469/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:46:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837469/content)


***

### [2024-10-25 04:52:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170210)
>Successfully finished modulating the following circuits:


| Circuit | PCs | Type | PERIOD [s] | # PERIODS | AMPLITUDE [A] | MOD. STATUS |
| --- | --- | --- | --- | --- | --- | --- |
| MQXA1.L1 | RPHFC.UL14.RQX.L1 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
|  | RPMBC.UL14.RTQX1.L1 | Sinusoidal | 90 | 2 | 8.71531357497679 | COMPLETED |
|  | RPHGC.UL14.RTQX2.L1 | Sinusoidal | 90 | 2 | 0.0 | COMPLETED |
| MQXA1.R1 | RPMBC.UL16.RTQX1.R1 | Sinusoidal | 90 | 2 | -8.697798419092578 | COMPLETED |
|  | RPHFC.UL16.RQX.R1 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |
|  | RPHGC.UL16.RTQX2.R1 | Sinusoidal | 90 | 2 | -0.0 | COMPLETED |


*Sent By pyKmodLHC*



creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:52:53)

[tmpScreenshot_1729824772.9462504.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837473/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/25 04:52:53)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837473/content)


***

### [2024-10-25 05:00:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170214)
>Starting scan for IR2 feed-down.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 05:00:18)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-25_05:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837477/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 05:00:19)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837477/content)


***

### [2024-10-25 05:21:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170219)
>vertical crossing now at 0. I will start the horizontal scan.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 05:22:02)

[ LHC Lumi Commissioning.MD Toolkit 0.19.0 24-10-25_05:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837487/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/25 05:22:02)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3837487/content)


***

### [2024-10-25 05:51:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170236)
>Global Post Mortem Event

Event Timestamp: 25/10/24 05:51:27.466
Fill Number: 10279
Accelerator / beam mode: PROTON PHYSICS / SQUEEZE
Energy: 6799440 [MeV]
Intensity B1/B2: 2 / 2 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / OPERATOR\_SWITCH
First BIC input Triggered: First USR\_PERMIT change: Ch 4-Operator Buttons: A T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/25 05:54:20)


***

### [2024-10-25 05:54:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4170230)
>LHC RUN CTRL: New FILL NUMBER set to 10280

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/25 05:54:09)


