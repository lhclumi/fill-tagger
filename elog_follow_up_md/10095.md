# FILL 10095
**start**:		 2024-09-08 03:35:57.503988525+02:00 (CERN time)

**end**:		 2024-09-08 20:16:47.193613525+02:00 (CERN time)

**duration**:	 0 days 16:40:49.689625


***

### [2024-09-08 03:35:58](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139339)
>LHC RUN CTRL: New FILL NUMBER set to 10095

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:35:58)


***

### [2024-09-08 03:36:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139345)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:36:35)


***

### [2024-09-08 03:38:39](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139346)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:38:39)


***

### [2024-09-08 03:38:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139348)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:38:47)


***

### [2024-09-08 03:40:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139349)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:40:11)


***

### [2024-09-08 03:41:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139350)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:41:15)


***

### [2024-09-08 03:41:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139351)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:41:25)


***

### [2024-09-08 03:42:18](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139353)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:42:19)


***

### [2024-09-08 03:42:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139356)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:42:36)


***

### [2024-09-08 03:43:10](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139357)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:43:10)


***

### [2024-09-08 03:44:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139359)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:44:21)


***

### [2024-09-08 03:45:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139361)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:45:19)


***

### [2024-09-08 03:45:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139363)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:45:26)


***

### [2024-09-08 03:46:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139364)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:46:47)


***

### [2024-09-08 03:46:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139366)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:46:47)


***

### [2024-09-08 03:46:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139368)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:46:49)


***

### [2024-09-08 03:47:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139372)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:47:03)


***

### [2024-09-08 03:49:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139375)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:49:02)


***

### [2024-09-08 03:50:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139377)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 03:50:46)


***

### [2024-09-08 04:01:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139379)
>This one tripped as well.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/08 04:01:59)

[Set SECTOR23 24-09-08_04:01.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760814/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/09/08 04:01:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760814/content)


***

### [2024-09-08 04:05:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139380)
>Could turn it on without any issues.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:05:32)

[ Equip State 24-09-08_04:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760816/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:15:51)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760816/content)


***

### [2024-09-08 04:05:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139381)
>LHC RUN CTRL: BEAM MODE changed to NO BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 04:05:54)


***

### [2024-09-08 04:17:37](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139382)
>Doing manual pre-cycle for it.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:17:46)

[ Equip State 24-09-08_04:17.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760820/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:36:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760820/content)


***

### [2024-09-08 04:21:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139383)
>Succesful intervention. All the vales are now open.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:21:39)

[LHC vac 24-09-08_04:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760824/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 04:21:39)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760824/content)


***

### [2024-09-08 04:32:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139385)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 04:32:02)


***

### [2024-09-08 04:38:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139389)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 04:38:48)


***

### [2024-09-08 04:44:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139392)
>Injection corrections.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/08 04:44:15)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-09-08_04:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760853/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/08 04:44:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760853/content)

[Accelerator Cockpit v0.0.38 24-09-08_04:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760855/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/08 04:44:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760855/content)

[OpenYASP DV LHCRING . RAMP-SQUEEZE-6.8TeV-ATS-2m-2024_V1@0_[START] 24-09-08_04:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760857/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/08 04:44:40)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760857/content)

[Accelerator Cockpit v0.0.38 24-09-08_04:45.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760859/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/08 04:45:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760859/content)

[Accelerator Cockpit v0.0.38 24-09-08_04:46.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760863/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/09/08 04:46:38)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760863/content)


***

### [2024-09-08 04:48:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139393)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 04:48:26)


***

### [2024-09-08 05:15:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139395)
>Since we again were above the target in LHCb I decided to increase the 
 separation (2\*3)um.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 05:16:28)

[ LSA Applications Suite (v 16.6.26) 24-09-08_05:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760865/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 05:16:28)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760865/content)


***

### [2024-09-08 05:27:41](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139396)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:27:42)


***

### [2024-09-08 05:27:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139397)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:27:52)


***

### [2024-09-08 05:28:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139398)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:28:01)


***

### [2024-09-08 05:28:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139399)
>LHC Injection Complete

Number of injections actual / planned: 62 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:39:35 / 0:33:48 (117.1 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/09/08 05:28:02)


***

### [2024-09-08 05:29:49](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139400)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:29:49)


***

### [2024-09-08 05:29:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139402)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:29:50)


***

### [2024-09-08 05:30:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139404)
>Beginning of ramp.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 05:30:18)

[ LHC Fast BCT v1.3.2 24-09-08_05:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760867/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 05:30:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760867/content)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-08_05:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760871/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 05:30:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760871/content)


***

### [2024-09-08 05:30:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139405)
>Some losses in the beginning of ramp.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/08 05:30:33)

[ LHC BLM Fixed Display 24-09-08_05:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760869/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/09/08 05:30:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760869/content)


***

### [2024-09-08 05:50:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139416)
>
```
tmp summary:  
-Arrived during filling. LHCb arrived a bit high so increased separation for the next fill.   
-After ~3 hours at stable  beams we got dumped by a vacuum interlock. I could see that there were several error so looked like a communication issue in point 1. I called the piquet and he confirmed and said they needed an access. After they changed the card it worked fine and i could open the valves.   
-A Q4.L3 tripped while at standby. Could restart without any issues. I did a manual pre-cycle of it before restarting.   
-No issue re-filling.   
-Losses start of ramp around 35%.   
-Had to do some abort gab cleaning in the end of ramp  
-LHCb arrived now on target (before it was above)  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:46:32)


***

### [2024-09-08 05:51:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139411)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:51:09)


***

### [2024-09-08 05:51:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139413)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:51:23)


***

### [2024-09-08 05:55:30](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139417)
>abort gap cleaning needed in beam 1.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/08 05:55:46)

[Abort gap cleaning control v2.1.0 24-09-08_05:55.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760881/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/08 05:55:46)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760881/content)


***

### [2024-09-08 05:56:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139418)
>Also beam 2 has some.

creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/08 05:56:58)

[Abort gap cleaning control v2.1.0 24-09-08_05:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760883/content)
creator:	 lhcop  @cwo-ccc-d5lf.cern.ch (2024/09/08 05:56:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760883/content)


***

### [2024-09-08 05:59:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139419)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 05:59:53)


***

### [2024-09-08 06:00:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139420)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:00:43)


***

### [2024-09-08 06:01:38](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139423)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:01:38)


***

### [2024-09-08 06:05:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139425)
>Optimizations.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:05:35)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_06:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760890/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:05:35)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760890/content)

[Desktop 24-09-08_06:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760894/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:07:11)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760894/content)


***

### [2024-09-08 06:06:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139426)
>Event created from ScreenShot Client.  
 LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08\_06:06.png

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:06:55)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_06:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760892/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:06:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760892/content)


***

### [2024-09-08 06:07:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139427)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:07:21)


***

### [2024-09-08 06:07:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139427)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:07:21)


***

### [2024-09-08 06:13:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139428)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:13:45)


***

### [2024-09-08 06:15:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139430)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 06:15:46)


***

### [2024-09-08 06:21:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139432)
>Emittance scan

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:21:09)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_06:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760898/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:35:05)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760898/content)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_06:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760900/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:21:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760900/content)


***

### [2024-09-08 06:25:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139433)
>lumi better now for IP8 going into collision, but if we get the same again 
 we could consider moving even further apart.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:25:37)

[LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_06:25.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760902/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:25:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760902/content)


***

### [2024-09-08 06:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139436)
>
```
**SHIFT SUMMARY:**  
  
-Arrived during filling. LHCb arrived a bit high so increased separation for the next fill.   
-After ~3 hours at stable  beams we got dumped by a vacuum interlock. I could see that there were several error so looked like a communication issue in point 1. I called the piquet and he confirmed and said they needed an access. After they changed the card it worked fine and i could open the valves.   
-A Q4.L3 tripped while at standby. Could restart without any issues. I did a manual pre-cycle of it before restarting.   
-No issue re-filling.   
-Losses start of ramp around 35%.   
-Had to do some abort gab cleaning in the end of ramp  
-LHCb arrived now on target (before it was above)  
-Leaving in stable beams  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:47:33)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760904/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 06:47:33)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3760904/content)


***

### [2024-09-08 10:13:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139642)
>
```
Increased the IP8 separation by a significant step to bring LHCb down below their typical target.  
  
Regenerated the physics BP reference orbit.  
  
/JW  

```


creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/09/08 17:16:38)

[ LSA Applications Suite (v 16.6.26) 24-09-08_17:15.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3762464/content)
creator:	 jwenning  @cwe-513-vpl898.cern.ch (2024/09/08 17:16:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3762464/content)


***

### [2024-09-08 14:19:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139556)
>Both IP1 and IP5 sep levelling finished.  
I switch on the wires.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 14:20:11)


***

### [2024-09-08 14:59:00](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139580)
>
```
**SHIFT SUMMARY:**  
  
Stable beams all shift.  
  
Delphine  
  
  

```


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 15:22:52)

[mail_notification_result.log](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3761939/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 15:22:52)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3761939/content)


***

### [2024-09-08 19:10:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139706)
>Intervention in P8 is not urgent, signal is intermittent and circuits could be switched ON when OK.  
  
TI confirmed it is an intermittent communication issue with the UPS systems of P8.  
Crucially for the LHC, they are connected to the QPS system.  


creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 19:12:24)


***

### [2024-09-08 19:21:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139707)
>Event created from ScreenShot Client.  
STABLE Beams - Bunch Length Control 24-09-08\_19:21.png

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:21:55)

[STABLE Beams - Bunch Length Control 24-09-08_19:21.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763097/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:21:56)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763097/content)


***

### [2024-09-08 19:53:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139710)
>Event created from ScreenShot Client.  
STABLE Beams - Bunch Length Control 24-09-08\_19:53.png

creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:53:49)

[STABLE Beams - Bunch Length Control 24-09-08_19:53.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763101/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:53:50)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763101/content)

[STABLE Beams - Bunch Length Control 24-09-08_19:54.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763103/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:54:10)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763103/content)

[STABLE Beams - Bunch Length Control 24-09-08_19:56.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763105/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 19:56:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763105/content)

[STABLE Beams - Bunch Length Control 24-09-08_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763111/content)
creator:	 lhcop  @cwo-ccc-d4lf.cern.ch (2024/09/08 20:06:13)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763111/content)


***

### [2024-09-08 20:05:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139711)
>LHC SEQ: Beam dump handshake starting

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:05:25)


***

### [2024-09-08 20:05:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139712)
>Emittance scan end of fill

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 20:05:52)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_20:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763107/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 20:46:15)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763107/content)

[ LHC Luminosity Scan Client 0.62.1 [pro] 24-09-08_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763109/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 20:06:00)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763109/content)


***

### [2024-09-08 20:06:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139713)
>Blownup B2 end of fill

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 20:06:36)

[ BSRT-vs-Emit v0.1.2 - January 2024 -  INCA DISABLED - PRO CCDA 24-09-08_20:06.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763113/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/09/08 20:06:37)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3763113/content)


***

### [2024-09-08 20:09:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139714)
>LHC SEQ: AFP ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:09:32)


***

### [2024-09-08 20:09:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139715)
>LHC SEQ: TOTEM ROMAN POTS TO PARKING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:09:33)


***

### [2024-09-08 20:10:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139716)
>LHC SEQ: beam dump handshake status: experiments and LHC ready for beam dump

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:10:25)


***

### [2024-09-08 20:10:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139721)
>Global Post Mortem Event

Event Timestamp: 08/09/24 20:10:35.729
Fill Number: 10095
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799440 [MeV]
Intensity B1/B2: 22102 / 22352 [e^10 charges]
Event Category / Classification: PROGRAMMED\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 1-Programable Dump b1: B T -> F on CIB.CCR.LHC.B1


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/08 20:13:27)


***

### [2024-09-08 20:10:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139767)
>Global Post Mortem Event Confirmation

Dump Classification: Programmed Dump
Operator / Comment: acalia / Programmed dump after physics fill


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/09/08 20:46:05)


***

### [2024-09-08 20:11:19](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139717)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:11:19)


***

### [2024-09-08 20:11:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139718)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:11:22)


***

### [2024-09-08 20:11:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139719)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:11:24)


***

### [2024-09-08 20:11:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139720)
>LHC SEQ: beam dump handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:11:26)


***

### [2024-09-08 20:16:47](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4139722)
>LHC RUN CTRL: New FILL NUMBER set to 10096

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/09/08 20:16:48)


