# FILL 10265
**start**:		 2024-10-20 15:39:16.479613525+02:00 (CERN time)

**end**:		 2024-10-20 16:23:23.277238525+02:00 (CERN time)

**duration**:	 0 days 00:44:06.797625


***

### [2024-10-20 15:39:16](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167781)
>LHC RUN CTRL: New FILL NUMBER set to 10265

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 15:39:17)


***

### [2024-10-20 15:40:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167783)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 15:40:33)


***

### [2024-10-20 15:49:24](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167785)
>KAROL JAN MOTALA(KMOTALA) assigned RBAC Role: PO-LHC-Piquet and will expire on: 20-OCT-24 09.49.19.410000 PM


creator:	 cconfsrv  @cs-ccr-apop4.cern.ch (2024/10/20 15:49:24)


***

### [2024-10-20 16:23:17](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167803)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 16:23:18)


***

### [2024-10-20 16:23:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167804)
>LHC RUN CTRL: BEAM MODE changed to CYCLING

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 16:23:22)


***

### [2024-10-20 16:23:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4167805)
>LHC RUN CTRL: New FILL NUMBER set to 10266

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/20 16:23:23)


