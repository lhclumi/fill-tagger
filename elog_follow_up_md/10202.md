# FILL 10202
**start**:		 2024-10-06 15:21:51.482613525+02:00 (CERN time)

**end**:		 2024-10-06 18:14:44.044238525+02:00 (CERN time)

**duration**:	 0 days 02:52:52.561625


***

### [2024-10-06 15:21:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156943)
>LHC RUN CTRL: New FILL NUMBER set to 10202

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:21:52)


***

### [2024-10-06 15:23:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156944)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:23:24)


***

### [2024-10-06 15:34:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156951)
>LHC SEQ: UNDULATORS U_RES RESETED

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:34:23)


***

### [2024-10-06 15:36:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156952)
>LHC SEQ: B1 and B2 Collimators have been sent to parking

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:36:33)


***

### [2024-10-06 15:36:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156953)
>LHC SEQ: BPMLHC calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:36:35)


***

### [2024-10-06 15:38:02](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156955)
>LHC SEQ: RF LBDS frequency checks done

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:38:02)


***

### [2024-10-06 15:39:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156956)
>LHC SEQ: all B1&B2 colls at injection settings

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:39:08)


***

### [2024-10-06 15:39:14](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156957)
>LHC SEQ: Checking BLM crates connectivity status. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:39:14)


***

### [2024-10-06 15:40:28](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156959)
>LHC SEQ: resynchronize RF beam control SPS connected finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:40:29)


***

### [2024-10-06 15:41:03](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156961)
>ADT SET WITH INJECTION BANDWIDTH SETTINGS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:41:03)


***

### [2024-10-06 15:43:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156963)
>LHC SEQ: Checking BLM crates Beam Permit threshold. Overall result: SUCCESS
Number of failed crates: 0 / 27

(For more details see BI-LHC ELogBook

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:43:09)


***

### [2024-10-06 15:43:15](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156965)
>LHC SEQ: BLM MCS, SRAM and CONNECTIVITY and BEAM PERMIT CHECKS finished

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:43:15)


***

### [2024-10-06 15:48:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156968)
>LHC SEQ: BPMI calibration finished. Overall result: SUCCESS

Chosen bunch spacing: (B1 & B2) BUNCH_72 (manually chosen)
(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:48:23)


***

### [2024-10-06 15:50:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156970)
>LHC SEQ: BctDc210 tests finished. Overall result: SUCCESS
All tests OK.

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:50:26)


***

### [2024-10-06 15:52:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156972)
>LHC SEQ: DC BCT calibration finished. Overall result: SUCCESS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:52:51)


***

### [2024-10-06 15:52:52](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156974)
>LHC SEQ: Checking COLLIMATOR DOROS BPMs status. Overall result: SUCCESS
Number of failed BPMs: 0 / 61

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:52:52)


***

### [2024-10-06 15:52:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156976)
>LHC SEQ: Checking SRAM Integrity for BPMs via BOMEMCHK devices. Overall result: SUCCESS
Number of failed devices: 0 / 67

(For more details see BI-LHC ELogBook)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:52:54)


***

### [2024-10-06 15:53:08](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156980)
>LHC SEQ: Setting IIRSetting to all BPMs finished. Overall result: SUCCESS



creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:53:08)


***

### [2024-10-06 15:53:36](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156982)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:53:37)


***

### [2024-10-06 15:55:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156983)
>LHC SEQ: LHC.BSRA.US45.B1 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:55:07)


***

### [2024-10-06 15:56:51](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156986)
>LHC SEQ: LHC.BSRA.US45.B2 calibration finished. Overall result: OK

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:56:51)


***

### [2024-10-06 15:56:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156988)
>LHC RUN CTRL: BEAM MODE changed to SETUP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 15:56:53)


***

### [2024-10-06 16:05:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156998)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PROBE BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:05:53)


***

### [2024-10-06 16:07:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4156999)
>injection corrections

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 16:08:00)

[Beam Feedbacks - Dashboard - v4.97.2 - BFC.LHC 24-10-06_16:07.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805293/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 16:08:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805293/content)

[Accelerator Cockpit v0.0.38 24-10-06_16:08.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805295/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 16:08:18)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805295/content)

[Accelerator Cockpit v0.0.38 24-10-06_16:09.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805297/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 16:09:17)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805297/content)


***

### [2024-10-06 16:10:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157001)
>LHC RUN CTRL: BEAM MODE changed to INJECTION PHYSICS BEAM

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:10:42)


***

### [2024-10-06 16:14:54](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157002)
>Correct half of it.

creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 16:15:01)

[Accelerator Cockpit v0.0.38 24-10-06_16:14.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805309/content)
creator:	 lhcop  @cwo-ccc-d2lc.cern.ch (2024/10/06 16:15:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805309/content)


***

### [2024-10-06 16:48:23](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157015)
>LHC SEQ: INJ PROT COLLIMATORS ARE OUT

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:48:23)


***

### [2024-10-06 16:48:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157016)
>LHC SEQ: injection handshake closed; LHC=STANBY, EXP=VETO

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:48:33)


***

### [2024-10-06 16:48:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157017)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:48:43)


***

### [2024-10-06 16:48:42](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157017)
>LHC RUN CTRL: BEAM MODE changed to PREPARE RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:48:43)


***

### [2024-10-06 16:48:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157018)
>LHC Injection Complete

Number of injections actual / planned: 55 / 48
SPS SuperCycle length: 36.0 [s]
Actual / minimum time: 0:38:01 / 0:33:48 (112.5 %)

creator:	 lhcop  @cwo-ccc-d3lf.cern.ch (2024/10/06 16:48:43)


***

### [2024-10-06 16:50:26](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157023)
>LHC RUN CTRL: BEAM MODE changed to RAMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:50:27)


***

### [2024-10-06 16:50:27](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157025)
>LHC SEQ: ramp started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 16:50:28)


***

### [2024-10-06 17:00:11](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157030)
>middle of the ramp

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:00:19)

[ LHC Fast BCT v1.3.2 24-10-06_17:00.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805369/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:00:20)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805369/content)


***

### [2024-10-06 17:11:45](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157037)
>LHC RUN CTRL: BEAM MODE changed to FLAT TOP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:11:45)


***

### [2024-10-06 17:12:01](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157039)
>LHC RUN CTRL: BEAM MODE changed to SQUEEZE

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:12:02)


***

### [2024-10-06 17:20:20](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157044)
>LHC SEQ: qchange started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:20:21)


***

### [2024-10-06 17:20:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157045)
>LHC RUN CTRL: BEAM MODE changed to ADJUST

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:20:53)


***

### [2024-10-06 17:21:50](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157047)
>LHC SEQ: collisions BP started

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:21:50)


***

### [2024-10-06 17:25:59](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157048)
>optimization.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:26:06)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805381/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:26:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805381/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:26.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805383/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:26:21)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805383/content)


***

### [2024-10-06 17:26:43](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157049)
>LHC RUN CTRL: BEAM MODE changed to STABLE BEAMS

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:26:43)


***

### [2024-10-06 17:28:46](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157059)
>A bit higher luminsoity for ALICE this fill.STi

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:39:29)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:28.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805395/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:39:30)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805395/content)


***

### [2024-10-06 17:30:35](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157054)
>Following the increase we see some warnings now.

creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 17:30:59)

[ LHC BLM Fixed Display 24-10-06_17:30.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805393/content)
creator:	 lhcop  @cwo-ccc-d2lf.cern.ch (2024/10/06 17:30:59)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805393/content)


***

### [2024-10-06 17:33:53](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157057)
>LHC SEQ : TOTEM ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:33:53)


***

### [2024-10-06 17:35:55](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157058)
>LHC SEQ : AFP ROMAN POTS IN PHYSICS POSITION (LOW-BETA)

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 17:35:55)


***

### [2024-10-06 17:39:32](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157060)
>emittance scan IP1/5

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:39:47)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805397/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:39:47)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805397/content)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:39.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805399/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:39:58)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805399/content)


***

### [2024-10-06 17:44:25](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157061)
>just separated them to be further away during the squeeze.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:44:47)

[ LHC Luminosity Scan Client 0.62.5 [pro] 24-10-06_17:44.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805401/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 17:44:48)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805401/content)


***

### [2024-10-06 18:05:21](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157065)
>Global Post Mortem Event

Event Timestamp: 06/10/24 18:05:21.862
Fill Number: 10202
Accelerator / beam mode: PROTON PHYSICS / STABLE BEAMS
Energy: 6799320 [MeV]
Intensity B1/B2: 36657 / 37326 [e^10 charges]
Event Category / Classification: PROTECTION\_DUMP / MULTIPLE\_SYSTEM\_DUMP
First BIC input Triggered: First USR\_PERMIT change: Ch 12-PIC\_MSK: B T -> F on CIB.USC55.R5.B2


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/06 18:08:10)


***

### [2024-10-06 18:05:22](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157127)
>Global Post Mortem Event Confirmation

Dump Classification: QPS trigger
Operator / Comment: tpersson / QPS for 600A in sector56. 


creator:	 copera  @cs-ccr-pm3.cern.ch (2024/10/06 18:45:57)


***

### [2024-10-06 18:05:48](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157062)
>Looks like a qps crate.

creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/06 18:06:06)

[Set SECTOR45 24-10-06_18:05.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805403/content)
creator:	 lhcop  @cwo-ccc-d0lf.cern.ch (2024/10/06 18:06:07)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805403/content)


***

### [2024-10-06 18:07:07](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157063)
>LHC RUN CTRL: BEAM MODE changed to BEAM DUMP

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:07:07)


***

### [2024-10-06 18:07:09](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157064)
>LHC RUN CTRL: BEAM MODE changed to RAMP DOWN

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:07:10)


***

### [2024-10-06 18:12:33](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157066)
>Looks like there were several crates but TI didn't see any glitch or 
 anything.

creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:13:01)

[qps-lhc-swisstool 24-10-06_18:12.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805405/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:13:01)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805405/content)

[ LHC CIRCUIT SUPERVISION v9.0 24-10-06_18:13.png](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805407/content)
creator:	 lhcop  @cwo-ccc-d3lc.cern.ch (2024/10/06 18:13:04)
![image alt](https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/3805407/content)


***

### [2024-10-06 18:14:44](https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/4157067)
>LHC RUN CTRL: New FILL NUMBER set to 10203

creator:	 lhcop  @cs-ccr-seq1.cern.ch (2024/10/06 18:14:45)


