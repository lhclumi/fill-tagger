##
# LHC Fill Annotation  tool
#  
#    You can add tags and comments = annotation to LHC fills for a year
#    Existing annotations can be modified and updated. 
#    New tags can be introduced.
#    Bulk annotations are possible
#    
#    The script produces a yaml file per fill, in the output directory
#
# Usage: python annotate_fills.py -y <year> -i <input directory> -o <output directory> 
#    
import os
import numpy as np
import pandas as pd
import datetime
from datetime import date

import glob
import ruamel.yaml

print(' --- loading annotate_fills.py version 1.01 @ 27.03.2024')

basic_menu = f'''
        1. Load existing yaml files
        2. Select fills to annotate
        3. List known tags
        4. List non annotated fills
        5. Annotate fills
        6. Modify annotations for a fill
        7. List modified fills
        8. Dump selected fills to csv file
        9. Load csv file with annotations
     99 = Save and exit
    -99 = exit without saving
    '''
    
tags_menu = f'''
    1. List known tags
    2. Add new tags
    3. Remove tags
    99. exit tag menu
    '''
fill_annotation_menu = f'''
    1: Active - yes/no
    2. Change usage
    3. Update comment
    99 = exit edit mode
    '''

config = {
    'YAMLDIR' : f'{os.getenv("HOME")}/Work/fill-tagger/weekly_follow_up',
    'tags'    : [
        '#BEAM_COMMISSIONING',  # beam commissioning period
        '#OP_DUMP',             # fill dumped by OP.
        '#UFO_DUMP',            # fill dumped by UFO.
        '#BSRT',                # BSRT calibration
        '#SCRUBBING',           # Scrubbing fill
        '#RAMP_UP',             # intensity ramp_up
        '#WIRES',
    ] 
}

yaml = ruamel.yaml.YAML()

def display_menu(title, text_menu):
    print('\n')
    print(title)
    print(len(title)*'-')
    print(f'''{text_menu}''')
    print(len(title) * '-')

def load_yaml_files(inpdir):
    _existing_yaml = glob.glob(f'{inpdir}/*.yaml')
    if len(_existing_yaml):
        print(f'>>> {len(_existing_yaml)} annotated fill yaml files found and will be loaded! ')
        _dlist = []
        for fyml in _existing_yaml:
            with open(fyml, 'r') as fin:
                _dlist.append(yaml.load(fin))
        _dfy = pd.DataFrame(_dlist)
        _dfy =  _dfy.sort_values(by='HX:FILLN', ascending=True).reindex()
        _dfy.dropna(subset=['HX:FILLN'], inplace=True)
        _dfy['fill'] = _dfy['HX:FILLN'].astype(int)
        _dfy.set_index('fill', inplace=True)
    else:
        _dfy = pd.DataFrame()
        print(f'>>> No yaml files found - should exit!')
    return _dfy

def load_tags(df):
    from itertools import chain
    tags_in_data = list(chain.from_iterable(df.dropna().tags.values))
    all_tags = tags_in_data + config['tags']
    unique_tags = list(set(all_tags))
    print(f' --- known tags = {unique_tags}')
    return unique_tags    

def set_tags(tags):
    print(' --- select tags from list below or add a new one:')
    print(f' --- known tags : {list(enumerate(tags))}')
    uinp = input('Enter tag from list or comma separated tags IDs or new tag:')
    new_tags = []
    if not len(uinp):
        return new_tags
    
    for x in uinp.replace(' ','').split(','):
        try:
            if int(x) < len(tags):
                new_tags.append(tags[int(x)])
        except:
            new_tags.append(x)
    return new_tags

def flatten(A):
    rt = []
    for i in A:
        if isinstance(i,list): rt.extend(flatten(i))
        else: rt.append(i)
    return rt

def merge_annotated(dfold, dfnew):
    _upd = pd.concat([dfold.copy(), dfnew])
    _aux = _upd.groupby('fill').agg({'start':'first', 'end':'first', 'duration':'first',
                                     'tags' :list, 'comment':list})
    # jointags = lambda x: [item for row in x for item in row]
    
    # -- flatten lists and remove duplicates
    _aux['tags'] = _aux['tags'].apply(flatten)
    _aux['comment'] = _aux['comment'].apply(flatten)

    # -- used for debugging 
    # xtags = []
    # xcomment = []
    # for i, row in _aux.iterrows():
    #     print (i, row.tags, row.comment)
    #     xtags.append(list(dict.fromkeys(row.tags)))
    #     xcomment.append(list(dict.fromkeys(row.comment)))   
    _aux['tags'] = _aux['tags'].apply(lambda x: list(dict.fromkeys(x)))
    _aux['comment'] = _aux['comment'].apply(lambda x: list(dict.fromkeys(x)))
    return _aux

def merge_modified(dfold, dfnew):
    for i, row in dfnew.iterrows():
        dfold.at[i, 'tags'] = row.tags
        dfold.at[i, 'comment'] = row.comment
    return dfold

def annotate_fills(df, alltags):
    finput = input('Enter rill(s) to annotate. [single number, comma separated, or range, or any mixture]:')
    print(f'{finput=}')
    if not len(finput):
        print(f' -- empty input do nothing!')
        return pd.DataFrame()
    
    tags = set_tags(alltags)
    comment = input('Enter comment: [text - single line]:')
    is_updated = False
    flist = get_fill_list(finput)
    _dfy = df[df.index.isin(flist)].copy()
    if len(tags) :
        print(f'--- add tag {tags}')
        _dfy['tags'] = _dfy['tags'].apply(lambda x: tags if x is None else x+tags)
        is_updated = True
    if len(comment) : 
        _dfy['comment'] = _dfy['comment'].apply(lambda x: comment if x is None else x+','+comment)
        is_updated = True
    if is_updated : 
        print(f' --- annotated fills:')
        print(_dfy)
        return(_dfy)
    else:
        print(f' -- no annotations entered')
        return pd.DataFrame()
    
def update_fill_annotations(df):
    finput = input('Enter rill(s) to update. [single number, comma separated, or range, or any mixture]:')
    print(f'{finput=}')
    
    flist = get_fill_list(finput)
    _dfy = df[df.index.isin(flist)].copy()
    is_updated = False
    print(_dfy)
    print('Note:')
    print('   when you update fill\'s annotation, the tags and comment you will REPLACE all existing ones ')
    tags = input('Enter new tag or comma separated list: ')
    if len(tags):
        _dfy['tags'] = _dfy['tags'].apply(lambda x : tags.replace(' ','').split(','))
        is_updated = True

    comment = input('Enter comment: [text with comma separated entries]: ')
    if len(comment):
        _dfy['comment'] = _dfy['comment'].apply(lambda x : comment.replace(' ','').split(','))
        is_updated = True
    
    if is_updated:
        print(f' --- updated fills:')
        print(_dfy)
        return _dfy
    else:
        print(f' -- nothing to update')
        return pd.DataFrame()

def non_annotated_fills(df):
    _dfy = df[(df['tags'].isnull())]
    print(f' --- {_dfy.shape[0]} non-annotated fills found ')
    print(_dfy)    
    return _dfy

def _non_annotated_fills(df):
    _dfy = df[df.apply(lambda x: x.tags == None and x.comment == None, axis=1)]
    print(f' --- {_dfy.shape[0]} non-annotated fills found ')
    print(_dfy)    
    return _dfy

def save_to_yaml(df, outdir):
        
    for k,v in df.to_dict('index').items():
        print(f' --- fill {k} : values : {v}')
        vout = {'HX:FILLN': k,
                'start' : v['start'],
                'end' : v['end'],
                'duration' : v['duration'],
                'tags' : v['tags'],
                'comment' : v['comment']
                }
        time_str = pd.to_datetime(v['start']).strftime('%Y_%m_%d_%H_%M_%S')
        foutname = f'{outdir}/{time_str}_F{k}.yaml'
        yaml.default_flow_style = False
        yaml.indent(mapping=2, sequence=4, offset=2)
        with open(foutname, 'w') as fout:
            yaml.dump(vout, fout)
    return

def load_csv(finp):
    _aux = pd.read_csv(finp).dropna(how='all').dropna(axis=1,how='all')
    _aux['fill'] = _aux['fill'].astype(int)
    _aux['HX:FILLN'] = _aux['HX:FILLN'].astype(int)
    _aux.set_index('fill', inplace=True)
    _aux['comment'] = _aux.comment.apply(lambda x: [i.strip() for i in str(x).split(',')])
    return _aux

def get_fill_list(finput):
    finput = finput.replace(' ', '')
    flist = []
    for xf in finput.split(','):
        if xf.find('-') > 0:
            f0, f1 = xf.split('-')
            flist = flist + np.arange(int(f0), int(f1)+1).tolist()
        else:
            flist.append(int(xf))
    flist.sort()
    return flist

def select_fills(df):
    print('Enter starting day to consider or period [comma separated dates: t0, t1].')
    print('   If t1 is missing, the fills up to the latest will be used ')
    _dates = input('dates in [YYYY-MM-DD] format, none=last 7 days, -1=non yet annotated - your choice? ')
    if _dates.find(',')>0 : 
        t0, t1 = _dates.replace(' ', '').split(',')
        selectedfills_df = df[(df.start > t0) & (df.start < t1)].copy()
    elif _dates == '-1' :
        selectedfills_df = df[df.tags.isnull()]
    else:
        if len(_dates) > 0:
            t0 = _dates.replace(' ','')
            selectedfills_df = df[df.start > t0].copy()
        else:
            t0 = datetime.date.today() - datetime.timedelta(days=7)
            selectedfills_df = df[df.start > t0.strftime('%Y-%m-%d')].copy()

    print(f' --- {selectedfills_df.shape[0]} fills in the reference period')
    print(f' --- Fills : {selectedfills_df.index.values}')
    print(selectedfills_df[['start','end','duration','tags','comment']].to_string())
    return selectedfills_df

def get_user_choice():
    choice = input('Enter your choice:')
    return int(choice)

def cleanup_tags(df):

    # remove empty tags and non starting with #
    _dfy = df.copy()
    _dfy['ctags'] = _dfy['tags'].apply(lambda x : len([1 for i in x if (len(i)==0) or (i.find('#')<0) ]))
    _dfy = _dfy[_dfy.ctags>0].copy()
    _dfy.drop('ctags',axis=1,inplace=True)
    _dfy['tags'] = _dfy['tags'].apply(lambda x: [i for i in x if (len(i)>0) and (i.find('#')==0)])
    return _dfy
    
def _removenone(x):
    _tmp = flatten([a.split(',') for a in x])
    _aux = [w for w in _tmp if w != 'None']
    if len(_aux) == 0:
        return None
    else:
        return _aux
    
def cleanup_comments(df):
    _dfy = df.dropna(subset='comment')
    _dfy['comment'] = _dfy['comment'].apply(_removenone)
    return _dfy
    
def main(inpdir, outdir):
            
    main_menu_exit = False
    annotations_list = []
    modified_list = []
    while not main_menu_exit:
        display_menu('Basic Menu', basic_menu)
        choice = get_user_choice()
      
        if choice == 1:         # 1. Load existing yaml files
            knownfills_df = load_yaml_files(inpdir)
            if knownfills_df.empty:
                main_menu_exit = True
            known_tags = load_tags(knownfills_df)
            
        elif choice == 2:       # 2. Select fills to annotate
            selectedfills_df = select_fills(knownfills_df)
            
        elif choice == 3:       # 3. List known tags
            print(f' --- known tags: {known_tags}')

        elif choice == 4:       # 4. List non annotated fills
            non_annotated_fills_df = non_annotated_fills(knownfills_df)
            
        elif choice == 5:       # 5. Annotate fills
            annotatedfills_df = annotate_fills(selectedfills_df, known_tags)
            annotations_list.append(list(annotatedfills_df.index.values))
            knownfills_df = merge_annotated(knownfills_df, annotatedfills_df)
            known_tags = load_tags(knownfills_df)
            
        elif choice == 6:       # 6. Modify annotations for a fill
            modified_df = update_fill_annotations(knownfills_df) 
            modified_list.append(list(modified_df.index.values))
            knownfills_df = merge_modified(knownfills_df, modified_df)
            known_tags = load_tags(knownfills_df)
            
        elif choice == 7:       # 7. List modified fills
            _fno = list(set(flatten(annotations_list)+flatten(modified_list)))
            if len(_fno):
                print(f' ---- new annotations & modifications:')
                print(knownfills_df[knownfills_df.index.isin(_fno)])
        
        elif choice == 8:       # 8. Dump selected fills to csv file
            fcsvout = f'{outdir}/selected_fills.csv'
            anfname = input(f' --- csv file name ({fcsvout})? ')
            if len(anfname):
                fcsvout = anfname
            selectedfills_df.to_csv(fcsvout)
            print(f"Non annotated fills saved to {fcsvout}")
        
        elif choice == 9:       # 9. Load cvs file with annotations
            fcsvin = f'{outdir}/selected_fills.csv'
            anfname = input(f'-- enter csv file with annotations ({fcsvin}):')
            if len(anfname):
                fcsvin = anfname
            annotatedfills_df = load_csv(fcsvin)
            _inpfno = annotatedfills_df.index.values.tolist()
            annotations_list.append(_inpfno)
            knownfills_df = merge_annotated(knownfills_df[~knownfills_df.index.isin(_inpfno)], annotatedfills_df)
            known_tags = load_tags(knownfills_df)

        elif choice == 99:
            _fills = list(set(flatten(annotations_list)+flatten(modified_list)))
            save_to_yaml(knownfills_df[knownfills_df.index.isin(_fills)], outdir)
            print(f"New annotations and modifications saved to {outdir} as yaml files.")
            print(' ')
            print("Thanks for using the tool .....")
            main_menu_exit = True
        elif choice == -99:
            main_menu_exit = True
        else:
            input("Wrong option selection. Enter any key to try again..")

if __name__ == '__main__':
    
    import argparse
    parser = argparse.ArgumentParser(description='Annotate LHC fills')
    # parser.add_argument('-y', '--year', dest='year', required=True, help='Year to consider')
    parser.add_argument('-i', '--inpdir', dest='inpdir', default=f'{config["YAMLDIR"]}', help='input directory for the annotated fills' )
    parser.add_argument('-o', '--outdir', dest='outdir', default=f'{config["YAMLDIR"]}', help='output directory for the annotated fills' )
    args = parser.parse_args()
    print(args)
    main(args.inpdir, args.outdir)
 
# %%
