# Fill tagger

This repository aims to have a collaborative and easy way to tag LHC fills.

The 2024 fills are in the `weekly_follow_up` folder.


The possible `#TAGS` (UPPER case) are 

- #BEAM_COMMISSIONING: beam commissioning period
- #OP_DUMP: fill dumped by OP 
- #UFO_DUMP: fill dumped by UFO
- #BLM_DUMP: fill dumped by BLM
- #10HZ_DUMP: fill dumped by 10 Hz like event
- #TO_DISCUSS: in case you have questions, open points
- #BSRT: BSRT calibration
- #SCRUBBING 
- #MD: MD
- #INTENSITY_RAMPUP: intensity ramp_up
- #WIRES: wires compensation activities.
- #DUCK: fills with no INJPHYS
- #ION_OPTICS: ion fill
- #12BUNCHES: 12 bunches fill
- #PRESCRUBBING: preparation to the scrubbing
- #VDM: Van der Meers scan
- #OPTICS_MEASUREMENTS: optics commissionign
- #PHYSICS: pp physics
- #TO_STUDY: fill with feature to study
- #TS: technical stop period


Feel free to propose other tags (it is a start).

Thanks.

To install the `pylogbook`

```bash
pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config
pip install pylogbook
```


## Annotate fills script

Use the script `annotate_fills.py` to annotate the fills from the terminal. 

The script reads the yaml files in the `weekly_follow_up` directory, and has options alllowing to view/add/modify the annotations (tags and comments) of the fills. By default saves the updated yaml files in the same directory, or to another directory if specified by the command argument `-o <dir>`. 

You can also use the script to read the yaml files into a dataframe

```python
import annotate_fills as afills

df = afills.load_yaml_files("./weekly_follow_up")
df.head()

# These fills are STILL to be tagged
df[(df['tags'].isnull())]

my_fill = XYZ
df.at[my_fill, 'tags'] = ['#TAG1',
                          '#TAG2']
df.at[my_fill, 'comment'] = ['Comment 1',
                             'Comment 2']

```
and to save a modified dataframe to new yaml files:
```python
afills.save_to_yaml(df, "./weekly_follow_up")
```

To find the tags you can to
```python
# take all the unique elements in all the rows of  df['tags']
tags = set()
for t in df['tags']:
    if t is not None:
        tags = tags.union(set(t))
    if t == None:
        print("empty tag")
tags
```
or to find the fills with a given tag
```python
# return all rows were the df['tags'] contains the tag '#DUCK'
df[df['tags'].apply(lambda x: '#DUCK' in x)]
```

---
### Use example

#### Step 1
Start the scrtipt using python: 
``` 
python annotate_fills.py -o ./temp
```
The option _-o_ defined the input/output directory to use, otherwise all files are written directly in the _./weekly_followup_ that can be risky. 

Use the sequence of options from the menu:

- **1** : load known yaml files (all fills)
- **2** : select fills to annotate
    - **-1** : non annotated filles
- **8** : dump selected fills to a csv file. You will be prompted for the fine name, default: ```./temp/selected_fills.csv```

#### Step 2:
edit the csv file using excel or equivalent. 

Note: modify only the _tags_ and _comments_ columns.

save the file

#### Step 3:
return to the _annotate_fills.py_ script and use the sequence of options:
 - **1** : load known yaml files (all fills)
 - **9** : load csv file with annotations. You will be prompted to provide the file name, default (_./temp/selected_fills.csv') 
 - **7** : list the modifications
 - **99** : will create the yaml files for the modified fills in the outptu directroy _./temp_ in this example. 

#### Step 4:
verify the new yaml files and copy them over to the _./weekly_followup_ directory and then to upload to gitlab of the project.

