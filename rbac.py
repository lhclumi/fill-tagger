# %%
import os
import sys
from typing import Optional

import requests

# poor man's (aka Michi) pure python RBAC
RBAC_BASE = "https://rbac-pro1.cern.ch:8443/rba/api/v1"
RBAC_KRB5_SERVICE = "RBAC@rbac-pro-lb.cern.ch"


def authenticate_explicit(user: str, password: str, lifetime_minutes: int = 60 * 8) -> str:
    application = sys.argv[0]
    response = requests.post(f"{RBAC_BASE}/authenticate/explicit", verify=False,
                             data={
                                 "UserName": user,
                                 "Password": password,
                                 "AccountName": user,
                                 "Application": application,
                                 "Lifetime": lifetime_minutes
                             })
    response.raise_for_status()
    return response.content.decode("utf-8")


def authenticate_location(user: Optional[str] = None, lifetime_minutes: int = 60 * 8) -> str:
    application = sys.argv[0]
    response = requests.post(f"{RBAC_BASE}/authenticate/location", verify=False,
                             data={
                                 "AccountName": user if user is not None else os.getlogin(),
                                 "Application": application,
                                 "Lifetime": lifetime_minutes
                             })
    response.raise_for_status()
    return response.content.decode("utf-8")


def authenticate_kerberos(lifetime_minutes: int = 60 * 8) -> str:
    try:
        import winkerberos as kerberos
    except ImportError:
        import pathlib
        os.environ['KRB5_CONFIG'] = str(pathlib.Path(__file__).parent / 'rbac-krb5.conf')
        import kerberos
    res, ctx = kerberos.authGSSClientInit(RBAC_KRB5_SERVICE)
    assert res == kerberos.AUTH_GSS_COMPLETE
    kerberos.authGSSClientStep(ctx, "")
    krb_ticket = kerberos.authGSSClientResponse(ctx)
    assert krb_ticket
    application = sys.argv[0]
    response = requests.post(f"{RBAC_BASE}/authenticate/kerberos", verify=False,
                             data={
                                 "AccountName": os.getlogin(),
                                 "Krb5Ticket": krb_ticket,
                                 "Application": application,
                                 "Lifetime": lifetime_minutes
                             })
    kerberos.authGSSClientClean(ctx)
    response.raise_for_status()
    return response.content.decode("utf-8")

# %%
# authenticate_kerberos()

# %%
