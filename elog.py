# %%
# Thanks to Philip 
# before you need to import an RBAC from the TN
# I connect to my TN machine and do
# rbac-authenticate -e
# then copy and paste to 002


# set the environment variable
import os
import rbac
import markdownify
# get environment variable
try:
    my_path = os.environ['PATH_FILL_TAGGER']
    my_path = my_path + '/fill-tagger/'
except KeyError:
    my_path = './'


os.environ['RBAC_TOKEN_SERIALIZED'] = rbac.authenticate_kerberos()


# %%
import pylogbook
import datetime
client = pylogbook.Client()
# from_date = datetime.datetime.now() - datetime.timedelta(minutes=60)
# events = client.get_events(activities=pylogbook.NamedActivity.LHC, from_date=from_date)
# for event in events:
#     print(event.comment)

# # %%
import os
import getpass
import logging
import sys
import numpy as np
import ruamel.yaml
import nx2pd as nx
import pandas as pd
yaml = ruamel.yaml.YAML()

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

folder_variable = os.environ.get('PATH_FILL_TAGGER')
if folder_variable is None:
    folder_variable = './'
else:
    folder_variable = folder_variable + '/fill-tagger'
os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
print(f'Assuming that your kerberos keytab is in the home folder, '
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/'+
          f'{getpass.getuser()}.keytab {getpass.getuser()}');

# %%

from nxcals.spark_session_builder import get_or_create, Flavor

logging.info('Creating the spark instance')
logging.info('Creating the spark instance')
spark = get_or_create(flavor=Flavor.LOCAL,
    )
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')
# %%
data_list = ["HX:FILLN"]
t0 = '2024.05.01 00:00:00'
t1 = '2025.01.01 00:00:00'
df = sk.get(t0, t1, data_list)
# %%
# for all rows in df
for index, row in df.iterrows():
    try:
        time = row.name
        # from time build a string in ther format 
        # 'YYYY_MM_DD_HH_MM_SS'
        time_str = time.strftime('%Y_%m_%d_%H_%M_%S')
        filename_string = str(row['HX:FILLN'])
        print(filename_string)
        # check if the file exists
        # if it exists, skip the row
        if not os.path.exists(
            folder_variable + '/elog_follow_up/' + filename_string + '.yaml'
        ):
            my_dict = {'HX:FILLN': row.to_dict()['HX:FILLN']}
            fill_dict = sk.get_fill_time(my_dict['HX:FILLN'])
            my_dict['start'] = str(fill_dict['start'].tz_convert('CET'))
            my_dict['end'] = str(fill_dict['end'].tz_convert('CET'))
            my_dict['duration'] = str(fill_dict['duration'])

            events = client.get_events(activities=pylogbook.NamedActivity.LHC, 
                                       from_date=fill_dict['start'].tz_convert('CET').to_pydatetime(),
                                       to_date=fill_dict['end'].tz_convert('CET').to_pydatetime())
            my_list = []

            for event in events:
                my_dict_event = {key: event.__dict__[key] for key in ['creator', 'date']}
                my_dict_event['event_id'] = f"https://be-op-logbook.web.cern.ch/elogbook-server/GET/showEventInLogbook/{event.__dict__['event_id']}"
                my_dict_event['comment'] = event.__dict__['comment']
                my_dict_event['attachments'] = [
                    {
                        'attachment_id': attachment.attachment_id,
                        'filename': attachment.filename,
                        'creator': attachment.creator,
                    }
                    for attachment in event.__dict__['attachments']
                ]
                for attachment in my_dict_event['attachments']:
                    attachment['url_download'] = f"https://be-op-logbook.web.cern.ch/elogbook-server/GET/attachment/{attachment['attachment_id']}/content"
                    attachment['url_show'] = f"https://be-op-logbook.web.cern.ch/elogbook-server/#/fullScreenImg?img={attachment['attachment_id']}"
                my_list.append(my_dict_event)
            my_dict['events'] = my_list
            
            yaml.default_flow_style = False
            yaml.indent(mapping=2, sequence=4, offset=2)
            with open(folder_variable+'/elog_follow_up/'+filename_string+'.yaml', 'w') as file:
                yaml.dump(my_dict, file)
    except Exception as e:
        print(e)


# %%
# for each file in the folder elog_follow_up 
# read the file and print the content

def format_comment(comment):
    if comment[:8] == 'LHC SEQ:':
        return comment
    else:
        return markdownify.markdownify(comment)

for filename in sorted(os.listdir(folder_variable+'/elog_follow_up')):
    # if the file.md does not exist in the folder elog_follow_up_md
    if not os.path.exists(folder_variable+f"/elog_follow_up_md/{filename[:-5]}.md"):    
        with open(folder_variable+'/elog_follow_up/'+filename) as file:
            content = yaml.load(file)
        print(filename)
        # save the content in a markdown file
        with open(folder_variable+f"/elog_follow_up_md/{content['HX:FILLN']}.md", 'w') as file:
            file.write(f"# FILL {content['HX:FILLN']}\n")
            # convert the time in a human readable format and CET timezone

            file.write(f"**start**:\t\t {pd.Timestamp(content['start'])} (CERN time)\n\n")
            file.write(f"**end**:\t\t {pd.Timestamp(content['end'])} (CERN time)\n\n")
            file.write(f"**duration**:\t {content['duration']}\n\n")
            file.write("\n")

            # invert the order of the events
            for event in content['events'][::-1]:

                file.write(f"***\n\n")
                file.write(f"### [{event['date']}]({event['event_id']})\n")
                file.write(f">{format_comment(event['comment'])}\n\n")
                file.write(f"creator:\t {event['creator']}\n\n")
                #file.write(f"**event_id**:\t {event['event_id']}\n\n")
                if len(event['attachments']) > 0:
                    for attachment in event['attachments']:
                        file.write(f"[{attachment['filename']}]({attachment['url_download']})\n")
                        file.write(f"creator:\t {attachment['creator']}\n")
                        #file.write(f"**attachment_id**:\t {attachment['attachment_id']}\n")
                        #file.write(f"**url_download**:\t {attachment['url_download']}\n")
                        #file.write(f"**url_show**:\t {attachment['url_show']}\n")
                        file.write(f"![image alt]({attachment['url_download']})\n\n")
                file.write("\n")
    
# %%