# %%
import os
import getpass
import logging
import sys
import numpy as np
import ruamel.yaml


logging.basicConfig(stream=sys.stdout, level=logging.INFO)

folder_variable = os.environ.get('PATH_FILL_TAGGER')
folder_variable = folder_variable + '/fill-tagger'
os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
print(f'Assuming that your kerberos keytab is in the home folder, '
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/'+
          f'{getpass.getuser()}.keytab {getpass.getuser()}');

# %%
import nx2pd as nx
import pandas as pd

from nxcals.spark_session_builder import get_or_create, Flavor

logging.info('Creating the spark instance')

# Here I am using YARN (to do compution on the cluster)
#spark = get_or_create(flavor=Flavor.YARN_SMALL, master='yarn')

# Here I am using the LOCAL (assuming that my data are small data,
# so the overhead of the YARN is not justified)
# WARNING: the very first time you need to use YARN
# spark = get_or_create(flavor=Flavor.LOCAL)

logging.info('Creating the spark instance')
spark = get_or_create(flavor=Flavor.LOCAL,
# conf={'spark.driver.maxResultSize': '8g',
#     'spark.executor.memory':'8g',
#     'spark.driver.memory': '16g',
#     'spark.executor.instances': '20',
#     'spark.executor.cores': '2',
#     }
    )
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')
# %%
data_list = ["HX:FILLN"]
t0 = '2024.01.01 00:00:00'
t1 = '2025.01.01 00:00:00'
df = sk.get(t0, t1, data_list)
# %%
# for all rows in df
for index, row in df.iterrows():
    try:
        time = row.name
        # from time build a string in ther format 
        # 'YYYY_MM_DD_HH_MM_SS'
        time_str = time.strftime('%Y_%m_%d_%H_%M_%S')
        filename_string = time_str+'_F'+str(row['HX:FILLN'])
        print(filename_string)
        # check if the file exists
        # if it exists, skip the row
        if os.path.exists(folder_variable + '/weekly_follow_up/'+filename_string+'.yaml'):
            pass
        else:
            my_dict = dict()
            my_dict['HX:FILLN'] = row.to_dict()['HX:FILLN']
            fill_dict = sk.get_fill_time(my_dict['HX:FILLN'])
            my_dict['start'] = str(fill_dict['start'])
            my_dict['end'] = str(fill_dict['end'])
            my_dict['duration'] = str(fill_dict['duration'])
            my_dict['tags']= None
            my_dict['comment'] = None
            

            yaml = ruamel.yaml.YAML()
            yaml.default_flow_style = False
            yaml.indent(mapping=2, sequence=4, offset=2)
            with open(folder_variable+'/weekly_follow_up/'+filename_string+'.yaml', 'w') as file:
                yaml.dump(my_dict, file)
    except Exception as e:
        print(e)
# %%

data = []
for filename in os.listdir(folder_variable + '/weekly_follow_up'):
    if filename.endswith(".yaml"):
        with open(folder_variable + '/weekly_follow_up/'+filename, 'r') as file:
            yaml = ruamel.yaml.YAML()
            data.append(yaml.load(file))

import pandas as pd
aux = pd.DataFrame(data)
# order the dataframe by start time
aux = aux.sort_values(by='HX:FILLN', ascending=True).reindex()
# save the dataframe to a csv file
aux.to_csv(folder_variable + '/2024.csv')
# %%
