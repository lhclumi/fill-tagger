#!/bin/bash
# You can update your crontab with the following command:
# crontab -e
# and add the following line to update the tagger every 15 minutes:
# */15 * * * *  /home/sterbini/fill-tagger/fill-tagger/update_script.sh
# set variable
export PATH_FILL_TAGGER=~/fill-tagger
source $PATH_FILL_TAGGER/miniconda/bin/activate
# kinit with keytab
kinit -f -r 5d -kt ~/$(whoami).keytab $(whoami)
python $PATH_FILL_TAGGER/fill-tagger/python_script.py
python $PATH_FILL_TAGGER/fill-tagger/elog.py
cd  $PATH_FILL_TAGGER/fill-tagger
git pull
git add  $PATH_FILL_TAGGER/fill-tagger/weekly_follow_up
git add  $PATH_FILL_TAGGER/fill-tagger/elog_follow_up
git add  $PATH_FILL_TAGGER/fill-tagger/elog_follow_up_md
git commit -am 'Automatic update from update_script.sh'
git push
